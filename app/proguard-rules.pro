# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile
-keep class androidx.databinding.** {*;}
-keep public class ru.takeeat.takeeat.*
-keep public class ru.takeeat.takeeat.**
-keep public class ru.takeeat.takeeat.* {*;}
-keep class ru.takeeat.takeeat.*
-keep class ru.takeeat.takeeat.**
-keep class ru.takeeat.takeeat.** {*;}
-keep public class ru.takeeat.takeeat.databinding.*
-keep public class ru.takeeat.takeeat.databinding.**
-keep public class ru.takeeat.takeeat.databinding.* {*;}
-keep class com.mapbox.android.telemetry.** { *; }
#-keep class androidx.databinding.** { *; }
#-keep class * extends androidx.databinding.DataBinderMapper { *; }
#-keep class ru.takeeat.takeeat.databinding.** {*;}
#-keep public class * extends androidx.viewbinding.ViewBinding
-keep class androidx.viewbinding.BuildConfig
-keep class androidx.viewbinding.R
-keep public interface androidx.viewbinding.ViewBinding{
public abstract android.view.View getRoot();
}
-keep class androidx.viewbinding.ViewBindings

-keep public class androidx.viewbinding.BuildConfig {
    public static final java.lang.String BUILD_TYPE;
    public static final boolean DEBUG;
    public static final java.lang.String LIBRARY_PACKAGE_NAME;
}
