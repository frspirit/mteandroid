package ru.takeeat.takeeat

import android.graphics.*

fun curvedLine(width:Float,height:Float): Bitmap{
    val bitmap = Bitmap.createBitmap( width.toInt(), height.toInt(), Bitmap.Config.ARGB_8888)

    val canvas = Canvas(bitmap)

    val paint: Paint = Paint();
    paint.color = Color.RED
    paint.isAntiAlias = true
    paint.setStyle(Paint.Style.STROKE)
    paint.setStrokeJoin(Paint.Join.ROUND)
    paint.setStrokeCap(Paint.Cap.ROUND)
    paint.setPathEffect(CornerPathEffect(10F))
    paint.strokeWidth = width/13


    val path: Path = Path()

    path.moveTo(5F, height-5F);
    path.quadTo(width/2, (height/2)-height/8, width-5F, 7F);
    canvas.drawPath(path, paint)
//            Работает
//            canvas.drawArc(0F,10F, width.toFloat()*2, height.toFloat()*2,10F,-180F,false,paint)
//            canvas.drawArc(0F,10F, width.toFloat()*2, height.toFloat()*2,-10F,-180F,false,paint)
    return bitmap
}