package ru.takeeat.takeeat

import android.text.Editable

import android.text.InputFilter
import android.text.InputFilter.LengthFilter

import android.widget.EditText

import android.text.TextWatcher
import android.util.Log


//func format(with mask: String, phone: String) -> String {
//    let numbers = phone.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression)
//    var result = ""
//    var index = numbers.startIndex // numbers iterator
//
//    // iterate over the mask characters until the iterator of numbers ends
//    for ch in mask where index < numbers.endIndex {
//        if ch == "X" {
//            // mask requires a number in this place, so take the next one
//            result.append(numbers[index])
//
//            // move numbers iterator to the next index
//            index = numbers.index(after: index)
//
//        } else {
//            result.append(ch) // just append a mask character
//        }
//    }
//    return result
//}
fun format(mask: String, phone: String): String{
    val numbers = phone.replace("[^0-9]".toRegex(),"").toMutableList()
    var result = ""
    if (numbers.size > 0){
        var index = 0 // numbers iterator
        for (ch in mask){
            if (index > numbers.lastIndex ){
                break
            }else{
                if ("$ch" == "X") {
                    // mask requires a number in this place, so take the next one
//                    result = result.plus(numbers[index])
                    result = "$result${numbers[index]}"
                    // move numbers iterator to the next index
                    index = nextIndex(numbers,index)

                } else {
                    result = "$result$ch" // just append a mask character
                }
            }

        }
        return result
    }
    return phone

}

fun nextIndex(array: MutableList<Char>, index: Int): Int{
    return if(array.lastIndex > index){
        index + 1
    }else{
        index + 1
    }
}

class PhoneTextFormatter(private val mEditText: EditText, private val mPattern: String) :
    TextWatcher {
    private val TAG = this.javaClass.simpleName
    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        val phone = StringBuilder(s)
        Log.d(TAG, "join")
        if (count > 0 && !isValid(phone.toString())) {
            for (i in 0 until phone.length) {
                Log.d(TAG, String.format("%s", phone))
                val c = mPattern[i]
                if (c != '#' && c != phone[i]) {
                    phone.insert(i, c)
                }
            }
            mEditText.setText(phone)
            mEditText.setSelection(mEditText.text.length)
        }
    }

    override fun afterTextChanged(s: Editable) {}
    private fun isValid(phone: String): Boolean {
        for (i in 0 until phone.length) {
            val c = mPattern[i]
            if (c == '#') continue
            if (c != phone[i]) {
                return false
            }
        }
        return true
    }

    init {
        //set max length of string
        val maxLength = mPattern.length
        mEditText.filters = arrayOf<InputFilter>(LengthFilter(maxLength))
    }
}