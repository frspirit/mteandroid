package ru.takeeat.takeeat


import android.content.Context
import android.content.Intent
import android.content.IntentSender.SendIntentException
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build.VERSION.SDK_INT
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.Window
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import coil.ImageLoader
import coil.decode.GifDecoder
import coil.decode.ImageDecoderDecoder
import coil.load
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.snackbar.Snackbar
import com.google.android.play.core.appupdate.AppUpdateInfo
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.UpdateAvailability
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.ktx.messaging
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import okhttp3.*
import ru.takeeat.takeeat.databinding.ActivityMainBinding
import ru.takeeat.takeeat.db.AppDataBase
import ru.takeeat.takeeat.db.core.*
import ru.takeeat.takeeat.db.order.ProductOrder
import ru.takeeat.takeeat.network.mtesapi.interfaces.Result
import ru.takeeat.takeeat.network.mtesapi.core.CoreAPI
import ru.takeeat.takeeat.network.mtesapi.core.EMPTY_SHOPS
import ru.takeeat.takeeat.network.mtesapi.core.ERR_GET_SHOPS
import ru.takeeat.takeeat.network.mtesapi.core.SubOrg
import ru.takeeat.takeeat.network.mtesapi.core.Group as GroupN
import ru.takeeat.takeeat.network.mtesapi.core.Selection as SelectionN
import ru.takeeat.takeeat.network.mtesapi.core.Modifier as ModifierN
import ru.takeeat.takeeat.network.mtesapi.core.Shop as ShopN
import ru.takeeat.takeeat.network.mtesapi.customer.CustomerAPI
import ru.takeeat.takeeat.network.mtesapi.customer.data.*


const val SERVER_IMAGE: String = "https://api.takeeat.ru"

class MainActivity : AppCompatActivity(), LifecycleObserver {
    private lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController
    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var uiScope: CoroutineScope
    private lateinit var productInBasket: LiveData<List<ProductOrder>>
    private var countProductInBasket: Int = 0
    private lateinit var db: AppDataBase
    private val token: MutableLiveData<String> = MutableLiveData()

    private var appUpdateManager: AppUpdateManager? = null

    companion object {
        private const val TAG = "MainActivity"
        private const val IMMEDIATE_APP_UPDATE_REQ_CODE = 124
    }

    private fun checkUpdate() {
        val appUpdateInfoTask = appUpdateManager!!.appUpdateInfo
        appUpdateInfoTask.addOnSuccessListener { appUpdateInfo: AppUpdateInfo ->
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)
            ) {
                startUpdateFlow(appUpdateInfo)
            } else if (appUpdateInfo.updateAvailability() == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
                startUpdateFlow(appUpdateInfo)
            }
        }
    }

    private fun startUpdateFlow(appUpdateInfo: AppUpdateInfo) {
        try {
            appUpdateManager!!.startUpdateFlowForResult(
                appUpdateInfo,
                AppUpdateType.IMMEDIATE,
                this,
                IMMEDIATE_APP_UPDATE_REQ_CODE
            )
        } catch (e: SendIntentException) {
            e.printStackTrace()
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == IMMEDIATE_APP_UPDATE_REQ_CODE) {
            if (resultCode == RESULT_CANCELED) {
                Toast.makeText(
                    applicationContext,
                    "Возможны сбои в работе приложения!", Toast.LENGTH_LONG
                ).show()
            } else if (resultCode == RESULT_OK) {
                Toast.makeText(
                    applicationContext,
                    "Обновление успешно установлено", Toast.LENGTH_LONG
                ).show()
            } else {
                Toast.makeText(
                    applicationContext,
                    "Ошибка обновления!", Toast.LENGTH_LONG
                ).show()
                checkUpdate()
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appUpdateManager = AppUpdateManagerFactory.create(getApplicationContext());
        checkUpdate()
        val viewModelJob = Job()
        uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val window: Window = this.window

        window.statusBarColor = ContextCompat.getColor(this.baseContext, R.color.preloadStatusBarColor)
        window.navigationBarColor = ContextCompat.getColor(this.baseContext, R.color.preloadNavigationBarColor)

        window.decorView.systemUiVisibility = 1
        setContentView(binding.root)


        val imageLoader = ImageLoader.Builder(baseContext)
            .componentRegistry {
                if (SDK_INT >= 28) {
                    add(ImageDecoderDecoder(baseContext))
                } else {
                    add(GifDecoder())
                }
            }
            .build()
//
//
        binding.preview.preloaderImageView.load(R.drawable.logogif, imageLoader=imageLoader){
            this.crossfade(enable = true)
            this.crossfade(durationMillis = 60)
            placeholder(R.drawable.logo)
        }


        uiScope.launch {
            if (!isOnline(context = baseContext)) {

                binding.preview.preloaderInternetReloadetCardView.visibility = View.VISIBLE
                binding.preview.preloaderInternetReloadetButton.setOnClickListener {
                    if (isOnline(context = baseContext)) {
                        binding.preview.preloaderInternetReloadetCardView.visibility = View.GONE
                        onStartMyCode()
                    } else {
                        Snackbar.make(binding.root.rootView,"Отсутствует подключение к интернету!", Snackbar.LENGTH_LONG).apply {
                            setBackgroundTint(resources.getColor(R.color.AccentColor))
                            setTextColor(resources.getColor(R.color.white))

                            val params = view.layoutParams as FrameLayout.LayoutParams
                            params.topMargin = 118
                            params.gravity = Gravity.CENTER_HORIZONTAL or Gravity.TOP
                            show()
                        }
                    }
                }
                Snackbar.make(binding.root.rootView,"Отсутствует подключение к интернету!", Snackbar.LENGTH_LONG).apply {
                    setBackgroundTint(resources.getColor(R.color.AccentColor))
                    setTextColor(resources.getColor(R.color.white))

                    val params = view.layoutParams as FrameLayout.LayoutParams
                    params.topMargin = 118
                    params.gravity = Gravity.CENTER_HORIZONTAL or Gravity.TOP
                    show()
                }

            } else {
                binding.preview.preloaderInternetReloadetCardView.visibility = View.GONE
                onStartMyCode()
            }

        }

    }

    fun onStartMyCode() {

        setSupportActionBar(binding.myToolbar)
        supportActionBar?.title = R.string.app_name.toString()
        supportActionBar?.setDisplayShowTitleEnabled(true)
        val application = requireNotNull(this).application
        db = AppDataBase.getInstance(application)
        onSetOrUpdateProductDB()
        onGetSubOrgInfo()
//        Handler().postDelayed({
//            binding.preview.root.visibility = View.GONE
//            window.statusBarColor = ContextCompat.getColor(this.baseContext, R.color.purple_light)
//            window.navigationBarColor =
//                ContextCompat.getColor(this.baseContext, R.color.purple_light)
//            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
//        }, 5000)

        fmcNotify()

        val navView = binding.navView

        val navHostFragment = supportFragmentManager.findFragmentById(
            R.id.nav_host_fragment_activity_main
        ) as NavHostFragment

        navController = navHostFragment.navController

        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_main, R.id.navigation_basket, R.id.navigation_profile
            )
        )

        navView.setupWithNavController(navController)
        setupActionBarWithNavController(navController, appBarConfiguration)

    }

    private fun fmcNotify() {


        uiScope.launch {
            val service = CustomerAPI()
            val customerDB = db.customerDAO().getCheckCustomerNoLive()
//            Log.d(TAG, "CustomerOSERDB - ${customerDB}")
            if (customerDB == null) {
                createCustomer(service, db, "None")
            } else {
                val customerApiServer: MutableLiveData<Customer> = MutableLiveData()
                val errorStatus = MutableLiveData<Int>()
                service.callGetCustomer(
                    customerDB.uuid!!,
                    customerApiServer,
                    errorStatus
                )
                customerApiServer.observeForever { customer ->
//                    Log.d(TAG, "CustomerOSER - ${customer}")
                    if (customer != null) {
                        if (customer.uuid != customerDB.uuid) {
                            customerDB.phone = null
                        }
                        uiScope.launch {
                            updateCusomerInfo(db, customerDB, customer)
                        }
                    } else {
                        // Log.d(TAG, "CustomerOSER - ELSE?-  ${customer}")
                    }

                }
                errorStatus.observeForever {
                    if (it != null) {
                        // Log.d("StatusCode", "$it")
                        if (it == 204) {
                            service.callAttachCustomer(
                                Customer(
                                    android_device = Android(
                                        name = " ",
                                        registration_id = customerDB.android_device?.registration_id
                                            ?: "",
                                    )
                                ),
                                customerApiServer,
                                errorStatus
                            )
//                            val view = layoutInflater.inflate(
//                                R.layout.custom_alet_dialog_customer_error,
//                                null
//                            )
//                            val buttonOk = view.findViewById<MaterialButton>(R.id.alertDialogReady)
//                            view.findViewById<TextView>(R.id.alertDialogTitle).text = "Ошибка"
//                            view.findViewById<TextView>(R.id.alertDialogTextError).text =
//                                "В ваш акканут вошли с другого мобильного телефона, если это не вы обратитесь к администрации!"
//                            val alertDialogBuilder = AlertDialog.Builder(
//                                this@MainActivity,
//                                R.style.CustomAlertDialogOrderInfo
//                            )
//                            alertDialogBuilder.setView(view)
//                            alertDialogBuilder.setCancelable(false)
//                            val alert = alertDialogBuilder.create()
//
//                            buttonOk.setOnClickListener {
//                                alert.cancel()
//                            }
//
//                            alert.show()
                        } else {
//                            TODO(Kebrick): Дописать создание нового пользователя
                            return@observeForever
                        }
                    }
                }
            }

            Firebase.messaging.subscribeToTopic("takeeat")
                .addOnCompleteListener { task ->
                    var msg = getString(R.string.msg_subscribed)
                    if (!task.isSuccessful) {
                        msg = getString(R.string.msg_subscribe_failed)
                    }

                    Firebase.messaging.token.addOnCompleteListener(OnCompleteListener { task ->
                        if (!task.isSuccessful) {
                            Log.w(TAG, "Fetching FCM registration token failed", task.exception)
                            return@OnCompleteListener
                        }
                        uiScope.launch {
                            val customerDB = db.customerDAO().getCheckCustomerNoLive()
                            if (customerDB != null) {
//                                if (customerDB.android_device?.registration_id != task.result){
                                val customerApIServer =
                                    MutableLiveData<ru.takeeat.takeeat.network.mtesapi.customer.data.Customer>()
                                customerDB.android_device = ru.takeeat.takeeat.db.customer.Android(
                                    nameAndroid = customerDB.android_device?.nameAndroid
                                        ?: customerDB.uuid,
                                    registration_id = task.result ?: " "
                                )
                                uiScope.launch {
                                    db.customerDAO().update(customerDB)
                                }
                                val customerUpdInfo =
                                    ru.takeeat.takeeat.network.mtesapi.customer.data.Customer(
                                        android_device = Android(
                                            name = customerDB.android_device?.nameAndroid,
                                            registration_id = task.result ?: " "
                                        )
                                    )
                                CustomerAPI().callUpdateCustomer(
                                    customerUpdateInfo = customerUpdInfo,
                                    customerUuid = customerDB.uuid!!,
                                    customerApIServer
                                )
                                // Log.d("CustFcmTokenUpd", task.result)
//                                }

                            }
                        }

                        token.value = task.result
                        // Log.d("FCM_TOKEN", task.result)
                    })

                     Log.d("FCM_MESSAGE", msg)
                }


        }

        productInBasket = db.productOrderDAO().getAll()

        val badge: BadgeDrawable = binding.navView.getOrCreateBadge(R.id.basket)

        productInBasket.observeForever { listProductOrder ->
            countProductInBasket = 0
            listProductOrder.forEach { productOrder ->
                countProductInBasket += (productOrder.amount)
            }
            if (countProductInBasket > 0) {
                badge.backgroundColor = getColor(R.color.AccentColor)
                badge.badgeTextColor = getColor(R.color.white)
                badge.isVisible = true
                badge.number = countProductInBasket
            } else {
                badge.isVisible = false

            }


        }

    }


    private suspend fun createCustomer(service: CustomerAPI, db: AppDataBase, token: String) {
        val customerCreate: MutableLiveData<Customer> = MutableLiveData()
        service.callAttachCustomer(
            Customer(
                android_device = Android(
                    name = Settings.Secure.NAME,
                    registration_id = token
                )
            ), customerCreate,
            null
        )

        customerCreate.observeForever {
            // Log.d(TAG, "customeCreate = ${it}")
            if (it != null) {
                uiScope.launch {
                    db.customerDAO().insert(
                        ru.takeeat.takeeat.db.customer.Customer(
                            uuid = customerCreate.value?.uuid,
                            name = customerCreate.value?.name,
                            surname = customerCreate.value?.surname,
                            email = customerCreate.value?.email,
                            phone = customerCreate.value?.phone,
                            balance = customerCreate.value?.balance!!.toInt(),
                            sex = customerCreate.value?.sex,
                            consentStatus = customerCreate.value?.consentStatus,
                            birthday = customerCreate.value?.birthday,
                            android_device = customerCreate.value?.android_device?.registration_id?.let {
                                ru.takeeat.takeeat.db.customer.Android(
                                    nameAndroid = customerCreate.value?.android_device?.name,
                                    registration_id = it,
                                )
                            })
                    )
                }
                return@observeForever
            }
        }
    }

    private suspend fun updateCusomerInfo(
        db: AppDataBase,
        customerDB: ru.takeeat.takeeat.db.customer.Customer,
        customerApiServer: Customer?
    ) {
        if (customerApiServer == null) {
            return
        }
        customerDB.uuid = customerApiServer.uuid
        customerDB.name = customerApiServer.name
        customerDB.surname = customerApiServer.surname
        customerDB.email = customerApiServer.email
//        customerDB.phone = customerApiServer.phone
        customerDB.balance = customerApiServer.balance
        customerDB.sex = customerApiServer.sex
        customerDB.consentStatus = customerApiServer.consentStatus
        customerDB.birthday = customerApiServer.birthday
        db.customerDAO().update(customerDB)
    }


    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp(appBarConfiguration)
    }

    fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivityManager != null) {
            val capabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                    return true
                }
            }
        }
        return false
    }
    private fun onGetSubOrgInfo(){
        uiScope.launch {
            val serviceCore = CoreAPI()
            serviceCore.callSubOrgInfo(object: Result<SubOrg>{
                override fun result(data: SubOrg) {
                    uiScope.launch {
                        val idSubOrg = db.subOrgDAO().insert(
                            SubOrgEntity(
                                name=data.name,
                                auth_description_login = data.auth_description_login,
                                auth_description_passwd = data.auth_description_passwd,
                                logo = data.logo,
                                web_link = data.web_link,
                                vk_link = data.vk_link,
                            )
                        )
                    }
                }

                override fun statusCode(code: Int) {
                    super.statusCode(code)
                }
            }
        )

        }
    }
    private fun onSetOrUpdateProductDB() {
        val serviceCore = CoreAPI()
        uiScope.launch {

            val group = MutableLiveData<List<GroupN>>()
            serviceCore.callGroups(liveData = group)

            group.observeForever { listGroup ->
                if (!listGroup.isNullOrEmpty()) {
                    binding.preview.previewErrorServerCV.visibility = View.GONE

                    uiScope.launch {

                        db.productCore().removeAllByGroupAndSubGroup()
                        db.groupCore().removeAll()
                        db.subGroupCore().removeAll()
                        db.groupModifierDAO().removeAllProduct()
                        db.modifierStrDAO().removeAllGM()

                        listGroup.forEach { group: GroupN ->
                            val idGroup = db.groupCore().insert(
                                ru.takeeat.takeeat.db.core.Group(
                                    uuid = group.uuid,
                                    name = group.name,
                                    image = group.image,
                                    image_priority = group.image_priority,
                                )
                            )
                            group.products?.forEach { product: ru.takeeat.takeeat.network.mtesapi.core.Product ->
                                val idProduct = db.productCore().insert(
                                    Product(
                                        uuid = product.uuid,
                                        idGroup = idGroup,
                                        idSubGroup = null,
                                        idSelection = null,
                                        name = product.name,
                                        hidden = product.hidden,
                                        description = product.description,
                                        price = product.price,
                                        p_price = product.p_price,
                                        weight = product.weight,
                                        carbohydrateFullAmount = product.carbohydrateFullAmount,
                                        energyFullAmount = product.energyFullAmount,
                                        fatFullAmount = product.fatFullAmount,
                                        fiberFullAmount = product.fiberFullAmount,
                                        image = product.image,
                                        image_priority = product.image_priority,
                                        groupModifierBool = !product.group_modifier.isNullOrEmpty()
                                    )
                                )
                                product.group_modifier.forEach { groupModifier: ru.takeeat.takeeat.network.mtesapi.core.GroupModifier ->
                                    val idGroupModifier = db.groupModifierDAO().insert(
                                        GroupModifier(
                                            uuid = groupModifier.uuid,
                                            idProduct = idProduct,
                                            name = groupModifier.name,
                                            maxAmount = groupModifier.maxAmount,
                                            minAmount = groupModifier.minAmount,
                                            required = groupModifier.required
                                        )
                                    )
                                    groupModifier.child_modifier.forEach { uuidModifier: String ->
                                        db.modifierStrDAO().insert(
                                            ModifierStr(
                                                uuid = uuidModifier,
                                                idMGroup = idGroupModifier,
                                            )
                                        )
                                    }
                                }

                            }
                            // Log.d("idGroup", "$idGroup")
                            group.sub_groups?.forEach { subGroup ->
                                val idSubGroup = db.subGroupCore().insert(
                                    SubGroup(
                                        uuid = subGroup.uuid,
                                        idGroup = idGroup,
                                        name = subGroup.name,
                                        image = subGroup.image,
                                        image_priority = subGroup.image_priority
                                    )
                                )
                                subGroup.products?.forEach { product ->
                                    val idProduct = db.productCore().insert(
                                        Product(
                                            uuid = product.uuid,
                                            idGroup = null,
                                            idSubGroup = idSubGroup,
                                            idSelection = null,
                                            name = product.name,
                                            hidden = product.hidden,
                                            description = product.description,
                                            price = product.price,
                                            p_price = product.p_price,
                                            weight = product.weight,
//                                            carbohydrateAmount = product.carbohydrateAmount,
                                            carbohydrateFullAmount = product.carbohydrateFullAmount,
//                                            energyAmount = product.energyAmount,
                                            energyFullAmount = product.energyFullAmount,
//                                            fatAmount = product.fatAmount,
                                            fatFullAmount = product.fatFullAmount,
//                                            fiberAmount = product.fiberAmount,
                                            fiberFullAmount = product.fiberFullAmount,
                                            image = product.image,
                                            image_priority = product.image_priority,
                                            groupModifierBool = !product.group_modifier.isNullOrEmpty()
                                        )
                                    )
                                    product.group_modifier.forEach { groupModifier: ru.takeeat.takeeat.network.mtesapi.core.GroupModifier ->
                                        val idGroupModifier = db.groupModifierDAO().insert(
                                            GroupModifier(
                                                uuid = groupModifier.uuid,
                                                idProduct = idProduct,
                                                name = groupModifier.name,
                                                maxAmount = groupModifier.maxAmount,
                                                minAmount = groupModifier.minAmount,
                                                required = groupModifier.required
                                            )
                                        )
                                        groupModifier.child_modifier.forEach { uuidModifier: String ->
                                            db.modifierStrDAO().insert(
                                                ModifierStr(
                                                    uuid = uuidModifier,
                                                    idMGroup = idGroupModifier,
                                                )
                                            )
                                        }
                                    }
                                }
                            }
                        }
                        binding.preview.root.visibility = View.GONE

                        window.statusBarColor = ContextCompat.getColor(
                            this@MainActivity.baseContext,
                            R.color.purple_light
                        )
                        window.navigationBarColor =
                            ContextCompat.getColor(
                                this@MainActivity.baseContext,
                                R.color.purple_light
                            )

//                        WindowCompat.setDecorFitsSystemWindows(window, false)

                        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                        binding.mailFullCL.visibility = View.VISIBLE
                    }
                    return@observeForever
                } else {
                    binding.preview.preloaderImageView.visibility = View.GONE
                    binding.preview.preloaderErrorImageView.visibility = View.VISIBLE
                    binding.preview.preloaderErrorMessage.visibility = View.VISIBLE
                    binding.preview.previewErrorServerCV.visibility = View.VISIBLE
                    binding.preview.previewErrorServerB.setOnClickListener {
                        Snackbar.make(
                            binding.root.rootView,
                            "Запрашиваем продукты...",
                            Snackbar.LENGTH_LONG
                        ).apply {
                            setBackgroundTint(resources.getColor(R.color.AccentColor))
                            setTextColor(resources.getColor(R.color.white))

                            val params = view.layoutParams as FrameLayout.LayoutParams
                            params.topMargin = 118
                            params.gravity = Gravity.CENTER_HORIZONTAL or Gravity.TOP
                            show()
                        }
                        serviceCore.callGroups(liveData = group)

                    }

                }
            }
            // selection in db
         }
        uiScope.launch {
            val selections = MutableLiveData<List<SelectionN>>()
            serviceCore.callSelections(liveData = selections)

            selections.observeForever { data ->

                if (!data.isNullOrEmpty()) {
//                    // Log.d("SELECTIONSDATA!!", "$data")

                    uiScope.launch {
                        db.productCore().removeAllBySelection()
                        db.selectionDAO().removeAll()
                        data.forEach { selection ->
                            val idSelection = db.selectionDAO().insert(
                                ru.takeeat.takeeat.db.core.Selection(
                                    uuid = selection.uuid,
                                    title = selection.title,
                                    emoji_android = selection.emoji_android,
                                    emoji_ios = selection.emoji_ios,
                                )
                            )
//                            // Log.d("IDSELECTIONSDATA!!", "$idSelection")
                            selection.product.forEach { product ->
                                val idProduct = db.productCore().insert(
                                    Product(
                                        uuid = product.uuid,
                                        idGroup = null,
                                        idSubGroup = null,
                                        idSelection = idSelection,
                                        name = product.name,
                                        hidden = product.hidden,
                                        description = product.description,
                                        price = product.price,
                                        p_price = product.p_price,
                                        weight = product.weight,
//                                        carbohydrateAmount = product.carbohydrateAmount,
                                        carbohydrateFullAmount = product.carbohydrateFullAmount,
//                                        energyAmount = product.energyAmount,
                                        energyFullAmount = product.energyFullAmount,
//                                        fatAmount = product.fatAmount,
                                        fatFullAmount = product.fatFullAmount,
//                                        fiberAmount = product.fiberAmount,
                                        fiberFullAmount = product.fiberFullAmount,
                                        image = product.image,
                                        image_priority = product.image_priority,
                                        groupModifierBool = !product.group_modifier.isNullOrEmpty()
                                    )
                                )
                                product.group_modifier.forEach { groupModifier: ru.takeeat.takeeat.network.mtesapi.core.GroupModifier ->
                                    val idGroupModifier = db.groupModifierDAO().insert(
                                        GroupModifier(
                                            uuid = groupModifier.uuid,
                                            idProduct = idProduct,
                                            name = groupModifier.name,
                                            maxAmount = groupModifier.maxAmount,
                                            minAmount = groupModifier.minAmount,
                                            required = groupModifier.required
                                        )
                                    )
                                    groupModifier.child_modifier.forEach { uuidModifier: String ->
                                        db.modifierStrDAO().insert(
                                            ModifierStr(
                                                uuid = uuidModifier,
                                                idMGroup = idGroupModifier,
                                            )
                                        )
                                    }
                                }
                            }
                        }
                    }
                    return@observeForever
                }
            }
        }
        uiScope.launch {
            val modifiers = MutableLiveData<List<ModifierN>>()
            serviceCore.callModifiers(liveData = modifiers)

            modifiers.observeForever { data ->
                if (!data.isNullOrEmpty()) {
                    uiScope.launch {
                        db.modifierDAO().removeAll()
                        data.forEach { modifier: ModifierN ->
                            db.modifierDAO().insert(
                                ru.takeeat.takeeat.db.core.Modifier(
                                    uuid = modifier.uuid,
                                    name = modifier.name,
                                    price = modifier.price.toFloat(),
                                    carbohydrateFullAmount = modifier.carbohydrateFullAmount.toFloat(),
                                    energyFullAmount = modifier.energyFullAmount.toFloat(),
                                    fatFullAmount = modifier.fatFullAmount.toFloat(),
                                    fiberFullAmount = modifier.fiberFullAmount.toFloat(),
                                    image = modifier.image,
                                    image_priority = modifier.image_priority,
                                )
                            )

                        }
                    }

                    return@observeForever
                }
            }

        }
        uiScope.launch {
            val shops = MutableLiveData<List<ShopN>>()
            serviceCore.callShops(liveData = shops, object: Result<Int> {
                override fun result(code: Int) {
                    if(code == ERR_GET_SHOPS || code == EMPTY_SHOPS){
                        uiScope.launch {
                            db.shopDAO().removeAll()
                        }
                    }
                }
            })



            shops.observeForever { data ->
                if (!data.isNullOrEmpty()) {
                    uiScope.launch {
                        db.shopDAO().removeAll()
                        data.forEach{ shop ->
                            db.shopDAO().insert(
                                ShopEntity(
                                    uuid=shop.uuid,
                                    pickup = shop.pickup,
                                    name = shop.name,
                                    phone = shop.phone,
                                    date_work = shop.date_work,
                                    time_work = shop.time_work,
                                    address = shop.address,
                                    latitude = shop.latitude,
                                    longitude = shop.longitude,
                                )
                            )
                        }

                    }
                    return@observeForever
                }
            }


        }
    }
}


