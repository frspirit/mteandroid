package ru.takeeat.takeeat

import android.content.Context
import android.location.Address
import android.location.Geocoder
import android.util.Log
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.PolyUtil
import com.google.maps.android.data.kml.KmlContainer
import com.google.maps.android.data.kml.KmlLayer
import kotlinx.coroutines.*
import java.io.FileOutputStream
import java.io.IOException
import java.lang.RuntimeException
import java.net.URL
import java.nio.channels.Channels


class MapsUtils(val context: Context) {
    data class PolygonInfo(
        var text: String,
        var minPriceFree: Int,
        var priceDelivery: Int,
        val uuidDeliveryProduct: String,
        val polygonString: String,
    )

    val takeEatGeo = LatLng(59.896282, 29.773629)

    val testPolygon = PolygonInfo(
        "",
        0,
        0,
        "528b903f-ccd7-48fc-a4f8-554672e271a8",
        ""
    )


    companion object {

        val TAG = "MapsUtils"

    }



    fun parseSringLatLng(clickLatLng: LatLng, strZoneLanLng: String): Boolean {
        var polyStr = strZoneLanLng
        polyStr = polyStr.replace("[[", "")
        polyStr = polyStr.replace("lat/lng: (", "")
        polyStr = polyStr.replace("), ", ":")
        polyStr = polyStr.replace(")]]", "")
        val polyListSrt = polyStr.split(":")
        val arrayLatLng: MutableList<LatLng> = mutableListOf()
        polyListSrt.forEach { latLngStr ->
            val out = latLngStr.split(",")
             Log.d( "KMLGEOClick!", "LatLng: ${out}" )
            val latLngObj = LatLng(out.get(0).toDouble(), out.get(1).toDouble())
            arrayLatLng.add(latLngObj)

        }

//        PolyUtil.decode(polyStr)
        return try {
            PolyUtil.containsLocation(clickLatLng, arrayLatLng, true)
        } catch (e: IOException){
            false
        }catch (e: RuntimeException){
            false
        }
    }
    fun checkPolygon(layer: KmlLayer, latLng: LatLng): PolygonInfo?{


        for (containers in layer.containers.iterator()) {
            Log.d( "KMLGEOClick!", "1 LatLng: ${layer.containers} -  ${layer.containers}" )

            val b = accessContainers(latLng = latLng,containers = containers.containers)
            if (b != null){
                return b
            }
        }

        return null

    }



    fun accessContainers(latLng: LatLng,containers: Iterable<KmlContainer>):PolygonInfo? {

        for (container in containers) {
            if (container.hasContainers()) {
                accessContainers(latLng, container.containers)
            }
            for (placemark in container.placemarks) {
                Log.d( "KMLGEOClick!", "1 LatLng: ${parseSringLatLng(latLng, placemark.geometry.geometryObject.toString())}" )

                if(parseSringLatLng(latLng, placemark.geometry.geometryObject.toString())){
                    Log.d( "KMLGEOClick!", "2 LatLng: ${parseSringLatLng(latLng, placemark.geometry.geometryObject.toString())}" )
                    if (placemark.hasProperty("description")) {
                        var dArray = placemark.getProperty("description").split("::")
                        if (dArray.count() >= 2){
                            val price = dArray[0]
                            val minPrice = dArray[1]
                            val text = dArray[2]
                            testPolygon.minPriceFree = minPrice.toInt()
                            testPolygon.priceDelivery = price.toInt()
                            testPolygon.text = text
                            return testPolygon
                        }


                    }

                }

            }
            return null
        }
        return null
    }
    fun getAddress(latLng: LatLng): Address? {
        val geoCoder = Geocoder(context)
        val matches: List<Address> = geoCoder.getFromLocation(latLng.latitude, latLng.longitude, 1) as List<Address>
        val bestMatch: Address? = matches.getOrNull(0)
        return bestMatch
    }

    fun getLatLngAddress(address: String): LatLng? {
        val geoCoder = Geocoder(context)
        val matches: List<Address> =
            geoCoder.getFromLocationName(address, 1,59.612537, 29.235768,60.343445, 30.945934) as List<Address>
        val addressObj = matches.getOrNull(0)
        val bestMatch: LatLng? = addressObj?.let { LatLng(it.latitude, addressObj.longitude) }
        return bestMatch
    }




}