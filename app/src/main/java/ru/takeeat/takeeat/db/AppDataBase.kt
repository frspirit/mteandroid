package ru.takeeat.takeeat.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import ru.takeeat.takeeat.db.core.*
import ru.takeeat.takeeat.db.customer.Address
import ru.takeeat.takeeat.db.customer.Customer
import ru.takeeat.takeeat.db.customer.CustomerDAO
import ru.takeeat.takeeat.db.order.*


@Database(
    entities = arrayOf(
        ProductOrder::class,
        ModifierOrder::class,
        GroupModifierOrder::class,
        ModifierStrOrder::class,
        GroupModifier::class,
        ModifierStr::class,
        Modifier::class,
        Customer::class,
        Address::class,
        Product::class,
        SubGroup::class,
        Group::class,
        Selection::class,
        ShopEntity::class,
        SubOrgEntity::class,
    ), version = 32, exportSchema = false)
abstract class AppDataBase : RoomDatabase() {
    abstract fun productCore(): ProductDAO
    abstract fun groupCore(): GroupDAO
    abstract fun subGroupCore(): SubGroupDAO
    abstract fun productOrderDAO(): ProductOrderDAO
    abstract fun customerDAO(): CustomerDAO
    abstract fun modifierOrderDAO(): ModifierOrderDAO
    abstract fun groupModifierOrderDAO(): GroupModifierOrderDAO
    abstract fun modifierStrOrderDAO(): ModifierStrOrderDAO
    abstract fun groupModifierDAO(): GroupModifierDAO
    abstract fun modifierDAO(): ModifierDAO
    abstract fun modifierStrDAO(): ModifierStrDAO
    abstract fun selectionDAO(): SelectionDAO
    abstract fun shopDAO(): ShopDAO
    abstract fun subOrgDAO(): SubOrgDAO


    companion object {
        @Volatile
        private var INSTANCE: AppDataBase? = null

        fun getInstance(context: Context): AppDataBase {
            synchronized(this) {
                var instance = INSTANCE
                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        AppDataBase::class.java,
                        "TakeEatDB"
                    ).allowMainThreadQueries()
                        .fallbackToDestructiveMigration()
                        .build()
                    INSTANCE = instance

                }
                return instance

            }
        }
    }
}