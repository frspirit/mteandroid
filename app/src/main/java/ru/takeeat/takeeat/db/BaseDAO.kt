package ru.takeeat.takeeat.db

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Update

interface BaseDAO<T> {

    @Insert
    suspend fun insert(obj: T): Long

    @Update
    suspend fun update(vararg obj: T)

    @Delete
    suspend fun remove(vararg obj: T)
}