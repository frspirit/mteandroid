package ru.takeeat.takeeat.db.core

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query

import ru.takeeat.takeeat.db.BaseDAO



@Dao
interface GroupDAO: BaseDAO<Group> {
    @Query("SELECT * from group_product WHERE id = :id")
    suspend fun get(id: Long): Group

    @Query("SELECT * FROM group_product ORDER BY id asc")
    fun getAll(): LiveData<List<Group>>

    @Query("SELECT * FROM group_product ORDER BY id asc")
    suspend fun getAllNoLive(): List<Group>

    @Query("SELECT COUNT(*) FROM group_product")
    suspend fun getCount(): Int

    @Query("SELECT * FROM group_product ORDER BY id DESC LIMIT 1")
    suspend fun getTonight(): Group?

    @Query("DELETE FROM group_product")
    suspend fun removeAll()

    @Query("SELECT * from group_product order by id asc")
    suspend fun getGroup(): List<GroupWithProductSubGroup>?
//    @Query("DELETE FROM modifier_order WHERE id = :id ")
//    suspend fun removeAllByProduct(id: Long){}
}