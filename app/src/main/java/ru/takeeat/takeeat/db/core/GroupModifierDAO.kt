package ru.takeeat.takeeat.db.core

import androidx.lifecycle.LiveData
import androidx.room.*
import ru.takeeat.takeeat.db.BaseDAO

@Dao
interface GroupModifierDAO: BaseDAO<GroupModifier> {
    @Query("SELECT * from group_modifier WHERE id = :id")
    suspend fun get(id: Long): GroupModifier

    @Query("SELECT * FROM group_modifier ORDER BY id DESC")
    fun getAll(): LiveData<List<GroupModifier>>

    @Query("SELECT * from group_modifier WHERE idProduct = :idProduct ORDER BY id DESC")
    suspend fun getByProduct(idProduct: Long): List<GroupModifier>
    @Query("SELECT * from group_modifier WHERE idProductOrder = :idProduct ORDER BY id DESC")
    suspend fun getByProductOrder(idProduct: Long): List<GroupModifier>

//    WHERE idProduct = :idProduct
    @Transaction
    @Query("SELECT * FROM group_modifier  ORDER BY id DESC")
    fun getGroupModifierAll(): LiveData<List<GroupModifierWithModifierStr>>


    @Query("SELECT COUNT(*) FROM group_modifier WHERE idProduct = :idProduct")
    suspend fun getCountByProduct(idProduct: Long): Int

    @Transaction
    @Query("SELECT * FROM group_modifier WHERE idProduct = :idProduct  ORDER BY id DESC")
    fun getGroupModifiersByProduct(idProduct: Long): LiveData<List<GroupModifierWithModifierStr>>
    @Transaction
    @Query("SELECT * FROM group_modifier WHERE idProduct = :idProduct  ORDER BY id DESC")
    suspend fun getNLDGroupModifiersByProduct(idProduct: Long): List<GroupModifierWithModifierStr>
    @Transaction
    @Query("SELECT * FROM group_modifier WHERE idProductOrder = :idProduct  ORDER BY id DESC")
    fun getGroupModifiersByProductDB(idProduct: Long): LiveData<List<GroupModifierWithModifierStr>>

    @Query("SELECT * FROM group_modifier ORDER BY id DESC LIMIT 1")
    suspend fun getTonight(): GroupModifier?

    @Query("DELETE FROM group_modifier")
    suspend fun removeAll()
    @Query("DELETE FROM group_modifier where idProduct is not null")
    suspend fun removeAllProduct()
//
    @Query("DELETE FROM group_modifier WHERE id = :id ")
    suspend fun removeById(id: Long){}

    @Query("DELETE FROM group_modifier WHERE idProduct = :id ")
    suspend fun removeAllByProduct(id: Long)
    @Query("DELETE FROM group_modifier WHERE idProductOrder = :id ")
    suspend fun removeAllByProductOrder(id: Long)



}