package ru.takeeat.takeeat.db.core

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query

import ru.takeeat.takeeat.db.BaseDAO



@Dao
interface ModifierDAO: BaseDAO<Modifier> {
    @Query("SELECT * from modifier WHERE id = :id")
    suspend fun get(id: Long): Modifier

    @Query("SELECT * FROM modifier ORDER BY id DESC")
    fun getLDAll(): LiveData<List<Modifier>>

    @Query("SELECT * FROM modifier ORDER BY id DESC")
    suspend fun getAll(): List<Modifier>

    @Query("SELECT COUNT(*) FROM modifier")
    suspend fun getCount(): Int

    @Query("SELECT * FROM modifier ORDER BY id DESC LIMIT 1")
    suspend fun getTonight(): Modifier?

    @Query("DELETE FROM modifier")
    suspend fun removeAll()
//    @Query("DELETE FROM modifier_order WHERE id = :id ")
//    suspend fun removeAllByProduct(id: Long){}
}