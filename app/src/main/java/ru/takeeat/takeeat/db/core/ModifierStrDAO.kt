package ru.takeeat.takeeat.db.core

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import ru.takeeat.takeeat.db.BaseDAO
@Dao
interface ModifierStrDAO: BaseDAO<ModifierStr> {
    @Query("SELECT * FROM modifier_str ORDER BY id DESC")
    fun getAll(): LiveData<List<ModifierStr>>
    @Query("DELETE FROM modifier_str where idMGroup is not null")
    suspend fun removeAllGM()
    @Query("DELETE FROM modifier_str where idMGroupOrder is not null")
    suspend fun removeAllGMOrder()
    @Query("DELETE FROM modifier_str WHERE idMGroup = :idMGroup ")
    suspend fun removeAllModifierStrByGroupModifier(idMGroup: Long)

    @Query("delete from modifier_str where idMGroupOrder = :id")
    suspend fun removeByMGroupOrder(id: Long)
    @Query("select * from modifier_str WHERE idMGroup = :idMGroup ORDER BY id DESC")
    suspend fun getByMGroup(idMGroup: Long): List<ModifierStr>
}