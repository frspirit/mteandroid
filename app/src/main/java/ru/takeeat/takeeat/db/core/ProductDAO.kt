package ru.takeeat.takeeat.db.core

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction

import ru.takeeat.takeeat.db.BaseDAO



@Dao
interface ProductDAO: BaseDAO<Product> {
    @Query("SELECT * from product WHERE id = :id")
    suspend fun get(id: Long): Product
    @Query("SELECT * from product WHERE id = :id")
    fun getLD(id: Long): LiveData<Product>

    @Query("SELECT * from product WHERE idGroup = :idGroup")
    suspend fun getByGroupProductAll(idGroup: Long): List<Product>
    @Query("SELECT * from product WHERE idGroup = :idGroup")
    fun getLDByGroupProductAll(idGroup: Long): LiveData<List<Product>>

    @Query("SELECT * FROM product ORDER BY id DESC")
    fun getAll(): LiveData<List<Product>>

    @Query("SELECT * FROM product ORDER BY id DESC")
    suspend fun getAllNoLive(): List<Product>

    @Query("SELECT COUNT(*) FROM product")
    suspend fun getCount(): Int

    @Query("SELECT * FROM product ORDER BY id DESC LIMIT 1")
    suspend fun getTonight(): Product?

    @Query("DELETE FROM product")
    suspend fun removeAll()

    @Query("delete from product where idGroup is not null or idSubGroup is not null")
    suspend fun removeAllByGroupAndSubGroup()

    @Query("delete from product where idSelection is not null")
    suspend fun removeAllBySelection()


    @Query("SELECT * from product where id = :idProduct order by id desc")
    suspend fun getProductWithGModifierAndModifier(idProduct: Long): ProductWithModifierGroupModifier?

    @Query("SELECT * from product where idGroup = :idGroup order by id desc")
    fun getLDAllProductWithGModifierAndModifier(idGroup: Long): LiveData<List<ProductWithModifierGroupModifier>>

    @Query("SELECT * from product where id = :idProduct order by id desc")
    fun getLDProductWithGModifierAndModifier(idProduct: Long): LiveData<ProductWithModifierGroupModifier>

    @Transaction
    @Query("SELECT * from product where idSubGroup in (:idSubGroup) order by id desc ")
    fun getLDSubGroupProductFull(idSubGroup: List<Long>): LiveData<List<ProductWithModifierGroupModifier>>
    @Query("SELECT * from product where idSubGroup in (:idSubGroup) order by id desc")
    suspend fun getSubGroupProductFull(idSubGroup: List<Long>): List<Product>
}