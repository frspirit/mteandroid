package ru.takeeat.takeeat.db.core

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query

import ru.takeeat.takeeat.db.BaseDAO



@Dao
interface SelectionDAO: BaseDAO<Selection> {
    @Query("SELECT * from selection WHERE id = :id")
    suspend fun get(id: Long): Selection
    @Query("SELECT * from selection WHERE id = :id")
    fun getLD(id: Long): LiveData<Selection>

    @Query("SELECT * FROM selection ORDER BY id DESC")
    fun getAll(): LiveData<List<Selection>>

    @Query("SELECT * FROM selection ORDER BY id DESC")
    suspend fun getAllNoLD(): List<Selection>

    @Query("SELECT COUNT(*) FROM selection")
    suspend fun getCount(): Int


    @Query("DELETE FROM selection")
    suspend fun removeAll()

    @Query("SELECT * from selection where id = :idSelection order by id desc")
    fun getSelectionProducts(idSelection: Long): LiveData<SelectionWithProduct>
//    @Query("DELETE FROM modifier_order WHERE id = :id ")
//    suspend fun removeAllByProduct(id: Long){}
}