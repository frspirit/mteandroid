package ru.takeeat.takeeat.db.core

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import ru.takeeat.takeeat.db.BaseDAO


@Dao
interface ShopDAO: BaseDAO<ShopEntity> {
    @Query("SELECT * from shop WHERE id = :id")
    suspend fun get(id: Long): ShopEntity

    @Query("SELECT * FROM shop ORDER BY id asc")
    fun getAll(): LiveData<List<ShopEntity>>
    @Query("SELECT * FROM shop WHERE pickup = :pickup ORDER BY id asc")
    fun getAllDelivery(pickup: Boolean): LiveData<List<ShopEntity>>

    @Query("SELECT * FROM shop ORDER BY id asc")
    suspend fun getAllNoLive(): List<ShopEntity>

    @Query("SELECT COUNT(*) FROM shop")
    suspend fun getCount(): Int

    @Query("SELECT * FROM shop ORDER BY id DESC LIMIT 1")
    suspend fun getTonight(): ShopEntity?

    @Query("DELETE FROM shop")
    suspend fun removeAll()

//    @Query("DELETE FROM modifier_order WHERE id = :id ")
//    suspend fun removeAllByProduct(id: Long){}
}