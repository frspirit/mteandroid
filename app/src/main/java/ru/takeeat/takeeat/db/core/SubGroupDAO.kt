package ru.takeeat.takeeat.db.core

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction

import ru.takeeat.takeeat.db.BaseDAO



@Dao
interface SubGroupDAO: BaseDAO<SubGroup> {
    @Query("SELECT * from sub_group WHERE id = :id")
    suspend fun get(id: Long): SubGroup

    @Query("SELECT * from sub_group WHERE idGroup = :idGroup")
    suspend fun getByGroupSubGroupAll(idGroup: Long): List<SubGroup>

    @Query("SELECT * FROM sub_group ORDER BY id DESC")
    fun getAll(): LiveData<List<SubGroup>>

    @Query("SELECT * FROM sub_group ORDER BY id DESC")
    suspend fun getAllNoLive(): List<SubGroup>

    @Query("SELECT COUNT(*) FROM sub_group")
    suspend fun getCount(): Int

    @Query("SELECT * FROM sub_group ORDER BY id DESC LIMIT 1")
    suspend fun getTonight(): SubGroup?

    @Query("DELETE FROM sub_group")
    suspend fun removeAll()

    @Query("SELECT * from sub_group order by id desc")
    suspend fun getGroup(): List<SubGroupWithProduct>?

    @Transaction
    @Query("SELECT * from sub_group where idGroup = :idGroup order by id desc")
    fun getSubGroupByGroupProductFull(idGroup: Long): LiveData<List<SubGroupWithProductFull>>


    @Transaction
    @Query("SELECT * from sub_group where idGroup = :idGroup order by id desc")
    fun getSubGroupByGroupProduct(idGroup: Long): LiveData<List<SubGroupWithProduct>>
//    @Query("DELETE FROM modifier_order WHERE id = :id ")
//    suspend fun removeAllByProduct(id: Long){}
}