package ru.takeeat.takeeat.db.core

import androidx.room.Dao
import androidx.room.Query

import ru.takeeat.takeeat.db.BaseDAO



@Dao
interface SubOrgDAO: BaseDAO<SubOrgEntity> {
    @Query("SELECT * from sub_org WHERE id = :id")
    suspend fun get(id: Long): SubOrgEntity

    @Query("SELECT COUNT(*) FROM sub_org")
    suspend fun getCount(): Int

    @Query("SELECT * FROM sub_org ORDER BY id DESC LIMIT 1")
    suspend fun getTonight(): SubOrgEntity?

    @Query("DELETE FROM sub_org")
    suspend fun removeAll()
}