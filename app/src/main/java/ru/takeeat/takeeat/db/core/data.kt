package ru.takeeat.takeeat.db.core

import androidx.room.*
import com.google.android.gms.maps.model.LatLng

@Entity(tableName = "group_product")
data class Group(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0L,

    @ColumnInfo(name="uuid")
    val uuid: String,

    @ColumnInfo(name="name")
    val name: String,

    @ColumnInfo(name="image")
    val image: String? = null,

    @ColumnInfo(name="image_priority")
    val image_priority: String? = null,
//    val products: List<Product>? = null,
//    val sub_groups: List<SubGroup>? = null,
)

@Entity(tableName = "sub_group")
data class SubGroup(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0L,

    @ColumnInfo(name="uuid")
    val uuid: String,

    @ColumnInfo(name="idGroup")
    val idGroup: Long,

    @ColumnInfo(name="name")
    val name: String,

    @ColumnInfo(name="image")
    val image: String? = null,

    @ColumnInfo(name="image_priority")
    val image_priority: String? = null,
)
@Entity(tableName = "product")
data class Product(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0L,

    @ColumnInfo(name="uuid")
    val uuid: String,

    @ColumnInfo(name="idGroup")
    val idGroup: Long?,

    @ColumnInfo(name="idSubGroup")
    val idSubGroup: Long?,

    @ColumnInfo(name="idSelection")
    val idSelection: Long?,

    @ColumnInfo(name="name")
    val name: String,

    @ColumnInfo(name="hidden")
    val hidden: Boolean,

    @ColumnInfo(name="description")
    val description: String,

    @ColumnInfo(name="price")
    val price: Float,

    @ColumnInfo(name="p_price")
    val p_price: Float? = null,

    @ColumnInfo(name="weight")
    val weight: Float,

//    @ColumnInfo(name="carbohydrateAmount")
//    val carbohydrateAmount: Float,

    @ColumnInfo(name="carbohydrateFullAmount")
    val carbohydrateFullAmount: Float,

//    @ColumnInfo(name="energyAmount")
//    val energyAmount: Float,

    @ColumnInfo(name="energyFullAmount")
    val energyFullAmount: Float,

//    @ColumnInfo(name="fatAmount")
//    val fatAmount: Float,

    @ColumnInfo(name="fatFullAmount")
    val fatFullAmount: Float,

//    @ColumnInfo(name="fiberAmount")
//    val fiberAmount: Float,

    @ColumnInfo(name="fiberFullAmount")
    val fiberFullAmount: Float,

    @ColumnInfo(name="image")
    val image: String?,

    @ColumnInfo(name="image_priority")
    val image_priority: String?,

    @ColumnInfo(name="groupModifierBool")
    val groupModifierBool: Boolean,

//    val modifier: List<String>,

//    val group_modifier: List<GroupModifier>
)
@Entity(tableName = "group_modifier")
data class GroupModifier(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name="id")
    val id: Long = 0L,

    @ColumnInfo(name="uuid")
    val uuid: String,

    @ColumnInfo(name="idProduct")
    val idProduct: Long? = null,
    @ColumnInfo(name="idProductOrder")
    val idProductOrder: Long? = null,

    @ColumnInfo(name="name")
    val name: String,

    @ColumnInfo(name="maxAmount")
    val maxAmount: Float,

    @ColumnInfo(name="minAmount")
    val minAmount: Float,

    @ColumnInfo(name="required")
    val required: Boolean,
//    val child_modifier: List<String>
)

// TODO(developer): дописать интерфейс ДАО для  модификатора
@Entity(tableName = "modifier")
data class Modifier(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name="id")
    val id: Long = 0L,

    @ColumnInfo(name="uuid")
    val uuid: String,
//    val uuidMGroup: String?,
//    val uuidProduct: String?,

    @ColumnInfo(name="name")
    val name: String,

    @ColumnInfo(name="price")
    val price: Float,

//    @ColumnInfo(name="carbohydrateAmount")
//    val carbohydrateAmount: Float? = null,

    @ColumnInfo(name="carbohydrateFullAmount")
    val carbohydrateFullAmount: Float,

//    @ColumnInfo(name="energyAmount")
//    val energyAmount: Float? = null,

    @ColumnInfo(name="energyFullAmount")
    val energyFullAmount: Float,

//    @ColumnInfo(name="fatAmount")
//    val fatAmount: Float? = null,

    @ColumnInfo(name="fatFullAmount")
    val fatFullAmount: Float,

//    @ColumnInfo(name="fiberAmount")
//    val fiberAmount: Float? = null,

    @ColumnInfo(name="fiberFullAmount")
    val fiberFullAmount: Float,

    @ColumnInfo(name="image")
    val image: String?,

    @ColumnInfo(name="image_priority")
    val image_priority: String?,
)
@Entity(tableName = "modifier_str")
data class ModifierStr(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name="id")
    val id: Long = 0L,

    @ColumnInfo(name="uuid")
    val uuid: String,

    @ColumnInfo(name="idMGroup")
    var idMGroup: Long? = null,
    @ColumnInfo(name="idMGroupOrder")
    var idMGroupOrder: Long? = null,

)


data class GroupWithProductSubGroup(
    @Embedded val group: Group,

    @Relation(
        parentColumn = "id",
        entityColumn = "idGroup",
        entity = Product::class
    )
    val products: List<ProductWithModifierGroupModifier>?,


    @Relation(
        parentColumn = "id",
        entityColumn = "idGroup",
        entity = SubGroup::class
    )
    val sub_groups: List<SubGroupWithProduct>?
)
data class SubGroupWithProductFull(
    @Embedded val sub_groups: SubGroup,

    @Relation(
        parentColumn = "id",
        entityColumn = "idSubGroup",
        entity = Product::class
    )
    val products: List<ProductWithModifierGroupModifier>?,
)
data class SubGroupWithProduct(
    @Embedded val sub_groups: SubGroup,

    @Relation(
        parentColumn = "id",
        entityColumn = "idSubGroup",
        entity = Product::class
    )
    val products: List<Product>?,
)
data class ProductWithModifierGroupModifier(
    @Embedded val product: Product,

    @Relation(
        parentColumn = "id",
        entityColumn = "idProduct",
        entity = GroupModifier::class
    )
    val group_modifier: List<GroupModifierWithModifierStr>?,
)


data class GroupModifierWithModifierStr(
    @Embedded val group_modifier: GroupModifier,

    @Relation(
        parentColumn = "id",
        entityColumn = "idMGroup",
        entity = ModifierStr::class
    )
    val child_modifier: List<ModifierStr>?,

    @Relation(
        parentColumn = "id",
        entityColumn = "idMGroupOrder",
        entity = ModifierStr::class
    )
    val childModifierOrder: List<ModifierStr>?,



)


@Entity(tableName = "selection")
data class Selection(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0L,

    @ColumnInfo(name="uuid")
    val uuid: String,

    @ColumnInfo(name="title")
    val title: String,

    @ColumnInfo(name="emoji_android")
    val emoji_android: String? = null,

    @ColumnInfo(name="emoji_ios")
    val emoji_ios: String? = null,

)
data class SelectionWithProduct(
    @Embedded val selection: Selection,

    @Relation(
        parentColumn = "id",
        entityColumn = "idSelection",
        entity = Product::class
    )
    val products: List<ProductWithModifierGroupModifier>?,
)
@Entity(tableName = "shop")
data class ShopEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0L,

    @ColumnInfo(name="uuid")
    val uuid: String,

    @ColumnInfo(name="pickup")
    val pickup: Boolean,

    @ColumnInfo(name="name")
    val name: String,

    @ColumnInfo(name="phone")
    val phone: String? = null,

    @ColumnInfo(name="date_work")
    val date_work: String? = null,

    @ColumnInfo(name="time_work")
    val time_work: String? = null,

    @ColumnInfo(name="address")
    val address: String,

    @ColumnInfo(name="latitude")
    val latitude: Double? = null,

    @ColumnInfo(name="longitude")
    val longitude: Double? = null,
)

@Entity(tableName = "sub_org")//"name", "domain", "auth_description", "logo", "time_work", "web_link", "vk_link"
data class SubOrgEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0L,
    @ColumnInfo(name="name")
    val name: String,

    @ColumnInfo(name="auth_description_login")
    val auth_description_login: String? = null,

    @ColumnInfo(name="auth_description_passwd")
    val auth_description_passwd: String? = null,

    @ColumnInfo(name="logo")
    val logo: String? = null,

    @ColumnInfo(name="web_link")
    val web_link: String? = null,

    @ColumnInfo(name="vk_link")
    val vk_link: String? = null,
)


