package ru.takeeat.takeeat.db.core.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "type_delivery")
data class TypeDeliveryEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0L,

    @ColumnInfo(name="uuid")
    val uuid: String,

    @ColumnInfo(name="name")
    val name: String,

    @ColumnInfo(name="bonus_pay")
    val bonus_pay: Boolean? = null,

//    val products: List<Product>? = null,
//    val sub_groups: List<SubGroup>? = null,
)