package ru.takeeat.takeeat.db.customer

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import ru.takeeat.takeeat.db.BaseDAO



@Dao
interface CustomerDAO : BaseDAO<Customer> {
    @Query("SELECT * from customer WHERE id = :id")
    fun getCustomer(id: String): LiveData<Customer>

    @Query("SELECT * FROM customer ORDER BY id DESC LIMIT 1")
     fun getCheckCustomer(): LiveData<Customer>

    @Query("SELECT * FROM customer ORDER BY id DESC LIMIT 1")
    suspend fun getCheckCustomerNoLive(): Customer?

    @Query("DELETE FROM customer ")
    suspend fun removeCustomer()

    @Insert
    suspend fun insertAddress(address: Address)

    @Update
    suspend fun updateAddress(address: Address)

    @Query("SELECT * from customer_address WHERE id = :id")
    suspend fun getAddress(id: Long): Address?

    @Query("SELECT * FROM customer_address ORDER BY id DESC")
    fun getAddressAll(): LiveData<List<Address>>
    @Query("SELECT * FROM customer_address ORDER BY id DESC")
    suspend fun getAddresses(): List<Address>?

    @Query("SELECT * FROM customer_address ORDER BY id DESC LIMIT 1")
    suspend fun getCheckAddress(): Address?

    @Query("DELETE FROM customer_address WHERE id = :id")
    suspend fun removeAddress(id: Long)
    @Query("DELETE FROM customer_address")
    suspend fun removeAddressAll()

    @Query("SELECT uuid FROM customer_address WHERE id = :id")
    suspend fun getUuidAddress(id: Long): String?
}