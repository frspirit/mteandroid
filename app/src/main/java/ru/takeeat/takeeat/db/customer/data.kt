package ru.takeeat.takeeat.db.customer

import androidx.room.*
import com.google.android.gms.maps.model.LatLng

@Entity(tableName = "customer")
data class Customer(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0L,
    var uuid: String? = null,
    var name: String? = null,
    var surname: String? = null,
    var email: String? = null,
    var phone: String? = null,
    var balance: Int,
    var sex: Int? = null,
    var consentStatus: Int? = null,
    var birthday: String? = null,
    var platform: String? = null,
    var token_device: String? = null,
    @Embedded var android_device: Android?,

    )
@Entity(tableName = "android_customer")
data class Android(
    var nameAndroid: String? = null,
    var registration_id: String,
)
@Entity(tableName = "customer_address")
data class Address(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0L,
    var uuid: String? = null,
    var name: String? = null,
    var idCustomer: Long? = null,
    var city: String,
    var street: String,
    var home: String,
    var housing: String? = null,
    var apartment: String? = null,
    var entrance: String? = null,
    var floor: String? = null,
    var doorphone: String? = null,
    var lat: Double? = null,
    var lng: Double? = null,
    @Embedded var latLng: LatLng? = null
)