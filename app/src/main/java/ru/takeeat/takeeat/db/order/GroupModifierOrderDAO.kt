package ru.takeeat.takeeat.db.order

import androidx.lifecycle.LiveData
import androidx.room.*
import ru.takeeat.takeeat.db.BaseDAO

@Dao
interface GroupModifierOrderDAO: BaseDAO<GroupModifierOrder> {
    @Query("SELECT * from group_modifier_order WHERE id = :id")
    suspend fun get(id: Long): GroupModifierOrder

    @Query("SELECT * FROM group_modifier_order ORDER BY id DESC")
    fun getAll(): LiveData<List<GroupModifierOrder>>

    @Query("SELECT * from group_modifier_order WHERE idProduct = :idProduct ORDER BY id DESC")
    fun getByProduct(idProduct: Long): LiveData<List<GroupModifierOrder>>

//    WHERE idProduct = :idProduct
    @Transaction
    @Query("SELECT * FROM group_modifier_order  ORDER BY id DESC")
    fun getGroupModifierAll(): LiveData<List<GroupModifierWithModifierStrOrder>>


    @Query("SELECT COUNT(*) FROM group_modifier_order WHERE idProduct = :idProduct")
    suspend fun getCountByProduct(idProduct: Long): Int

    @Transaction
    @Query("SELECT * FROM group_modifier_order WHERE idProduct = :idProduct  ORDER BY id DESC")
    fun getGroupModifiersByProduct(idProduct: Long): LiveData<List<GroupModifierWithModifierStrOrder>>
    @Transaction
    @Query("SELECT * FROM group_modifier_order WHERE idProduct = :idProduct  ORDER BY id DESC")
    suspend fun getNLDGroupModifiersByProduct(idProduct: Long): List<GroupModifierWithModifierStrOrder>
    @Transaction
    @Query("SELECT * FROM group_modifier_order WHERE idProduct = :idProduct  ORDER BY id DESC")
    fun getGroupModifiersByProductDB(idProduct: Long): LiveData<List<GroupModifierWithModifierStrOrder>>

    @Query("SELECT * FROM group_modifier_order ORDER BY id DESC LIMIT 1")
    suspend fun getTonight(): GroupModifierOrder?

    @Query("DELETE FROM group_modifier_order")
    suspend fun removeAll()
//
    @Query("DELETE FROM group_modifier_order WHERE id = :id ")
    suspend fun removeById(id: Long){}

    @Query("DELETE FROM group_modifier_order WHERE idProduct = :id ")
    suspend fun removeAllByProduct(id: Long)



}