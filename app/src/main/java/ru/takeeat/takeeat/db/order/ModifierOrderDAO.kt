package ru.takeeat.takeeat.db.order

import androidx.lifecycle.LiveData
import androidx.room.*
import ru.takeeat.takeeat.db.BaseDAO


@Dao
interface ModifierOrderDAO:BaseDAO<ModifierOrder> {
//    @Transaction
//    @Query("SELECT * FROM group_modifier")
//    fun getGroupModifierWithModifier(): LiveData<List<ProductOrderWithModifierOrder>>

    @Query("SELECT * from modifier_order WHERE id = :id")
    suspend fun get(id: Long): ModifierOrder

    @Query("SELECT * FROM modifier_order ORDER BY id DESC")
    fun getAll(): LiveData<List<ModifierOrder>>

    @Query("SELECT * FROM modifier_order WHERE idProductOrder = :idProduct ORDER BY id DESC")
    fun getProductModifiers(idProduct: Long): LiveData<List<ModifierOrder>>

    @Query("SELECT * FROM modifier_order WHERE idProductOrder = :idProduct ORDER BY id DESC")
    suspend fun getProductModifiersNoLive(idProduct: Long): List<ModifierOrder>

    @Query("SELECT * FROM modifier_order ORDER BY id DESC LIMIT 1")
    suspend fun getTonight(): ModifierOrder?

    @Query("DELETE FROM modifier_order")
    suspend fun removeAll()

    @Query("DELETE FROM modifier_order WHERE idProductOrder = :id ")
    suspend fun removeAllByProduct(id: Long)

//    @Query("DELETE FROM modifier_order WHERE id = :id")
//    suspend fun remove(id: Long)
}