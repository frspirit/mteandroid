package ru.takeeat.takeeat.db.order

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import ru.takeeat.takeeat.db.BaseDAO
@Dao
interface ModifierStrOrderDAO: BaseDAO<ModifierStrOrder> {
    @Query("SELECT * FROM modifier_str_order ORDER BY id DESC")
    fun getAll(): LiveData<List<ModifierStrOrder>>
    @Query("DELETE FROM modifier_str_order")
    suspend fun removeAll()
    @Query("DELETE FROM modifier_str_order WHERE idMGroup = :idMGroup ")
    suspend fun removeAllModifierStrByGroupModifier(idMGroup: Long)
    @Query("select * from modifier_str_order WHERE idMGroup = :idMGroup ORDER BY id DESC")
    suspend fun getByMGroup(idMGroup: Long): List<ModifierStrOrder>
}