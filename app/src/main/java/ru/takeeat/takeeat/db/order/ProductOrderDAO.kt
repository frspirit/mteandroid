package ru.takeeat.takeeat.db.order

import androidx.lifecycle.LiveData
import androidx.room.*
import ru.takeeat.takeeat.db.BaseDAO

@Dao
interface ProductOrderDAO: BaseDAO<ProductOrder> {

    @Query("SELECT * from product_order WHERE id = :id")
    suspend fun get(id: Long): ProductOrder
    @Query("SELECT * from product_order WHERE uuid = :uuid")
    suspend fun getByUuid(uuid: String): ProductOrder?

    @Query("SELECT * from product_order WHERE uuid = :uuid ORDER BY id DESC LIMIT 1")
    suspend fun getDesc(uuid: String): ProductOrder?

    @Query("SELECT * FROM product_order ORDER BY id DESC")
    fun getAll(): LiveData<List<ProductOrder>>

    @Query("SELECT * FROM product_order ORDER BY id DESC")
    fun getAllNoLive(): List<ProductOrder>

    @Query("SELECT * FROM product_order ORDER BY id DESC LIMIT 1")
    suspend fun getTonight(): ProductOrder?


    @Query("DELETE FROM product_order")
    suspend fun removeAll()

//    @Query("DELETE FROM product_order WHERE id = :id")
//    suspend fun remove(id: Long)



}