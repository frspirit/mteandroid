package ru.takeeat.takeeat.db.order

import androidx.room.*
import ru.takeeat.takeeat.db.core.GroupModifier
import ru.takeeat.takeeat.db.core.ModifierStr


@Entity(tableName = "product_order")
data class ProductOrder(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0L,
    @ColumnInfo(name="uuid")
    val uuid: String,
    @ColumnInfo(name="name")
    var name: String,
    @ColumnInfo(name="amount")
    var amount: Int,
    @ColumnInfo(name="image")
    var image: String,
    @ColumnInfo(name="price")
    var price: Float,
    @ColumnInfo(name="priceProduct")
    var priceProduct: Float,
    @ColumnInfo(name="p_price")
    val p_price: Float? = null,
    @ColumnInfo(name="weight")
    val weight: Int,

//    @Embedded val modifiers: List<ModifierOrder>?

)

@Entity(tableName = "modifier_order")
data class ModifierOrder(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Long = 0L,
    @ColumnInfo(name="idProductOrder")
    val idProductOrder: Long? = null,
    @ColumnInfo(name="uuid")
    val uuid: String,
    @ColumnInfo(name="name")
    val name: String,
    @ColumnInfo(name="price")
    var price: Float,
    @ColumnInfo(name="amount")
    var amount: Int,
    @ColumnInfo(name="group_name")
    val group_name: String,
    @ColumnInfo(name="group_uuid")
    val group_uuid: String,
)



@Entity(tableName = "group_modifier_order")
data class GroupModifierOrder(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name="id")
    val id: Long = 0L,

    @ColumnInfo(name="uuid")
    val uuid: String,

    @ColumnInfo(name="idProduct")
    val idProduct: Long,

    @ColumnInfo(name="name")
    val name: String,

    @ColumnInfo(name="maxAmount")
    val maxAmount: Float,

    @ColumnInfo(name="minAmount")
    val minAmount: Float,

    @ColumnInfo(name="required")
    val required: Boolean,
//    val child_modifier: List<String>
)
@Entity(tableName = "modifier_str_order")
data class ModifierStrOrder(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name="id")
    val id: Long = 0L,

    @ColumnInfo(name="uuid")
    val uuid: String,

    @ColumnInfo(name="idMGroup")
    var idMGroup: Long?,

    )
data class GroupModifierWithModifierStrOrder(
    @Embedded val group_modifier: GroupModifierOrder,

    @Relation(
        parentColumn = "id",
        entityColumn = "idMGroup",
        entity = ModifierStrOrder::class
    )
    val child_modifier: List<ModifierStrOrder>?,
)

@Entity(tableName = "order")
data class Order(
    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: Int,
    @ColumnInfo(name = "number")
    val number: Int?,
    @ColumnInfo(name = "total_amount")
    val total_amount: Float,
    @ColumnInfo(name = "delivery_amount")
    val delivery_amount: Float,
    @ColumnInfo(name = "balls")
    val balls: Float,
    @ColumnInfo(name = "person_count")
    val person_count: Int,
    @ColumnInfo(name = "comment")
    val comment: String,
    @ColumnInfo(name = "shop")
    val shop: String,
    @ColumnInfo(name = "type_pay")
    val type_pay: String,
    @ColumnInfo(name = "type_delivery")
    val type_delivery: String,
    @ColumnInfo(name = "platform")
    val platform: Int = 1,
    @ColumnInfo(name = "address")
    val address: String,
    @ColumnInfo(name = "customer")
    val customer: String,
//    @Embedded val products: List<ProductOrder>?
)