package ru.takeeat.takeeat.network.mtesapi.core

import androidx.lifecycle.MutableLiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.takeeat.takeeat.network.mtesapi.interfaces.Result
import ru.takeeat.takeeat.ui.basket.interfaces.CheckoutAddressClickListener

class CoreAPI : CoreApp() {
    fun callSubOrgInfo(data: Result<SubOrg>) {
        // Your Api Call with response callback
        return coreService.getSubOrgInfo().enqueue(object : Callback<SubOrg> {

            override fun onFailure(call: Call<SubOrg>, t: Throwable) {
//                TODO("Not yet implemented")
//                liveData.value = null
                return
            }

            override fun onResponse(
                call: Call<SubOrg>,
                response: Response<SubOrg>
            ) {
                if (response.code() == 200 || response.code() == 201) {
                    if (response.code() == 200 || response.code() == 201) {
                        if (response.body() != null) {
                            data.result(response.body()!!)
                        }
                    }
                } else {
                    data.statusCode(response.code())
                }


            }
        })
    }


    fun callNews(liveData: MutableLiveData<List<New>>) {
        // Your Api Call with response callback
        return coreService.getNews().enqueue(object : Callback<List<New>> {

            override fun onFailure(call: Call<List<New>>, t: Throwable) {
//                TODO("Not yet implemented")
                liveData.value = null
            }

            override fun onResponse(call: Call<List<New>>, response: Response<List<New>>) {
                if (response.code() == 200 || response.code() == 201) {
                    liveData.value = response.body()
                } else if (response.code() == 401) {
                    liveData.value = null
                } else {
                    liveData.value = null
                }


            }
        })

    }

    fun callSelections(liveData: MutableLiveData<List<Selection>>) {
        // Your Api Call with response callback
        return coreService.getSelections().enqueue(object : Callback<List<Selection>> {

            override fun onFailure(call: Call<List<Selection>>, t: Throwable) {
//                TODO("Not yet implemented")
                liveData.value = null
            }

            override fun onResponse(
                call: Call<List<Selection>>,
                response: Response<List<Selection>>
            ) {
                if (response.code() == 200 || response.code() == 201) {
                    liveData.value = response.body()
                } else if (response.code() == 401) {
                    liveData.value = null
                } else {
                    liveData.value = null
                }


            }
        })

    }

    fun callGroups(liveData: MutableLiveData<List<Group>>) {
        // Your Api Call with response callback
        return coreService.getProducts().enqueue(object : Callback<List<Group>> {

            override fun onFailure(call: Call<List<Group>>, t: Throwable) {
//                TODO("Not yet implemented")
                liveData.value = null
            }

            override fun onResponse(call: Call<List<Group>>, response: Response<List<Group>>) {
                if (response.code() == 200 || response.code() == 201) {
                    liveData.value = response.body()
                } else if (response.code() == 401) {
                    liveData.value = null
                } else {
                    liveData.value = null
                }


            }
        })

    }

    fun callModifiers(liveData: MutableLiveData<List<Modifier>>) {
        // Your Api Call with response callback
        return coreService.getModifiers().enqueue(object : Callback<List<Modifier>> {

            override fun onFailure(call: Call<List<Modifier>>, t: Throwable) {
//                TODO("Not yet implemented")
                liveData.value = null
            }

            override fun onResponse(
                call: Call<List<Modifier>>,
                response: Response<List<Modifier>>
            ) {
                if (response.code() == 200 || response.code() == 201) {
                    liveData.value = response.body()
                } else if (response.code() == 401) {
                    liveData.value = null
                } else {
                    liveData.value = null
                }


            }
        })

    }

    fun callTypeDelivery(liveData: MutableLiveData<List<TypeDelivery>>) {
        // Your Api Call with response callback
        return coreService.getTypeDelivery().enqueue(object : Callback<List<TypeDelivery>> {

            override fun onFailure(call: Call<List<TypeDelivery>>, t: Throwable) {
//                TODO("Not yet implemented")
                liveData.value = null
            }

            override fun onResponse(
                call: Call<List<TypeDelivery>>,
                response: Response<List<TypeDelivery>>
            ) {
                if (response.code() == 200 || response.code() == 201) {
                    liveData.value = response.body()
                } else if (response.code() == 401) {
                    liveData.value = null
                } else {
                    liveData.value = null
                }


            }
        })

    }

    fun callTypePay(liveData: MutableLiveData<List<TypePay>>) {
        // Your Api Call with response callback
        return coreService.getTypePay().enqueue(object : Callback<List<TypePay>> {

            override fun onFailure(call: Call<List<TypePay>>, t: Throwable) {
//                TODO("Not yet implemented")
                liveData.value = null
            }

            override fun onResponse(
                call: Call<List<TypePay>>,
                response: Response<List<TypePay>>
            ) {
                if (response.code() == 200 || response.code() == 201) {
                    liveData.value = response.body()
                } else {
                    liveData.value = null
                }


            }
        })

    }

    fun callShops(liveData: MutableLiveData<List<Shop>>?, success: Result<Int>) {
        // Your Api Call with response callback
        return coreService.getShops().enqueue(object : Callback<List<Shop>> {

            override fun onFailure(call: Call<List<Shop>>, t: Throwable) {
                liveData?.value = null
                success.result(ERR_GET_SHOPS)
            }

            override fun onResponse(
                call: Call<List<Shop>>,
                response: Response<List<Shop>>
            ) {
                if (response.code() == 200) {
                    if (response.body().isNullOrEmpty()) {
                        success.result(EMPTY_SHOPS)
                    } else {
                        success.result(SUCCESS_GET_SHOPS)
                    }

                    liveData?.value = response.body()

                } else {
                    liveData?.value = null
                    success.result(ERR_GET_SHOPS)
                }


            }
        })

    }
}