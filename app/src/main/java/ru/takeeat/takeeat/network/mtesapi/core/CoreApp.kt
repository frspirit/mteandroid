package ru.takeeat.takeeat.network.mtesapi.core

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.*
//import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

open class CoreApp {
    lateinit var coreService : CoreService
    init {
        configureCoreService()
    }


    private fun configureCoreService(){
        val httpLogingInterceptor = HttpLoggingInterceptor()
        httpLogingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(httpLogingInterceptor)
//            .connectTimeout(10, TimeUnit.SECONDS)
//            .readTimeout(15, TimeUnit.SECONDS)
            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl("https://api.takeeat.ru/mapi/")
//            .baseUrl("https://api.takeeat.ru/mapi/")
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
//            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
        coreService = retrofit.create(CoreService::class.java)
    }

}