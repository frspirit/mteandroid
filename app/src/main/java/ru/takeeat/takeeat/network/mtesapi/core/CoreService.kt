package ru.takeeat.takeeat.network.mtesapi.core


import retrofit2.Call
import retrofit2.http.*


interface CoreService {
    @GET("core/{org}/delivery_types")
    fun getTypeDelivery(@Path("org") org: String = "697d74fe-7491-42d7-8709-a48c146023f8"): Call<List<TypeDelivery>>

    @GET("core/{org}/payment_types/")
    fun getTypePay(@Path("org") org: String = "697d74fe-7491-42d7-8709-a48c146023f8"): Call<List<TypePay>>


    @GET("core/{org}/news")
    fun getNews(@Path("org") org: String = "697d74fe-7491-42d7-8709-a48c146023f8"): Call<List<New>>

    @GET("core/{org}/selections/")
    fun getSelections(@Path("org") org: String = "697d74fe-7491-42d7-8709-a48c146023f8"): Call<List<Selection>>

    @Headers("Accept: application/json; version=1")
    @GET("core/{org}/products/")
    fun getProducts(@Path("org") org: String = "697d74fe-7491-42d7-8709-a48c146023f8"): Call<List<Group>>

    @GET("core/{org}/modifiers/")
    fun getModifiers(@Path("org") org: String = "697d74fe-7491-42d7-8709-a48c146023f8"): Call<List<Modifier>>

    @GET("core/{org}/shops/")
    fun getShops(@Path("org") org: String = "697d74fe-7491-42d7-8709-a48c146023f8"): Call<List<Shop>>

    @GET("core/{org}/")
    fun getSubOrgInfo(@Path("org") org: String = "697d74fe-7491-42d7-8709-a48c146023f8"): Call<SubOrg>

//    @GET("core/{org}/timework/")
//    fun getSubOrgTimeWork(@Path("org") org: String = "697d74fe-7491-42d7-8709-a48c146023f8"): Call<T>

}





