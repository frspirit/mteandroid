package ru.takeeat.takeeat.network.mtesapi.core

import android.os.Parcelable
import com.google.android.gms.maps.model.LatLng
import kotlinx.parcelize.Parcelize

//import com.google.gson.annotations.SerializedName     @SerializedName("")

@Parcelize
data class TypeDelivery(
    var uuid: String,
    val name: String,
    val pickup: Boolean? = null
) : Parcelable

@Parcelize
data class TypePay(
    var uuid: String,
    val name: String,
    val bonus_pay: Boolean,
) : Parcelable

@Parcelize
data class New(
    val uuid: String,
    val title: String,
    val text: String? = null,
    val image: String,
    val priority: Int? = null,
) : Parcelable

@Parcelize
data class Selection(
    val uuid: String,
    val title: String,
    val emoji_android: String,
    val emoji_ios: String,
    val product: List<Product>,
) : Parcelable
@Parcelize
data class Modifier(
    val uuid: String,
    val name: String,
    val price: Number,
//    val carbohydrateAmount: Number,
    val carbohydrateFullAmount: Number,
//    val energyAmount: Number,
    val energyFullAmount: Number,
//    val fatAmount: Number,
    val fatFullAmount: Number,
//    val fiberAmount: Number,
    val fiberFullAmount: Number,
    val image: String,
    val image_priority: String,
) : Parcelable

@Parcelize
data class Product(
    val uuid: String,
    val name: String,
    val hidden: Boolean,
    val description: String,
    val price: Float,
    val p_price: Float? = null,
    val weight: Float,
//    val carbohydrateAmount: Float,
    val carbohydrateFullAmount: Float,
//    val energyAmount: Float,
    val energyFullAmount: Float,
//    val fatAmount: Float,
    val fatFullAmount: Float,
//    val fiberAmount: Float,
    val fiberFullAmount: Float,
    val image: String? = null,
    val image_priority: String? = null,
    val modifier: List<String>,
    val group_modifier: List<GroupModifier>
) : Parcelable

@Parcelize
data class GroupModifier(
    val uuid: String,
    val name: String,
    val maxAmount: Float,
    val minAmount: Float,
    val required: Boolean,
    val child_modifier: List<String>
) : Parcelable

@Parcelize
data class Group(
    val uuid: String,
    val name: String,
    val image: String? = null,
    val image_priority: String? = null,
    val products: List<Product>? = null,
    val sub_groups: List<SubGroup>? = null,
) : Parcelable

@Parcelize
data class SubGroup(
    val uuid: String,
    val name: String,
    val image: String? = null,
    val image_priority: String? = null,
    val products: List<Product>? = null,
) : Parcelable
@Parcelize
data class ListModifier(
    val modifier: List<Modifier>
) : Parcelable


@Parcelize
data class Shop(
    val uuid: String,
    val pickup: Boolean,
    val name: String,
    val phone: String? = null,
    val date_work: String? = null,
    val time_work: String? = null,
    val address: String,
    val latitude: Double? = null,
    val longitude: Double? = null,
    val latLng: LatLng? = null


) : Parcelable
@Parcelize //"name", "domain", "auth_description", "logo", "time_work", "web_link", "vk_link"
data class SubOrg(
//    val domain: T,
    val name: String,
    val auth_description_login: String? = null,
    val auth_description_passwd: String? = null,
    val logo: String? = null,
//    val time_work: String? = null,
    val web_link: String? = null,
    val vk_link: String? = null,
) : Parcelable

@Parcelize
data class ListShop(
    var shops: List<Shop>
): Parcelable


const val ERR_GET_SHOPS: Int = 0
const val SUCCESS_GET_SHOPS: Int = 1
const val EMPTY_SHOPS: Int = 2


