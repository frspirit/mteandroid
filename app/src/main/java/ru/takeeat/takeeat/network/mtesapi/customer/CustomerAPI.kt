package ru.takeeat.takeeat.network.mtesapi.customer

import android.util.Log
import androidx.lifecycle.MutableLiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.takeeat.takeeat.network.mtesapi.customer.data.Address
import ru.takeeat.takeeat.network.mtesapi.customer.data.Balance
import ru.takeeat.takeeat.network.mtesapi.customer.data.Customer
import ru.takeeat.takeeat.network.mtesapi.customer.data.OutPinCode
import ru.takeeat.takeeat.ui.basket.interfaces.OrderListener
import ru.takeeat.takeeat.ui.profile.interfaces.OutPinCodeListener
import ru.takeeat.takeeat.ui.profile.interfaces.OutStatusGetCodeListener

class CustomerAPI : CustomerApp() {
    private val orgId: String = "697d74fe-7491-42d7-8709-a48c146023f8"
    fun callGetCustomer(userId: String, liveData: MutableLiveData<Customer>, errorStatus: MutableLiveData<Int>?) {
        // Your Api Call with response callback
        return customerService.getCustomer(orgId, userId).enqueue(object : Callback<Customer> {

            override fun onFailure(call: Call<Customer>, t: Throwable) {
                liveData.value = null


            }

            override fun onResponse(call: Call<Customer>, response: Response<Customer>) {

                if (response.code() == 200 || response.code() == 201){
                    liveData.value = response.body()
                }else if (response.code() == 401){
                    liveData.value = null
                }else{
                    liveData.value = null
                }

                if(errorStatus != null){
                    errorStatus.value = response.code()
                }


            }
        })

    }

    fun callAttachCustomer(customerCreateInfo: Customer, liveData: MutableLiveData<Customer>,errorStatus: MutableLiveData<Int>?) {
        // Your Api Call with response callback
        return customerService.attach(orgId, customerCreateInfo)
            .enqueue(object : Callback<Customer> {

                override fun onFailure(call: Call<Customer>, t: Throwable) {
                    liveData.value = null
                }

                override fun onResponse(call: Call<Customer>, response: Response<Customer>) {
                    if (response.code() == 200 || response.code() == 201){
                        liveData.value = response.body()
                    }else if (response.code() == 401){
                        liveData.value = null
                    }else{
                        liveData.value = null
                    }

                    if(errorStatus != null){
                        errorStatus.value = response.code()
                    }


                }
            })

    }

    fun callUpdateCustomer(
        customerUpdateInfo: Customer,
        customerUuid: String,
        liveData: MutableLiveData<Customer>,
    ) {
        // Your Api Call with response callback
        return customerService.updateCustomer(orgId, customerUuid, customerUpdateInfo)
            .enqueue(object : Callback<Customer> {

                override fun onFailure(call: Call<Customer>, t: Throwable) {
                    liveData.value = null
                }

                override fun onResponse(call: Call<Customer>, response: Response<Customer>) {
                    if (response.code() == 200 || response.code() == 201){
                        liveData.value = response.body()
                    }else if (response.code() == 401){
                        liveData.value = null
                    }else{
                        liveData.value = null
                    }



                }
            })

    }

    fun callGetPinCode(
        customerUuid: String,
        customerPhone: String,
//        data: OutPinCodeListener
//        liveData: MutableLiveData<OutPinCode>,
        outStatus: OutStatusGetCodeListener<Boolean>
    ) {
        // Your Api Call with response callback
        return customerService.getPinCode(orgId, customerUuid, customerPhone)
            .enqueue(object : Callback<OutPinCode> {

                override fun onFailure(call: Call<OutPinCode>, t: Throwable) {
//                    data.onResponse(null)
//                    Log.d("AUTHASF-getPinCode-F", "${call.request().body}")
                    outStatus.onSend(success =false, outCode = null)
//                    liveData.value = null
                }

                override fun onResponse(call: Call<OutPinCode>, response: Response<OutPinCode>) {

                    if (response.code() == 200 || response.code() == 201){
//                        Log.d("AUTHASF-getPinCode-R", "${response.code()} - ${response.body()} - ${response.errorBody()}")
                        outStatus.onSend(success =true, outCode = response.body())
                    }else{
                        outStatus.onSend(success =false, outCode = null)
//                        liveData.value = response.body()
                    }


                }
            })

    }

    fun callCheckPinCode(
        customerUuid: String,
        pinCode: String,
        liveData: MutableLiveData<OutPinCode>,
    ) {
        // Your Api Call with response callback
        return customerService.checkPinCode(orgId, customerUuid, pinCode)
            .enqueue(object : Callback<OutPinCode> {

                override fun onFailure(call: Call<OutPinCode>, t: Throwable) {
                    liveData.value = null
                }

                override fun onResponse(call: Call<OutPinCode>, response: Response<OutPinCode>) {
                    if (response.code() == 500 || response.code() == 404){
                        liveData.value = null
                    }else{
                        liveData.value = response.body()
                    }


                }
            })

    }

    fun callAttachAddress(
        customerUuid: String,
        address: ru.takeeat.takeeat.db.customer.Address,
        liveData: MutableLiveData<Address>,
    ) {
        // Your Api Call with response callback
        return customerService.attachAddress(orgId, customerUuid, address)
            .enqueue(object : Callback<Address> {

                override fun onFailure(call: Call<Address>, t: Throwable) {
                    liveData.value = null
                }

                override fun onResponse(call: Call<Address>, response: Response<Address>) {
                    if (response.code() == 200 || response.code() == 201){
                        liveData.value = response.body()
                    }else if (response.code() == 401){
                        liveData.value = null
                    }else{
                        liveData.value = null
                    }


                }
            })

    }

    fun callGetAddresses(
        customerUuid: String,
        liveData: MutableLiveData<List<ru.takeeat.takeeat.db.customer.Address>>
    ) {
        // Your Api Call with response callback
        return customerService.getAddresses(orgId, customerUuid)
            .enqueue(object : Callback<List<ru.takeeat.takeeat.db.customer.Address>> {

                override fun onFailure(
                    call: Call<List<ru.takeeat.takeeat.db.customer.Address>>,
                    t: Throwable
                ) {
                    liveData.value = null
                }


                override fun onResponse(
                    call: Call<List<ru.takeeat.takeeat.db.customer.Address>>,
                    response: Response<List<ru.takeeat.takeeat.db.customer.Address>>
                ) {
                    if (response.code() == 200 || response.code() == 201){
                        liveData.value = response.body()
                    }else if (response.code() == 401){
                        liveData.value = null
                    }else{
                        liveData.value = null
                    }


                }
            })
    }

    fun callRemoveAddress(addressUuid: String) {
        // Your Api Call with response callback
        return customerService.removeAddress(orgId, addressUuid)
            .enqueue(object : Callback<Void> {

                override fun onFailure(call: Call<Void>, t: Throwable) {
                }


                override fun onResponse(
                    call: Call<Void>,
                    response: Response<Void>
                ) {


                }
            })
    }

    fun callUpdateAddress(
        address: ru.takeeat.takeeat.db.customer.Address,
//        liveData: MutableLiveData<Address>,
    ) {
        // Your Api Call with response callback
        return customerService.updateAddress(orgId, address.uuid!!, address)
            .enqueue(object : Callback<Address> {

                override fun onFailure(call: Call<Address>, t: Throwable) {
//                    liveData.value = null
                }

                override fun onResponse(call: Call<Address>, response: Response<Address>) {
//                    liveData.value = response.body()

                }
            })

    }

    fun callGetBalance(userId: String, liveData: MutableLiveData<Balance>) {
        // Your Api Call with response callback
        return customerService.balance(orgId, userId).enqueue(object : Callback<Balance> {


            override fun onResponse(call: Call<Balance>, response: Response<Balance>) {

                if (response.code() == 200 || response.code() == 201){
                    liveData.value = response.body()
                }else{
                    liveData.value = null
                }


            }

            override fun onFailure(call: Call<Balance>, t: Throwable) {
                TODO("Not yet implemented")
            }
        })

    }
}