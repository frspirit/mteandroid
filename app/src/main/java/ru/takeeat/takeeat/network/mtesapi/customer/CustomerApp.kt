package ru.takeeat.takeeat.network.mtesapi.customer

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

open class CustomerApp {
    lateinit var customerService : CustomerService
    init {
        configureCoreService()
    }


    private fun configureCoreService(){
        val httpLogingInterceptor = HttpLoggingInterceptor()
        httpLogingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(httpLogingInterceptor)
//            .readTimeout(30, TimeUnit.SECONDS)
//            .connectTimeout(10, TimeUnit.SECONDS)
            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl("https://api.takeeat.ru/mapi/")
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
//            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
        customerService = retrofit.create(CustomerService::class.java)
    }

}