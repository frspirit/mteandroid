package ru.takeeat.takeeat.network.mtesapi.customer

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Headers
import ru.takeeat.takeeat.network.mtesapi.customer.data.Address
import ru.takeeat.takeeat.network.mtesapi.customer.data.Balance
import ru.takeeat.takeeat.network.mtesapi.customer.data.Customer
import ru.takeeat.takeeat.network.mtesapi.customer.data.OutPinCode


interface CustomerService {

    @GET("customer/{orgId}/customer/{customerUuid}/")
    fun getCustomer(
        @Path("orgId") orgId: String,
        @Path("customerUuid") customerUuid: String
    ): Call<Customer>

    @POST("customer/{orgId}/customer/{customerUuid}/")
    fun updateCustomer(
        @Path("orgId") orgId: String,
        @Path("customerUuid") customerUuid: String,
        @Body customerUpdateInfo: Customer
    ): Call<Customer>

    @POST("customer/{orgId}/attach/")
    fun attach(@Path("orgId") orgId: String, @Body customerCreateInfo: Customer): Call<Customer>


    @GET("customer/{orgId}/auth/check_pincode/{customerUuid}/{pincode}")
    fun checkPinCode(
        @Path("orgId") orgId: String,
        @Path("customerUuid") customerUuid: String,
        @Path("pincode") pincode: String
    ): Call<OutPinCode>

    @GET("customer/{orgId}/auth/pincode/{customerUuid}/{customerPhone}")
    fun getPinCode(
        @Path("orgId") orgId: String,
        @Path("customerUuid") customerUuid: String,
        @Path("customerPhone") customerPhone: String
    ): Call<OutPinCode>
    @Headers("Accept: application/json; version=1")
    @POST("customer/{orgId}/addresses/{customerUuid}/")
    fun attachAddress(
        @Path("orgId") orgId: String,
        @Path("customerUuid") customerUuid: String,
        @Body address: ru.takeeat.takeeat.db.customer.Address
    ): Call<Address>
    @Headers("Accept: application/json; version=1")
    @GET("customer/{orgId}/addresses/{customerUuid}/")
    fun getAddresses(
        @Path("orgId") orgId: String,
        @Path("customerUuid") customerUuid: String,

        ): Call<List<ru.takeeat.takeeat.db.customer.Address>>

    @GET("customer/{orgId}/remove_address/{addressUuid}/")
    fun removeAddress(
        @Path("orgId") orgId: String,
        @Path("addressUuid") addressUuid: String,

        ): Call<Void>


    @Headers("Accept: application/json; version=1")
    @POST("customer/{orgId}/update_address/{addressUuid}/")
    fun updateAddress(
        @Path("orgId") orgId: String,
        @Path("addressUuid") addressUuid: String,
        @Body address: ru.takeeat.takeeat.db.customer.Address
    ): Call<Address>

    @Headers("Accept: application/json; version=1")
    @POST("customer/{orgId}/balance/{customerUuid}")
    fun balance(
        @Path("orgId") orgId: String,
        @Path("customerUuid") customerUuid: String,
    ): Call<Balance>
}
