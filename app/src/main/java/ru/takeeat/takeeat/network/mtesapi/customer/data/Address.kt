package ru.takeeat.takeeat.network.mtesapi.customer.data

data class Address(
    val uuid: String? = null,
    val name: String? = null,
    val lat: Double? = null,
    val lng: Double? = null,
    val city: String,
    val street: String,
    val home: String,
    val housing: String? = null,
    val apartment: String? = null,
    val entrance: String? = null,
    val floor: String? = null,
    val doorphone: String? = null,
    val customer: String? = null,
)
