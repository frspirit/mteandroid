package ru.takeeat.takeeat.network.mtesapi.customer.data


data class Android(
    val name: String? = null,
    val registration_id: String,
)

