package ru.takeeat.takeeat.network.mtesapi.customer.data

data class Balance(
    val balance: Double
)
