package ru.takeeat.takeeat.network.mtesapi.customer.data

data class Customer(
    val uuid: String? = null,
    var name: String? = null,
    val surname: String? = null,
    var email: String? = null,
    var phone: String? = null,
    val balance: Int = 0,
    val sex: Int? = null,
    val consentStatus: Int? = null,
    val birthday: String? = null,
    val platform: Int = 1,
    val token_device: String? = null,
//    val addresses: List<Address>,
    val android_device: Android,
)