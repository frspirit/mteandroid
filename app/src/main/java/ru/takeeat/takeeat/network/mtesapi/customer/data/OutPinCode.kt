package ru.takeeat.takeeat.network.mtesapi.customer.data

data class OutPinCode(
    val succeed: Boolean,
    val again: Boolean,
    val customer: String? = null,
    val iikoStatus: String? = null,
    val send: Boolean? = null,
    val message: String? = null
)
