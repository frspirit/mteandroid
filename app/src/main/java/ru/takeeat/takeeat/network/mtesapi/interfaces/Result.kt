package ru.takeeat.takeeat.network.mtesapi.interfaces

interface Result<T> {

    fun result(data: T){}

    fun statusCode(code: Int){}
}