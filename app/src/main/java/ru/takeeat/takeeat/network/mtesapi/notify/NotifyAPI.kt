package ru.takeeat.takeeat.network.mtesapi.order

import android.util.Log
import androidx.lifecycle.MutableLiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.takeeat.takeeat.network.mtesapi.core.ErrorCheckOrder
import ru.takeeat.takeeat.network.mtesapi.core.Order
import ru.takeeat.takeeat.network.mtesapi.order.data.AlertError

class NotifyAPI : NotifyApp() {
    private val orgId: String = "697d74fe-7491-42d7-8709-a48c146023f8"


    fun callSendAlertError(customerUuid: String,alertError: AlertError, liveData: MutableLiveData<AlertError>) {
        // Your Api Call with response callback
        return notifyService.sendAlertError(orgId,customerUuid, alertError)
            .enqueue(object : Callback<AlertError> {

                override fun onFailure(call: Call<AlertError>, t: Throwable) {
                    liveData.value = null

                }

                override fun onResponse(call: Call<AlertError>, response: Response<AlertError>) {
                    if (response.code() == 200 || response.code() == 201){
                        liveData.value = response.body()
                    }else if (response.code() == 401){
                        liveData.value = null
                    }else{
                        liveData.value = null
                    }
                }
            })

    }

    fun callAllAlertError(
        customerUuid: String,
        liveData: MutableLiveData<AlertError>,
    ) {
        // Your Api Call with response callback
        return notifyService.getAllAlertError(orgId,customerUuid)
            .enqueue(object : Callback<AlertError> {

                override fun onFailure(call: Call<AlertError>, t: Throwable) {
                    liveData.value = null
                }

                override fun onResponse(call: Call<AlertError>, response: Response<AlertError>) {
                    if (response.code() == 200 || response.code() == 201){
                        liveData.value = response.body()
                    }else if (response.code() == 401){
                        liveData.value = null
                    }else{
                        liveData.value = null
                    }
                }
            })

    }


}