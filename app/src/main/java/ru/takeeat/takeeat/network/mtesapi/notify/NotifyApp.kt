package ru.takeeat.takeeat.network.mtesapi.order

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

open class NotifyApp {
    lateinit var notifyService : NotifyService
    init {
        configureCoreService()
    }


    private fun configureCoreService(){
        val httpLogingInterceptor = HttpLoggingInterceptor()
        httpLogingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(httpLogingInterceptor)
            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl("https://api.takeeat.ru/mapi/")
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
//            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
        notifyService = retrofit.create(NotifyService::class.java)
    }

}