package ru.takeeat.takeeat.network.mtesapi.order

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import ru.takeeat.takeeat.network.mtesapi.core.ErrorCheckOrder
import ru.takeeat.takeeat.network.mtesapi.core.Order
import ru.takeeat.takeeat.network.mtesapi.customer.data.Address
import ru.takeeat.takeeat.network.mtesapi.customer.data.Customer
import ru.takeeat.takeeat.network.mtesapi.customer.data.OutPinCode
import ru.takeeat.takeeat.network.mtesapi.order.data.AlertError


interface NotifyService {

    @GET("notify/{orgId}/error_alert/{customerUuid}/")
    fun getAllAlertError(
        @Path("orgId") orgId: String,
        @Path("customerUuid") customerUuid: String
    ): Call<AlertError>

    @POST("notify/{orgId}/error_alert/{customerUuid}/")
    fun sendAlertError(
        @Path("orgId") orgId: String,
        @Path("customerUuid") customerUuid: String,
        @Body alertError: AlertError
    ): Call<AlertError>

}








































