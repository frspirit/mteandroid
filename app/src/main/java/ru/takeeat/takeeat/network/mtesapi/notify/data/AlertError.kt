package ru.takeeat.takeeat.network.mtesapi.order.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize


@Parcelize
data class AlertError(
val uuid: String?=null,
val name: String? = null,
val text: String,
val status: String? = null,
var customer: String? = null,
) :  Parcelable
