package ru.takeeat.takeeat.network.mtesapi.order

import android.util.Log
import androidx.lifecycle.MutableLiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.takeeat.takeeat.network.mtesapi.core.ErrorCheckOrder
import ru.takeeat.takeeat.network.mtesapi.core.Order
import ru.takeeat.takeeat.network.mtesapi.core.OrderCreated
import ru.takeeat.takeeat.network.mtesapi.order.data.OrderHistory
import ru.takeeat.takeeat.ui.basket.interfaces.OrderListener

class OrderAPI : OrderApp() {
    private val orgId: String = "697d74fe-7491-42d7-8709-a48c146023f8"
    fun callGetOrderHistory(userId: String, liveData: MutableLiveData<List<OrderHistory>>) {
        // Your Api Call with response callback
        return orderService.getOrderHistory(orgId, userId).enqueue(object : Callback<List<OrderHistory>> {

            override fun onFailure(call: Call<List<OrderHistory>>, t: Throwable) {
                liveData.value = null
//                TODO("Not yet implemented")
            }

            override fun onResponse(call: Call<List<OrderHistory>>, response: Response<List<OrderHistory>>) {
                if (response.code() == 200 || response.code() == 201){
                    liveData.value = response.body()
                }else if (response.code() == 401){
                    liveData.value = null
                }else{
                    liveData.value = null
                }


            }
        })

    }

    fun callAttachOrder(orderInfo: Order, liveData: MutableLiveData<OrderCreated>, outStatus: OrderListener<Boolean>) {
        // Your Api Call with response callback
        return orderService.attach(orgId, orderInfo)
            .enqueue(object : Callback<OrderCreated> {

                override fun onFailure(call: Call<OrderCreated>, t: Throwable) {
                    outStatus.onSend(created = false, orderInfo = null)

                }

                override fun onResponse(call: Call<OrderCreated>, response: Response<OrderCreated>) {
                    if (response.code() == 200 || response.code() == 201){
                        liveData.value = response.body()
                        outStatus.onSend(created = true, orderInfo = response.body())
                    }
                    else{
                        liveData.value = null
                        outStatus.onSend(created = false, orderInfo = null)
                    }


                }
            })

    }

    fun callCheckOrder(
        orderInfo: Order,
        liveData: MutableLiveData<ErrorCheckOrder>,
    ) {
        // Your Api Call with response callback
        return orderService.checkOrder(orgId, orderInfo)
            .enqueue(object : Callback<ErrorCheckOrder> {

                override fun onFailure(call: Call<ErrorCheckOrder>, t: Throwable) {
                    liveData.value = null
                }

                override fun onResponse(call: Call<ErrorCheckOrder>, response: Response<ErrorCheckOrder>) {
                    if (response.code() == 200 || response.code() == 201){
                        liveData.value = response.body()
                    }else if (response.code() == 401){
                        liveData.value = null
                    }else{
                        liveData.value = null
                    }
                }
            })

    }


}