package ru.takeeat.takeeat.network.mtesapi.order

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import ru.takeeat.takeeat.network.mtesapi.core.ErrorCheckOrder
import ru.takeeat.takeeat.network.mtesapi.core.Order
import ru.takeeat.takeeat.network.mtesapi.core.OrderCreated
import ru.takeeat.takeeat.network.mtesapi.customer.data.Address
import ru.takeeat.takeeat.network.mtesapi.customer.data.Customer
import ru.takeeat.takeeat.network.mtesapi.customer.data.OutPinCode
import ru.takeeat.takeeat.network.mtesapi.order.data.OrderHistory


interface OrderService {

    @GET("order/{orgId}/history/{customerUuid}")
    fun getOrderHistory(
        @Path("orgId") orgId: String,
        @Path("customerUuid") customerUuid: String
    ): Call<List<OrderHistory>>

    @POST("order/{orgId}/check_order/")
    fun checkOrder(
        @Path("orgId") orgId: String,
        @Body orderInfo: Order
    ): Call<ErrorCheckOrder>

    @POST("order/{orgId}/attach/")
    fun attach(@Path("orgId") orgId: String, @Body orderInfo: Order): Call<OrderCreated>


}








































