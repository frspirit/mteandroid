package ru.takeeat.takeeat.network.mtesapi.order.data

import ru.takeeat.takeeat.network.mtesapi.core.Product

data class Basket(
    val products: List<Product>
)