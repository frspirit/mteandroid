package ru.takeeat.takeeat.network.mtesapi.core

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

//import com.google.gson.annotations.SerializedName     @SerializedName("")


@Parcelize
data class Order(
    var id: Int? = null,
    var date: String? = null,
    var number: Int? = null,
    var total_amount: Int? = null,
    var delivery_amount: Int = 0,
    var min_shipping_cost: Int = 0,
    var balls: Int = 0 ,
    var person_count: Int = 1,
    var comment: String = "",
    var shop: String? = null,
    var type_pay: String? = null,
    var type_delivery: String? = null,
    var platform: Int = 1,
    var address: String? = null,
    var customer: String? = null,
    var products: MutableList<ProductOrder>? = null
) : Parcelable

@Parcelize
data class ProductOrder(
    val uuid: String,
    val name: String,
    val price: Int,
    val amount: Int,
    var modifiers: MutableList<ModifierOrder>,
) : Parcelable


data class OrderCreated(
    val id: Int? = null,
    val total_amount: Int? = null,
    val date_delivery: String? = null,
    val date_create: String? = null,
)


@Parcelize
data class ModifierOrder(
    val uuid: String,
    val name: String,
    val price: Int,
    var amount: Int,
    val group_name: String,
    val group_uuid: String,
) : Parcelable


@Parcelize
data class ErrorCheckProductOrder(
    val uuid: String,
    val name: String?,
    val delete: Boolean,
    val price: Int?,
    var amount: Int?,
) : Parcelable


@Parcelize
data class ErrorCheckModifierOrder(
    val uuid: String,
    val name: String?,
    val delete: Boolean,
    val price: Int?,
    var amount: Int?,
) : Parcelable

@Parcelize
data class ErrorCheckOrder(
    val products: List<ErrorCheckProductOrder>?,
    val modifiers: List<ErrorCheckModifierOrder>?,
    val total_amount: Int?,
) : Parcelable