package ru.takeeat.takeeat.network.mtesapi.order.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize


@Parcelize
data class OrderHistory(
    val uuid: String,
    var id: Int? = null,
    var date_create: String,
    var number: Int? = null,
    var total_amount: Int? = null,
    var final_amount: Int? = null,
    var delivery_amount: Int = 0,
    var balls: Int = 0,
    var products: MutableList<ProductOrderHistory>? = null
) : Parcelable


