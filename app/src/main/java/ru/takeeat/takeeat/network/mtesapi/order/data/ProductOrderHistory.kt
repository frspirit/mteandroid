package ru.takeeat.takeeat.network.mtesapi.order.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize


@Parcelize
data class ProductOrderHistory(
    val name: String,
    val image: String? = null,
    val delivery: Boolean
) : Parcelable
