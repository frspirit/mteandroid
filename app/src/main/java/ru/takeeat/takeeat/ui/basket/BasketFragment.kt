package ru.takeeat.takeeat.ui.basket

import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.google.android.material.appbar.AppBarLayout
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.databinding.FragmentBasketBinding
import ru.takeeat.takeeat.db.AppDataBase
import ru.takeeat.takeeat.db.order.ModifierOrder
import ru.takeeat.takeeat.db.order.ProductOrder
import ru.takeeat.takeeat.ui.basket.adapters.ProductBasketAdapter
import ru.takeeat.takeeat.ui.basket.interfaces.ProductBasketClickListener
import ru.takeeat.takeeat.ui.basket.modifierEdit.ModifierEditDialogFragment

class BasketFragment : Fragment() {

    private lateinit var viewModel: BasketViewModel
    private lateinit var viewModelFactory: BasketViewModelFactory
    private var _binding: FragmentBasketBinding? = null
    private var isProducts: Boolean = false

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        activity?.findViewById<AppBarLayout>(R.id.AppBarLayout)?.visibility = View.VISIBLE
//        val window: Window = requireActivity().window
//        window.statusBarColor = ContextCompat.getColor(requireContext(), R.color.white)
//        activity?.findViewById<Toolbar>(R.id.my_toolbar)
//            ?.setBackgroundColor(resources.getColor(R.color.white))
//        window.decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR)


        val application = requireNotNull(this.activity).application
        val db = AppDataBase.getInstance(application)
        viewModelFactory = BasketViewModelFactory(db, application)
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(BasketViewModel::class.java)
    }

    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        Log.d("ANIMATIONFRAGMENT","$transit - $enter - $nextAnim - ${this.javaClass.name}")
        if(enter){
            //activity?.findViewById<TextView>(R.id.toolbar_)?.visibility = View.VISIBLE
            //activity?.findViewById<TextView>(R.id.toolbar_)?.text = "Корзина"
            activity?.let { actv ->
                actv.findViewById<AppBarLayout>(R.id.AppBarLayout)?.visibility = View.VISIBLE
                actv.findViewById<Toolbar>(R.id.my_toolbar)
                    ?.setBackgroundColor(resources.getColor(R.color.white))
                actv.window.statusBarColor = resources.getColor(R.color.white,null)
                actv.window.decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR)



            }

        }
        return if (enter) {
            AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left)
        } else {
            AnimationUtils.loadAnimation(context, android.R.anim.slide_out_right)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        //activity?.findViewById<TextView>(R.id.toolbar_)?.text = "Корзина"



        _binding = FragmentBasketBinding.inflate(inflater, container, false)
        binding.basketCheckoutMaterialButton.alpha = 0.5F
        binding.basketCheckoutMaterialButton.isEnabled = false

//        if(!viewModel.modifiersDB.value.isNullOrEmpty()){
//            viewModel.getModifiers()
//
//        }


        onRenderProductOrder()
        onBasketCheckout()

        binding.basketEmptyMB.setOnClickListener{
            requireView().findNavController().popBackStack()
        }
        return binding.root

    }


    private fun onRenderProductOrder() {
//        var lastEditProduct: ProductOrder? = null
        viewModel.products.observe(viewLifecycleOwner, { data ->
            if (!data.isNullOrEmpty()) {
                binding.basketEmptyLayout.visibility = View.GONE
                binding.basketCL.visibility = View.VISIBLE
                isProducts = true

                val adapter = ProductBasketAdapter(data, viewModel.modifiers, viewModel = viewModel,
                    object : ProductBasketClickListener {

                        override fun onEditableModifier(
                            product: ProductOrder,
                            productModifierOrder: List<ModifierOrder>
                        ) {
                            val bottomSheetRequiredFragment =
                                ModifierEditDialogFragment(
                                    idProduct = product.id,
                                    product = product,
                                    productModifierOrder = productModifierOrder
                                )
                            bottomSheetRequiredFragment.show(
                                childFragmentManager,
                                "ModifierEditDialogFragment"
                            )

                        }

                        override fun onMinusProduct(
                            product: ProductOrder,
                            countTV: TextView,
                            priceTV: TextView
                        ) {
//                            if (product.amount >1){
//                                val out = viewModel.onMinusCountProductOrder(product)
//                                countTV.text = out.toString()
//                                priceTV.text = "${out * product.amount} ₽"
//                            }else{
//
//                                viewModel.onRemoveProductInBasket(product)
//                            }
                        }

                        override fun onPlusProduct(
                            product: ProductOrder,
                            countTV: TextView,
                            priceTV: TextView
                        ) {
//                            val out = viewModel.onPlusProductInBasket(product)
//                            countTV.text = out.toString()
//                            priceTV.text = "${out * product.amount} ₽"
                        }
                    })
                binding.productBasketRV.adapter = adapter

                var tAmount = 0
                data.forEach { productOrder ->
                    tAmount += (productOrder.price.toInt() * productOrder.amount)
                }
                viewModel.setTotalAmount(tAmount)

                binding.basketProductOrderTotalAmount.text = viewModel.totalAmount.toString()
                viewModel.onCheckProductBasket(data)
                viewModel.errorCheckOrder.observe(viewLifecycleOwner, { errorCheckOrder ->

                    if (errorCheckOrder != null) {
                        binding.basketCheckoutMaterialButton.alpha = 0.5F
                        binding.basketCheckoutMaterialButton.isEnabled = false
                        viewModel.onCheckingErrorCheckProductOrder(
                            requireView(),
                            errorCheckOrder.products,
                            binding.basketCheckoutMaterialButton
                        )
                        if (errorCheckOrder.products.isNullOrEmpty() && errorCheckOrder.modifiers.isNullOrEmpty() && errorCheckOrder.total_amount == null) {
                            binding.basketCheckoutMaterialButton.alpha = 1F
                            binding.basketCheckoutMaterialButton.isEnabled = true
                        }
                    }
//                    else {
//                        binding.basketCheckoutMaterialButton.alpha = 1F
//                        binding.basketCheckoutMaterialButton.isEnabled = true
//                    }
                })
                return@observe
            } else {
                binding.basketEmptyLayout.visibility = View.VISIBLE
                binding.basketCL.visibility = View.GONE
                isProducts = false

            }

        })


    }


    private fun onBasketCheckout() {
        binding.basketCheckoutMaterialButton.setOnClickListener {
            if (viewModel.auth && isProducts) {

                requireView().findNavController().navigate(
                    BasketFragmentDirections.actionNavigationBasketToCheckoutFragment(
                                viewModel.totalAmount
                    ),
                )
            } else if (!viewModel.auth) {
                requireView().findNavController().navigate(
                    BasketFragmentDirections.actionNavigationBasketToAuthFromBasketFragment(),
//                    ProfileFragmentDirections.actionNavigationProfileToAuthFragment2()
                )
            }
        }

    }

//    override fun onStart() {
//        super.onStart()
//        //activity?.findViewById<TextView>(R.id.toolbar_)?.text = "Корзина"
//    }
    override fun onStop() {
        super.onStop()
        activity?.window?.statusBarColor =
            ContextCompat.getColor(requireContext(), R.color.purple_light)
    }
}