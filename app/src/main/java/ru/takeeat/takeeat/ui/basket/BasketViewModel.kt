package ru.takeeat.takeeat.ui.basket

import android.app.Application
import android.view.Gravity
import android.view.View
import android.widget.FrameLayout
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.material.button.MaterialButton
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.db.AppDataBase
import ru.takeeat.takeeat.db.customer.Customer
import ru.takeeat.takeeat.db.order.ModifierOrder
import ru.takeeat.takeeat.db.order.ProductOrder
import ru.takeeat.takeeat.network.mtesapi.core.*
import ru.takeeat.takeeat.network.mtesapi.order.OrderAPI

class BasketViewModel(val db: AppDataBase, application: Application) :
    AndroidViewModel(application) {
    private val _serviceCore = CoreAPI()
    private val _serviceOrder = OrderAPI()

    private val _customer = db.customerDAO().getCheckCustomer()
    val customer: LiveData<Customer> get() = _customer
    var auth: Boolean = false

//    private val _modifiersNetwork = MutableLiveData<List<Modifier>>()
//    val modifiersNetwork: LiveData<List<Modifier>>
//        get() = _modifiersNetwork

//    fun getModifiers() {
//        _serviceCore.callModifiers(_modifiersNetwork)
//    }

    private val _modifiersDB = db.modifierDAO().getLDAll()
    val modifiersDB: LiveData<List<ru.takeeat.takeeat.db.core.Modifier>>
        get() = _modifiersDB

    private val _products = db.productOrderDAO().getAll()
    val products: LiveData<List<ProductOrder>>
        get() = _products

    private var _modifiers = db.modifierOrderDAO().getAll()
    val modifiers: LiveData<List<ModifierOrder>>
        get() = _modifiers

//    private var _groupModifiers = db.groupModifierDAO().getModifiers()
//    val groupModifiers: LiveData<List<GroupModifierWithModifierStr>>
//        get() = _groupModifiers


    fun getGroupModifierByProductCount(idProduct: Long): Int {
        var out = 0
        uiScope.launch {
            out = db.groupModifierDAO().getCountByProduct(idProduct)

        }
        return out
    }
//    private val _productWithModifiers = db.modifierOrderDAO().getProductOrderWithModifierOrder()
//    val productWithModifiers : LiveData<List<ProductOrderWithModifierOrder>>
//        get() = _productWithModifiers

    private var viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    fun onPlusProductInBasket(productObj: ProductOrder): Int {
        productObj.amount += 1
        uiScope.launch {
//            val productDB = db.orderDAO().getProductOrder(productObj.id)
//            productDB.amount += 1
            db.productOrderDAO().update(productObj)

        }
        return productObj.amount
    }

//    fun onGetGroupModifierForProduct(id: Long): List<GroupModifierWithModifierStr>{
//        uiScope.launch{
//            _groupModifiers =
//        }
//        Log.d("GRoupModifier", "${_groupModifiers.size}")
//        return _groupModifiers
//    }

    fun onRemoveProductInBasket(productObj: ProductOrder) {
        uiScope.launch {
            db.productOrderDAO().remove(productObj)
            db.modifierOrderDAO().removeAllByProduct(productObj.id)
            val gmo = db.groupModifierDAO().getByProductOrder(productObj.id)
            gmo.forEach {
                db.modifierStrDAO().removeByMGroupOrder(it.id)
            }
            db.groupModifierDAO().removeAllByProductOrder(productObj.id)
        }
    }

    fun onMinusCountProductOrder(productObj: ProductOrder): Int {
        uiScope.launch {
            productObj.amount -= 1
            db.productOrderDAO().update(productObj)
        }
        return productObj.amount
    }

    private var _totalAmount: Int = 0
    val totalAmount get() = _totalAmount

    fun setTotalAmount(tAmount: Int) {
        _totalAmount = tAmount
    }


//    private val _orderInfo = MutableLiveData<Order>()
//    val orderInfo: LiveData<Order> get() = _orderInfo

    private val _errorCheckOrder = MutableLiveData<ErrorCheckOrder>()
    val errorCheckOrder: LiveData<ErrorCheckOrder> get() = _errorCheckOrder
    fun onCheckProductBasket(productDB: List<ProductOrder>) {
        val order: Order =
            Order(products = mutableListOf<ru.takeeat.takeeat.network.mtesapi.core.ProductOrder>())
        uiScope.launch {
            productDB.forEach { productOrder ->
                var product: ru.takeeat.takeeat.network.mtesapi.core.ProductOrder =
                    ru.takeeat.takeeat.network.mtesapi.core.ProductOrder(
                        uuid = productOrder.uuid,
                        name = productOrder.name,
                        price = productOrder.priceProduct.toInt(),
                        amount = productOrder.amount,
                        modifiers = mutableListOf<ru.takeeat.takeeat.network.mtesapi.core.ModifierOrder>()
                    )
                val modifiersOrderDB =
                    db.modifierOrderDAO().getProductModifiersNoLive(productOrder.id)
                modifiersOrderDB.forEach { modifierOrder ->
                    val modifier = ru.takeeat.takeeat.network.mtesapi.core.ModifierOrder(
                        uuid = modifierOrder.uuid,
                        name = modifierOrder.name,
                        price = modifierOrder.price.toInt(),
                        amount = modifierOrder.amount,
                        group_name = modifierOrder.group_name,
                        group_uuid = modifierOrder.group_uuid
                    )
                    product.modifiers.add(modifier)
                }
                order.products!!.add(product)
            }


            order.total_amount = _totalAmount
//            _orderInfo.value = order
            _serviceOrder.callCheckOrder(order, _errorCheckOrder)


        }

    }

    fun onCheckingErrorCheckProductOrder(
        view: View,
        listErrorCheckProductOrder: List<ErrorCheckProductOrder>?,
        button: MaterialButton
    ) {
        if (!listErrorCheckProductOrder.isNullOrEmpty()) {
            var countProductRemove = 0
            var countProductEdit = 0
            uiScope.launch {

                val productOrder = db.productOrderDAO().getAllNoLive()
                listErrorCheckProductOrder.forEach { errorCheckProductOrder ->
                    productOrder.forEach { productOrder ->
                        if ((productOrder.uuid == errorCheckProductOrder.uuid)) {
                            if (errorCheckProductOrder.delete) {
                                db.groupModifierDAO().removeAllByProduct(productOrder.id)
                                db.modifierOrderDAO().removeAllByProduct(productOrder.id)
                                db.productOrderDAO().remove(productOrder)
                                countProductRemove += 1

                            } else if (!errorCheckProductOrder.delete) {
                                productOrder.name = errorCheckProductOrder.name.toString()
                                productOrder.priceProduct =
                                    errorCheckProductOrder.price?.toFloat()
                                        ?: productOrder.priceProduct
                                db.productOrderDAO().update(productOrder)
                                countProductEdit += 1
                            }

                        }
                    }
                }
                if (countProductRemove > 0) {
                    Snackbar.make(
                        view,
                        "Удалено $countProductRemove несуществующих продукта",
                        Snackbar.LENGTH_LONG
                    ).apply {
                        setBackgroundTint(view.resources.getColor(R.color.AccentColor))
                        setTextColor(view.resources.getColor(R.color.white))

                        val params = view.layoutParams as FrameLayout.LayoutParams
                        params.topMargin = 118
                        params.gravity = Gravity.CENTER_HORIZONTAL or Gravity.TOP
                        show()
                    }
                }

                if (countProductEdit > 0) {
                    Snackbar.make(view, "Изменено $countProductEdit продукта", Snackbar.LENGTH_LONG)
                        .apply {
                            setBackgroundTint(view.resources.getColor(R.color.AccentColor))
                            setTextColor(view.resources.getColor(R.color.white))

                            val params = view.layoutParams as FrameLayout.LayoutParams
                            params.topMargin = 118
                            params.gravity = Gravity.CENTER_HORIZONTAL or Gravity.TOP
                            show()
                        }
                }
            }
        }

    }


    init {
//        uiScope.launch {
//            _groupModifiers = db.groupModifierDAO().getModifiers()
////            _products = db.orderDAO().getProductOrderAll().observe()
//        }
//        getModifiers()
        _customer.observeForever { customer ->
            if (customer != null ) {
                if(customer.phone != null){
                    auth = true
                    return@observeForever
                }
            }
        }

    }
}