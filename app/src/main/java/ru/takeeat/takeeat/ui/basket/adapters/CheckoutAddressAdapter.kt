package ru.takeeat.takeeat.ui.basket.adapters

import android.content.Context
import android.content.res.ColorStateList
import android.content.res.TypedArray
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.TextView
import androidx.appcompat.widget.AppCompatRadioButton
import androidx.core.widget.CompoundButtonCompat
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.card.MaterialCardView
import com.google.android.material.color.MaterialColors
import com.google.android.material.internal.ThemeEnforcement
import com.google.android.material.radiobutton.MaterialRadioButton
import com.google.android.material.resources.MaterialResources
import com.google.android.material.theme.overlay.MaterialThemeOverlay
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.db.customer.Address
import ru.takeeat.takeeat.ui.basket.interfaces.CheckoutAddressClickListener

class CheckoutAddressAdapter (
    private val addresses: List<Address>,
    private val selectAddress: String?,
    private val onCardClickListener: CheckoutAddressClickListener,
) : RecyclerView.Adapter<CheckoutAddressAdapter.MyViewHolder>() {
    private var lastCheckoutAddress: MaterialCardView? = null
    class MyViewHolder constructor(view: View) : RecyclerView.ViewHolder(view) {
//        val radio_button: MaterialRadioButton = view.findViewById(R.id.addressRadioButton)
        val button: MaterialCardView = view.findViewById(R.id.addressRadioButton)
        val title: TextView = view.findViewById(R.id.addressRadioButtonTitle)
        val text: TextView = view.findViewById(R.id.addressRadioButtonText)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_checkout_address_dialog_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val address = addresses[position]
//        holder.radio_button.text =  "${address.city},${address.street}, д. ${address.home} ${address.apartment?.let{", кв. $it,"} ?: ""}${address.entrance?.let {" под. $it," } ?: ""}${address.floor?.let { " этаж $it" }?: ""}"4
        if (address.name != null && address.name != ""){
            holder.title.text = address.name
            holder.title.visibility = View.VISIBLE
        }else{
            holder.title.visibility = View.GONE
        }

        holder.text.text =  "${address.city},${address.street}, д. ${address.home} ${if (address.apartment != null &&  address.apartment != "") ", кв. ${address.apartment},"  else ""}"
//        holder.button.setOnCheckedChangeListener { buttonView, isChecked ->
//            if (lastCheckoutAddress != null){
//                lastCheckoutAddress!!.isChecked = false
//            }
//            lastCheckoutAddress = holder.button
//            onCardClickListener.onCheckoutAddress(address)
//        }
        if (!selectAddress.isNullOrEmpty() && selectAddress == address.uuid){
            holder.button.strokeColor =  holder.button.context.resources.getColor(R.color.AccentColor)
            lastCheckoutAddress = holder.button
        }
        holder.button.setOnClickListener { buttonView ->
            if (lastCheckoutAddress != null){
                lastCheckoutAddress!!.strokeColor = buttonView.context.resources.getColor(R.color.purple_light)
            }
            holder.button.strokeColor = buttonView.context.resources.getColor(R.color.AccentColor)
            lastCheckoutAddress = holder.button
            onCardClickListener.onCheckoutAddress(address)
        }

    }


    override fun getItemCount(): Int {
        return addresses.size
    }

}

