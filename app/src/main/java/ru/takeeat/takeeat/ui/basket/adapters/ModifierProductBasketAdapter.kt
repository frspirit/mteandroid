package ru.takeeat.takeeat.ui.basket.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.db.order.ModifierOrder
import ru.takeeat.takeeat.ui.basket.interfaces.ModifierProductBasketClickListener


class ModifierProductBasketAdapter (
    private val modifier_items: List<ModifierOrder>,
//    private val product_modifiers: List<ModifierOrder>,
    private val onClickListener: ModifierProductBasketClickListener,
) : RecyclerView.Adapter<ModifierProductBasketAdapter.MyViewHolder>() {

    class MyViewHolder constructor(view: View) : RecyclerView.ViewHolder(view) {
        val modifier_title: TextView = view.findViewById(R.id.productBasketModifierTitle)
        val modifier_price: TextView = view.findViewById(R.id.productBasketModifierPrice)
        val modifier_remove_button: Button = view.findViewById(R.id.productBasketRemoveModifier)
        val modifier_count: TextView = view.findViewById(R.id.productBasketModifierCount)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_product_in_basket_modifier_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val modifier_item = modifier_items[position]

        holder.modifier_title.text = modifier_item.name
        holder.modifier_count.text = modifier_item.amount.toString()
        holder.modifier_price.text = "${modifier_item.price.toInt()} ₽"
        holder.modifier_remove_button.setOnClickListener {
            onClickListener.onRemoveModifier(modifier_item)
        }

    }
    override fun getItemCount(): Int {
        return modifier_items.size
    }


}