package ru.takeeat.takeeat.ui.basket.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.marginStart
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.transform.CircleCropTransformation
import coil.transform.RoundedCornersTransformation
import com.google.android.material.button.MaterialButton
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.SERVER_IMAGE
import ru.takeeat.takeeat.curvedLine
import ru.takeeat.takeeat.db.order.ModifierOrder
import ru.takeeat.takeeat.db.order.ProductOrder
import ru.takeeat.takeeat.ui.basket.BasketViewModel
import ru.takeeat.takeeat.ui.basket.interfaces.ProductBasketClickListener

class ProductBasketAdapter(
    private val product_items: List<ProductOrder>,
    private val product_modifiers: LiveData<List<ModifierOrder>>,
    private val viewModel: BasketViewModel,
    private val onClickListener: ProductBasketClickListener,
) : RecyclerView.Adapter<ProductBasketAdapter.MyViewHolder>() {

    class MyViewHolder constructor(view: View) : RecyclerView.ViewHolder(view) {
        val product_title: TextView = view.findViewById(R.id.productBasketTitle)
        val product_modifiers_names: TextView = view.findViewById(R.id.productBasketModifiersName)
        val product_weight: TextView = view.findViewById(R.id.productBasketWeight)
        val product_image: ImageView = view.findViewById(R.id.productBasketImage)
        val product_minus_count: MaterialButton = view.findViewById(R.id.productBasketMinusCount)
        val product_count: TextView = view.findViewById(R.id.productBasketCount)
        val product_plus_count: MaterialButton = view.findViewById(R.id.productBasketPlusCount)
        val product_price: TextView = view.findViewById(R.id.productBasketPrice)
        val product_modifier_button: MaterialButton =
            view.findViewById(R.id.productBasketModifierButton)
        val pProductTextView: TextView = view.findViewById(R.id.pPriceTextView)
        val productPriceCurveLine: ImageView = view.findViewById(R.id.productPriceCurveLine)
//        val product_modifierRV: RecyclerView = view.findViewById(R.id.productBasketModifierRV)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_product_in_basket_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val product_item = product_items[position]
//        val modifier_product: MutableList<ModifierOrder> = mutableListOf()
        val modifier_product: MutableList<ModifierOrder> = mutableListOf()
        product_modifiers.observeForever {
            val modifier_product_names: MutableList<String> = mutableListOf()

            product_modifiers.value?.forEach {
                if (it.idProductOrder == product_item.id) {
                    modifier_product_names.add(it.name)
                    modifier_product.add(it)
//                    modifier_product.add(it)
                }
            }
            if (!modifier_product_names.isNullOrEmpty()) {
                holder.product_modifier_button.visibility = View.VISIBLE
                if (modifier_product_names.size > 0) {
                    val out = modifier_product_names.joinToString()
//                        .removeRange(0,out.lastIndex)
                    holder.product_modifiers_names.text = out
                }
            }

        }
        // Строка названий выбранных модификаторов
//        if(viewModel.getGroupModifierByProductCount(product_item.id)>0){
//            holder.product_modifier_button.visibility = View.VISIBLE
//
//        }
//        holder.product_modifiers_names.text = modifier_product_names2
        // Вес продукта
        holder.product_weight.text = "${product_item.weight} г"

        // Название продукта
        holder.product_title.text = product_item.name
        // Картинка продукта
        holder.product_image.load("${SERVER_IMAGE}${product_item.image}") {
            crossfade(false)
            placeholder(R.drawable.defimage)
//            transformations(RoundedCornersTransformation())
        }
        // Цена товара (цена на кол-во)
        holder.product_price.text = "${product_item.price.toInt() * product_item.amount} ₽"

        if (product_item.p_price != null) {
            holder.productPriceCurveLine.visibility = View.VISIBLE
            holder.pProductTextView.visibility = View.VISIBLE
            holder.pProductTextView.text = "${product_item.p_price.toInt()} ₽"
            holder.pProductTextView.measure(0, 0)
            val width = holder.pProductTextView.measuredWidth.toFloat()
            val height = holder.pProductTextView.measuredHeight.toFloat()
            val bitmap = curvedLine(width, height)
            holder.productPriceCurveLine.setImageBitmap(bitmap)
        }else{
            holder.productPriceCurveLine.visibility = View.GONE
            holder.pProductTextView.visibility = View.GONE
        }

        // Кол-во выбранных товаров
        holder.product_count.text = product_item.amount.toString()
        // Минус 1 кол-во товара(при кол-во = 1, товар удаляется)
        holder.product_minus_count.setOnClickListener {
            if (product_item.amount > 1) {
                val out = viewModel.onMinusCountProductOrder(product_item)
                holder.product_count.text = out.toString()
                holder.product_price.text = "${out * product_item.amount} ₽"

            } else {
                notifyItemRemoved(position)
                viewModel.onRemoveProductInBasket(product_item,)
            }
            onClickListener.onMinusProduct(product_item, holder.product_count, holder.product_price)
        }
        // Плюс 1 кол-во товара
        holder.product_plus_count.setOnClickListener {
            val out = viewModel.onPlusProductInBasket(product_item)
            holder.product_count.text = out.toString()
            holder.product_price.text = "${out * product_item.amount} ₽"
            onClickListener.onPlusProduct(product_item, holder.product_count, holder.product_price)
        }
        // Редактирование модификаторов
        holder.product_modifier_button.setOnClickListener { view ->
//            Toast.makeText(view.context, modifier_product_names2, Toast.LENGTH_LONG).show()


            onClickListener.onEditableModifier(
                product_item,
                modifier_product,
            )

        }

    }

    override fun getItemCount(): Int {
        return product_items.size
    }
}