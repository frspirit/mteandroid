package ru.takeeat.takeeat.ui.basket.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.radiobutton.MaterialRadioButton
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.ui.basket.interfaces.ShopClickListener


class ShopAdapter(
    private val shopEntities: List<ru.takeeat.takeeat.db.core.ShopEntity>,
    private val lastShop: String? = null,
    private val onClickListener: ShopClickListener<ru.takeeat.takeeat.db.core.ShopEntity>,
) : RecyclerView.Adapter<ShopAdapter.MyViewHolder>() {
    private var lastRadioButton: RadioButton? = null
    class MyViewHolder constructor(view: View) : RecyclerView.ViewHolder(view) {
        val radio_button: MaterialRadioButton = view.findViewById(R.id.shopRadioButton)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_shop_dialog_list_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val shop_item = shopEntities[position]
        holder.radio_button.text = "${shop_item.name}\n${shop_item.address}"

        if (lastShop != null){
            shopEntities.forEach {
                if (shop_item.uuid == lastShop){
                    holder.radio_button.isChecked = true
                    lastRadioButton = holder.radio_button

                }
            }
        }
        holder.radio_button.setOnCheckedChangeListener { buttonView, isChecked ->
            if (lastRadioButton != null){
                lastRadioButton!!.isChecked = false
            }
            lastRadioButton = holder.radio_button
            onClickListener.onClick(shop_item, )
        }
//        shops.forEach { shop ->
//            if (modifier.uuid == modifier_item.uuid) {
////                Log.i("ProductModifierItem", modifier.name)
//                if (modifier_item.uuid == lastRequireModifier){
//                    holder.radio_button.isChecked = true
//                    lastRadioButton = holder.radio_button
//                }
//
//                holder.radio_button.text = modifier.name
//                holder.radio_button.setOnCheckedChangeListener { buttonView, isChecked ->
//                    if (lastRadioButton != null){
//                        lastRadioButton!!.isChecked = false
//                    }
//                    lastRadioButton = holder.radio_button
//                    onCardClickListener.onClicked(modifier, )
//                }
//
//                return@forEach
//            }
//        }
    }


    override fun getItemCount(): Int {
        return shopEntities.size
    }

}