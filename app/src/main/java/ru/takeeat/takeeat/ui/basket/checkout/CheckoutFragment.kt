package ru.takeeat.takeeat.ui.basket.checkout

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.SeekBar
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.core.widget.doOnTextChanged
import ru.takeeat.takeeat.network.mtesapi.interfaces.Result
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.button.MaterialButton

import ru.takeeat.takeeat.MapsUtils
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.databinding.CheckoutFragmentBinding
import ru.takeeat.takeeat.db.AppDataBase
import ru.takeeat.takeeat.db.customer.Address
import ru.takeeat.takeeat.network.mtesapi.core.OrderCreated
import ru.takeeat.takeeat.network.mtesapi.core.TypePay
import ru.takeeat.takeeat.ui.basket.dialogs.address.CheckoutAddressDialogFragment
import ru.takeeat.takeeat.ui.basket.dialogs.datetime.CheckoutDateDialogFragment
import ru.takeeat.takeeat.ui.basket.dialogs.shop.ShopDialogFragment
import ru.takeeat.takeeat.ui.basket.dialogs.typepay.CheckoutTypePayDialogFragment
import ru.takeeat.takeeat.ui.basket.interfaces.OrderListener
import java.lang.NullPointerException
import java.time.format.DateTimeFormatter

class CheckoutFragment : Fragment() {
    private lateinit var _binding: CheckoutFragmentBinding
    private val binding get() = _binding

    private lateinit var viewModel: CheckoutViewModel
    private lateinit var viewModelFactory: CheckoutViewModelFactory

    private lateinit var args: CheckoutFragmentArgs

    private lateinit var mapsUtils: MapsUtils

    private var bonusAmount: Int = 0
    private var atTheTime: MutableLiveData<Int> = MutableLiveData(0)
    private lateinit var addressObserver: MutableLiveData<Address>
    private lateinit var typePayObserver: MutableLiveData<TypePay>
    private lateinit var shopEntityObserver: MutableLiveData<ru.takeeat.takeeat.db.core.ShopEntity>
    private lateinit var dateObserver: MutableLiveData<String?>

    companion object {
        val TAG = "CheckoutFragment"
        private var enableButton: Boolean = false

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.findViewById<BottomNavigationView>(R.id.nav_view)?.visibility = View.GONE
        mapsUtils = MapsUtils(requireContext())
        //activity?.findViewById<TextView>(R.id.toolbar_)?.text = "Оформление заказа"
    }

    override fun onResume() {
        super.onResume()
        activity?.let { actv ->
            actv.findViewById<AppBarLayout>(R.id.AppBarLayout)?.visibility = View.VISIBLE
            actv.findViewById<BottomNavigationView>(R.id.nav_view).visibility = View.GONE
        }

        //activity?.findViewById<TextView>(R.id.toolbar_)?.text = "Оформление заказа"
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        activity?.let { actv ->
            actv.window.statusBarColor = resources.getColor(R.color.white, null)
            actv.findViewById<Toolbar>(R.id.my_toolbar)
                ?.setBackgroundColor(resources.getColor(R.color.white, null))
        }

        shopEntityObserver = MutableLiveData()
        dateObserver = MutableLiveData()
        args = CheckoutFragmentArgs.fromBundle(requireArguments())
        val application = requireNotNull(this.activity).application
        val db = AppDataBase.getInstance(application)
        viewModelFactory = CheckoutViewModelFactory(db, application)
        viewModel = ViewModelProvider(this, viewModelFactory).get(CheckoutViewModel::class.java)
        _binding = CheckoutFragmentBinding.inflate(inflater, container, false)


        binding.checkoutCustomerBonusLayout.visibility = View.GONE
        binding.customerBonusSeekBar.isEnabled = false
        binding.checkoutProductOrderTotalAmount.text = args.totalAmount.toString()
        binding.checkoutTotalAmountText.text = args.totalAmount.toString()
        viewModel.setTotalAmount(args.totalAmount)


//        binding.basketCheckoutMaterialButton.alpha = 0.5F
//        binding.basketCheckoutMaterialButton.isEnabled = false


        viewModel.totalAmount.observe(viewLifecycleOwner, {
            binding.checkoutProductOrderTotalAmount.text = it.toString()
        })

        onCheckShops()
        onCustomer()
        onComment()
        onBonusSeekBar()
        onDeliveryAmount()
        onAddress()
        onShops()
        onTypeDelivery()
        onTypePay()
        onDate()
        onSendOrderInfo()

        return binding.root
    }


    private fun onCheckShops() {
        viewModel.getShops(object : Result<Boolean> {

            override fun result(code: Boolean) {

                if (code) {
                    binding.checkoutTypeDeliveryRBPickup.visibility = View.VISIBLE
                } else {
                    binding.checkoutTypeDeliveryRBPickup.visibility = View.GONE
                    binding.checkoutTypeDeliveryRBDelivery.isChecked = true
                    viewModel.onSetCheckoutTypeDelivery(CheckoutViewModel.typeDeliveryDelivery.uuid)

                }
            }
        })
    }

    private fun onShops() {
        this.viewModel.shops.observe(viewLifecycleOwner, { listShop ->
            if (listShop.isNullOrEmpty()) {
                binding.checkoutTypeDeliveryRBPickup.visibility = View.GONE
                binding.checkoutTypeDeliveryRBDelivery.isChecked = true
                viewModel.onSetCheckoutTypeDelivery(CheckoutViewModel.typeDeliveryDelivery.uuid)


            } else {
                binding.checkoutTypeDeliveryRBPickup.visibility = View.VISIBLE
            }
        })
    }


    private fun onTypeDelivery() {
        binding.checkoutTypeDeliveryRBDelivery.text = CheckoutViewModel.typeDeliveryDelivery.name
        binding.checkoutTypeDeliveryRBPickup.text = CheckoutViewModel.typeDeliveryPickup.name



        binding.checkoutTypeDeliveryRBDelivery.isChecked = true
        viewModel.onSetCheckoutTypeDelivery(CheckoutViewModel.typeDeliveryDelivery.uuid)

        binding.checkoutTypeDeliveryRadioGroup.setOnCheckedChangeListener { _, checkedId ->

            if (checkedId == binding.checkoutTypeDeliveryRBDelivery.id) {
                binding.checkoutAddressShopCardView.visibility = View.GONE
                binding.checkoutAddressCustomerCardView.visibility = View.VISIBLE
                binding.checkoutCustomerBonusLayout.visibility = View.GONE
                //bonus
                binding.customerBonusSeekBar.isEnabled = false
                binding.customerBonusSeekBar.progress = 0
                binding.checkoutBonusTitle.text = "Оплата бонусами"

                //
                binding.textView20.visibility = View.VISIBLE
                binding.checkoutDeliveryAmountText.visibility = View.VISIBLE
                binding.checkoutDeliveryAmountCurrencySign.visibility = View.VISIBLE


                shopEntityObserver.value = null
                binding.checkoutAddressShopText.text = ""

                viewModel.onSetCheckoutTypeDelivery(CheckoutViewModel.typeDeliveryDelivery.uuid)


            } else if (checkedId == binding.checkoutTypeDeliveryRBPickup.id) {
                // TODO(kebrick): дописать проверку totalAmount на мин сумму мин доставки
                viewModel.setFinalTotalAmount((args.totalAmount - viewModel.order.balls))
                binding.checkoutAddressCustomerCardView.visibility = View.GONE
                binding.checkoutAddressShopCardView.visibility = View.VISIBLE
                binding.textView20.visibility = View.GONE
                binding.checkoutDeliveryAmountText.visibility = View.GONE
                binding.checkoutDeliveryAmountCurrencySign.visibility = View.GONE
                binding.checkoutCustomerBonusLayout.visibility = View.VISIBLE
                binding.customerBonusSeekBar.isEnabled = true

                viewModel.order.delivery_amount = 0

                viewModel.customer.value.let { customer ->
                    if (customer != null) {
                        val outBals =
                            (args.totalAmount + viewModel.order.delivery_amount) - customer.balance
                        if (outBals >= 1) {
                            binding.customerBonusSeekBar.max = customer.balance
                        } else {
                            binding.customerBonusSeekBar.max = customer.balance + (outBals - 1)
                        }
                    }


                }
                viewModel.setAddress(null)
                binding.checkoutDeliveryAmountText.text = "0"

                viewModel.onSetCheckoutTypeDelivery(CheckoutViewModel.typeDeliveryPickup.uuid)
                binding.checkoutAddressShopCardView.setOnClickListener {
                    val bottomSheetRequiredFragment =
                        ShopDialogFragment(shopEntityObserver)
                    bottomSheetRequiredFragment.show(
                        childFragmentManager,
                        "ShopDialogFragment"
                    )
                }
                viewModel.setFinalTotalAmount((args.totalAmount - viewModel.order.balls))
//                onCheckFields()
            }
        }


        shopEntityObserver.observe(viewLifecycleOwner, {
            if (it != null) {
                viewModel.setShop(it)
                binding.checkoutAddressShopText.text = "${it.name}"
            } else {
                binding.checkoutAddressShopText.text = ""
                viewModel.setShop(null)
            }
//            onCheckFields()
        })
    }

    private fun onCustomer() {
        viewModel.customer.observe(viewLifecycleOwner, { customer ->
            var phone: String = customer.phone!!
            if (phone.length == 12) {
                var phone2 = phone.toMutableList()
                phone2.add(2, "(".first())
                phone2.add(6, ")".first())
                phone2.add(10, "-".first())
                phone2.add(13, "-".first())
                phone = phone2.joinToString("")
            }
            bonusAmount = customer.balance
            if (customer.name != null && customer.name != "") {
                binding.checkoutCustomerInfoText.text = "${customer.name}, $phone"
            } else {
                binding.checkoutCustomerInfoText.text = "$phone"
            }

            binding.checkoutCustomerInfoCardView.setOnClickListener {
                requireView().findNavController().navigate(
                    CheckoutFragmentDirections.actionCheckoutFragmentToCustomerEditInfoFragment(true)
                )
            }
            viewModel.setCustomer(customer)
            try {
                viewModel.onGetServerAddresses(customer.uuid!!)
            } catch (e: NullPointerException) {
            }

        })
    }

    private fun onDeliveryAmount() {
        binding.checkoutDeliveryAmountText.text = "0"

        viewModel.polygonInfo.observe(viewLifecycleOwner, {
            if (it != null) {
                if (args.totalAmount < it.minPriceFree) {
                    viewModel.setDeliveryAmount(it.priceDelivery)

                } else {
                    viewModel.setDeliveryAmount(0)
                }
                viewModel.setMinShippingCost(it.minPriceFree)
            }
            binding.checkoutDeliveryAmountText.text = it.priceDelivery.toString()
        })

    }

    private fun onComment() {
        binding.customerEditInfoNameInputEditText.doOnTextChanged { text, _, _, _ ->
            viewModel.onSetComment(text.toString())
        }
    }


//    private fun onContactlessDelivery() {
//        binding.checkoutContactlessDelivery.setOnCheckedChangeListener { _, isChecked ->
////            Toast.makeText(context,"ContactlessDelivery $isChecked",Toast.LENGTH_SHORT).show()
//        }
//    }RR


    private fun onBonusSeekBar() {
        viewModel.customer.observe(viewLifecycleOwner, { customer ->
            if (customer.balance > 0) {
                // TODO(kebrick): Дописать логику списания бонусов по цене корзины
                binding.customerBonusSeekBar.max = customer.balance
                binding.customerBonusSeekBar.setOnSeekBarChangeListener(object :
                    SeekBar.OnSeekBarChangeListener {

                    override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                        if (i == 0) {
                            binding.checkoutBonusTitle.text = "Оплата бонусами"
                        }
                        val outAmount: Int =
                            ((args.totalAmount + viewModel.order.delivery_amount) - i)
                        if (outAmount <= 1) {
                            seekBar.progress = i + outAmount
                            binding.checkoutBonusTitle.text =
                                "Спишем ${i + (outAmount - 1)} бонусов"
//                            viewModel.setFinalTotalAmount(((args.totalAmount + viewModel.order.delivery_amount) - i))
                        } else {
                            binding.checkoutBonusTitle.text = "Спишем $i бонусов"
                            viewModel.setFinalTotalAmount(((args.totalAmount + viewModel.order.delivery_amount) - i))
                        }

                    }

                    override fun onStartTrackingTouch(seekBar: SeekBar) {
//                Toast.makeText(context,"start tracking",Toast.LENGTH_SHORT).show()
                    }

                    override fun onStopTrackingTouch(seekBar: SeekBar) {
                        // stopTracking -> set viewmodel
                        viewModel.setCheckoutBonus(seekBar.progress)
                        viewModel.setFinalTotalAmount(((args.totalAmount + viewModel.order.delivery_amount) - seekBar.progress))
                    }
                })
            } else {
                binding.checkoutCustomerBonusLayout.visibility = View.GONE
                binding.customerBonusSeekBar.max = 0
                binding.checkoutBonusTitle.text = "Оплата бонусами"

            }
        })


    }


    private fun onAddress() {
        binding.checkoutAddressCustomerCardView.setOnClickListener {
            val bottomSheetRequiredFragment = CheckoutAddressDialogFragment(viewModel)
            bottomSheetRequiredFragment.show(
                childFragmentManager,
                "checkoutAddressDialogFragment"
            )
        }

        viewModel.polygonInfo.observeForever { polygonInfo ->
            if (polygonInfo != null) {
                viewModel.order.balls = 0
                if (args.totalAmount > polygonInfo.minPriceFree) {
                    binding.checkoutDeliveryAmountText.text = "0"
                    binding.customerBonusSeekBar.isEnabled = true
                    binding.checkoutCustomerBonusLayout.visibility = View.VISIBLE
                    viewModel.setFinalTotalAmount((args.totalAmount - viewModel.order.balls))

                    val odds = args.totalAmount - polygonInfo.minPriceFree
                    if (odds > bonusAmount) {
                        binding.customerBonusSeekBar.max = bonusAmount
                    } else {
                        binding.customerBonusSeekBar.max = odds
                    }

                } else {
                    binding.checkoutDeliveryAmountText.text = polygonInfo.priceDelivery.toString()
                    val totalAmount = args.totalAmount + polygonInfo.priceDelivery
                    if (totalAmount > polygonInfo.minPriceFree) {
                        val odds = totalAmount - polygonInfo.minPriceFree
                        if (odds >= bonusAmount) {
                            binding.customerBonusSeekBar.max = bonusAmount
                        } else {
                            binding.customerBonusSeekBar.max = odds
                        }
                        binding.customerBonusSeekBar.isEnabled = true
                        binding.checkoutCustomerBonusLayout.visibility = View.VISIBLE
                    } else {
                        binding.customerBonusSeekBar.max = 0
                        binding.customerBonusSeekBar.isEnabled = false
                        binding.checkoutCustomerBonusLayout.visibility = View.GONE
                    }
                    viewModel.setFinalTotalAmount((args.totalAmount + polygonInfo.priceDelivery) - viewModel.order.balls)
                }

            } else {
                binding.checkoutCustomerBonusLayout.visibility = View.GONE
                binding.checkoutDeliveryAmountText.text = "0"
                viewModel.setFinalTotalAmount((args.totalAmount - viewModel.order.balls))
                //TODO(kebrick): Отображаем цену доставки = 0
            }
            binding.customerBonusSeekBar.progress = 0
        }

        viewModel.checkoutAddressLive.observe(viewLifecycleOwner, { address ->
            if (address != null) {
                if (address.uuid != null) {
                    viewModel.setAddress(address.uuid!!)

                }
                binding.checkoutAddressCustomerText.text =
                    "${address.city}, ${address.street}, ${address.home}"
            } else {
                binding.checkoutAddressCustomerText.text = ""
            }
//            onCheckFields()
        })
    }


    private fun onTypePay() {

        binding.checkoutTypePayCardView.setOnClickListener {
            val bottomSheetRequiredFragment =
                CheckoutTypePayDialogFragment(viewModel)
            bottomSheetRequiredFragment.show(
                childFragmentManager,
                "ShopDialogFragment"
            )
        }

        binding.checkoutTypePayText.text = "Наличными/картой"

        viewModel.checkoutTypePay.observe(viewLifecycleOwner, { checkoutTypePay ->
            if (checkoutTypePay != null) {
                if (checkoutTypePay.uuid == CheckoutViewModel.typePayCash.uuid) {
                    binding.checkoutTypePayText.text = "Наличными"
                    binding.checkoutDeliveryFromIL.visibility = View.VISIBLE
                } else if (checkoutTypePay.uuid == CheckoutViewModel.typePayCard.uuid) {
                    binding.checkoutTypePayText.text = "Картой"
                    binding.checkoutDeliveryFromIL.visibility = View.GONE
                    binding
                }
            } else {
                binding.checkoutTypePayText.text = "Наличными/картой"
                binding.checkoutDeliveryFromIL.visibility = View.GONE
            }

        })


        binding.checkoutDeliveryFromTIET.doOnTextChanged { text, _, _, _ ->
            viewModel.onSetDeliveryFrom(text.toString())
        }
    }


    private fun onDate() {
        binding.checkoutDateCardView.setOnClickListener {
            val bottomSheetRequiredFragment =
                CheckoutDateDialogFragment(
                    dateObserver,
                    atTheTime
                )
            bottomSheetRequiredFragment.show(
                childFragmentManager,
                "CheckoutDateDialogFragment"
            )
        }

        dateObserver.observe(viewLifecycleOwner, { date ->
            if (date != null) {
                viewModel.setDate(date, atTheTime.value!!)
                val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")
                val dateP = formatter.parse(date)
                val desiredFormat = DateTimeFormatter.ofPattern("EEE dd MMM HH:mm").format(dateP)
                binding.checkoutDateText.text = desiredFormat
//                if (date != "Ближайшее"){
//                    val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")
//                    val dateP = formatter.parse(date)
//                    val desiredFormat = DateTimeFormatter.ofPattern("EEE dd MMM HH:mm").format(dateP)
//                    binding.checkoutDateText.text = desiredFormat
//                }else{
//                    val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")
//                    val dateP = formatter.parse(date)
//                    val desiredFormat = DateTimeFormatter.ofPattern("EEE dd MMM HH:mm").format(dateP)
//                    binding.checkoutDateText.text = desiredFormat
//                }


            } else {
                binding.checkoutDateText.text = ""
            }
//            onCheckFields()
        })

    }

    private fun onCheckFields() {
        enableButton = true
        if (binding.checkoutTypeDeliveryRBDelivery.isChecked) {
            // Log.d( "onCheckFields1", "${viewModel.order.address} - ${viewModel.order.type_pay} - ${viewModel.order.type_delivery}")

            if (viewModel.order.address == null) {
                binding.checkoutAddressCustomerCardView.startAnimation(
                    AnimationUtils.loadAnimation(
                        context,
                        R.anim.shake
                    )
                )
                enableButton = false
            }
            if (viewModel.order.type_pay == null) {
                binding.checkoutTypePayCardView.startAnimation(
                    AnimationUtils.loadAnimation(
                        context,
                        R.anim.shake
                    )
                )
                enableButton = false
            }
            if (viewModel.order.date == null) {
                binding.checkoutDateCardView.startAnimation(
                    AnimationUtils.loadAnimation(
                        context,
                        R.anim.shake
                    )
                )
                enableButton = false
            }
        } else if (binding.checkoutTypeDeliveryRBPickup.isChecked) {
            // Log.d("onCheckFields2","${viewModel.order.shop} - ${viewModel.order.type_pay} - ${viewModel.order.type_delivery}")
            if (viewModel.order.shop == null) {
                binding.checkoutAddressShopCardView.startAnimation(
                    AnimationUtils.loadAnimation(
                        context,
                        R.anim.shake
                    )
                )
                enableButton = false
            }
            if (viewModel.order.type_pay == null) {
                binding.checkoutTypePayCardView.startAnimation(
                    AnimationUtils.loadAnimation(
                        context,
                        R.anim.shake
                    )
                )
                enableButton = false
            }
            if (viewModel.order.date == null) {
                binding.checkoutDateCardView.startAnimation(
                    AnimationUtils.loadAnimation(
                        context,
                        R.anim.shake
                    )
                )
                enableButton = false
            }
        }
    }


    private fun onSendOrderInfo() {
        binding.basketCheckoutMaterialButton.setOnClickListener {
            binding.basketCheckoutMaterialButton.isEnabled = false
            val imm =
                context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view?.windowToken, 0)
            onCheckFields()

            if (enableButton) {
//                onObserveOutputOrderInfo()
                viewModel.onSendOrderInfo(
                    requireView(),
                    binding.basketCheckoutMaterialButton,
                    object : OrderListener<Boolean> {
                        override fun onSend(created: Boolean, orderInfo: OrderCreated?) {
                            if (created) {
                                binding.includeProgressBar.progressBarLL.visibility = View.GONE
                                val view = layoutInflater.inflate(
                                    R.layout.custom_aler_dialog_order_info,
                                    null
                                )
                                val button =
                                    view.findViewById<MaterialButton>(R.id.alertDialogOnPopUpMaterialButton)
                                val alertDialogBuilder =
                                    AlertDialog.Builder(context, R.style.CustomAlertDialogOrderInfo)
                                alertDialogBuilder.setView(view)
                                alertDialogBuilder.setCancelable(false)
                                val alert = alertDialogBuilder.create()

                                button.setOnClickListener {
                                    alert.cancel()

                                    requireView().findNavController().popBackStack()
                                }

                                alert.show()
                                viewModel.onRemoveBasket()
                            } else {
                                binding.includeProgressBar.progressBarLL.visibility = View.GONE
                                val view = layoutInflater.inflate(
                                    R.layout.custom_alet_dialog_order_error,
                                    null
                                )
                                val buttonOk =
                                    view.findViewById<MaterialButton>(R.id.alertDialogReady)
                                val buttonCall =
                                    view.findViewById<MaterialButton>(R.id.alertDialogCallPhone)


                                view.findViewById<TextView>(R.id.alertDialogTitle).text =
                                    "Техническая ошибка"
                                view.findViewById<TextView>(R.id.alertDialogTextError).text =
                                    "Создание заказа временно не возможно, попробуйте через 10-15 сек или позвоните нам:\n${resources.getString(R.string.phone_org)}"


                                val intent =
                                    Intent(Intent.ACTION_DIAL, Uri.parse("tel:${resources.getString(R.string.phone_org)}"))
                                buttonCall.setOnClickListener {

//                                    if (intent.resolveActivity(requireActivity().packageManager) != null) {
//                                        startActivity(intent)
//                                    }

                                    startActivity(intent)

                                }
                                val alertDialogBuilder = AlertDialog.Builder(
                                    context,
                                    R.style.CustomAlertDialogOrderInfo
                                )
                                alertDialogBuilder.setView(view)
                                alertDialogBuilder.setCancelable(false)
                                val alert = alertDialogBuilder.create()

                                buttonOk.setOnClickListener {
                                    alert.cancel()
                                    binding.basketCheckoutMaterialButton.isEnabled = true
                                }

                                alert.show()
                            }
                        }

                    })
                binding.includeProgressBar.progressBarLL.visibility = View.VISIBLE

            } else {
                binding.basketCheckoutMaterialButton.isEnabled = true
            }
        }
    }

    private fun onObserveOutputOrderInfo() {
        viewModel.outputOrderInfo.observe(viewLifecycleOwner, { order ->
            if (order != null) {
                binding.includeProgressBar.progressBarLL.visibility = View.GONE
                val view = layoutInflater.inflate(R.layout.custom_aler_dialog_order_info, null)
                val button =
                    view.findViewById<MaterialButton>(R.id.alertDialogOnPopUpMaterialButton)
//                view.findViewById<TextView>(R.id.alertDialogTitle).text =
//                    "Заказ принят!"
//                view.findViewById<TextView>(R.id.alertDialogText).text =
//                    "Ваш заказ оформлен, если с вами не связался наш оператор в течение 10 минут, позвоните нам 40-720-40!"

                val alertDialogBuilder =
                    AlertDialog.Builder(context, R.style.CustomAlertDialogOrderInfo)
                alertDialogBuilder.setView(view)
                alertDialogBuilder.setCancelable(false)
                val alert = alertDialogBuilder.create()

                button.setOnClickListener {
                    alert.cancel()
//                    requireView().findNavController().navigate(

//                        CheckoutFragmentDirections.actionCheckoutFragmentToMain()
//                    )
                    requireView().findNavController().popBackStack()
                }

                alert.show()
                viewModel.onRemoveBasket()

            } else {
                binding.includeProgressBar.progressBarLL.visibility = View.GONE
                val view = layoutInflater.inflate(
                    R.layout.custom_alet_dialog_customer_error,
                    null
                )
                val buttonOk = view.findViewById<MaterialButton>(R.id.alertDialogReady)
                view.findViewById<TextView>(R.id.alertDialogTitle).text = "Техническая ошибка"
                view.findViewById<TextView>(R.id.alertDialogTextError).text =
                    "Создание заказа временно не возможно, обратитесь к оператору по номеру в контактах для оформления заказа!"
                val alertDialogBuilder = AlertDialog.Builder(
                    context,
                    R.style.CustomAlertDialogOrderInfo
                )
                alertDialogBuilder.setView(view)
                alertDialogBuilder.setCancelable(false)
                val alert = alertDialogBuilder.create()

                buttonOk.setOnClickListener {
                    alert.cancel()
                    binding.basketCheckoutMaterialButton.isEnabled = true
                }

                alert.show()
            }


        })
    }


    override fun onDestroyView() {
        super.onDestroyView()
        activity?.let { actv ->
            actv.findViewById<BottomNavigationView>(R.id.nav_view).visibility = View.VISIBLE
        }

    }


}


