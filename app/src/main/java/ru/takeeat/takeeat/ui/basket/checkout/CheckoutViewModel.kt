package ru.takeeat.takeeat.ui.basket.checkout

import android.app.Application
import android.view.Gravity
import android.view.View
import android.widget.FrameLayout
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.material.button.MaterialButton
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import ru.takeeat.takeeat.MapsUtils
import ru.takeeat.takeeat.network.mtesapi.interfaces.Result
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.db.AppDataBase
import ru.takeeat.takeeat.db.core.ShopEntity
import ru.takeeat.takeeat.db.customer.Address
import ru.takeeat.takeeat.db.customer.Customer
import ru.takeeat.takeeat.network.mtesapi.core.*
import ru.takeeat.takeeat.network.mtesapi.customer.CustomerAPI
import ru.takeeat.takeeat.network.mtesapi.order.OrderAPI
import ru.takeeat.takeeat.ui.basket.interfaces.OrderListener
import java.util.*

class CheckoutViewModel(val db: AppDataBase, application: Application) :
    AndroidViewModel(application) {
    private var viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    private val _serviceCore = CoreAPI()
    private val _serviceOrder = OrderAPI()

    private var _order: Order = Order()
    val order get() = _order
    private var _date: String = "Ближайшее"
    private var _dateTime: String = "Временной заказ к "


    private val _addresses = db.customerDAO().getAddressAll()
    val addresses get() = _addresses

    private val _checkoutAddressLive = MutableLiveData<Address?>()
    val checkoutAddressLive: LiveData<Address?> get() = _checkoutAddressLive
    private var _checkoutAddress: Address? = null
    fun onSetCheckoutAddress(address: Address) {
        _checkoutAddress = address
        _order.address = address.uuid
        _checkoutAddressLive.value = address
    }

    fun onGetServerAddresses(customerUuid: String) {
        if (_customer.value != null) {
            uiScope.launch {
                val getAddresses = MutableLiveData<List<Address>>()
                CustomerAPI().callGetAddresses(customerUuid, getAddresses)
                getAddresses.observeForever { listAddresses ->
                    if (!listAddresses.isNullOrEmpty()) {
                        uiScope.launch {
                            db.customerDAO().removeAddressAll()

                            db.customerDAO().removeAddressAll()
                            listAddresses.forEach { addressS ->
                                db.customerDAO().insertAddress(addressS)
                            }
                        }
                        return@observeForever
                    }
                }
            }
        }

    }

    fun updateAddressInfo() {
        if (_checkoutAddress != null) {
            uiScope.launch {
                db.customerDAO().updateAddress(_checkoutAddress!!)
            }
        }
    }


    private val _polygonInfo = MutableLiveData<MapsUtils.PolygonInfo>()
    val polygonInfo: LiveData<MapsUtils.PolygonInfo> get() = _polygonInfo
    fun onSetPolygonInfo(polygonInfo: MapsUtils.PolygonInfo) {
        _polygonInfo.value = polygonInfo
    }


    private var _comment: String = ""
    private var _comment_shop: String = ""
    private var _commentDeliveryFrom: String = ""
    fun onSetComment(comment: String) {
        _order.comment = comment
    }

    fun onSetDeliveryFrom(str: String) {
        _commentDeliveryFrom = str
    }


    private val _customer = db.customerDAO().getCheckCustomer()
    val customer get() = _customer

    private val _totalAmount = MutableLiveData<Int>()
    val totalAmount: LiveData<Int> get() = _totalAmount
    fun setFinalTotalAmount(i: Int) {
        _totalAmount.value = i
    }


    private val _typePay = MutableLiveData<List<TypePay>>()
    val typePay: LiveData<List<TypePay>> get() = _typePay
    private val _checkoutTypePay: MutableLiveData<TypePay> = MutableLiveData()
    val checkoutTypePay: LiveData<TypePay> get() = _checkoutTypePay
    fun onSetCheckoutTypePay(typePay: TypePay) {
        _order.type_pay = typePay.uuid
        _checkoutTypePay.value = typePay
    }


    private val _typeDelivery = MutableLiveData<List<TypeDelivery>>()
    val typeDelivery: LiveData<List<TypeDelivery>> get() = _typeDelivery

    private var _checkoutTypeDelivery: String? = null
    val checkoutTypeDelivery get() = _checkoutTypeDelivery
    fun onSetCheckoutTypeDelivery(idTypeDelivery: String) {
        _checkoutTypeDelivery = idTypeDelivery
        _order.type_delivery = idTypeDelivery
    }


    private var _checkoutBonus = 0
    fun setCheckoutBonus(count: Int) {
        _checkoutBonus = count
        _order.balls = count
    }

    private var _shop: ShopEntity? = null
    fun setShop(shop: ShopEntity?) {
        if (shop == null) {
            _order.shop = null
            _checkoutAddress = null
            _checkoutAddressLive.value = null
            _shop = null
            _comment_shop = ""
        } else {
            _order.shop = shop.uuid
            _order.min_shipping_cost = 0
            _order.delivery_amount = 0
            _shop = shop
            _comment_shop = ": Самовывоз ${_shop!!.name}, ${_shop!!.address}"
        }
    }

    fun setAddress(uuidAddress: String?) {
        _order.address = uuidAddress
    }

    fun setDeliveryAmount(amount: Int) {
        _order.delivery_amount = amount
    }

    fun setMinShippingCost(amount: Int) {
        _order.min_shipping_cost = amount
    }

    fun setTotalAmount(amount: Int) {
        _order.total_amount = amount
    }

    fun setCustomer(customer: Customer) {
        _order.customer = customer.uuid
    }

    fun setDate(date: String,atTheTime: Int) {
        if (atTheTime == 1) {
            _date = date
            _order.date = date
            _comment = ""
        } else if (atTheTime == 2) {
            _date = ": $_dateTime $date"
            _comment = ": $_dateTime $date"
            _order.date = date
        }


    }

    companion object {
        var typeDeliveryDelivery = TypeDelivery(
            uuid = "528b903f-ccd7-48fc-a4f8-554672e271a8",
            name = "Доставка"
        )
        var typeDeliveryPickup = TypeDelivery(
            uuid = "5e908d20-7602-44cb-b178-208001281839",
            name = "Самовывоз"
        )

        var typePayCash = TypePay(
            uuid = "4fcc1580-2460-4790-8b24-45c6d11581fe",
            name = "Наличные",
            bonus_pay = false
        )
        var typePayCard = TypePay(
            uuid = "5ccbf48b-53c1-4add-87e7-2bd5553c117d",
            name = "Картой курьеру",
            bonus_pay = false
        )
    }


    private val _outputOrderInfo = MutableLiveData<OrderCreated>()
    val outputOrderInfo: LiveData<OrderCreated> get() = _outputOrderInfo
    fun onSendOrderInfo(view: View, button: MaterialButton, outStatus: OrderListener<Boolean>): Boolean {

        if (_order.date == null) {
            Snackbar.make(view, "Не выбрано \"Время\"", Snackbar.LENGTH_LONG).apply {
                setBackgroundTint(view.resources.getColor(R.color.AccentColor))
                setTextColor(view.resources.getColor(R.color.white))

                val params = view.layoutParams as FrameLayout.LayoutParams
                params.topMargin = 118
                params.gravity = Gravity.CENTER_HORIZONTAL or Gravity.TOP
                show()
            }
            button.isEnabled = false
            button.alpha = 0.5F
            return false
        } else if (_order.type_pay == null) {
            Snackbar.make(view, "Не выбран тип оплаты", Snackbar.LENGTH_LONG).apply {
                setBackgroundTint(view.resources.getColor(R.color.AccentColor))
                setTextColor(view.resources.getColor(R.color.white))

                val params = view.layoutParams as FrameLayout.LayoutParams
                params.topMargin = 118
                params.gravity = Gravity.CENTER_HORIZONTAL or Gravity.TOP
                show()
            }
            button.isEnabled = false
            button.alpha = 0.5F
            return false
        } else if (_order.type_delivery == null) {
            Snackbar.make(view, "Не выбран тип доставки", Snackbar.LENGTH_LONG).apply {
                setBackgroundTint(view.resources.getColor(R.color.AccentColor))
                setTextColor(view.resources.getColor(R.color.white))

                val params = view.layoutParams as FrameLayout.LayoutParams
                params.topMargin = 118
                params.gravity = Gravity.CENTER_HORIZONTAL or Gravity.TOP
                show()
            }
            button.isEnabled = false
            button.alpha = 0.5F
            return false
        }

//        _order.comment =
//            "${_order.comment} $_comment_shop, $_comment, \n Сдача с: $_commentDeliveryFrom"
        if (!_comment_shop.isNullOrEmpty()){
            _order.comment = "${order.comment}, $_comment_shop"
        }
        if (!_comment.isNullOrEmpty()){
            _order.comment = "${order.comment}, $_comment"
        }
        if (!_commentDeliveryFrom.isNullOrEmpty()){
            _order.comment = "${order.comment},\nСдача с: $_commentDeliveryFrom"
        }
//        val formatter = DateTimeFormatter.ofPattern("EEE dd MMM HH:mm")
//            .withLocale(Locale("ru"))
//            .withZone(ZoneId.from(ZoneOffset.UTC))
//        val dateP = formatter.parse(_order.date)
//        val desiredFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm").format(dateP)
//        _order.date = desiredFormat




//        if (_date == "Ближайшее" || _order.date == "Ближайшее") {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                var current = LocalDateTime.now()
//                current = current.plusHours(1)
//                val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")
//                _order.date = current.format(formatter)
//            } else {
//                var date = Date()
//                date.hours = date.hours + 1
//                val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm")
//                _order.date = formatter.format(date)
//            }
//
//        }

        uiScope.launch {
            val customer = _customer.value
            if (customer != null) {
                customer.balance = customer.balance - _order.balls
                db.customerDAO().update(customer)
            }

        }
//        _serviceOrder.callAttachOrder(_order, _outputOrderInfo, object: OrderListener<Boolean>{
//            override fun onSend(created: Boolean,orderInfo: OrderCreated?) {
//                if (created){
//                    outStatus.onSend(created = true, orderInfo = orderInfo)
//                }
//            }
//
//        })
        _serviceOrder.callAttachOrder(_order, _outputOrderInfo, outStatus)


        return true
    }

    fun onRemoveBasket() {
        uiScope.launch {
            db.modifierOrderDAO().removeAll()
            val produts = db.productOrderDAO().getAllNoLive()
            produts.forEach { productOrder ->
                val gms = db.groupModifierDAO().getByProduct(productOrder.id)
                gms.forEach{ groupModifier ->
                    db.modifierStrDAO().removeByMGroupOrder(id = groupModifier.id)
                }
                db.groupModifierDAO().removeAllByProductOrder(id = productOrder.id)
            }
            db.productOrderDAO().removeAll()
        }
    }
    private val _shops = db.shopDAO().getAll()
    val shops get() = _shops
    fun getShops(result: Result<Boolean>) {
        val _shopsN: MutableLiveData<List<Shop>> =  MutableLiveData<List<Shop>>()
        _serviceCore.callShops(_shopsN, object: Result<Int> {
            override fun result(code: Int) {
                if(code == ERR_GET_SHOPS || code == EMPTY_SHOPS){
                    result.result(false)
                    uiScope.launch {

                        db.shopDAO().removeAll()
                    }
                }else{
                    result.result(true)
                }
            }
        })
        _shopsN.observeForever{ listShop ->
            if (listShop != null){
                if (!listShop.isNullOrEmpty()){
                    uiScope.launch {
                        db.shopDAO().removeAll()
                        listShop.forEach{ shop ->
                            db.shopDAO().insert(
                                ShopEntity(
                                    uuid=shop.uuid,
                                    pickup = shop.pickup,
                                    name = shop.name,
                                    phone = shop.phone,
                                    date_work = shop.date_work,
                                    time_work = shop.time_work,
                                    address = shop.address,
                                    latitude = shop.latitude,
                                    longitude = shop.longitude,
                                )
                            )
                        }

                        return@launch

                    }
                }
                return@observeForever

            }else{
                return@observeForever
            }
        }
    }
    private fun onTypeDelivery(){
        _serviceCore.callTypeDelivery(_typeDelivery)

        _typeDelivery.observeForever{ listTypeDelivery ->
            if (!listTypeDelivery.isNullOrEmpty()) {
                if (listTypeDelivery.count() == 2){
                    listTypeDelivery.forEach { typeDelivery ->

                        if (typeDelivery.pickup != null){
                            if (typeDelivery.pickup){
                                typeDeliveryPickup.uuid = typeDelivery.uuid
                            }else{
                                typeDeliveryDelivery.uuid = typeDelivery.uuid

                            }
                        }else{
                            if (typeDeliveryDelivery.name.lowercase() in typeDelivery.name.lowercase()){
                                typeDeliveryDelivery.uuid = typeDelivery.uuid
                            }else if (typeDeliveryPickup.name.lowercase() in typeDelivery.name.lowercase()){
                                typeDeliveryPickup.uuid = typeDelivery.uuid

                            }
                        }
                    }
                }
                return@observeForever
            }
        }
    }

    private fun onTypePay(){
        _serviceCore.callTypePay(_typePay)
        _typePay.observeForever{ listTypePay ->
            if (!listTypePay.isNullOrEmpty()) {
                if (listTypePay.count() == 2){
                    listTypePay.forEach { typePay ->
                        if (typePayCard.name.lowercase() in typePay.name.lowercase()){
                            typePayCard.uuid = typePayCard.uuid
                        }else if (typePayCash.name.lowercase() in typePay.name.lowercase()){
                            typePayCash.uuid = typePay.uuid

                        }

                    }
                }
                return@observeForever
            }
        }
    }
    init {

        onTypeDelivery()

        onTypePay()




        uiScope.launch {
            val productDB = db.productOrderDAO().getAllNoLive()
            productDB.forEach { productOrder ->
                var product: ProductOrder =
                    ProductOrder(
                        uuid = productOrder.uuid,
                        name = productOrder.name,
                        price = productOrder.priceProduct.toInt(),
                        amount = productOrder.amount,
                        modifiers = mutableListOf<ModifierOrder>()
                    )
                val modifiersOrderDB =
                    db.modifierOrderDAO().getProductModifiersNoLive(productOrder.id)
                modifiersOrderDB.forEach { modifierOrder ->
                    val modifier = ModifierOrder(
                        uuid = modifierOrder.uuid,
                        name = modifierOrder.name,
                        price = modifierOrder.price.toInt(),
                        amount = modifierOrder.amount,
                        group_name = modifierOrder.group_name,
                        group_uuid = modifierOrder.group_uuid
                    )
                    product.modifiers.add(modifier)
                }
                if (_order.products != null) {
                    _order.products!!.add(product)
                } else {
                    _order.products = mutableListOf(product)
                }

            }

        }
    }

}

