package ru.takeeat.takeeat.ui.basket.checkout

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.takeeat.takeeat.db.AppDataBase

class CheckoutViewModelFactory(private val db: AppDataBase, private val application: Application): ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CheckoutViewModel::class.java)) {
            return CheckoutViewModel(db,application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
