package ru.takeeat.takeeat.ui.basket.dialogs.address

import android.os.Bundle
import android.os.Handler
import android.view.Gravity
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.navigation.findNavController
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.snackbar.Snackbar
import com.google.maps.android.data.kml.KmlLayer
import ru.takeeat.takeeat.MapsUtils
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.databinding.FragmentCheckoutAddressDialogBinding
import ru.takeeat.takeeat.db.customer.Address
import ru.takeeat.takeeat.ui.basket.adapters.CheckoutAddressAdapter
import ru.takeeat.takeeat.ui.basket.checkout.CheckoutFragmentDirections
import ru.takeeat.takeeat.ui.basket.checkout.CheckoutViewModel
import ru.takeeat.takeeat.ui.basket.interfaces.CheckoutAddressClickListener

//class CheckoutAddressDialogFragment(
//    val viewModel: CheckoutViewModel
//
//) : BottomSheetDialogFragment() {
//
//    private var _binding: FragmentCheckoutAddressDialogBinding? = null
//
//    private val binding get() = _binding!!
//    private lateinit var mapsUtils: MapsUtils
//
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        mapsUtils = MapsUtils(requireContext())
//
//        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialog)
//    }
//
//    //    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
////        val dialog = BottomSheetDialog(requireContext(), theme)
////        dialog.behavior.state = BottomSheetBehavior.STATE_EXPANDED
////        return dialog
////    }
//    override fun onCreateView(
//        inflater: LayoutInflater, container: ViewGroup?,
//        savedInstanceState: Bundle?
//    ): View {
//
//        _binding = FragmentCheckoutAddressDialogBinding.inflate(inflater, container, false)
//        onAddresses()
//
//        binding.checkoutAddressDialogMaterialButton.setOnClickListener {
//            onDestroy()
//            dismiss()
//        }
//
//
//        return binding.root
//    }
//
//    private fun onAddresses() {
//
//
//        viewModel.addresses.observe(viewLifecycleOwner, { listAddress ->
//            val adapter = CheckoutAddressAdapter(
//                listAddress,
//                object : CheckoutAddressClickListener {
//                    override fun onCheckoutAddress(address: Address) {
//                        viewModel.onSetCheckoutAddress(address)
//                        if ((address.lat != null && address.lng != null) && (address.lat ?: 0.00 >= 0.0 && address.lng ?: 0.00 >= 0.00)) {
//
//                            val polyInfo = mapsUtils.onCheckLatLngInPolygons(
//                                LatLng(
//                                    address.lat!!,
//                                    address.lng!!
//                                )
//                            )
//                            if (polyInfo != null) {
//                                viewModel.onSetPolygonInfo(polyInfo)
//                            } else {
//                                val polyInfo1 = mapsUtils.onCheckLatLngInPolygons(
//                                    LatLng(
//                                        address.lat!!,
//                                        address.lng!!
//                                    )
//                                )
//                                if (polyInfo1 != null) {
//                                    viewModel.onSetPolygonInfo(polyInfo1)
//                                }
//                            }
//                        } else {
//                            val latLng =
//                                mapsUtils.getLatLngAddress("${address.city}, ${address.street}, ${address.home}")
//                            if (latLng != null) {
//                                address.lat = latLng.latitude
//                                address.lng = latLng.longitude
//                                val polyInfo = mapsUtils.onCheckLatLngInPolygons(
//                                    LatLng(
//                                        address.lat!!,
//                                        address.lng!!
//                                    )
//                                )
//                                if (polyInfo != null) {
//                                    viewModel.onSetPolygonInfo(polyInfo)
//                                }
//                            }
//
//                            viewModel.updateAddressInfo()
//                        }
//                    }
//                })
//            binding.checkoutAddressCustomerRecyclerView.adapter = adapter
//        })
//
//        binding.checkoutAddressAttachMB.setOnClickListener {
//            binding.checkoutAddressAttachMB.isEnabled = false
//            activity?.findNavController(requireParentFragment().id)?.navigate(
//                CheckoutFragmentDirections.actionCheckoutFragmentToMapsCheckoutFragment()
//            )
//            Handler().postDelayed({
//                binding.checkoutAddressAttachMB.isEnabled = true
//            }, 3000)
//        }
//
//    }
//
//}

class CheckoutAddressDialogFragment(
    val viewModel: CheckoutViewModel

) : BottomSheetDialogFragment(), OnMapReadyCallback {

    private var _binding: FragmentCheckoutAddressDialogBinding? = null

    private val binding get() = _binding!!
    private lateinit var mapsUtils: MapsUtils
    private var map: GoogleMap? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mapsUtils = MapsUtils(requireContext())

        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialog)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentCheckoutAddressDialogBinding.inflate(inflater, container, false)

        initGoogleMap(savedInstanceState)
        binding.checkoutAddressDialogMaterialButton.setOnClickListener {
            onDestroy()
            dismiss()
        }


        return binding.root
    }

    private fun initGoogleMap(savedInstanceState: Bundle?) {

        val mapView = binding.mapView
        var mapViewBundle: Bundle? = null
        if (savedInstanceState != null) {
            val MAPVIEW_BUNDLE_KEY = resources.getString(R.string.gapikey)
            mapViewBundle = savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY)
        }

        mapView.onCreate(mapViewBundle)
        mapView.getMapAsync(this)
    }

    private fun onAddresses(map: GoogleMap) {
        val layer = KmlLayer(map, R.raw.geo, context)
        layer.addLayerToMap()
        viewModel.addresses.observe(viewLifecycleOwner) { listAddress ->
            val adapter = CheckoutAddressAdapter(
                listAddress,
                selectAddress = viewModel.order.address,
                object : CheckoutAddressClickListener {
                    override fun onCheckoutAddress(address: Address) {

                        if ((address.lat != null && address.lng != null) && (address.lat ?: 0.00 >= 0.0 && address.lng ?: 0.00 >= 0.00)) {

                            val polyInfo = mapsUtils.checkPolygon(
                                layer,
                                LatLng(
                                    address.lat!!,
                                    address.lng!!
                                )

                            )
                            if (polyInfo != null) {
                                viewModel.onSetCheckoutAddress(address)
                                viewModel.onSetPolygonInfo(polyInfo)
                            } else {
                                val polyInfo1 = mapsUtils.checkPolygon(layer,
                                    LatLng(
                                        address.lat!!,
                                        address.lng!!
                                    )
                                )
                                if (polyInfo1 != null) {
                                    viewModel.onSetCheckoutAddress(address)
                                    viewModel.onSetPolygonInfo(polyInfo1)
                                } else {
                                    Snackbar.make(
                                        requireView(),
                                        "Адрес не входит в зону доставки!",
                                        Snackbar.LENGTH_LONG
                                    ).apply {
                                        setBackgroundTint(resources.getColor(R.color.AccentColor))
                                        setTextColor(resources.getColor(R.color.white))

                                        val params =
                                            view.layoutParams as CoordinatorLayout.LayoutParams
                                        params.topMargin = 118
                                        params.gravity = Gravity.CENTER_HORIZONTAL or Gravity.TOP
                                        show()
                                    }
                                }
                            }
                        } else {
                            val latLng =
                                mapsUtils.getLatLngAddress("${address.city}, ${address.street}, ${address.home}")
                            if (latLng != null) {
                                address.lat = latLng.latitude
                                address.lng = latLng.longitude
                                val polyInfo = mapsUtils.checkPolygon(layer,
                                    LatLng(
                                        address.lat!!,
                                        address.lng!!
                                    )
                                )
                                if (polyInfo != null) {
                                    viewModel.onSetCheckoutAddress(address)
                                    viewModel.onSetPolygonInfo(polyInfo)
                                } else {
                                    Snackbar.make(
                                        requireView(),
                                        "Адрес не входит в зону доставки!",
                                        Snackbar.LENGTH_LONG
                                    ).apply {
                                        setBackgroundTint(resources.getColor(R.color.AccentColor))
                                        setTextColor(resources.getColor(R.color.white))

                                        val params =
                                            view.layoutParams as CoordinatorLayout.LayoutParams
                                        params.topMargin = 118
                                        params.gravity = Gravity.CENTER_HORIZONTAL or Gravity.TOP
                                        show()
                                    }
                                }
                            }

                            viewModel.updateAddressInfo()
                        }
                    }
                })
            binding.checkoutAddressCustomerRecyclerView.adapter = adapter
        }

        binding.checkoutAddressAttachMB.setOnClickListener {
            binding.checkoutAddressAttachMB.isEnabled = false
            activity?.findNavController(requireParentFragment().id)?.navigate(
                CheckoutFragmentDirections.actionCheckoutFragmentToMapsCheckoutFragment()
            )
            Handler().postDelayed({
                binding.checkoutAddressAttachMB.isEnabled = true
            }, 3000)
        }

    }

    override fun onMapReady(map: GoogleMap) {
        onAddresses(map = map)
    }
}


//    override fun onDestroyView() {
//        super.onDestroyView()
//        _binding = null
//    }
