package ru.takeeat.takeeat.ui.basket.dialogs.datetime

import android.app.DatePickerDialog
import android.os.Build
import android.os.Bundle
import android.util.Log
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.databinding.FragmentCheckoutDateDialogBinding
import ru.takeeat.takeeat.ui.basket.dialogs.shop.ShopDialogFragment
import ru.takeeat.takeeat.ui.basket.dialogs.typepay.CheckoutTypePayDialogFragment
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*


class CheckoutDateDialogFragment(
    val dateObserver: MutableLiveData<String?>,
    var atTheTime: MutableLiveData<Int> ,
): BottomSheetDialogFragment() {

    private var _binding: FragmentCheckoutDateDialogBinding? = null
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.AppBottomSheetDialogTheme)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCheckoutDateDialogBinding.inflate(inflater, container, false)
        val hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY)
        Log.d("DateTimePicker","$hour")
        if (hour >= 23 || hour < 11  ){
            binding.checkoutAddressCustomerTitleNoWork.visibility = View.VISIBLE
            binding.checkoutDateRadioButton1.isEnabled = false
        }else{
            binding.checkoutAddressCustomerTitleNoWork.visibility = View.GONE
            binding.checkoutDateRadioButton1.isEnabled = true
        }
        if (atTheTime.value == 1){
            binding.checkoutDateRadioButton1.isChecked = true
        }else if (atTheTime.value == 2){
            binding.checkoutDateRadioButton2.isChecked = true
        }
        onCheckoutDate()
        return binding.root
    }

    private fun onCheckoutDate(){

//        binding.checkoutDateRadioGroup.setOnCheckedChangeListener { _, checkedId ->
//            Log.d("DateTimePickerValue","$checkedId")
//            if (binding.checkoutDateRadioButton1.id == checkedId){
//                // Как можно быстрее
//
//                dateObserver.value = "Ближайшее"
//                binding.checkoutDateToTimeText.visibility = View.GONE
//
//            }else if (binding.checkoutDateRadioButton2.id == checkedId){
//
//                val bottomSheetRequiredFragment =
//                    CheckoutDatePickerDialogFragment(dateObserver)
//                bottomSheetRequiredFragment.show(
//                    childFragmentManager,
//                    "ShopDialogFragment"
//                )
//                binding.checkoutDateToTimeText.visibility = View.VISIBLE
//                dateObserver.observe(viewLifecycleOwner,{ dateTime ->
//                    if (dateTime != "Ближайшее"){
//                        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")
//                        val dateP = formatter.parse(dateTime)
//                        val desiredFormat = DateTimeFormatter.ofPattern("EEE dd MMM HH:mm").format(dateP)
//
//                        binding.checkoutDateToTimeText.text = desiredFormat
//                        // Log.d("DateTimePickerValue","$dateTime")
//                    }
//
//                })
//            }
//
//        }
        binding.checkoutDateRadioButton1.setOnClickListener {
            atTheTime.value = 1
            var current = LocalDateTime.now()
            current = current.plusHours(1)
            val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")

            dateObserver.value = current.format(formatter)
            binding.checkoutDateToTimeText.visibility = View.GONE
        }
        binding.checkoutDateRadioButton2.setOnClickListener {
            val dateObserverLocal = MutableLiveData<String?>()
            atTheTime.value = 2
            val bottomSheetRequiredFragment =
                CheckoutDatePickerDialogFragment(dateObserverLocal)
            bottomSheetRequiredFragment.show(
                childFragmentManager,
                "ShopDialogFragment"
            )
            binding.checkoutDateToTimeText.visibility = View.VISIBLE
            dateObserverLocal.observe(viewLifecycleOwner,{ dateTime ->
                if (!dateTime.isNullOrEmpty()){
                    val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:m")
                    val dateP = formatter.parse(dateTime)
                    val desiredFormat = DateTimeFormatter.ofPattern("EEE dd MMM HH:mm").format(dateP)

                    binding.checkoutDateToTimeText.text = desiredFormat
                    dateObserver.value = dateTime
                }

                // Log.d("DateTimePickerValue","$dateTime")


            })

        }
        binding.checkoutAddressDialogMaterialButton.setOnClickListener {
            this.dismiss()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}