package ru.takeeat.takeeat.ui.basket.dialogs.datetime

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.lifecycle.MutableLiveData
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.databinding.FragmentCheckoutDatePickerBinding
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter
import java.util.*


class CheckoutDatePickerDialogFragment(
    val dateObserver: MutableLiveData<String?>
) : BottomSheetDialogFragment() {

    private var _binding: FragmentCheckoutDatePickerBinding? = null
    private val binding get() = _binding!!
    private var time: String? = null
    private var date: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.AppBottomSheetDialogTheme)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentCheckoutDatePickerBinding.inflate(inflater, container, false)
        onPicker()
        onButtonClickListener()
        return binding.root
    }

    private fun onPicker() {
        val dateD = Date()
        val c = Calendar.getInstance()
        val hour = c.get(Calendar.HOUR_OF_DAY)
        val datePicker = binding.checkoutDateDatePicker
        date = "${dateD.year+1900}-${changeNumber(dateD.month+1)}-${changeNumber(dateD.date)}"


        datePicker.minDate = System.currentTimeMillis()
        if(hour >= 23){
            datePicker.minDate = System.currentTimeMillis() + 3600000
            date = "${dateD.year+1900}-${changeNumber(dateD.month+1)}-${changeNumber(dateD.date +1)}"
        }
        datePicker.maxDate = System.currentTimeMillis() + 15778800000
        datePicker.firstDayOfWeek = Calendar.MONDAY


//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            datePicker.setOnDateChangedListener { _, year, monthOfYear, dayOfMonth ->
////                val toast = Toast.makeText(context, "$dayOfMonth.${monthOfYear+1}.$year", Toast.LENGTH_SHORT)
////                toast.setGravity(Gravity.TOP, 0,0)
////                toast.setMargin(0F, 80F)
////                toast.show()
//                date = "$year-${monthOfYear + 1}-$dayOfMonth"
//            }
//        }else{
//            val today = Calendar.getInstance()
//            datePicker.init(today.get(Calendar.YEAR), today.get(Calendar.MONTH),
//                today.get(Calendar.DAY_OF_MONTH)
//
//            ) { view, year, month, day ->
//                val month = month + 1
//                val msg = "You Selected: $day/$month/$year"
//                Log.d("DatePickerValue", msg)
//            }
//        }
        val today = Calendar.getInstance()
        datePicker.init(today.get(Calendar.YEAR), today.get(Calendar.MONTH),
            today.get(Calendar.DAY_OF_MONTH)

        ) { view, year, month, day ->
            val month = month + 1
//            val msg = "You Selected: $day/$month/$year"
//            Log.d("DateTimePickerValueDate", msg)
        }

        binding.checkoutDateTimePicker.setIs24HourView(true)
        if(dateD.hours >= 23 || dateD.hours <=11){
            binding.checkoutDateTimePicker.hour = 11
            binding.checkoutDateTimePicker.minute = 0
            time = "${11}:00"
        }else{
            binding.checkoutDateTimePicker.hour = dateD.hours + 1
            binding.checkoutDateTimePicker.minute = dateD.minutes
            time = "${dateD.hours + 1}:${changeNumber(dateD.minutes)}"
        }

        binding.checkoutDateTimePicker.setOnTimeChangedListener { _, hour, minute ->
            val date = Date()
            Log.d("DateTimePickerValueDate", "${hour} - ${date.hours} - ${Calendar.getInstance().get(Calendar.DAY_OF_MONTH)} < ${binding.checkoutDateDatePicker.dayOfMonth}")
//            if (hour >= 23 || hour < 11) {
//                if ((date.hours >= 23 || date.hours <= 11) && ((date.hours <= 11 && date.minutes == 0) || date.hours <= 11)) {
            if ((hour >= 23 && minute >= 0) || hour < 11) {
                if (((date.hours >= 23 && minute >= 0) || date.hours <= 11)  || ((date.hours <= 11 && date.minutes == 0) || date.hours <= 11)) {
                    binding.checkoutDateTimePicker.hour = 11
                    time = "11:00"
                } else {
                    binding.checkoutDateTimePicker.hour = date.hours
                    binding.checkoutDateTimePicker.minute = date.minutes
                    time = "${date.hours}:${changeNumber(date.minutes)}"
                }

            } else if ((hour == date.hours) && (Calendar.getInstance().get(Calendar.DAY_OF_MONTH) == binding.checkoutDateDatePicker.dayOfMonth)){
                binding.checkoutDateTimePicker.hour = hour + 1
                time = "${hour + 1}:${changeNumber(minute)}"

            }
            else if ((hour < date.hours) && (Calendar.getInstance().get(Calendar.DAY_OF_MONTH) < binding.checkoutDateDatePicker.dayOfMonth)){
                time = "${hour}:${changeNumber(minute)}"

            }else{
                time = "${hour}:${changeNumber(minute)}"

            }

        }
        // Отслеживание выбора даты
        binding.checkoutDateDatePicker.setOnDateChangedListener { dateP, year, month, day ->
            val day1 = changeNumber(day)
            val month1 = changeNumber(month+1)
            date = "$year-$month1-$day1"
            if (dateP.dayOfMonth == Calendar.getInstance().get(Calendar.DAY_OF_MONTH)){
                binding.checkoutDateTimePicker.hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY)
                date = "$year-$month1-$day1"
            }
//            val toast = Toast.makeText(context, date, Toast.LENGTH_SHORT)
//            toast.setGravity(Gravity.TOP, 0,0)
//            toast.setMargin(0F, 80F)
//            toast.show()
        }

    }

    private fun onButtonClickListener(){

        binding.checkoutDateCancel.setOnClickListener {
            dateObserver.value = null
            this.dismiss()
        }

        binding.checkoutDateReady.setOnClickListener {

            val formatter =  DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")
            val formatDate = formatter.parse("$date $time")
            val currentDate = formatter.format(formatDate)
            dateObserver.value = currentDate
//            Log.d("DateTimePickerValue","$date $time --- $currentDate")
            this.dismiss()
        }
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun changeNumber(num: Int): String{
        when(num){
            in 0..9 -> {return "0$num" }
            else -> {return "$num"}
        }
    }

}