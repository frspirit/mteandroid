package ru.takeeat.takeeat.ui.basket.dialogs.shop

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.databinding.FragmentShopDialogListBinding
import ru.takeeat.takeeat.db.AppDataBase
import ru.takeeat.takeeat.db.core.ShopEntity

import ru.takeeat.takeeat.ui.basket.adapters.ShopAdapter
import ru.takeeat.takeeat.ui.basket.interfaces.ShopClickListener


class ShopDialogFragment(
    val shopEntityObserver: MutableLiveData<ShopEntity>
) : BottomSheetDialogFragment() {

    private var _binding: FragmentShopDialogListBinding? = null
    private val binding get() = _binding!!
    private var db:AppDataBase? = null
    private lateinit var navController: NavController
    private lateinit var viewModel: ShopDialogViewModel
    private lateinit var viewModelFactory: ShopDialogViewModelFactory
    private var shopEntityObserverThen: MutableLiveData<ShopEntity>  = MutableLiveData<ShopEntity>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val application = requireNotNull(this.activity).application
        db = AppDataBase.getInstance(application)
        viewModelFactory = ShopDialogViewModelFactory(db!!, application)
        viewModel = ViewModelProvider(this, viewModelFactory).get(ShopDialogViewModel::class.java)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialog)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        navController = findNavController()
        _binding = FragmentShopDialogListBinding.inflate(inflater, container, false)

        onSetShopInfo()
//        onCheckedShop()
        onShopMaps()

        binding.checkoutShopDialogMaterialButton.setOnClickListener {
            dismiss()
        }
        return binding.root

    }

    private fun onSetShopInfo(){
        viewModel.shops.observe(viewLifecycleOwner,{ listShop ->
            val listShopEntityPickup: MutableList<ShopEntity> = mutableListOf()
            listShop.forEach { shop ->
                if (shop.pickup){
                    listShopEntityPickup.add(shop)
                }
            }
            val adapter = ShopAdapter(listShopEntityPickup, lastShop = shopEntityObserverThen.value?.uuid, object: ShopClickListener<ShopEntity>{
                override fun onClick(shopEntity: ShopEntity) {
                    shopEntityObserverThen.value = shopEntity
                }
            })
            binding.shopRV.adapter = adapter
        })


        shopEntityObserverThen.observe(viewLifecycleOwner,{
            if(it != null){
                shopEntityObserver.value = it
            }else{
                shopEntityObserver.value = null
            }
        })

    }



    private fun onShopMaps(){

        binding.shopMapsCardView.setOnClickListener {
            val bottomSheetFragment = ShopMapsFragment(
                db!!,
                shopEntityObserver
            )
            bottomSheetFragment.show(
                childFragmentManager,
                "ShopMapsFragment"
            )
        }
        shopEntityObserver.observe(viewLifecycleOwner,{ shop ->
            if (shop != null) {
                val adapter =
                    viewModel.shops.value?.let {
                        val listShopEntityPickup: MutableList<ShopEntity> = mutableListOf()
                        it.forEach { shop ->
                            if (shop.pickup) {
                                listShopEntityPickup.add(shop)
                            }
                        }
                        ShopAdapter(
                            listShopEntityPickup,
                            lastShop = shop.uuid,
                            object : ShopClickListener<ShopEntity> {
                                override fun onClick(shopEntity: ShopEntity) {
                                    shopEntityObserver.value = shopEntity
                                }
                            })
                    }
                binding.shopRV.adapter = adapter
                return@observe
            }
        })
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}