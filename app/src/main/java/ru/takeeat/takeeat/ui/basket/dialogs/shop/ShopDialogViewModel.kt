package ru.takeeat.takeeat.ui.basket.dialogs.shop

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

import ru.takeeat.takeeat.network.mtesapi.core.ERR_GET_SHOPS
import ru.takeeat.takeeat.network.mtesapi.core.Shop
import ru.takeeat.takeeat.db.AppDataBase
import ru.takeeat.takeeat.network.mtesapi.interfaces.Result
import ru.takeeat.takeeat.db.core.ShopEntity as ShopDB
import ru.takeeat.takeeat.network.mtesapi.core.*


class ShopDialogViewModel (val db: AppDataBase, application: Application) :
    AndroidViewModel(application){
    private var viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    private val _serviceCore = CoreAPI()

    private val _shopsN = MutableLiveData<List<Shop>>()
    val shopsN: LiveData<List<Shop>>
        get() = _shopsN
    fun getShops() {
        _serviceCore.callShops(_shopsN, object: Result<Int> {
            override fun result(code: Int) {
                if(code == ERR_GET_SHOPS || code == EMPTY_SHOPS){
                    uiScope.launch {
                        db.shopDAO().removeAll()
                    }
                }
            }
        })
    }

    private val _shopsDB: LiveData<List<ShopDB>> = db.shopDAO().getAllDelivery(pickup = true)
    val shops: LiveData<List<ShopDB>> get() = _shopsDB

    init {
        getShops()
        shopsN.observeForever{ listShop ->
            if (listShop != null){
                if (!listShop.isNullOrEmpty()){
                    uiScope.launch {
                        db.shopDAO().removeAll()
                        listShop.forEach{ shop ->
                            db.shopDAO().insert(ShopDB(
                                uuid=shop.uuid,
                                pickup = shop.pickup,
                                name = shop.name,
                                phone = shop.phone,
                                date_work = shop.date_work,
                                time_work = shop.time_work,
                                address = shop.address,
                                latitude = shop.latitude,
                                longitude = shop.longitude,
                            ))
                        }

                        return@launch

                    }
                }
                return@observeForever

            }else{
                return@observeForever
            }
        }
    }
}