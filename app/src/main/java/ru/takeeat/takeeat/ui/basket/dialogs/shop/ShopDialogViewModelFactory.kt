package ru.takeeat.takeeat.ui.basket.dialogs.shop

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.takeeat.takeeat.db.AppDataBase

class ShopDialogViewModelFactory(private val db: AppDataBase, private val application: Application): ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ShopDialogViewModel::class.java)) {
            return ShopDialogViewModel(db,application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
