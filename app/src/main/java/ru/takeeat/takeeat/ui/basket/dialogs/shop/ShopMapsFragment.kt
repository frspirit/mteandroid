package ru.takeeat.takeeat.ui.basket.dialogs.shop

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.databinding.FragmentShopMapsFragmentBinding

import com.google.android.material.bottomsheet.BottomSheetBehavior

import android.widget.FrameLayout
import androidx.lifecycle.LiveData
import com.google.android.gms.maps.model.BitmapDescriptorFactory

import com.google.android.material.bottomsheet.BottomSheetDialog
import ru.takeeat.takeeat.db.AppDataBase
import ru.takeeat.takeeat.db.core.ShopEntity
import android.view.Gravity
import com.google.android.material.bottomnavigation.BottomNavigationView


class ShopMapsFragment(
//    val shops: ListShop,
    val db: AppDataBase,
    val shopEntityObserver: MutableLiveData<ShopEntity>

) : BottomSheetDialogFragment() ,
    OnMapReadyCallback {
    private lateinit var _binding: FragmentShopMapsFragmentBinding
    private val binding get() = _binding
    private lateinit var args: ShopMapsFragmentArgs
    private lateinit var mapView: MapView
    private lateinit var mGoogleMap: GoogleMap
    private val shops: LiveData<List<ShopEntity>> = db.shopDAO().getAll()


    companion object {
        const val TAG = "MapsCheckoutFragment"
        private val MAPVIEW_BUNDLE_KEY = "MapViewBundleKey"
        private var shopEntityName: ShopEntity? = null
    }



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentShopMapsFragmentBinding.inflate(inflater, container, false)
//        Places.initialize(requireContext(), "AIzaSyCcSsGillk_DYKWiRzWDYp2rU2tuZOmBeM")

        dialog?.setOnShowListener { dialog ->
            val d = dialog as BottomSheetDialog
            val bottomSheet = d.findViewById<View>(R.id.design_bottom_sheet) as FrameLayout
            val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            bottomSheetBehavior.isDraggable = false
        }

//        mapsUtils = MapsUtils(requireContext())
        mapView = binding.mapView
        initGoogleMap(savedInstanceState)
        return binding.root
    }

    private fun initGoogleMap(savedInstanceState: Bundle?) {

        val mapView = binding.mapView
        var mapViewBundle: Bundle? = null
        if (savedInstanceState != null) {
            val MAPVIEW_BUNDLE_KEY = resources.getString(R.string.gapikey)
            mapViewBundle = savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY)
        }
        mapView.onCreate(mapViewBundle)
        mapView.getMapAsync(this)
    }


    @SuppressLint("PotentialBehaviorOverride")
    override fun onMapReady(map: GoogleMap) {
        mGoogleMap = map
//        mGoogleMap.clear()

        map.setMinZoomPreference(5.0f)
        map.setMaxZoomPreference(50.0f)
        map.uiSettings.isZoomControlsEnabled = true
        map.uiSettings.isMyLocationButtonEnabled = true
        animateCamera()


//        val layer = KmlLayer(map, R.raw.geo, context)
//        layer.addLayerToMap()

        shops.observe(viewLifecycleOwner,{ listShop ->
            listShop.forEach { shop ->
                if (shop.latitude != null && shop.longitude != null){
                    map.addMarker(
                        MarkerOptions()
                            .position(LatLng(shop.latitude, shop.longitude))
                            .title(shop.name)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker))
                            .visible(true)
                    )
                }

            }

        })
//       map.addMarker(
//            MarkerOptions()
//                .position(LatLng(59.856023610420266, 30.224664871697396))
//                .title("TakeEat Жукова")
//                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker))
//                .visible(true)
//        )
//        map.addMarker(
//            MarkerOptions()
//                .position(LatLng(59.9163256516879, 30.512117183342802))
//                .title("TakeEat Каштановая")
//                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker))
//                .visible(true)
//        )
//        map.addMarker(
//            MarkerOptions()
//                .position(LatLng(59.896368287303964, 29.773891373013836))
//                .title("TakeEat Ломоносов")
//                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker))
//                .visible(true)
//        )
//        map.addMarker(
//            MarkerOptions()
//                .position(LatLng(59.854432, 30.020593))
//                .title("TakeEat Стрельна")
//                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker))
//                .visible(true)
//        )



//        mGoogleMap.setOnMarkerClickListener { marker ->
//            Toast.makeText(context, "${marker.id}", Toast.LENGTH_SHORT).show()
//            if (marker.title ==args.shops.shops[0].name) {
//                binding.addressInfoText.text = args.shops.shops[2].address
//                binding.mapsMaterialButton.isEnabled = true
//                binding.mapsMaterialButton.alpha = 1F
//                shopName = args.shops.shops[2].name
//            } else if (marker.title ==args.shops.shops[1].name) {
//                binding.addressInfoText.text = args.shops.shops[2].address
//                binding.mapsMaterialButton.isEnabled = true
//                binding.mapsMaterialButton.alpha = 1F
//                requireView().findNavController().currentBackStackEntry?.savedStateHandle?.set("shopName",args.shops.shops[2].name)
//            }else if (marker.title ==args.shops.shops[2].name) {
//                binding.addressInfoText.text = args.shops.shops[2].address
//                binding.mapsMaterialButton.isEnabled = true
//                binding.mapsMaterialButton.alpha = 1F
//                requireView().findNavController().currentBackStackEntry?.savedStateHandle?.set("shopName",args.shops.shops[2].name)
//            }else{
//                binding.addressInfoText.text = "Выберите точку"
//                binding.mapsMaterialButton.alpha = 0.5F
//            }
//            true
//        }
//        val shop: Shop? = null



        map.setOnMarkerClickListener(object : GoogleMap.OnMarkerClickListener {
            override fun onMarkerClick(marker: Marker): Boolean {
                var selected: Boolean = false
                shops.value?.forEach {
                    if (marker.title == it.name) {
                        if (it.pickup){
                            binding.addressInfoLLayout.visibility = View.VISIBLE
                            binding.addressInfoTitle.text = it.name
                            binding.addressInfoAddress.text = it.address
                            binding.addressInfoDate.text = "${it.date_work} ${it.time_work}"
                            binding.addressInfoAddress.gravity = Gravity.LEFT
                            binding.mapsMaterialButton.isEnabled = true
                            binding.mapsMaterialButton.alpha = 1F
                            shopEntityObserver.value = it
                            selected = true
                        }else{
                            binding.addressInfoLLayout.visibility = View.VISIBLE
                            binding.addressInfoTitle.text = it.name
                            binding.addressInfoAddress.text = "Самовывоз не осуществляется"
                            binding.addressInfoAddress.gravity = Gravity.CENTER_HORIZONTAL
                            binding.mapsMaterialButton.isEnabled = false
                            binding.addressInfoDate.text = ""
                            binding.mapsMaterialButton.alpha = 0.5F
                            shopEntityObserver.value = null
                            selected = true
                        }

                    }
                }
//                if (!selected){
//                    binding.addressInfoLLayout.visibility = View.GONE
//                    shopObserver.value = null
//                }
//                if (marker.title == ShopDialogFragment.shop1.name) {
//                    binding.addressInfoLLayout.visibility = View.VISIBLE
//                    binding.addressInfoTitle.text = ShopDialogFragment.shop1.name
//                    binding.addressInfoAddress.text = ShopDialogFragment.shop1.address
//                    binding.addressInfoDate.text = "${ShopDialogFragment.shop1.date_work} ${ShopDialogFragment.shop1.time_work}"
//                    binding.mapsMaterialButton.isEnabled = true
//                    binding.mapsMaterialButton.alpha = 1F
//                    shopObserver.value = ShopDialogFragment.shop1
//
////                    shopName = shops.shops[2]
//                } else if (marker.title == ShopDialogFragment.shop2.name) {
//                    binding.addressInfoLLayout.visibility = View.VISIBLE
//                    binding.addressInfoTitle.text = ShopDialogFragment.shop2.name
//                    binding.addressInfoAddress.text = ShopDialogFragment.shop2.address
//                    binding.addressInfoDate.text = "${ShopDialogFragment.shop2.date_work} ${ShopDialogFragment.shop2.time_work}"
//                    binding.mapsMaterialButton.isEnabled = true
//                    binding.mapsMaterialButton.alpha = 1F
//                    shopObserver.value = ShopDialogFragment.shop2
//
////                    shopName = shops.shops[2]
////                    requireView().findNavController().currentBackStackEntry?.savedStateHandle?.set(
////                        "shopName",
////                        shops.shops[2]
////                    )
//                } else if (marker.title == ShopDialogFragment.shop3.name) {
//                    binding.addressInfoLLayout.visibility = View.VISIBLE
//                    binding.addressInfoTitle.text = ShopDialogFragment.shop3.name
//                    binding.addressInfoAddress.text = ShopDialogFragment.shop3.address
//                    binding.addressInfoDate.text = "${ShopDialogFragment.shop3.date_work} ${ShopDialogFragment.shop3.time_work}"
//                    binding.mapsMaterialButton.isEnabled = true
//                    binding.mapsMaterialButton.alpha = 1F
//                    shopObserver.value = ShopDialogFragment.shop3
//
//
//                } else if (marker.title == ShopDialogFragment.shop4.name) {
//                    binding.addressInfoLLayout.visibility = View.VISIBLE
//                    binding.addressInfoTitle.text = ShopDialogFragment.shop4.name
//                    binding.addressInfoAddress.text = ShopDialogFragment.shop4.address
//                    binding.addressInfoDate.text = "${ShopDialogFragment.shop4.date_work} ${ShopDialogFragment.shop4.time_work}"
//                    binding.mapsMaterialButton.isEnabled = true
//                    binding.mapsMaterialButton.alpha = 1F
//                    shopObserver.value = ShopDialogFragment.shop4
//
//
//                } else {
//                    binding.addressInfoLLayout.visibility = View.GONE
//                    shopObserver.value = null
//                }
                return false
            }
        })
        binding.mapsMaterialButton.setOnClickListener {
            dismiss()
        }
    }


    private fun animateCamera() {
        mGoogleMap.animateCamera(
            CameraUpdateFactory.newLatLngZoom(
                LatLng(
                    59.856023610420266,
                    30.224664871697396
                ), 9.5F
            )
        )
//        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng))
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        var mapViewBundle = outState.getBundle(MAPVIEW_BUNDLE_KEY)
        if (mapViewBundle == null) {
            mapViewBundle = Bundle()
            outState.putBundle(MAPVIEW_BUNDLE_KEY, mapViewBundle)
        }
        mapView.onSaveInstanceState(mapViewBundle)
    }

    override fun onResume() {
        super.onResume()
//        activity?.title = "Карта"
        mapView.onResume()
    }

    override fun onStart() {
        super.onStart()
        mapView.onStart()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        activity?.let { actv ->
            actv.findViewById<BottomNavigationView>(R.id.nav_view)?.visibility = View.GONE
        }
        super.onCreate(savedInstanceState)

    }
    override fun onStop() {
        super.onStop()
        mapView.onStop()
        activity?.let { actv ->
            actv.findViewById<BottomNavigationView>(R.id.nav_view)?.visibility = View.VISIBLE
        }
    }


    override fun onPause() {
        mapView.onPause()
        super.onPause()
    }

    override fun onDestroy() {
        mapView.onDestroy()
        super.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

//    override fun onMarkerClick(marker: Marker): Boolean {
//        Toast.makeText(context, "$marker", Toast.LENGTH_SHORT).show()
//        if (marker.title == args.shops.shops[0].name) {
//            binding.addressInfoText.text = args.shops.shops[2].address
//            binding.mapsMaterialButton.isEnabled = true
//            binding.mapsMaterialButton.alpha = 1F
//            shopName = args.shops.shops[2].name
//        } else if (marker.title == args.shops.shops[1].name) {
//            binding.addressInfoText.text = args.shops.shops[2].address
//            binding.mapsMaterialButton.isEnabled = true
//            binding.mapsMaterialButton.alpha = 1F
//            requireView().findNavController().currentBackStackEntry?.savedStateHandle?.set(
//                "shopName",
//                args.shops.shops[2].name
//            )
//        } else if (marker.title == args.shops.shops[2].name) {
//            binding.addressInfoText.text = args.shops.shops[2].address
//            binding.mapsMaterialButton.isEnabled = true
//            binding.mapsMaterialButton.alpha = 1F
//            requireView().findNavController().currentBackStackEntry?.savedStateHandle?.set(
//                "shopName",
//                args.shops.shops[2].name
//            )
//        } else {
//            binding.addressInfoText.text = "Выберите точку"
//            binding.mapsMaterialButton.alpha = 0.5F
//        }
//
//        val clickCount = marker.tag as? Int
//
//        // Check if a click count was set, then display the click count.
//        clickCount?.let {
//            val newClickCount = it + 1
//            marker.tag = newClickCount
//            Toast.makeText(
//                context,
//                "${marker.title} has been clicked $newClickCount times.",
//                Toast.LENGTH_SHORT
//            ).show()
//        }
//
//        // Return false to indicate that we have not consumed the event and that we wish
//        // for the default behavior to occur (which is for the camera to move such that the
//        // marker is centered and for the marker's info window to open, if it has one).
//
//        return false
//    }


}

