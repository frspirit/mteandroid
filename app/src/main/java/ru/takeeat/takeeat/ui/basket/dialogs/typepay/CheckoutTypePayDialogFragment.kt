package ru.takeeat.takeeat.ui.basket.dialogs.typepay

import android.os.Bundle
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.databinding.FragmentCheckoutTypePayDialogBinding
import ru.takeeat.takeeat.ui.basket.checkout.CheckoutViewModel
import ru.takeeat.takeeat.ui.basket.dialogs.shop.ShopDialogFragment


class CheckoutTypePayDialogFragment(
    val viewModel: CheckoutViewModel
) : BottomSheetDialogFragment() {

    private var _binding: FragmentCheckoutTypePayDialogBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialog)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentCheckoutTypePayDialogBinding.inflate(inflater, container, false)

        binding.checkoutTypePayRadioButton1.text = "Наличными"
        binding.checkoutTypePayRadioButton2.text = "Картой"
        onClickSave()
        onCheckedTypePay()
        return binding.root
    }

    private fun onClickSave(){
        binding.checkoutTypePayMaterialButton.setOnClickListener {
            this.dismiss()
        }
    }

    private fun onCheckedTypePay(){
        binding.checkoutTypePayRadioGroup
            .setOnCheckedChangeListener { _, checkedId ->
                if (binding.checkoutTypePayRadioButton1.id == checkedId) {
                    viewModel.onSetCheckoutTypePay(CheckoutViewModel.typePayCash)
                } else if (binding.checkoutTypePayRadioButton2.id == checkedId) {
                    viewModel.onSetCheckoutTypePay(CheckoutViewModel.typePayCard)
                }
            }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}