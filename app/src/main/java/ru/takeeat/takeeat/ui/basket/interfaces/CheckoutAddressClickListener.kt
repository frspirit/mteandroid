package ru.takeeat.takeeat.ui.basket.interfaces

import ru.takeeat.takeeat.db.customer.Address

interface CheckoutAddressClickListener {
    fun onCheckoutAddress(address:Address)
}