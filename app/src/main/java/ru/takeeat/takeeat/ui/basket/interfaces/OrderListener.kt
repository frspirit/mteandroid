package ru.takeeat.takeeat.ui.basket.interfaces

import ru.takeeat.takeeat.network.mtesapi.core.OrderCreated

interface OrderListener<T> {
    fun onSend(created: Boolean, orderInfo: OrderCreated?){}
}