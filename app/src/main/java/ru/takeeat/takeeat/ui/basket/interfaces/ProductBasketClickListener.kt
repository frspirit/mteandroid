package ru.takeeat.takeeat.ui.basket.interfaces

import android.widget.TextView
import com.google.android.material.button.MaterialButton
import ru.takeeat.takeeat.db.order.ModifierOrder
import ru.takeeat.takeeat.db.order.ProductOrder

interface ProductBasketClickListener {
    fun onRemoveProductBasket(product: ProductOrder){
    }
    fun onEditableModifier(product: ProductOrder,productModifierOrder: List<ModifierOrder>){
    }
    fun onPlusProduct(product: ProductOrder, countTV: TextView, priceTV:TextView){
    }
    fun onMinusProduct(product: ProductOrder, countTV: TextView,priceTV:TextView){
    }
}