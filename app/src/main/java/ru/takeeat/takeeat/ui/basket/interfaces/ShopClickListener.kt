package ru.takeeat.takeeat.ui.basket.interfaces

interface ShopClickListener<T> {
    fun onClick(shop: T){}
}