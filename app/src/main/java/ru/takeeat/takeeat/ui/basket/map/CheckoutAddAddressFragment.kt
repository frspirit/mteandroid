package ru.takeeat.takeeat.ui.basket.map

import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import ru.takeeat.takeeat.databinding.FragmentCustomerAddAddressBinding
import ru.takeeat.takeeat.db.AppDataBase
import ru.takeeat.takeeat.db.customer.Address

class CheckoutAddAddressFragment : Fragment() {
    private lateinit var _binding: FragmentCustomerAddAddressBinding
    private val binding get() = _binding
    private lateinit var viewModel: CheckoutAddAddressViewModel
    private lateinit var viewModelFactory: CheckoutAddAddressViewModelFactory
    private lateinit var args: CheckoutAddAddressFragmentArgs
    private var idCustomer: Long? = null
    private var uuidCustomer: String? = null


    companion object {
        const val TAG = "CustomerAddAddressFragment"
    }



    override fun onResume() {
        super.onResume()
//        activity?.title = "Добавить"
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val application = requireNotNull(this.activity).application
        val db = AppDataBase.getInstance(application)
        viewModelFactory = CheckoutAddAddressViewModelFactory(db, application)
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(CheckoutAddAddressViewModel::class.java)
        args = CheckoutAddAddressFragmentArgs.fromBundle(requireArguments())
        _binding = FragmentCustomerAddAddressBinding.inflate(inflater, container, false)


        viewModel.customer.observe(viewLifecycleOwner, {
            idCustomer = it.id
            uuidCustomer = it.uuid
        })
        binding.customerAddAddressRemoveCardView.visibility = View.GONE
        onSetAddress()
        onAddAddress()
        onRemoveAddress()
        return binding.root
    }


    private fun onSetAddress() {
        binding.customerAddAddressCityInputEditText.text =
            Editable.Factory.getInstance().newEditable(args.city)
        binding.customerAddAddressCityInputEditText.isEnabled = false

        binding.customerAddAddressStreetInputEditText.text =
            Editable.Factory.getInstance().newEditable(args.street)
        binding.customerAddAddressStreetInputEditText.isEnabled = false

        binding.customerAddAddressHomeInputEditText.text =
            Editable.Factory.getInstance().newEditable(args.home)
        binding.customerAddAddressHomeInputEditText.isEnabled = false

        binding.customerAddAddressEntranceInputEditText.text =
            Editable.Factory.getInstance().newEditable(args.entrance ?: "")

        binding.customerAddAddressApartmentInputEditText.text =
            Editable.Factory.getInstance().newEditable(args.apartment ?: "")


        binding.customerAddAddressFloorInputEditText.text =
            Editable.Factory.getInstance().newEditable(args.floor ?: "")

    }


    private fun onAddAddress() {
        binding.customerAddAddressInsertMaterialButton.setOnClickListener {
            if (args.id != null){
                viewModel.onUpdateAddress(
                    args.id!!.toLong(),
                    binding.customerAddAddressApartmentInputEditText.text.toString(),
                    binding.customerAddAddressEntranceInputEditText.text.toString(),
                    binding.customerAddAddressFloorInputEditText.text.toString(),
                )
                Toast.makeText(context, "Адрес обновлён!", Toast.LENGTH_SHORT).show()
//                requireView().findNavController().navigate(
//                    CheckoutAddAddressFragmentDirections.actionCheckoutAddAddressFragmentToNavigationBasket2()
//                )
            }else{
                val address = Address(
                    idCustomer = idCustomer ?: 0,
                    city = args.city,
                    street = args.street,
                    home = args.home,
                    apartment = binding.customerAddAddressApartmentInputEditText.text.toString(),
                    entrance = binding.customerAddAddressEntranceInputEditText.text.toString(),
                    floor = binding.customerAddAddressFloorInputEditText.text.toString(),
                    lat = args.latLng?.latitude,
                    lng = args.latLng?.longitude,
                    latLng = args.latLng
                )

                viewModel.onAddAddress(uuidCustomer ?: "", address)

            }
//            requireView().findNavController().navigate(
//                CheckoutAddAddressFragmentDirections.actionCheckoutAddAddressFragmentToNavigationBasket2()
//            )
            requireView().findNavController().popBackStack()
        }
    }

    private fun onRemoveAddress(){
        binding.customerAddAddressRemoveMaterialButton.setOnClickListener{
            if(viewModel.onRemoveAddress(args.id?.toLong())){
//                requireView().findNavController().popBackStack()
                requireView().findNavController().navigate(
                    CheckoutAddAddressFragmentDirections.actionCheckoutAddAddressFragmentToNavigationBasket2()
                )
            }else{
                Toast.makeText(context, "Ошибка удаления!", Toast.LENGTH_SHORT).show()
            }


        }
    }
}