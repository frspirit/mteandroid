package ru.takeeat.takeeat.ui.basket.map

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import ru.takeeat.takeeat.db.AppDataBase
import ru.takeeat.takeeat.db.customer.Address
import ru.takeeat.takeeat.db.customer.Customer
import ru.takeeat.takeeat.network.mtesapi.customer.CustomerAPI

class CheckoutAddAddressViewModel(
    private val db: AppDataBase,
    application: Application
) : AndroidViewModel(application) {
    private val _service = CustomerAPI()
    private var viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    private val _customer = db.customerDAO().getCheckCustomer()
    val customer: LiveData<Customer> get() = _customer


    private val _address: MutableLiveData<ru.takeeat.takeeat.network.mtesapi.customer.data.Address> =
        MutableLiveData()
    val address: LiveData<ru.takeeat.takeeat.network.mtesapi.customer.data.Address> get() = _address

    fun onAddAddress(uuidCustomer: String, address: Address) {

        _service.callAttachAddress(
            customerUuid = uuidCustomer,
            address = address,
            _address
        )
        _address.observeForever {
            if (it != null) {
                uiScope.launch {
                    address.uuid = it.uuid
                    db.customerDAO().insertAddress(address)
                }
                return@observeForever
            }
        }


    }

    fun onUpdateAddress(addressId: Long?,apartment:String,entrance:String,floor:String,){
        if (addressId != null){
            uiScope.launch{
                val address = db.customerDAO().getAddress(addressId)
                if (address != null){
                    address.apartment = apartment
                    address.entrance = entrance
                    address.floor = floor
                    _service.callUpdateAddress(address)
                    db.customerDAO().updateAddress(address)
                }

            }
        }

    }

    fun onRemoveAddress(addressId: Long?):Boolean{
        if (addressId != null){
            uiScope.launch{
                val address = db.customerDAO().getAddress(addressId)
                if (address != null){
                    _service.callRemoveAddress(address.uuid!!)
                    db.customerDAO().removeAddress(addressId)
                }

            }
            return true
        }
        return false
    }
}