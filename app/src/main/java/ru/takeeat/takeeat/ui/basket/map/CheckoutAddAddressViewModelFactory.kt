package ru.takeeat.takeeat.ui.basket.map

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.takeeat.takeeat.db.AppDataBase


    class CheckoutAddAddressViewModelFactory (private val db: AppDataBase, private val application: Application) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(CheckoutAddAddressViewModel::class.java)) {
                return CheckoutAddAddressViewModel(db,application) as T
            }
            throw IllegalArgumentException("Unknown ViewModel class")
        }
    }
