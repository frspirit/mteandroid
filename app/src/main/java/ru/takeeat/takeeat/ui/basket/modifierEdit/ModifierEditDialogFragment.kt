package ru.takeeat.takeeat.ui.basket.modifierEdit

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.databinding.FragmentModifierEditDialogListDialogBinding
import ru.takeeat.takeeat.db.AppDataBase
import ru.takeeat.takeeat.db.order.ModifierOrder
import ru.takeeat.takeeat.db.order.ProductOrder
import ru.takeeat.takeeat.ui.home.detail.ModifierListDialogFragment
import ru.takeeat.takeeat.ui.home.detail.ModifierRequiredListDialogFragment

// *
// * A fragment that shows a list of items as a modal bottom sheet.
// *
// * You can show this modal bottom sheet from your activity like this:
// * <pre>
// *    ModifierEditDialogFragment.newInstance(30).show(supportFragmentManager, "dialog")
// * </pre>
// */
class ModifierEditDialogFragment(
    private val idProduct: Long,
    private val product: ProductOrder,
    private val productModifierOrder: List<ModifierOrder>
) : BottomSheetDialogFragment() {

    private var _binding: FragmentModifierEditDialogListDialogBinding? = null
    private val binding get() = _binding!!

    private lateinit var viewModel: ModifierEditDialogViewModel
    private lateinit var viewModelFactory: ModifierEditDialogViewModelFactory

    private lateinit var modObserver: MutableLiveData<MutableList<ModifierOrder>>
    private lateinit var requiredModObserver: MutableLiveData<ModifierOrder>

    private var selectedModifier = mutableListOf<ModifierOrder>()
    private var selectedRequireModifier: ModifierOrder? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialog)
        modObserver = MutableLiveData()
        requiredModObserver = MutableLiveData()
//        idProduct = ModifierEditDialogFragmentArgs.fromBundle(requireArguments()).idProduct
        val application = requireNotNull(this.activity).application
        val db = AppDataBase.getInstance(application)
        viewModelFactory = ModifierEditDialogViewModelFactory(idProduct, product, db, application)
//
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(ModifierEditDialogViewModel::class.java)
        viewModel.groupModifier.observeForever { data ->
            data.forEach { item ->
                when {
                    item.group_modifier.required -> {
                        for (modifierOrder in productModifierOrder) {
                            if (modifierOrder.group_uuid == item.group_modifier.uuid) {
                                selectedRequireModifier = modifierOrder
                                break
                            }
                        }
                    }
                    else -> {
                        for (modifierOrder in productModifierOrder) {
                            if (modifierOrder.group_uuid == item.group_modifier.uuid) {
                                selectedModifier.add(modifierOrder)
                            }
                        }
                    }
                }
            }
            return@observeForever
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentModifierEditDialogListDialogBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        onModifier()
        return binding.root

    }

    private fun onModifier() {
        viewModel.groupModifier.observe(viewLifecycleOwner, { data ->

            data.forEach { item ->


                when {
                    item.group_modifier.required -> {
                        binding.productEditModifierRequiredCard.visibility = View.VISIBLE
                        binding.productEditModifierRequiredButton.text = item.group_modifier.name

                        viewModel.modifiersDB.observe(viewLifecycleOwner, { modifiers ->


                            binding.productEditModifierRequiredButton.setOnClickListener {

                                val bottomSheetRequiredFragment =
                                    ModifierRequiredListDialogFragment(
                                        mod = requiredModObserver,
                                        selectedRequireModifier = selectedRequireModifier,
                                        modifiers = modifiers,
                                        group_modifier = item.group_modifier,
                                        childModifier = item.childModifierOrder!!
                                    )
                                bottomSheetRequiredFragment.show(
                                    childFragmentManager,
                                    "ModifierRequiredBottomSheet"
                                )


                            }

                        })

                    }
                    else -> {
                        binding.productEditModifierCard.visibility = View.VISIBLE
                        binding.productEditModifierButton.text = item.group_modifier.name

                        viewModel.modifiersDB.observe(viewLifecycleOwner, { modifiers ->

                            binding.productEditModifierButton.setOnClickListener {
                                val bottomSheetFragment = ModifierListDialogFragment(
                                    mod = modObserver,
                                    selectedModifier = selectedModifier,
                                    modifiers = modifiers,
                                    group_modifier = item.group_modifier,
                                    childModifier = item.childModifierOrder!!
                                )
                                bottomSheetFragment.show(
                                    childFragmentManager,
                                    "ModifierBottomSheet"
                                )
                            }

                        })

                    }


                }

            }


        })

        modObserver.observe(viewLifecycleOwner, {
//            if (!it.isNullOrEmpty()) {
//                viewModel.onSetModifier(selectedModifier = selectedModifier, mod = it)
//                binding.productEditOrderPriceText.text =
//                    "${viewModel.price.value}"
//                selectedModifier = it
//            }

            viewModel.onSetModifier(selectedModifier = selectedModifier, mod = it)
            binding.productEditOrderPriceText.text =
                "${viewModel.price.value}"
            selectedModifier = it
        })

        requiredModObserver.observe(viewLifecycleOwner, {
            if (it != null) {
                viewModel.onSetRequireModifier(selectedRequireModifier = selectedRequireModifier,mod = it)
                selectedRequireModifier = it
            }
        })
    }

    override fun onDestroyView() {
        viewModel.updateProduct(
            selectedModifier = selectedModifier,
            selectedRequireModifier = selectedRequireModifier
        )
        super.onDestroyView()

    }
}
