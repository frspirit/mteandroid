package ru.takeeat.takeeat.ui.basket.modifierEdit

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import ru.takeeat.takeeat.db.AppDataBase
import ru.takeeat.takeeat.db.core.GroupModifierWithModifierStr
import ru.takeeat.takeeat.db.order.ModifierOrder
import ru.takeeat.takeeat.db.order.ProductOrder
import ru.takeeat.takeeat.network.mtesapi.core.CoreAPI
import ru.takeeat.takeeat.network.mtesapi.core.Modifier


class ModifierEditDialogViewModel(val idProduct: Long,
                                  productInBasket: ProductOrder,
                                  val db: AppDataBase,
                                  application: Application
): AndroidViewModel(application) {
    private val _service = CoreAPI()
    private var viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    private val _product = productInBasket
    val product: ProductOrder
        get() = _product

    private var _groupModifier = db.groupModifierDAO().getGroupModifiersByProductDB(idProduct)
    val groupModifier: LiveData<List<GroupModifierWithModifierStr>>
        get() = _groupModifier

    private val _modifiersDB = db.modifierDAO().getLDAll()
    val modifiersDB: LiveData<List<ru.takeeat.takeeat.db.core.Modifier>>
        get() = _modifiersDB

    private var _modifierProduct = db.modifierOrderDAO().getProductModifiers(idProduct)
    val modifierProduct: LiveData<List<ModifierOrder>>
        get() = _modifierProduct

    private val _modifiers = MutableLiveData<List<Modifier>>()
    val modifiers: LiveData<List<Modifier>>
        get() = _modifiers

    fun getModifiers() {
        _service.callModifiers(_modifiers)
    }
    private val _require_modifier = MutableLiveData<ModifierOrder>()

    fun onSetRequireModifier(selectedRequireModifier: ModifierOrder?,mod: ModifierOrder){
        if(selectedRequireModifier != null){
            uiScope.launch{
                db.modifierOrderDAO().remove(selectedRequireModifier)
            }
        }
        _require_modifier.value = mod
    }
    private val _modifier = MutableLiveData<MutableList<ModifierOrder>>()
    val modifier: LiveData<MutableList<ModifierOrder>>
        get() = _modifier

    private val _price = MutableLiveData<Int>(0)
    val price: LiveData<Int>
        get() = _price
    fun onSetModifier(selectedModifier: List<ModifierOrder>?, mod: MutableList<ModifierOrder>) {
        if(!selectedModifier.isNullOrEmpty()){
            uiScope.launch{
                selectedModifier.forEach {
                    db.modifierOrderDAO().remove(it)
                }

            }
        }
        _modifier.value = mod
        var price_product: Int = product.priceProduct.toInt() ?: 0
        mod.forEach {
            price_product = price_product.plus(it.price.toInt() * it.amount)
        }
        _price.value = price_product ?: 0
    }

    fun updateProduct(selectedModifier: List<ModifierOrder>?, selectedRequireModifier: ModifierOrder?){
        uiScope.launch {
            _product.price = _price.value!!.toFloat()
            db.productOrderDAO().update(_product)
            if (!selectedModifier.isNullOrEmpty() && !_modifier.value.isNullOrEmpty()) {
//                selectedModifier.forEach {
//                    db.modifierOrderDAO().remove(it)
//                }
                onAddedModifierInDataBase()
            }
            if(selectedRequireModifier != null && _require_modifier.value != null){
//                db.modifierOrderDAO().remove(selectedRequireModifier)
                onAddedRequireModifierInDataBase()
            }


        }
    }
    private fun onAddedRequireModifierInDataBase() {
            uiScope.launch {
                val requireModifierOrderForInsert = ModifierOrder(
                    idProductOrder = _product.id,
                    uuid = _require_modifier.value!!.uuid,
                    name = _require_modifier.value!!.name,
                    price = _require_modifier.value!!.price,
                    amount = _require_modifier.value!!.amount,
                    group_name = _require_modifier.value!!.group_name,
                    group_uuid = _require_modifier.value!!.group_uuid,
                )

                db.modifierOrderDAO().insert(requireModifierOrderForInsert)
            }


    }

    private fun onAddedModifierInDataBase() {
        uiScope.launch {
            _modifier.value?.forEach { modifier_order ->
                val modifierOrderForInsert = ModifierOrder(
                    idProductOrder = _product.id,
                    uuid = modifier_order.uuid,
                    name = modifier_order.name,
                    price = modifier_order.price,
                    amount = modifier_order.amount,
                    group_name = modifier_order.group_name,
                    group_uuid = modifier_order.group_uuid,
                )
                db.modifierOrderDAO().insert(modifierOrderForInsert)
            }

        }


    }

    init {
        _price.value = product.price.toInt()
//        uiScope.launch{
////            _product.value = db.productOrderDAO().get(idProduct)
//
//            // TODO(developer): Дописать стартовую цену и разобраться в выводе цены
////            _groupModifier = db.groupModifierDAO().getGroupModifiersByProduct(idProduct)
//        }

    }
}