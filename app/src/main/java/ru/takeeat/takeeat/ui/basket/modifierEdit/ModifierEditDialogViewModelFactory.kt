package ru.takeeat.takeeat.ui.basket.modifierEdit

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.takeeat.takeeat.db.AppDataBase
import ru.takeeat.takeeat.db.order.ProductOrder


class ModifierEditDialogViewModelFactory(private val idProduct: Long, private val product: ProductOrder, private val db: AppDataBase, private val application: Application) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ModifierEditDialogViewModel::class.java)) {
            return ModifierEditDialogViewModel(idProduct, product,db,application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
