package ru.takeeat.takeeat.ui.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.google.android.material.appbar.AppBarLayout
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.databinding.FragmentMainBinding
import ru.takeeat.takeeat.db.AppDataBase
import ru.takeeat.takeeat.network.mtesapi.core.New
import ru.takeeat.takeeat.ui.home.adapters.GroupAdapter
import ru.takeeat.takeeat.ui.home.adapters.NewsAdapter
import ru.takeeat.takeeat.ui.home.adapters.SelectionAdapter
import ru.takeeat.takeeat.ui.home.interfaces.GroupClickListener
import ru.takeeat.takeeat.ui.home.interfaces.NewsClickListener
import ru.takeeat.takeeat.ui.home.interfaces.SelectionsClickListener


class MainFragment : Fragment() {

    private lateinit var viewModel: MainViewModel

        private lateinit var viewModelFactory: MainViewModelFactory
    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.let { actv ->
            actv.findViewById<AppBarLayout>(R.id.AppBarLayout)?.visibility = View.VISIBLE
            actv.findViewById<Toolbar>(R.id.my_toolbar)?.setBackgroundColor(resources.getColor(R.color.purple_light))
            actv.window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }
//        window.statusBarColor = ContextCompat.getColor(requireContext(), R.color.purple_light)

        //activity?.findViewById<TextView>(R.id.toolbar_)?.visibility = View.VISIBLE
        //activity?.findViewById<TextView>(R.id.toolbar_)?.text = resources.getString(R.string.app_name)



    }

    override fun onPause() {
        super.onPause()
        activity?.let { actv ->
            actv.findViewById<AppBarLayout>(R.id.AppBarLayout)?.visibility = View.VISIBLE
            actv.findViewById<Toolbar>(R.id.my_toolbar)?.setBackgroundColor(resources.getColor(R.color.purple_light))

        }

        //activity?.findViewById<TextView>(R.id.toolbar_)?.visibility = View.VISIBLE
        //activity?.findViewById<TextView>(R.id.toolbar_)?.text = resources.getString(R.string.app_name)

    }
    override fun onStart() {
        super.onStart()
        activity?.let { actv ->
            actv.findViewById<AppBarLayout>(R.id.AppBarLayout).visibility = View.VISIBLE
            actv.findViewById<Toolbar>(R.id.my_toolbar)?.setBackgroundColor(resources.getColor(R.color.purple_light))

        }
        //activity?.findViewById<TextView>(R.id.toolbar_)?.visibility = View.VISIBLE
        //activity?.findViewById<TextView>(R.id.toolbar_)?.text = resources.getString(R.string.app_name)

        val preview = activity?.findViewById<ConstraintLayout>(R.id.preview)?.visibility
        if(preview == View.GONE){
            activity?.window?.statusBarColor = resources.getColor(R.color.purple_light,null)
        }

    }
    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        Log.d("ANIMATIONFRAGMENT","$transit - $enter - $nextAnim - ${this.javaClass.name}")
        return if (enter) {
            AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left)
        } else {
            AnimationUtils.loadAnimation(context, android.R.anim.slide_out_right)
        }

    }
//    override fun onAttach(context: Context) {
//        super.onAttach(context)
//        requireActivity().window.statusBarColor = ContextCompat.getColor(requireContext(), R.color.purple_light)
//    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        activity?.findViewById<Toolbar>(R.id.my_toolbar)?.setBackgroundColor(resources.getColor(R.color.purple_light))
//        val rotate = RotateAnimation(
//            0F,
//            360F,
//            Animation.RELATIVE_TO_SELF,
//            0.5f,
//            Animation.RELATIVE_TO_SELF,
//            0.5f
//        )
//        rotate.duration = 5000
//        rotate.interpolator = LinearInterpolator()
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        val root: View = binding.root
        val application = requireNotNull(this.activity).application
        val db = AppDataBase.getInstance(application)
        viewModelFactory = MainViewModelFactory(db,application)

        viewModel =
            ViewModelProvider(this,viewModelFactory).get(MainViewModel::class.java)

        renderGroup()
        renderSelections()
        renderNews()



        return root


    }


    private fun renderNews() {
        viewModel.new_items.observe(viewLifecycleOwner) { data ->
            if (!data.isNullOrEmpty()) {
                binding.groupLinerLayout.visibility = View.VISIBLE
                val adapter = NewsAdapter(data, object : NewsClickListener {
                    override fun onClicked(new_item: New) {
                        requireView().findNavController().navigate(
                            MainFragmentDirections.actionNavigationMainToNewDetail(
                                new_item.title,
                                new_item.text ?: "",
                                new_item.image ?: ""
                            )
                        )

//                    Log.d("TagsListActivity", "New = " + new_item.text)
                    }
                })
                binding.newsRV.adapter = adapter
            }

        }

    }

    private fun renderSelections() {
        viewModel.selection_items.observe(viewLifecycleOwner) { data ->
            if (!data.isNullOrEmpty()) {
//                Log.d("SELECTIONSDATA1", "$data")
                val adapter = SelectionAdapter(
                    data.reversed(),
                    object : SelectionsClickListener<ru.takeeat.takeeat.db.core.Selection> {
                        override fun onClicked(selection_item: ru.takeeat.takeeat.db.core.Selection) {
                            requireView().findNavController().navigate(
                                MainFragmentDirections.actionNavigationMainToSelectionProductsFragment(
                                    selection_item.id
                                )
                            )
                        }
                    })
                binding.selectionRV.adapter = adapter
            }

        }
    }

    private fun renderGroup() {
        viewModel.group_items.observe(viewLifecycleOwner) { data ->
            if (!data.isNullOrEmpty()) {
                val adapter = GroupAdapter(data, object : GroupClickListener {
                    override fun onClicked(group_item: ru.takeeat.takeeat.db.core.Group) {
                        requireView().findNavController().navigate(
                            MainFragmentDirections.actionNavigationMainToGroupDetailFragment2(
                                group_item.id,
                                group_item.name
                            )
                        )
                    }
                })
                binding.groupRV.adapter = adapter
            }

        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
//        //activity?.findViewById<TextView>(R.id.toolbar_)?.setBackgroundColor(resources.getColor(R.color.white))
        _binding = null

    }

}
