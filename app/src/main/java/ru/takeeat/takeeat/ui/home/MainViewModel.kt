package ru.takeeat.takeeat.ui.home

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ru.takeeat.takeeat.db.AppDataBase
import ru.takeeat.takeeat.db.core.Group
import ru.takeeat.takeeat.db.core.Selection
import ru.takeeat.takeeat.network.mtesapi.core.CoreAPI
import ru.takeeat.takeeat.network.mtesapi.core.New
class MainViewModel(val db: AppDataBase, application: Application) : AndroidViewModel(application){
//class MainViewModel(): ViewModel() {
    private val _service = CoreAPI()

    private val _selection_items = db.selectionDAO().getAll()
    val selection_items: LiveData<List<Selection>>
        get() = _selection_items
//    fun getSelections(){
//        _service.callSelections(_selection_items)
//    }

    private val _new_items = MutableLiveData<List<New>>()
    val new_items: LiveData<List<New>>
        get() = _new_items
    fun getNews() {
        _service.callNews(_new_items)
    }

//    private val _group_items = MutableLiveData<List<Group>>()
//    val group_items: LiveData<List<Group>>
//        get() = _group_items
//    fun getGroup() {
//        _service.callGroups(_group_items)
//    }
    private val _group_items = db.groupCore().getAll()
    val group_items: LiveData<List<Group>>
        get() = _group_items
    init{
//        getSelections()
        getNews()
//        getGroup()


    }
//    private val _selection_title = MutableLiveData<String>().apply {
//        value = "Подборки"
//    }
//    val selection_title: LiveData<String> = _selection_title
//
//    private val _category_title = MutableLiveData<String>().apply {
//        value = "Категории"
//    }
//    val category_title: LiveData<String> = _category_title
}