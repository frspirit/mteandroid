package ru.takeeat.takeeat.ui.home

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.takeeat.takeeat.db.AppDataBase

class  MainViewModelFactory(private val db: AppDataBase, private val application: Application) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            return MainViewModel(db,application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}