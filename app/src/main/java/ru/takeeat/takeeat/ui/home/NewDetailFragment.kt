package ru.takeeat.takeeat.ui.home

import android.graphics.Point
import android.os.Bundle
import android.util.DisplayMetrics
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import coil.load
import coil.transform.CircleCropTransformation
import coil.transform.RoundedCornersTransformation
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.SERVER_IMAGE
import ru.takeeat.takeeat.databinding.FragmentNewDetailBinding
import android.view.Display
import android.widget.LinearLayout
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.MaterialToolbar


class NewDetailFragment : Fragment() {

    private var _binding: FragmentNewDetailBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.let { actv ->
            actv.window.statusBarColor = resources.getColor(R.color.white,null)
            actv.findViewById<Toolbar>(R.id.my_toolbar)?.setBackgroundColor(resources.getColor(R.color.white))
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentNewDetailBinding.inflate(inflater, container, false)

        val args = NewDetailFragmentArgs.fromBundle(requireArguments())
        activity?.findViewById<MaterialToolbar>(R.id.my_toolbar)?.title = ""
        val view: View = binding.root
        binding.apply {
            val height = resources.displayMetrics.heightPixels

            val params = binding.newDetailImageLL.layoutParams
            params.height = height / 2
            binding.newDetailImageLL.layoutParams = params
            newDetailImage.load("$SERVER_IMAGE${args.image}"){

                crossfade(false)
                placeholder(R.drawable.defimage)

//                transformations(RoundedCornersTransformation(radius=10f))
            }
//            newDetailImage.scaleType = ImageView.ScaleType.FIT_XY
            params.height = binding.newDetailImage.layoutParams.height
            binding.newDetailImageLL.layoutParams = params
            newDetailTitle.text = args.title
            newDetailText.text = args.text
        }

//        (activity as AppCompatActivity).supportActionBar?.title = args.title
//        inflater.inflate(R.layout.fragment_new_detail, container, false)
        return view
    }


}