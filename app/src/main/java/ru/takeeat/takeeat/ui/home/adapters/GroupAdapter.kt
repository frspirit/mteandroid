package ru.takeeat.takeeat.ui.home.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.transform.CircleCropTransformation
import coil.transform.RoundedCornersTransformation
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.SERVER_IMAGE
import ru.takeeat.takeeat.db.core.Group
import ru.takeeat.takeeat.ui.home.interfaces.GroupClickListener

class GroupAdapter(
    private val group_items: List<Group>, private val onClickListener: GroupClickListener,
) : RecyclerView.Adapter<GroupAdapter.MyViewHolder>() {

    class MyViewHolder constructor(view: View) : RecyclerView.ViewHolder(view) {
        val group_card: CardView = view.findViewById(R.id.group_cardView)
        val group_image: ImageView = view.findViewById(R.id.group_image)
        val group_title: TextView = view.findViewById(R.id.group_title)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_groups_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val group_item = group_items[position]
//        Log.i("NewsAdapter", group_items.toString())

        holder.group_image.load(uri = "$SERVER_IMAGE${group_item.image_priority ?: group_item.image}"){
            crossfade(false)
            placeholder(R.drawable.defimage)
            transformations(CircleCropTransformation())
        }
        holder.group_title.text = group_item.name
        holder.group_card.setOnClickListener { view ->
            onClickListener.onClicked(group_item)
        }
    }


    override fun getItemCount(): Int {
        return group_items.size
    }

}