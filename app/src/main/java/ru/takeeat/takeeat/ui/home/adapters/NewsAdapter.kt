package ru.takeeat.takeeat.ui.home.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.transform.CircleCropTransformation
import coil.transform.RoundedCornersTransformation
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.SERVER_IMAGE
import ru.takeeat.takeeat.network.mtesapi.core.New
import ru.takeeat.takeeat.ui.home.interfaces.NewsClickListener

class NewsAdapter(
    private val new_items: List<New>, private val onClickListener: NewsClickListener,
) : RecyclerView.Adapter<NewsAdapter.MyViewHolder>() {

    class MyViewHolder constructor(view: View) : RecyclerView.ViewHolder(view) {
        val newIB: ImageView = view.findViewById(R.id.news_item_image_view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_news_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val new_item = new_items[position]
//        Log.i("NewsAdapter", new_item.toString())
        holder.newIB.load(uri = "$SERVER_IMAGE${new_item.image}"){
            crossfade(false)
            placeholder(R.drawable.defimage)
            transformations(RoundedCornersTransformation())
        }
//        holder.newIB.setImageResource(R.drawable.slider_image_1)
        holder.newIB.scaleType = ImageView.ScaleType.FIT_CENTER

        holder.newIB.setOnClickListener { view ->
            onClickListener.onClicked(new_item)
        }
    }


    override fun getItemCount(): Int {
        return new_items.size
    }

}