package ru.takeeat.takeeat.ui.home.adapters

import android.graphics.*
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.metadata
import com.google.android.material.button.MaterialButton
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.SERVER_IMAGE
import ru.takeeat.takeeat.curvedLine
import ru.takeeat.takeeat.db.core.Product
import ru.takeeat.takeeat.ui.home.interfaces.ProductCardClickListener


class ProductDBAdapter(
    private val product_items: List<Product>,
    private val onCardClickListener: ProductCardClickListener<Product>,
) : RecyclerView.Adapter<ProductDBAdapter.MyViewHolder>() {

    class MyViewHolder constructor(view: View) : RecyclerView.ViewHolder(view) {
        val product_card: CardView = view.findViewById(R.id.product_card)
        val product_image: ImageView = view.findViewById(R.id.product_image)
        val product_title: TextView = view.findViewById(R.id.productBasketTitle)
        val product_description: TextView = view.findViewById(R.id.product_description)
        val product_weight: TextView = view.findViewById(R.id.product_weight)
        val product_button: MaterialButton = view.findViewById(R.id.productOrderPriceButton)
        val pProductTextView: TextView = view.findViewById(R.id.pPriceTextView)
        val productPriceCurveLine: ImageView = view.findViewById(R.id.productPriceCurveLine)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_product_card, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val product_item = product_items[position]

        holder.product_image.load("$SERVER_IMAGE${product_item.image_priority ?: product_item.image}"){
            crossfade(false)
            placeholder(R.drawable.defimage)
//            transformations(RoundedCornersTransformation())
        }

        holder.product_title.text = product_item.name
        holder.product_description.text = product_item.description
        holder.product_weight.text = weightString(product_item.weight, 1000)
        holder.product_button.text = "+ ${product_item.price.toInt()} ₽"
        if (product_item.p_price != null){
            holder.pProductTextView.text = "${product_item.p_price.toInt()} ₽"
            holder.pProductTextView.measure(0,0)
            val width = holder.pProductTextView.measuredWidth.toFloat()
            val height = holder.pProductTextView.measuredHeight.toFloat()
            val bitmap = curvedLine(width,height)
            holder.productPriceCurveLine.setImageBitmap(bitmap)
//            val bitmap = Bitmap.createBitmap( width.toInt(), height.toInt(), Bitmap.Config.ARGB_8888)
//
//            val canvas = Canvas(bitmap)
//
//            val paint: Paint = Paint();
//            paint.color = Color.RED
//            paint.isAntiAlias = true
//            paint.setStyle(Paint.Style.STROKE)
//            paint.setStrokeJoin(Paint.Join.ROUND)
//            paint.setStrokeCap(Paint.Cap.ROUND)
//            paint.setPathEffect(CornerPathEffect(10F))
//            paint.strokeWidth = width/13
//
//
//            val path:Path = Path()
//
//            path.moveTo(5F, height-5F);
//            path.quadTo(width/2, (height/2)-height/8, width-5F, 7F);
//            canvas.drawPath(path, paint)
//            Работает
//            canvas.drawArc(0F,10F, width.toFloat()*2, height.toFloat()*2,10F,-180F,false,paint)
//            canvas.drawArc(0F,10F, width.toFloat()*2, height.toFloat()*2,-10F,-180F,false,paint)



//            holder.productPriceCurveLine.setImageBitmap(bitmap)
        }

        holder.product_button.setOnClickListener { view ->
            onCardClickListener.onClickedAddBasket(product_item = product_item, memoryCacheKey = holder.product_image.metadata?.memoryCacheKey)
        }
        holder.product_card.setOnClickListener { view ->
            onCardClickListener.onClicked(product_item = product_item, memoryCacheKey = holder.product_image.metadata?.memoryCacheKey)
        }

    }


    override fun getItemCount(): Int {
        return product_items.size
    }

    private fun weightString(weight: Number, int: Int): String {
        val gramWeight = weight.toFloat() * int

        return "${gramWeight.toInt()} г"
    }
}