package ru.takeeat.takeeat.ui.home.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.db.core.Modifier
import ru.takeeat.takeeat.db.core.ModifierStr
import ru.takeeat.takeeat.db.order.ModifierOrder
import ru.takeeat.takeeat.db.order.ModifierStrOrder


import ru.takeeat.takeeat.ui.home.interfaces.ModifierClickListener

class ProductModifierAdapter(
    private val modifier_items: List<ModifierStr>,
    private val modifier_list: List<Modifier>,
    private val modifier_checked: List<ModifierOrder>?,
    private val onCardClickListener: ModifierClickListener<Modifier>,
) : RecyclerView.Adapter<ProductModifierAdapter.MyViewHolder>() {

    class MyViewHolder constructor(view: View) : RecyclerView.ViewHolder(view) {
        val modifier_price: TextView = view.findViewById(R.id.modifier_price)
        val modifier_title: TextView = view.findViewById(R.id.modifier_title)
        val modifier_minus_count: Button = view.findViewById(R.id.modifier_minus_count)
        val modifier_count: TextView = view.findViewById(R.id.modifier_count)
        val modifier_plus_count: Button = view.findViewById(R.id.modifier_plus_count)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_item_list_dialog_list_dialog_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val modifier_item = modifier_items[position]
        modifier_list.forEach { modifier ->
            if (modifier.uuid == modifier_item.uuid) {
                modifier_checked?.forEach { mod ->
                    if (modifier.uuid == mod.uuid){
                        holder.modifier_count.text = mod.amount.toString()
                        holder.modifier_count.visibility = View.VISIBLE
                        holder.modifier_minus_count.visibility = View.VISIBLE

                    }
                }
                holder.modifier_price.text = "${modifier.price.toInt()} ₽"
                holder.modifier_title.text = modifier.name
                holder.modifier_minus_count.setOnClickListener {
                    onCardClickListener.onMinusModifier(modifier,  holder.modifier_count, holder.modifier_minus_count)
                }
                holder.modifier_plus_count.setOnClickListener {
                    onCardClickListener.onPlusModifier(modifier, holder.modifier_count, holder.modifier_minus_count)

                }

                return@forEach
            }
        }
    }


    override fun getItemCount(): Int {
        return modifier_items.size
    }

}