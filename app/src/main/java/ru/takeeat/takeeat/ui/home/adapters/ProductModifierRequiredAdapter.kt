package ru.takeeat.takeeat.ui.home.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.radiobutton.MaterialRadioButton
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.db.core.Modifier
import ru.takeeat.takeeat.db.core.ModifierStr
import ru.takeeat.takeeat.db.order.ModifierStrOrder
import ru.takeeat.takeeat.ui.home.interfaces.*


class ProductModifierRequiredAdapter(
    private val lastRequireModifier: String? = null,
    private val modifier_items: List<ModifierStr>,
    private val modifier_list: List<Modifier>,
    private val onCardClickListener: ModifierRequiredClickListener<Modifier>,
) : RecyclerView.Adapter<ProductModifierRequiredAdapter.MyViewHolder>() {
    private var lastRadioButton: RadioButton? = null
    class MyViewHolder constructor(view: View) : RecyclerView.ViewHolder(view) {
        val radio_button: MaterialRadioButton = view.findViewById(R.id.modifierRadioButton)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_product_detail_modifier_required_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val modifier_item = modifier_items[position]
        modifier_list.forEach { modifier ->
            if (modifier.uuid == modifier_item.uuid) {
//                Log.i("ProductModifierItem", modifier.name)
                if (modifier_item.uuid == lastRequireModifier){
                    holder.radio_button.isChecked = true
                    lastRadioButton = holder.radio_button
                }

                holder.radio_button.text = modifier.name
                holder.radio_button.setOnCheckedChangeListener { buttonView, isChecked ->
                    if (lastRadioButton != null){
                        lastRadioButton!!.isChecked = false
                    }
                    lastRadioButton = holder.radio_button
                    onCardClickListener.onClicked(modifier, )
                }

                return@forEach
            }
        }
    }


    override fun getItemCount(): Int {
        return modifier_items.size
    }

}