package ru.takeeat.takeeat.ui.home.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.metadata
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.SERVER_IMAGE
import ru.takeeat.takeeat.curvedLine
import ru.takeeat.takeeat.db.core.ProductWithModifierGroupModifier
import ru.takeeat.takeeat.ui.home.interfaces.ProductCardClickListener

class ProductSelectionAdapter(
    private val product_items: List<ProductWithModifierGroupModifier>,
    private val onCardClickListener: ProductCardClickListener<ProductWithModifierGroupModifier>,
) : RecyclerView.Adapter<ProductSelectionAdapter.MyViewHolder>() {

    class MyViewHolder constructor(view: View) : RecyclerView.ViewHolder(view) {
        val product_card: CardView = view.findViewById(R.id.product_card)
        val product_image: ImageView = view.findViewById(R.id.product_image)
        val product_title: TextView = view.findViewById(R.id.productBasketTitle)
        val product_description: TextView = view.findViewById(R.id.product_description)
        val product_weight: TextView = view.findViewById(R.id.product_weight)
        val product_button: Button = view.findViewById(R.id.productOrderPriceButton)
        val pProductTextView: TextView = view.findViewById(R.id.pPriceTextView)
        val productPriceCurveLine: ImageView = view.findViewById(R.id.productPriceCurveLine)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_product_card, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val product_item = product_items[position]
//        Log.i("NewsAdapter", product_items.toString())

        holder.product_image.load(uri = "$SERVER_IMAGE${product_item.product.image_priority ?: product_item.product.image}"){
            crossfade(false)
            placeholder(R.drawable.defimage)
//            transformations(RoundedCornersTransformation())
        }

        holder.product_title.text = product_item.product.name
        holder.product_description.text = product_item.product.description
        holder.product_weight.text = weightString(product_item.product.weight, 1000)
        holder.product_button.text = "+ ${product_item.product.price.toInt()} ₽"

        if (product_item.product.p_price != null) {
            holder.pProductTextView.text = "${product_item.product.p_price.toInt()} ₽"
            holder.pProductTextView.measure(0, 0)
            val width = holder.pProductTextView.measuredWidth.toFloat()
            val height = holder.pProductTextView.measuredHeight.toFloat()
            val bitmap = curvedLine(width, height)
            holder.productPriceCurveLine.setImageBitmap(bitmap)
        }
        holder.product_button.setOnClickListener { view ->
            onCardClickListener.onClickedAddBasket(product_item,memoryCacheKey = holder.product_image.metadata?.memoryCacheKey)
        }
        holder.product_card.setOnClickListener { view ->
            onCardClickListener.onClicked(product_item,memoryCacheKey = holder.product_image.metadata?.memoryCacheKey)
        }

    }


    override fun getItemCount(): Int {
        return product_items.size
    }

    private fun weightString(weight: Number, int: Int): String {
        val gramWeight = weight.toFloat() * int

        return "${gramWeight.toInt()} г"
    }
}