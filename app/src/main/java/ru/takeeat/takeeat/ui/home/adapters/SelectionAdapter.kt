package ru.takeeat.takeeat.ui.home.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.db.core.Selection
import ru.takeeat.takeeat.ui.home.interfaces.SelectionsClickListener

class SelectionAdapter(
    private val selection_items: List<Selection>,
    private val onClickListener: SelectionsClickListener<Selection>,
) : RecyclerView.Adapter<SelectionAdapter.MyViewHolder>() {

    class MyViewHolder constructor(view: View) : RecyclerView.ViewHolder(view) {
        val selection_button: Button = view.findViewById(R.id.selection_button) as Button
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_selections_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val selection_item = selection_items[position]
//        Log.i("NewsAdapter", selection_item.toString())
        holder.selection_button.text = "${selection_item.emoji_android} ${selection_item.title}"

        holder.selection_button.setOnClickListener { view ->
            onClickListener.onClicked(selection_item)
        }
    }


    override fun getItemCount(): Int {
        return selection_items.size
    }

}