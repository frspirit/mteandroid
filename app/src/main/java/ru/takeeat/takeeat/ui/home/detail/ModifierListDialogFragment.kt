package ru.takeeat.takeeat.ui.home.detail


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.databinding.FragmentItemListDialogListDialogBinding
import ru.takeeat.takeeat.db.core.GroupModifierWithModifierStr
import ru.takeeat.takeeat.db.order.ModifierOrder
import ru.takeeat.takeeat.db.core.GroupModifier
import ru.takeeat.takeeat.db.core.Modifier
import ru.takeeat.takeeat.db.core.ModifierStr

import ru.takeeat.takeeat.ui.home.interfaces.ModifierClickListener
import ru.takeeat.takeeat.ui.home.adapters.ProductModifierAdapter


class ModifierListDialogFragment(
    private var mod: MutableLiveData<MutableList<ModifierOrder>>,
    private val selectedModifier: List<ModifierOrder>?,
    private val modifiers: List<Modifier>,
    private val group_modifier: GroupModifier,
    private val childModifier: List<ModifierStr>

) : BottomSheetDialogFragment() {
    private lateinit var viewModel: ModifierListDialogViewModel
    private var _binding: FragmentItemListDialogListDialogBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialog)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        viewModel = ViewModelProvider(this).get(ModifierListDialogViewModel::class.java)
        _binding = FragmentItemListDialogListDialogBinding.inflate(inflater, container, false)

        onSetModifier()

        val adapter = ProductModifierAdapter(
            childModifier,
            modifiers,
            selectedModifier,
            object : ModifierClickListener<Modifier> {
                override fun onPlusModifier(
                    modifier: Modifier,
                    countTV: TextView,
                    buttonMinus: Button
                ) {
                    if (viewModel.allCountModifier.value!! < group_modifier.maxAmount.toInt()) {

                        val out = viewModel.onAddModifier(
                            mod = modifier,
                            group_uuid = group_modifier.uuid,
                            group_name = group_modifier.name
                        )
//                        mod.value = out
                        mod.value = viewModel.modifier.value
                        countTV.visibility = View.VISIBLE
                        buttonMinus.visibility = View.VISIBLE
                        countTV.text = out.toString()

                    }


                }

                override fun onMinusModifier(
                    modifier: Modifier,
                    countTV: TextView,
                    buttonMinus: Button
                ) {
                    val out = viewModel.onRemoveModifier(
                        modUuid = modifier.uuid,
                    )
                    if (out == 0) {
                        countTV.text = ""
                        countTV.visibility = View.INVISIBLE
                        buttonMinus.visibility = View.INVISIBLE
                    } else {
                        countTV.text = out.toString()
                    }
                    mod.value = viewModel.modifier.value

                }
            })

        binding.modifierListRV.adapter = adapter

//        viewModel.allCountModifier.observe(viewLifecycleOwner, {
//            binding.modifierCheckedCount.text = it.toString()
//        })
//
//        binding.modifierMaxCount.text = group_modifier.maxAmount.toInt().toString()


        return binding.root
    }

    private fun onSetModifier() {
        if (!selectedModifier.isNullOrEmpty()) {
            viewModel.onSetModifier(selectedModifier)
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}