package ru.takeeat.takeeat.ui.home.detail

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ru.takeeat.takeeat.db.order.ModifierOrder
import ru.takeeat.takeeat.network.mtesapi.core.Modifier


class ModifierListDialogViewModel : ViewModel() {
    private val _allCountModifier = MutableLiveData<Int>(0)
    val allCountModifier: LiveData<Int>
        get() = _allCountModifier

    private val _modifier = MutableLiveData<MutableList<ModifierOrder>>(mutableListOf())
    val modifier: LiveData<MutableList<ModifierOrder>>
        get() = _modifier


    fun onAddModifier(mod: ru.takeeat.takeeat.db.core.Modifier, group_name: String, group_uuid: String): Int {
        if (_modifier.value != null) {
            var noEmpty = true
            _modifier.value?.forEach {
                if (it.uuid == mod.uuid) {
                    it.amount = it.amount.plus(1)
                    _allCountModifier.value = _allCountModifier.value?.plus(1)
                    noEmpty = false
                    return it.amount
                }
            }
            if (noEmpty) {
                val modOrder = ModifierOrder(
                    uuid = mod.uuid,
                    name = mod.name,
                    price = mod.price.toFloat(),
                    amount = 1,
                    group_name = group_name,
                    group_uuid = group_uuid,
                )
                _allCountModifier.value = _allCountModifier.value?.plus(1)
                _modifier.value!!.add(modOrder)
                return modOrder.amount
            }
        }
        val modOrder = ModifierOrder(
            uuid = mod.uuid,
            name = mod.name,
            price = mod.price.toFloat(),
            amount = 1,
            group_name = group_name,
            group_uuid = group_uuid
        )

        _modifier.value = mutableListOf(modOrder)

        _allCountModifier.value = _allCountModifier.value?.plus(1)
//        Log.i("modifierList", "Create - ${_modifier.value.toString()} ")
        return modOrder.amount


    }
//    fun onRemoveModifier(modUuid: String):  MutableList<ModifierOrder> {
//        _modifier.value?.forEach {
//            if (it.uuid == modUuid) {
//                allCountModifier.value!!.minus(it.amount)
//                _modifier.value?.remove(it)
//            }
//        }
//        return _modifier.value!!
//    }

    fun onRemoveModifier(modUuid: String): Int {
        var out = 0
        var modRemove: ModifierOrder? = null
        _modifier.value?.forEach {
            if (it.uuid == modUuid) {
                if (it.amount > 1) {
                    it.amount = it.amount.minus(1)
                    _allCountModifier.value = _allCountModifier.value?.minus(1)
                    out = it.amount
                } else {
                    _allCountModifier.value = _allCountModifier.value?.minus(1)
                    modRemove = it
                }

                return@forEach
            }
        }
        if( modRemove != null) {
            _modifier.value!!.remove(modRemove)
        }
        return out
    }

    fun onSetModifier(mod: List<ModifierOrder>) {
        _modifier.value = mod.toMutableList()
        mod.forEach {
            _allCountModifier.value = _allCountModifier.value?.plus(it.amount)
        }
    }
}