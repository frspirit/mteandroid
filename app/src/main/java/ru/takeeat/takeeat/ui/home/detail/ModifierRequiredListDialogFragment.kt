package ru.takeeat.takeeat.ui.home.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.databinding.FragmentItemListDialogListDialog2Binding
import ru.takeeat.takeeat.db.core.GroupModifier
import ru.takeeat.takeeat.db.core.GroupModifierWithModifierStr
import ru.takeeat.takeeat.db.core.Modifier
import ru.takeeat.takeeat.db.core.ModifierStr
import ru.takeeat.takeeat.db.order.GroupModifierWithModifierStrOrder
import ru.takeeat.takeeat.db.order.ModifierOrder
import ru.takeeat.takeeat.ui.home.adapters.ProductModifierRequiredAdapter
import ru.takeeat.takeeat.ui.home.interfaces.ModifierRequiredClickListener


class ModifierRequiredListDialogFragment(
    private val mod: MutableLiveData<ModifierOrder>,
    private val selectedRequireModifier: ModifierOrder?,
    private val modifiers: List<Modifier>,
    private val group_modifier: GroupModifier,
    private val childModifier: List<ModifierStr>
) : BottomSheetDialogFragment() {

    private var _binding: FragmentItemListDialogListDialog2Binding? = null
    private val binding get() = _binding!!


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialog)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentItemListDialogListDialog2Binding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val adapter = ProductModifierRequiredAdapter(
            lastRequireModifier = selectedRequireModifier?.uuid,
            modifier_items = childModifier,
            modifier_list = modifiers,
            object : ModifierRequiredClickListener<Modifier> {
                override fun onClicked(modifier: Modifier,) {
                    mod.value = ModifierOrder(
                        uuid = modifier.uuid,
                        name = modifier.name,
                        price = modifier.price.toFloat(),
                        amount = 1,
                        group_name = group_modifier.name,
                        group_uuid = group_modifier.uuid,
                    )

                }
            })
        binding.modifierRequiredRV.adapter = adapter

    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}