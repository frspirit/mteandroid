package ru.takeeat.takeeat.ui.home.detail


import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.*

import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider

import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.snackbar.Snackbar
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.SERVER_IMAGE
import ru.takeeat.takeeat.databinding.FragmentProductDetailBinding
import ru.takeeat.takeeat.db.AppDataBase
import ru.takeeat.takeeat.db.order.ModifierOrder

import android.widget.FrameLayout
import coil.load
import ru.takeeat.takeeat.curvedLine
import ru.takeeat.takeeat.db.core.Product


class ProductDetailFragment : Fragment() {
    private lateinit var modOnlyObserver: MutableLiveData<MutableList<ModifierOrder>>
    private lateinit var modObserver: MutableLiveData<MutableList<ModifierOrder>>
    private lateinit var requiredModObserver: MutableLiveData<ModifierOrder>
    private lateinit var viewModel: ProductDetailViewModel
    private lateinit var viewModelFactory: ProductDetailViewModelFactory
    private lateinit var _binding: FragmentProductDetailBinding
    private var args: ProductDetailFragmentArgs? = null
    private val binding get() = _binding


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        activity?.let { actv ->
            actv.title = ""
            actv.findViewById<BottomNavigationView>(R.id.nav_view).visibility = View.GONE
        }

        modObserver = MutableLiveData()
        requiredModObserver = MutableLiveData()

        _binding = FragmentProductDetailBinding.inflate(inflater, container, false)
        val application = requireNotNull(this.activity).application
        val db = AppDataBase.getInstance(application)
        args = ProductDetailFragmentArgs.fromBundle(requireArguments())
        viewModelFactory = ProductDetailViewModelFactory(args!!.product, db, application)
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(ProductDetailViewModel::class.java)

        binding.viewmodel = viewModel


        onRenderModifierSheets()
//        onAddInBasket()
        return binding.root
    }

    fun onSetTableInfo(data: Product) {
        if (data.weight != 0f || data.energyFullAmount != 0f || data.carbohydrateFullAmount != 0f || data.fatFullAmount != 0f || data.fiberFullAmount != 0f) {
            binding.tableRow.visibility = View.VISIBLE
            if (data.weight != 0f) {
                binding.productDetailLayoutWeigjht.visibility = View.VISIBLE
                binding.productDetailWeight.text = "${(data.weight * 1000).toInt()}"
            }
            if (data.energyFullAmount != 0f) {
                binding.productDetailLayoutEnergy.visibility = View.VISIBLE
                binding.productDetailEnergyFullAmount.text =
                    data.energyFullAmount.toInt().toString()
            }
            if (data.carbohydrateFullAmount != 0f) {
                binding.productDetailLayoutCarbohydrate.visibility = View.VISIBLE
                binding.productDetailCarbohydrateFullAmount.text =
                    data.carbohydrateFullAmount.toInt().toString()
            }
            if (data.fatFullAmount != 0f) {
                binding.productDetailLayoutFat.visibility = View.VISIBLE
                binding.productDetailFatFullAmount.text = data.fatFullAmount.toInt().toString()
            }
            if (data.fiberFullAmount != 0f) {
                binding.productDetailFiberFullAmount.visibility = View.VISIBLE
                binding.productDetailFiberFullAmount.text = data.fiberFullAmount.toInt().toString()
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun onRenderModifierSheets() {
        viewModel.product.observe(viewLifecycleOwner, { data ->
            if (data != null) {
                viewModel.setProductInfo(data)
                binding.productDetailName.text = data.name
                binding.productDetailDescription.text = data.description
//                binding.productDetailImage.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        requireActivity(),
//                        R.drawable.defimage
//                    )
//                ) $SERVER_IMAGE${data.image_priority ?: data.image}
                Log.d("PRODUCTDETAILIMAGE", "$SERVER_IMAGE${data.image_priority ?: data.image}")

                val params = binding.productDetailImageLL.layoutParams
                params.height = resources.displayMetrics.heightPixels / 2
                binding.productDetailImageLL.layoutParams = params

//            newDetailImage.scaleType = ImageView.ScaleType.FIT_XY

                binding.productDetailImage.load(uri = "$SERVER_IMAGE${data.image_priority ?: data.image}") {
                    memoryCacheKey(args?.memoryCacheKey)
                    crossfade(false)
                    placeholder(R.drawable.defimage)
                }
                params.height = binding.productDetailImage.layoutParams.height
                binding.productDetailImageLL.layoutParams = params
                binding.productDetailPrice.text = data.price.toInt().toString()
                if (data.p_price != null){
                    binding.pPriceTextView.text = "${data.p_price.toInt()} ₽"
                    binding.pPriceTextView.measure(0,0)
                    val width = binding.pPriceTextView.measuredWidth.toFloat()
                    val height = binding.pPriceTextView.measuredHeight.toFloat()
                    val bitmap = curvedLine(width,height)
                    binding.productPriceCurveLine.setImageBitmap(bitmap)
                }
                onSetTableInfo(data)
//                binding.productDetailWeight.text = "${(data.weight * 1000).toInt()}"
//                binding.productDetailCarbohydrateFullAmount.text =
//                    data.carbohydrateFullAmount.toInt().toString()
//                binding.productDetailEnergyFullAmount.text =
//                    data.energyFullAmount.toInt().toString()
//                binding.productDetailFatFullAmount.text = data.fatFullAmount.toInt().toString()
//                binding.productDetailFiberFullAmount.text = data.fiberFullAmount.toInt().toString()

            }

        })
        viewModel.groupModifiers.observe(viewLifecycleOwner, { data ->
            // Log.d("groupModifier3", data.toString())
            data.forEach { group_modifier ->

                when {
                    group_modifier.group_modifier.required -> {
                        binding.productDetailModifierRequiredCard.visibility = View.VISIBLE
                        binding.productDetailModifierRequiredButton.text =
                            group_modifier.group_modifier.name
                        binding.productDetailButtonInBasket.isEnabled = false
                        binding.productDetailButtonInBasket.alpha = 0.5F
                        binding.productDetailButtonInBasket.text =
                            "Выбирете \"${group_modifier.group_modifier.name}\""
                        viewModel.permissionRequireModifier = true

                        viewModel.require_modifier.observeForever {
                            if (it != null) {
                                binding.productDetailButtonInBasket.alpha = 1F
                                binding.productDetailButtonInBasket.isEnabled = true
                                binding.productDetailButtonInBasket.text = "В корзину"
                            }
                        }
                        viewModel.modifiersDB.observe(viewLifecycleOwner, { modifiers ->


                            binding.productDetailModifierRequiredButton.setOnClickListener {
                                val bottomSheetRequiredFragment =
                                    ModifierRequiredListDialogFragment(
                                        mod = requiredModObserver,
                                        selectedRequireModifier = requiredModObserver.value,
                                        modifiers = modifiers,
                                        group_modifier = group_modifier.group_modifier,
                                        childModifier = group_modifier.child_modifier!!
                                    )
                                bottomSheetRequiredFragment.show(
                                    childFragmentManager,
                                    "ModifierRequiredBottomSheet"
                                )
                            }

                        })


                    }
                    else -> {
                        binding.productDetailModifierCard.visibility = View.VISIBLE
                        binding.productDetailModifierButton.text =
                            group_modifier.group_modifier.name
                        viewModel.modifiersDB.observe(viewLifecycleOwner, { modifiers ->

                            binding.productDetailModifierButton.setOnClickListener {
                                val bottomSheetFragment = ModifierListDialogFragment(
                                    mod = modObserver,
                                    selectedModifier = modObserver.value,
                                    modifiers = modifiers,
                                    group_modifier = group_modifier.group_modifier,
                                    childModifier = group_modifier.child_modifier!!
                                )
                                bottomSheetFragment.show(
                                    childFragmentManager,
                                    "ModifierBottomSheet"
                                )
                            }

                        })

                    }

                }

            }


        })
        viewModel.price.observe(viewLifecycleOwner, {
            binding.productDetailPrice.text = it.toString()
        })

        modObserver.observeForever {
            viewModel.onSetModifier(it)
            binding.productDetailPrice.text = viewModel.price.value.toString()
        }
        requiredModObserver.observeForever {
            viewModel.onSetRequireModifier(it)
        }

        binding.productDetailButtonInBasket.setOnClickListener {
            Snackbar.make(requireView(), "Добавлено в корзину!", Snackbar.LENGTH_LONG).apply {
                setBackgroundTint(resources.getColor(R.color.AccentColor))
                setTextColor(resources.getColor(R.color.white))

                val params = view.layoutParams as FrameLayout.LayoutParams
                params.topMargin = 118
                params.gravity = Gravity.CENTER_HORIZONTAL or Gravity.TOP
                show()
            }
//            Toast.makeText(context, "Добавлено", Toast.LENGTH_LONG).show()
            viewModel.onAddedProductInBasket()

        }

    }


//    private fun onAddInBasket() {
//        binding.productDetailButtonInBasket.setOnClickListener {
//            Snackbar.make(requireView(),"Добавлено в корзину!", Snackbar.LENGTH_LONG).apply {
//                setBackgroundTint(resources.getColor(R.color.TakeEat))
//                setTextColor(resources.getColor(R.color.white))
//
//                val params = view.layoutParams as FrameLayout.LayoutParams
//                params.topMargin = 118
//                params.gravity = Gravity.CENTER_HORIZONTAL or Gravity.TOP
//                show()
//            }
////            Toast.makeText(context, "Добавлено", Toast.LENGTH_LONG).show()
//            viewModel.onAddedProductInBasket()
//
//        }
//    }

    override fun onDestroyView() {
        super.onDestroyView()
        activity?.let { actv ->
            actv.findViewById<BottomNavigationView>(R.id.nav_view).visibility = View.VISIBLE
        }

    }
}