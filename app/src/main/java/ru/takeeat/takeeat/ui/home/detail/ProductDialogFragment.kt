package ru.takeeat.takeeat.ui.home.detail

import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.snackbar.Snackbar
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.databinding.FragmentProductDetailDialogBinding
import ru.takeeat.takeeat.db.AppDataBase
import ru.takeeat.takeeat.db.order.ModifierOrder

import ru.takeeat.takeeat.network.mtesapi.core.Product

class ProductDialogFragment() : BottomSheetDialogFragment() {

    private lateinit var modObserver: MutableLiveData<MutableList<ModifierOrder>>
    private lateinit var requiredModObserver: MutableLiveData<ModifierOrder>
    private lateinit var viewModel: ProductDialogViewModel
    private lateinit var product: Product
    private lateinit var args: ProductDialogFragmentArgs
    private lateinit var viewModelFactory: ProductDialogViewModelFactory
    private var _binding: FragmentProductDetailDialogBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Log.d("onBindStart", "onStart ProductDialogFragment")
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialog)


        modObserver = MutableLiveData()
        requiredModObserver = MutableLiveData()

        args = ProductDialogFragmentArgs.fromBundle(requireArguments())
        val application = requireNotNull(this.activity).application
        val db = AppDataBase.getInstance(application)

        viewModelFactory = ProductDialogViewModelFactory(args.idProduct, db, application)

        viewModel = ViewModelProvider(this, viewModelFactory)
            .get(ProductDialogViewModel::class.java)


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProductDetailDialogBinding.inflate(inflater, container, false)




        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onAddInBasket()
        onModifier()
    }

    private fun onModifier() {

        viewModel.product.observeForever { data ->
            if (data != null) {
                binding.productDialogTitle.text = data.name
                viewModel.setProductInfo(data)

                // Log.d("onBindPProduct", "${data.name} - ${data.price}")
                return@observeForever
            }
        }
        viewModel.groupModifiers.observe(viewLifecycleOwner, { data ->

            data.forEach { group_modifier ->
                when {
                    group_modifier.group_modifier.required -> {
                        binding.productDetailModifierRequiredCard.visibility = View.VISIBLE
                        binding.productDetailModifierRequiredButton.text =
                            group_modifier.group_modifier.name
                        binding.productOrderPriceButton.isEnabled = false
                        binding.productOrderPriceButton.alpha = 0.5F
                        viewModel.permissionRequireModifier = true
                        viewModel.require_modifier.observe(viewLifecycleOwner, {
                            if (it != null) {
                                binding.productOrderPriceButton.isEnabled = true
                                binding.productOrderPriceButton.alpha = 1F
                            }
                        })
                        viewModel.modifiersDB.observe(viewLifecycleOwner, { modifiers ->
                            if (!modifiers.isNullOrEmpty()) {
                                binding.productDetailModifierRequiredButton.setOnClickListener {
                                    val bottomSheetRequiredFragment =
                                        ModifierRequiredListDialogFragment(
                                            mod = requiredModObserver,
                                            selectedRequireModifier = requiredModObserver.value,
                                            modifiers = modifiers,
                                            group_modifier = group_modifier.group_modifier,
                                            childModifier = group_modifier.child_modifier!!,
                                        )

                                    bottomSheetRequiredFragment.show(
                                        childFragmentManager,
                                        "ModifierRequiredBottomSheet"
                                    )
                                }
                            }

                        })

                    }
                    else -> {
                        binding.productDetailModifierCard.visibility = View.VISIBLE
                        binding.productDetailModifierButton.text =
                            group_modifier.group_modifier.name
                        viewModel.modifiersDB.observe(viewLifecycleOwner, { modifiers ->
                            if (!modifiers.isNullOrEmpty()) {
                                binding.productDetailModifierButton.setOnClickListener {
                                    val bottomSheetFragment = ModifierListDialogFragment(
                                        mod = modObserver,
                                        selectedModifier = modObserver.value,
                                        modifiers = modifiers,
                                        group_modifier = group_modifier.group_modifier,
                                        childModifier = group_modifier.child_modifier!!,
                                    )

                                    bottomSheetFragment.show(
                                        childFragmentManager,
                                        "ModifierBottomSheet"
                                    )
                                }
                            }
                        })
                    }

                }

            }
        })
        viewModel.price.observe(viewLifecycleOwner, {
            binding.productOrderPriceButton.text = "+ $it ₽"
        })

        modObserver.observe(viewLifecycleOwner, {

            viewModel.onSetModifier(it)

        })

        requiredModObserver.observe(viewLifecycleOwner, {
            viewModel.onSetRequireModifier(it)

        })
    }

    private fun onAddInBasket() {
        binding.productOrderPriceButton.setOnClickListener {
            Snackbar.make(it, "Добавлено в корзину!", Snackbar.LENGTH_LONG).apply {
                setBackgroundTint(resources.getColor(R.color.AccentColor))
                setTextColor(resources.getColor(R.color.white))

                val params = view.layoutParams as CoordinatorLayout.LayoutParams
                params.topMargin = 118
                params.gravity = Gravity.CENTER_HORIZONTAL or Gravity.TOP
                show()
            }
            viewModel.onAddedProductInBasket()
        }
    }

//    override fun onDestroyView() {
//        super.onDestroyView()
//
////        _binding = null
//    }
}