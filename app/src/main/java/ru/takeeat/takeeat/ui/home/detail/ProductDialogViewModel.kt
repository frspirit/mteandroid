package ru.takeeat.takeeat.ui.home.detail

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.*
import ru.takeeat.takeeat.db.AppDataBase
import ru.takeeat.takeeat.db.core.GroupModifier
import ru.takeeat.takeeat.db.core.GroupModifierWithModifierStr
import ru.takeeat.takeeat.db.core.ModifierStr
import ru.takeeat.takeeat.db.order.ModifierOrder
import ru.takeeat.takeeat.db.order.ProductOrder
import ru.takeeat.takeeat.network.mtesapi.core.Modifier
import ru.takeeat.takeeat.db.core.Product
import ru.takeeat.takeeat.network.mtesapi.core.CoreAPI


class ProductDialogViewModel(
    val idProduct: Long,
    val db: AppDataBase,
    application: Application
) : AndroidViewModel(application) {
    private val _service = CoreAPI()
    private var _product = db.productCore().getLD(idProduct)
    val product: LiveData<Product>
        get() = _product
    private val _modifiers = MutableLiveData<List<Modifier>>()
    val modifiers: LiveData<List<Modifier>>
        get() = _modifiers
    private val _modifiersDB = db.modifierDAO().getLDAll()
    val modifiersDB: LiveData<List<ru.takeeat.takeeat.db.core.Modifier>>
        get() = _modifiersDB

    fun getModifiers() {
        _service.callModifiers(_modifiers)
    }

    private var _groupModifiers = db.groupModifierDAO().getGroupModifiersByProduct(idProduct)
    val groupModifiers: LiveData<List<GroupModifierWithModifierStr>> get() = _groupModifiers



    private var permissionAddToBasket: Boolean = false
    var permissionRequireModifier: Boolean = false

    private val _require_modifier = MutableLiveData<ModifierOrder>()
    val require_modifier: LiveData<ModifierOrder>
        get() = _require_modifier
    private val _require_modifier_save = MutableLiveData<ModifierOrder>()

    fun onSetRequireModifier(mod: ModifierOrder) {
        _require_modifier.value = mod
        permissionAddToBasket = true

    }

    private val _modifier = MutableLiveData<MutableList<ModifierOrder>>()
    val modifier: LiveData<MutableList<ModifierOrder>>
        get() = _modifier
    private var _modifier_save = MutableLiveData<MutableList<ModifierOrder>>()

    fun onSetModifier(mod: MutableList<ModifierOrder>) {
        _modifier.value = mod
        var price_product: Int = _product.value?.price!!.toInt()
        mod.forEach {
            price_product = price_product.plus(it.price.toInt() * it.amount)
        }
        _price.value = price_product
    }

    private var _product_db = MutableLiveData<ProductOrder>()
    val product_db: LiveData<ProductOrder>
        get() = _product_db


    private var viewModelJob: CompletableJob = Job()
    private val uiScope: CoroutineScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    fun onAddedProductInBasket() {
        if (!modifiersListEqual() ||
            (permissionRequireModifier && (_require_modifier_save.value != _require_modifier.value))
        ) {
//            Log.d("BoolLogic","${!modifiersListEqual()}")
            _product_db = MutableLiveData<ProductOrder>()
        }
        // Log.d("BoolLogic", "${!modifiersListEqual()}")
        uiScope.launch {

            if (_product_db.value != null) {

                _product_db.value!!.amount += 1
                db.productOrderDAO().update(_product_db.value!!)
            } else {
                if (_groupModifiers.value.isNullOrEmpty()){
                    val product = db.productOrderDAO().getByUuid(_product.value!!.uuid)
                    if(product != null){
                        product.amount += 1
                        db.productOrderDAO().update(product)
                        return@launch
                    }
                }
                val idProductDB = db.productOrderDAO().insert(
                    ProductOrder(
                        uuid = product.value!!.uuid,
                        name = product.value!!.name,
                        amount = 1,
                        image = product.value!!.image_priority
                            ?: _product.value!!.image ?: "",
                        price = price.value!!.toFloat(),
                        priceProduct = product.value!!.price.toFloat(),
                        p_price = product.value!!.p_price,
                        weight = numToInt(product.value!!.weight ?: 0, 1000)
                    )
                )
                _product_db.value = db.productOrderDAO().get(idProductDB)

                if (permissionRequireModifier) {
                    onAddedRequireModifierInDataBase()
                    _require_modifier_save.value = _require_modifier.value
                }
                if (!_modifier.value.isNullOrEmpty()) {
                    onAddedModifierInDataBase()
                    _modifier_save.value = _modifier.value!!.toMutableList()
                }

                setGroupModifier(db.groupModifierDAO().getByProduct(idProduct))
            }

        }


    }

    fun modifiersListEqual(): Boolean {
//        Log.d("BoolLogic1","${_modifier.value?.size } - ${_modifier_save.value?.size} - ${_modifier.value?.size != _modifier_save.value?.size}")

        if (_modifier.value?.size != _modifier_save.value?.size)
            return false

        val pairList = _modifier.value?.zip(_modifier_save.value!!)

        if (pairList != null) {
            return pairList.all { (elt1, elt2) ->
                elt1 == elt2
            }
        }
        return false
    }

    //    fun setGroupModifier(groupModifier: GroupModifierWithModifierStr) {
//        uiScope.launch {
//            _product.value?.group_modifier?.forEach {
//                db.groupModifierDAO().insert(
//                    ru.takeeat.takeeat.db.core.GroupModifier(
//                        uuid = it.uuid,
//                        idProduct = _product_db.value!!.id,
//                        name = it.name,
//                        maxAmount = it.maxAmount.toFloat(),
//                        minAmount = it.minAmount.toFloat(),
//                        required = it.required,
//                    )
//                )
//                val gm = db.groupModifierDAO().getTonight()
//                if (gm != null) {
//                    it.child_modifier.forEach {
//                        db.modifierStrDAO().insert(
//                            ModifierStr(
//                                uuid = it,
//                                idMGroup = gm.id
//                            )
//                        )
//                    }
//                }
//
//            }
//
//        }
//    }
    private suspend fun setGroupModifier(groupModifiers: List<GroupModifier>) {
        groupModifiers.forEach { groupModifier ->
            val idGroupModifier = db.groupModifierDAO().insert(
                GroupModifier(
                    uuid = groupModifier.uuid,
                    idProductOrder = _product_db.value!!.id,
                    name = groupModifier.name,
                    maxAmount = groupModifier.maxAmount.toFloat(),
                    minAmount = groupModifier.minAmount.toFloat(),
                    required = groupModifier.required,
                )
            )
            val childModifier = db.modifierStrDAO().getByMGroup(groupModifier.id)
            // Log.d("setGroupModifier", "${childModifier}")
            childModifier.forEach { modifierStr ->
                db.modifierStrDAO().insert(
                    ModifierStr(
                        uuid = modifierStr.uuid,
                        idMGroupOrder = idGroupModifier
                    )
                )
            }


        }

    }

    private suspend fun onAddedRequireModifierInDataBase() {
            val requireModifierOrderForInsert = ModifierOrder(
                idProductOrder = product_db.value?.id,
                uuid = require_modifier.value!!.uuid,
                name = require_modifier.value!!.name,
                price = require_modifier.value!!.price,
                amount = require_modifier.value!!.amount,
                group_name = require_modifier.value!!.group_name,
                group_uuid = require_modifier.value!!.group_uuid,
            )

            db.modifierOrderDAO().insert(requireModifierOrderForInsert)
    }

    private suspend fun onAddedModifierInDataBase() {
        if (_product_db.value != null) {
            _modifier.value?.forEach { modifier_order ->
                val modifierOrderForInsert = ModifierOrder(
                    idProductOrder = product_db.value?.id,
                    uuid = modifier_order.uuid,
                    name = modifier_order.name,
                    price = modifier_order.price,
                    amount = modifier_order.amount,
                    group_name = modifier_order.group_name,
                    group_uuid = modifier_order.group_uuid,
                )

                db.modifierOrderDAO().insert(modifierOrderForInsert)
            }

        }

    }

    private val _allCountModifier = MutableLiveData<Int>(0)
    val allCountModifier: LiveData<Int>
        get() = _allCountModifier


    private val _price = MutableLiveData<Int>()
    val price: LiveData<Int>
        get() = _price


    private val _weight = MutableLiveData<Int>()
    val weight: LiveData<Int>
        get() = _weight


    private val _count_modifier = MutableLiveData<Int>(0)
    val modifier_count: LiveData<Int>
        get() = _count_modifier

    fun plusCountModifier() {
        _count_modifier.value
    }

    //    fun onSkip() {
//        _score.value = (_score.value)?.minus(1)
//        nextWord()
//    }
    fun setProductInfo(product: ru.takeeat.takeeat.db.core.Product) {
//        _weight.value = numToInt(product.weight , 1000)
        _price.value = product.price.toInt()
    }

    init {
//        getModifiers()
        // Log.d("onBindViewModel", this.toString())
    }

    private fun numToInt(num: Number, int: Int): Int {
        val outNum = num.toFloat() * int
        return outNum.toInt()
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}