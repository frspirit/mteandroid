package ru.takeeat.takeeat.ui.home.group

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.*
import android.widget.FrameLayout
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import coil.memory.MemoryCache
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.databinding.FragmentGroupDetailBinding
import ru.takeeat.takeeat.db.AppDataBase
import ru.takeeat.takeeat.db.core.Product
import ru.takeeat.takeeat.ui.home.adapters.ProductDBAdapter
import ru.takeeat.takeeat.ui.home.interfaces.ProductCardClickListener

class GroupDetailFragment : Fragment() {

    private lateinit var viewModel: GroupDetailViewModel
    private lateinit var viewModelFactory: GroupDetailViewModelFactory
    private var _binding: FragmentGroupDetailBinding? = null
    private val binding get() = _binding!!
    private lateinit var args: GroupDetailFragmentArgs
    private lateinit var all_products: MutableList<Product>
    private lateinit var db: AppDataBase
    private val adapter = MutableLiveData<ProductDBAdapter>()
    private var adapterObj: ProductDBAdapter? = null
    private var viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val application = requireNotNull(this.activity).application
        db = AppDataBase.getInstance(application)
        args = GroupDetailFragmentArgs.fromBundle(requireArguments())
        viewModelFactory = GroupDetailViewModelFactory(args.idGroup, db, application)
        viewModel = ViewModelProvider(this, viewModelFactory).get(GroupDetailViewModel::class.java)
        activity?.let { actv ->
            actv.findViewById<MaterialToolbar>(R.id.my_toolbar)?.title = args.name
            actv.window.statusBarColor = resources.getColor(R.color.white, null)
            actv.findViewById<Toolbar>(R.id.my_toolbar)
                ?.setBackgroundColor(resources.getColor(R.color.white))
        }


        viewModel.subGroupProduct.observeForever {
            if (!it.isNullOrEmpty()) {
                Handler().postDelayed({
                    all_products = it.toMutableList()
                    adapter.value = ProductDBAdapter(
                        all_products,
                        object : ProductCardClickListener<Product> {
                            override fun onClicked(
                                product_item: Product,
                                memoryCacheKey: MemoryCache.Key?
                            ) {
                                requireView().findNavController().navigate(
                                    GroupDetailFragmentDirections
                                        .actionGroupDetailFragment2ToProductDetailFragment(
                                            product_item.id,
                                            memoryCacheKey.toString()
                                        )
                                )
                            }

                            override fun onClickedAddBasket(
                                product_item: Product,
                                memoryCacheKey: MemoryCache.Key?
                            ) {
                                if (product_item.groupModifierBool) {
                                    requireView().findNavController().navigate(
                                        GroupDetailFragmentDirections
                                            .actionGroupDetailFragment2ToProductDialogFragment(
                                                product_item.id
                                            )
                                    )
                                } else {
                                    Snackbar.make(
                                        requireView(),
                                        "Добавлено в корзину!",
                                        Snackbar.LENGTH_LONG
                                    ).apply {
                                        setBackgroundTint(resources.getColor(R.color.AccentColor))
                                        setTextColor(resources.getColor(R.color.white))

                                        val params = view.layoutParams as FrameLayout.LayoutParams
                                        params.topMargin = 118
                                        params.gravity = Gravity.CENTER_HORIZONTAL or Gravity.TOP

                                        show()
                                    }
                                    viewModel.onAddedProductInBasket(product_item)
                                }
                            }
                        })
                    return@postDelayed

                }, 500)
                return@observeForever
            }
        }
        // Log.d("GroupDetailFragment","PuppyCounter - MainActivity - onCreate()")

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        //activity?.findViewById<TextView>(R.id.toolbar_)?.visibility = View.GONE

        _binding = FragmentGroupDetailBinding.inflate(inflater, container, false)

        binding.groupProductRV.layoutManager = LinearLayoutManager(context)
        onEnableRadioButton()
        onSetProductNoSubGroup()
        adapter.observeForever {
            if (it != null) {

                binding.groupProductLiner.visibility = View.VISIBLE
                binding.groupProductRV.adapter = it
                adapterObj = it
            }
        }
        // Log.d("GroupDetailFragment","PuppyCounter - MainActivity - onStart()")
        return binding.root
    }

//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//
//        binding.groupTitle.text = args.name
//
////        onEnableRadioButton()
////        onSetProductNoSubGroup()
//        // Log.d("GroupDetailFragment","PuppyCounter - MainActivity - onCreateView()")
//
//    }


    private fun onEnableRadioButton() {
        binding.groupProductLiner.visibility = View.INVISIBLE


        viewModel.subGroup.observe(viewLifecycleOwner, { listSubGroup ->

            if (!listSubGroup.isNullOrEmpty()) {
                val listSubGroup = listSubGroup.reversed()
                binding.scrollSubgroup.visibility = View.VISIBLE
                uiScope.launch {
                    listSubGroup.forEachIndexed { index, subGroup ->
                        when (index) {
                            0 -> {
                                binding.radioButton1.text = subGroup.sub_groups.name
                                binding.radioButton1.visibility = View.VISIBLE
                            }
                            1 -> {
                                binding.radioButton2.text = subGroup.sub_groups.name
                                binding.radioButton2.visibility = View.VISIBLE
                            }
                            2 -> {
                                binding.radioButton3.text = subGroup.sub_groups.name
                                binding.radioButton3.visibility = View.VISIBLE
                            }
                            3 -> {
                                binding.radioButton4.text = subGroup.sub_groups.name
                                binding.radioButton4.visibility = View.VISIBLE
                            }
                            4 -> {
                                binding.radioButton5.text = subGroup.sub_groups.name
                                binding.radioButton5.visibility = View.VISIBLE
                            }
                            5 -> {
                                binding.radioButton6.text = subGroup.sub_groups.name
                                binding.radioButton6.visibility = View.VISIBLE
                            }

                        }
                    }

                    binding.radioGroup.setOnCheckedChangeListener { group, checkedId ->
                        Log.d("PCT", "${listSubGroup[0]}")
                        when (checkedId) {
                            binding.radioButtonAll.id -> {
                                onSetProductInRV(all_products)
                            }
                            binding.radioButton1.id -> {
                                onSetProductInRV(listSubGroup[0].products!!)
                            }
                            binding.radioButton2.id -> {
                                onSetProductInRV(listSubGroup[1].products!!)
                            }
                            binding.radioButton3.id -> {
                                onSetProductInRV(listSubGroup[2].products!!)
                            }
                            binding.radioButton4.id -> {
                                onSetProductInRV(listSubGroup[3].products!!)
                            }
                            binding.radioButton5.id -> {
                                onSetProductInRV(listSubGroup[4].products!!)
                            }
                            binding.radioButton6.id -> {
                                onSetProductInRV(listSubGroup[5].products!!)
                            }

                        }
                    }
                }
                return@observe
            } else {
                binding.scrollSubgroup.visibility = View.GONE
            }

        })


    }

    private fun onSetProductNoSubGroup() {
        // TODO(developer): Отслеживание кнопок

        viewModel.products.observe(viewLifecycleOwner, { listProduct ->
            if (!listProduct.isNullOrEmpty()) {
                all_products = listProduct.toMutableList()
                // Log.d("ProductGroup", listProduct.toString())
                onSetProductInRV(all_products)
                return@observe
            }
        })


    }

    private fun onSetProductInRV(all_products: List<Product>) {
        if (all_products.isNullOrEmpty()) {
            binding.groupProductLiner.visibility = View.INVISIBLE
        } else {
            binding.groupProductLiner.visibility = View.VISIBLE
        }

        adapter.value = ProductDBAdapter(
            all_products,
            object : ProductCardClickListener<Product> {
                override fun onClicked(product_item: Product, memoryCacheKey: MemoryCache.Key?) {
                    requireView().findNavController().navigate(
                        GroupDetailFragmentDirections
                            .actionGroupDetailFragment2ToProductDetailFragment(
                                product_item.id,
                                memoryCacheKey.toString()
                            )
                    )
                }

                override fun onClickedAddBasket(
                    product_item: Product,
                    memoryCacheKey: MemoryCache.Key?
                ) {
                    if (product_item.groupModifierBool) {
                        requireView().findNavController().navigate(
                            GroupDetailFragmentDirections
                                .actionGroupDetailFragment2ToProductDialogFragment(
                                    product_item.id,
                                )
                        )
                    } else {
                        Snackbar.make(requireView(), "Добавлено в корзину!", Snackbar.LENGTH_LONG)
                            .apply {
                                setBackgroundTint(resources.getColor(R.color.AccentColor))
                                setTextColor(resources.getColor(R.color.white))

                                val params = view.layoutParams as FrameLayout.LayoutParams
                                params.topMargin = 118
                                params.gravity = Gravity.CENTER_HORIZONTAL or Gravity.TOP

                                show()
                            }
                        viewModel.onAddedProductInBasket(product_item)
                    }
                }
            })

    }

    override fun onDestroyView() {
        super.onDestroyView()
        //activity?.findViewById<TextView>(R.id.toolbar_)?.visibility = View.VISIBLE
        // Log.d("GroupDetailFragment","PuppyCounter - MainActivity - onDestroyView()")
    }

    override fun onStart() {

        // Log.d("GroupDetailFragment","PuppyCounter - MainActivity - onStart()")
        super.onStart()
    }


    override fun onResume() {
        // Log.d("GroupDetailFragment","PuppyCounter - MainActivity - onResume()")
        super.onResume()
        if (adapterObj != null) {
            binding.groupProductLiner.visibility = View.VISIBLE
            binding.groupProductRV.adapter = adapterObj
        }

    }

    override fun onPause() {
        // Log.d("GroupDetailFragment","PuppyCounter - MainActivity - onPause()")
        super.onPause()
    }

    override fun onStop() {
        // Log.d("GroupDetailFragment","PuppyCounter - MainActivity - onStop()")
        super.onStop()
    }

    override fun onDestroy() {
        // Log.d("GroupDetailFragment","PuppyCounter - MainActivity - onDestroy()")
        super.onDestroy()
    }
}