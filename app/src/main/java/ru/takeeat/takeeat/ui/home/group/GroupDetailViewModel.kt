package ru.takeeat.takeeat.ui.home.group

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import ru.takeeat.takeeat.db.AppDataBase
import ru.takeeat.takeeat.db.core.SubGroupWithProduct
import ru.takeeat.takeeat.db.order.ProductOrder

class GroupDetailViewModel(
    idGroup: Long,
    val db: AppDataBase,
    application: Application
) : AndroidViewModel(application) {
    private var viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

//    private val _products = db.productOrderDAO().getAll()
    private var _lastProduct: ProductOrder? = null




    private val _products = db.productCore().getLDByGroupProductAll(idGroup)
    val products: LiveData<List<ru.takeeat.takeeat.db.core.Product>>
        get() = _products

    private val _subGroup = db.subGroupCore().getSubGroupByGroupProduct(idGroup)
    val subGroup: LiveData<List<SubGroupWithProduct>>
        get() = _subGroup


    private var _subGroupProduct =  MutableLiveData<List<ru.takeeat.takeeat.db.core.Product>>()
    val subGroupProduct: LiveData<List<ru.takeeat.takeeat.db.core.Product>>
        get() = _subGroupProduct

    fun onAddedProductInBasket(productObj: ru.takeeat.takeeat.db.core.Product) {
        uiScope.launch {

            if(_lastProduct?.uuid == productObj.uuid){
                _lastProduct!!.amount += 1
                db.productOrderDAO().update(_lastProduct!!)
            }else{
                val gmCount = db.groupModifierDAO().getCountByProduct(productObj.id)
                if (gmCount==0){
                    val product = db.productOrderDAO().getByUuid(productObj.uuid)
                    if(product != null){
                        product.amount += 1
                        db.productOrderDAO().update(product)
                        return@launch
                    }
                }
                val product = ProductOrder(
                    uuid = productObj.uuid,
                    name = productObj.name,
                    amount = 1,
                    image = productObj.image_priority ?: productObj.image ?: "",
                    price = productObj.price.toFloat(),
                    priceProduct = productObj.price.toFloat(),
                    p_price = productObj.p_price,
                    weight = numToInt(productObj.weight ?: 0, 1000)
                )

                val idProduct = db.productOrderDAO().insert(product)
                _lastProduct = db.productOrderDAO().get(idProduct)
            }
        }


    }


    private fun numToInt(num: Number, int: Int): Int {
        val outNum = num.toFloat() * int
        return outNum.toInt()
    }
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }


    init{
        uiScope.launch{
            val subGroup = db.subGroupCore().getByGroupSubGroupAll(idGroup)
            val idSubGroups = mutableListOf<Long>()
            subGroup.forEach {
                idSubGroups.add(it.id)
            }
            _subGroupProduct.value = db.productCore().getSubGroupProductFull(idSubGroups)
            // Log.d("PRODUCTSGroup","${idSubGroups} --- ${db.productCore().getSubGroupProductFull(idSubGroups)}")
        }
    }
}