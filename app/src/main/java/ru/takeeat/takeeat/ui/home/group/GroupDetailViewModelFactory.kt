package ru.takeeat.takeeat.ui.home.group

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.takeeat.takeeat.db.AppDataBase


class GroupDetailViewModelFactory(private val idGroup: Long, private val db: AppDataBase, private val application: Application) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(GroupDetailViewModel::class.java)) {
            return GroupDetailViewModel(idGroup,db,application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
