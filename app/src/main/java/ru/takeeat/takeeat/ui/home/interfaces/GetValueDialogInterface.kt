package ru.takeeat.takeeat.ui.home.interfaces

import ru.takeeat.takeeat.network.mtesapi.core.ModifierOrder

interface GetValueDialogInterface {
    fun callbackGetModifier(date: MutableList<ModifierOrder>)
}