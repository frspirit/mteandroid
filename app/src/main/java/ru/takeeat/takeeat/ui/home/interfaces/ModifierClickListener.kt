package ru.takeeat.takeeat.ui.home.interfaces

import android.widget.Button
import android.widget.TextView
import ru.takeeat.takeeat.network.mtesapi.core.Modifier

interface ModifierClickListener<T> {
    fun onPlusModifier(modifier : T,countTV: TextView, buttonMinus: Button){

    }
    fun onMinusModifier(modifier : T, countTV: TextView, buttonMinus: Button){

    }
}