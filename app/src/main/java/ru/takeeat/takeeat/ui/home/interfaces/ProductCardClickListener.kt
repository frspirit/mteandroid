package ru.takeeat.takeeat.ui.home.interfaces

import coil.memory.MemoryCache


interface ProductCardClickListener<T> {
    fun onClicked(product_item : T, memoryCacheKey: MemoryCache.Key?){

    }
    fun onClickedAddBasket(product_item : T, memoryCacheKey: MemoryCache.Key?){

    }
}