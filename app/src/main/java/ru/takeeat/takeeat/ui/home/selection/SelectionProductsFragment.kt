package ru.takeeat.takeeat.ui.home.selection

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.FrameLayout
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import coil.memory.MemoryCache
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.snackbar.Snackbar
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.databinding.FragmentSelectionProductsBinding
import ru.takeeat.takeeat.db.AppDataBase
import ru.takeeat.takeeat.db.core.ProductWithModifierGroupModifier
import ru.takeeat.takeeat.ui.home.adapters.ProductSelectionAdapter
import ru.takeeat.takeeat.ui.home.interfaces.ProductCardClickListener


class SelectionProductsFragment : Fragment() {
    private lateinit var viewModel: SelectionProductsViewModel
    private lateinit var viewModelFactory: SelectionProductsViewModelFactory
    private var _binding: FragmentSelectionProductsBinding? = null
    private val binding get() = _binding!!
    private lateinit var args: SelectionProductsFragmentArgs

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val application = requireNotNull(this.activity).application
        val db = AppDataBase.getInstance(application)
        args = SelectionProductsFragmentArgs.fromBundle(requireArguments())
        viewModelFactory = SelectionProductsViewModelFactory(args.idSelection, db, application)
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(SelectionProductsViewModel::class.java)


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        //activity?.findViewById<TextView>(R.id.toolbar_)?.visibility = View.GONE
//        activity?.findViewById<AppBarLayout>(R.id.AppBarLayout)?.visibility = View.GONE

        _binding = FragmentSelectionProductsBinding.inflate(inflater, container, false)

//        binding.selectionTitle.text = args.selections.title
        val view: View = binding.root
        activity?.let { actv ->
            actv.findViewById<BottomNavigationView>(R.id.nav_view).visibility =
                View.VISIBLE
            actv.window.statusBarColor = ContextCompat.getColor(requireContext(), R.color.white)
            actv.findViewById<Toolbar>(R.id.my_toolbar)?.setBackgroundColor(resources.getColor(R.color.white))
            renderProducts()
        }


        return view
    }

    private fun renderProducts() {
        viewModel.selection.observe(viewLifecycleOwner, { selection ->
            if (selection != null) {
                // visible liner
                binding.selectionProductLiner.visibility = View.VISIBLE
                // selection title

                activity?.findViewById<MaterialToolbar>(R.id.my_toolbar)?.title = selection.selection.title

                // selection render products
                val adapter = ProductSelectionAdapter(selection.products ?: listOf(),
                    object : ProductCardClickListener<ProductWithModifierGroupModifier> {
                        override fun onClicked(product_item: ProductWithModifierGroupModifier, memoryCacheKey: MemoryCache.Key?) {
                            requireView().findNavController().navigate(
                                SelectionProductsFragmentDirections
                                    .actionSelectionProductsFragmentToProductDetailFragment(
                                        product_item.product.id,
                                        memoryCacheKey.toString()
                                    )
                            )
                        }

                        override fun onClickedAddBasket(product_item: ProductWithModifierGroupModifier, memoryCacheKey: MemoryCache.Key?) {
//                        product_item.modifier.isNotEmpty() ||
                            if (!product_item.group_modifier.isNullOrEmpty()) {
                                requireView().findNavController().navigate(
                                    SelectionProductsFragmentDirections
                                        .actionSelectionProductsFragmentToProductDialogFragment(
                                            product_item.product.id
                                        )
                                )
                            } else {
                                Snackbar.make(
                                    requireView(),
                                    "Добавлено в корзину!",
                                    Snackbar.LENGTH_SHORT
                                ).apply {
                                    setBackgroundTint(resources.getColor(R.color.AccentColor))
                                    setTextColor(resources.getColor(R.color.white))

                                    val params = view.layoutParams as FrameLayout.LayoutParams

                                    params.topMargin = 118
                                    params.gravity = Gravity.CENTER_HORIZONTAL or Gravity.TOP
                                    show()
                                }
                                viewModel.onAddedProductInBasket(product_item.product)
                            }
                        }
                    })
                binding.selectionProductRV.adapter = adapter
            } else {
                binding.selectionProductLiner.visibility = View.INVISIBLE
            }

        })


    }
}