package ru.takeeat.takeeat.ui.home.selection

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import ru.takeeat.takeeat.db.AppDataBase
import ru.takeeat.takeeat.db.core.SelectionWithProduct
import ru.takeeat.takeeat.db.order.ProductOrder

class SelectionProductsViewModel(
    idSelection: Long,
    val db: AppDataBase,
    application: Application
) : AndroidViewModel(application) {
    private var viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    private val _selection = db.selectionDAO().getSelectionProducts(idSelection)
    val selection: LiveData<SelectionWithProduct> get() = _selection

    private var _lastProduct: ProductOrder? = null

    fun onAddedProductInBasket(productObj: ru.takeeat.takeeat.db.core.Product) {
        uiScope.launch {

            if(_lastProduct?.uuid == productObj.uuid){
                _lastProduct?.amount = _lastProduct?.amount?.plus(1)!!
                db.productOrderDAO().update(_lastProduct!!)
            }else{
                val gmCount = db.groupModifierDAO().getCountByProduct(productObj.id)
                if (gmCount>0){
                    val product = db.productOrderDAO().getByUuid(productObj.uuid)
                    if(product != null){
                        product.amount += 1
                        db.productOrderDAO().update(product)
                        return@launch
                    }
                }
                val product: ProductOrder = ProductOrder(
                    uuid = productObj.uuid,
                    name = productObj.name,
                    amount = 1,
                    image = productObj.image_priority ?: productObj.image ?: "",
                    price = productObj.price.toFloat(),
                    priceProduct = productObj.price.toFloat(),
                    p_price = productObj.p_price,
                    weight = numToInt(productObj.weight ?: 0, 1000)
                )

                val idProduct = db.productOrderDAO().insert(product)
                _lastProduct = db.productOrderDAO().get(idProduct)
            }
        }


    }
    private fun numToInt(num: Number, int: Int): Int {
        val outNum = num.toFloat() * int
        return outNum.toInt()
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}