package ru.takeeat.takeeat.ui.home.selection

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.takeeat.takeeat.db.AppDataBase

class SelectionProductsViewModelFactory(private val idSelection: Long, private val db: AppDataBase, private val application: Application) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SelectionProductsViewModel::class.java)) {
            return SelectionProductsViewModel(idSelection,db,application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
