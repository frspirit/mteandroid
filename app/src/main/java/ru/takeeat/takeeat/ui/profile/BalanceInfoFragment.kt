package ru.takeeat.takeeat.ui.profile

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.MaterialToolbar
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.databinding.FragmentBalanceInfoBinding


class BalanceInfoFragment : Fragment() {
    private var _binding: FragmentBalanceInfoBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        activity?.let { actv ->
            actv.findViewById<AppBarLayout>(R.id.AppBarLayout)?.visibility  = View.VISIBLE
            actv.findViewById<Toolbar>(R.id.my_toolbar)?.setBackgroundColor(resources.getColor(R.color.white,null))
            actv.window.statusBarColor = resources.getColor(R.color.white,null)
            actv.window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            actv.title = "О бонусах"
        }

//        activity?.title = "О бонусах"
        super.onCreate(savedInstanceState)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
//        activity?.title = "О бонусах"

        // Inflate the layout for this fragment
        _binding = FragmentBalanceInfoBinding.inflate(inflater, container, false)
        return binding.root
    }


}