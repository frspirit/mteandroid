package ru.takeeat.takeeat.ui.profile

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputEditText
import ru.takeeat.takeeat.BuildConfig
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.databinding.FragmentProfileBinding
import ru.takeeat.takeeat.db.AppDataBase
import ru.takeeat.takeeat.ui.profile.orginfo.ProfileContactsFragment


class ProfileFragment : Fragment() {

    private lateinit var viewModel: ProfileViewModel
    private lateinit var viewModelFactory: ProfileViewModelFactory
    private lateinit var _binding: FragmentProfileBinding
    private val binding get() = _binding
    private lateinit var _window: Window
    private var customerUuid: String = ""

    override fun onResume() {
        super.onResume()
        activity?.let { actv ->
            actv.findViewById<AppBarLayout>(R.id.AppBarLayout)?.visibility = View.GONE

            actv.window.statusBarColor = resources.getColor(R.color.AccentColor,null)
            actv.window.decorView.systemUiVisibility = 0
        }


    }
    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        Log.d("ANIMATIONFRAGMENT","$transit - $enter - $nextAnim - ${this.javaClass.name}")
        if (!enter) {
            activity?.let { actv ->
                actv.findViewById<BottomNavigationView>(R.id.nav_view)?.visibility = View.VISIBLE
                actv.window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }

//            activity?.findViewById<Toolbar>(R.id.my_toolbar)?.setBackgroundColor(resources.getColor(R.color.white))
//            _window.statusBarColor = ContextCompat.getColor(requireContext(), R.color.purple_light)

        }
        return if (enter) {
            AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left)
        } else {
            AnimationUtils.loadAnimation(context, android.R.anim.slide_out_right)

        }
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        val application = requireNotNull(this.activity).application
        val db = AppDataBase.getInstance(application)
        viewModelFactory = ProfileViewModelFactory(db,application)
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(ProfileViewModel::class.java)
        _binding = FragmentProfileBinding.inflate(inflater, container, false)
        binding.versionName.text = "Версия: ${BuildConfig.VERSION_NAME}"
        viewModel.getServerCustomerInfo()
        binding.viewmodel = viewModel
        onBalance()
        onAboutUs()
        onShippingAndPayment()
        onContacts()
        onAuth()
        onReport()
        onAlertReport()
        onReferenceButton()
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        activity?.let { actv ->
            actv.findViewById<AppBarLayout>(R.id.AppBarLayout)?.visibility = View.GONE

            actv.window.statusBarColor = resources.getColor(R.color.AccentColor,null)
            actv.window.decorView.systemUiVisibility = 0
        }

    }
    private fun onBalance() {
        binding.profileBalanceInfoButton.setOnClickListener {
            requireView().findNavController().navigate(
                ProfileFragmentDirections.actionNavigationProfileToBalanceInfoFragment()
            )
        }

    }

    private fun onAboutUs() {
        binding.profileOrgAboutUsCard.setOnClickListener {
            requireView().findNavController().navigate(
                ProfileFragmentDirections.actionNavigationProfileToProfileAboutUsFragment()
            )
        }
    }

    private fun onContacts() {
        binding.profileOrgContactsCard.setOnClickListener {
            val bottomSheetRequiredFragment =
                ProfileContactsFragment()
            bottomSheetRequiredFragment.show(
                childFragmentManager,
                "ProfileContactsFragment"
            )
//            requireView().findNavController().navigate(
//                ProfileFragmentDirections.actionNavigationProfileToProfileContactsFragment()
//            )
        }
    }

    private fun onShippingAndPayment() {
        binding.profileShippingAndPaymentCard.setOnClickListener {
            requireView().findNavController().navigate(
                ProfileFragmentDirections.actionNavigationProfileToProfileShippingAndPaymentFragment()
            )
        }
    }

    private fun onReport() {
        binding.profileReportButton.setOnClickListener {
        }
    }



    private fun onAuth() {
        binding.profileAuthDV.setOnClickListener {
//            viewModel.authorizationCustomerTest()
            requireView().findNavController().navigate(
                ProfileFragmentDirections.actionNavigationProfileToAuthFragment2()
            )
//            Toast.makeText(context,"ClickAuth",Toast.LENGTH_LONG).show()
        }
        viewModel.auth.observe(viewLifecycleOwner, {
            if (it) {
                binding.profileAuthLL.visibility = View.GONE
                binding.profileAuthInfoUserLayout.visibility = View.VISIBLE
                binding.profileCustomerMenuList.visibility = View.VISIBLE
                viewModel.balance.observeForever {
                    viewModel.onTextBalance("У вас ${it} бонусов")
                }
                binding.profileCustomerNameLL.setOnClickListener{
                    requireView().findNavController().navigate(
                        ProfileFragmentDirections.actionNavigationProfileToCustomerEditInfoFragment(false)
                    )
                }
                onCustomerInfo()
                onCustomerOrders()
                onCustomerAddresses()

            } else {
                binding.profileAuthLL.visibility = View.VISIBLE
                binding.profileAuthInfoUserLayout.visibility = View.GONE
                binding.profileCustomerMenuList.visibility = View.GONE
                viewModel.onTextBalance("Получайте бонусы за заказ")
            }
        })
        viewModel.text_balance.observe(viewLifecycleOwner, {
            binding.profileCustomerBalanceText.text = it
        })
    }
    private fun onCustomerInfo(){

        viewModel.customer.observe(viewLifecycleOwner,{
            customerUuid = it.uuid.toString()
            binding.profileCustomerNameText.text = it.name
            var phone:String = it.phone ?: ""
            if (phone.length == 12) {
                var phone2 = phone.toMutableList()
                phone2.add(2," ".first())
                phone2.add(3,"(".first())
                phone2.add(7,")".first())
                phone2.add(8," ".first())
                phone2.add(12,"-".first())
                phone2.add(15,"-".first())
                phone = phone2.joinToString("")
            }
            binding.profileCustomerPhoneText.text = phone
        })
        viewModel.errorStatus.observe(viewLifecycleOwner,{
            if (it != null) {
                // Log.d("StatusCodeProfile", "$it")
                if (it == 404) {
                    val view = layoutInflater.inflate(R.layout.custom_alet_dialog_customer_error,null)
                    val buttonOk = view.findViewById<MaterialButton>(R.id.alertDialogReady)
                    view.findViewById<TextView>(R.id.alertDialogTitle).text = "Ошибка"
                    view.findViewById<TextView>(R.id.alertDialogTextError).text ="В ваш акканут вошли с другого мобильного телефона или он был удалён"
                    val alertDialogBuilder = AlertDialog.Builder(this.context,R.style.CustomAlertDialogOrderInfo)
                    alertDialogBuilder.setView(view)
                    alertDialogBuilder.setCancelable(false)
                    val alert = alertDialogBuilder.create()

                    buttonOk.setOnClickListener {
                        alert.cancel()
                    }

                    alert.show()
                    viewModel.onCreateNewCustomer()

                } else if (it == 200 || it == 201){
                    return@observe
                }
            }
        })
    }

    private fun onCustomerOrders() {
        binding.profileCustomerHistoryOrders.setOnClickListener {
            requireView().findNavController().navigate(
                ProfileFragmentDirections.actionNavigationProfileToOrderHistoryFragment(customerUuid)
            )
        }
    }

    private fun onCustomerAddresses() {
        binding.profileCustomerAddresses.setOnClickListener {
            requireView().findNavController().navigate(
                ProfileFragmentDirections.actionNavigationProfileToCustomerAddressesFragment()
            )
        }
    }

    private fun onAlertReport(){
        binding.profileReportButton.setOnClickListener {
            val view = layoutInflater.inflate(R.layout.custom_alet_dialog_customer_alert_report,null)
            val buttonCancel = view.findViewById<MaterialButton>(R.id.alertDialogCancel)
            val buttonReady = view.findViewById<MaterialButton>(R.id.alertDialogReady)
            val textInput = view.findViewById<TextInputEditText>(R.id.alertDialogTextInputText)
            val cLInput = view.findViewById<ConstraintLayout>(R.id.alertDialogCL)
            val cLSend = view.findViewById<ConstraintLayout>(R.id.successSendAlertCL)
            val buttonOk = view.findViewById<MaterialButton>(R.id.successSendAlertDialogOnPopUpMaterialButton)
            view.findViewById<TextView>(R.id.alertDialogTitle).text = "Сообщить о проблеме"

            val alertDialogBuilder = AlertDialog.Builder(context,R.style.CustomAlertDialogOrderInfo)
            alertDialogBuilder.setView(view)
            alertDialogBuilder.setCancelable(false)
            val alert = alertDialogBuilder.create()
            textInput.doOnTextChanged { text, _, _, _ ->
                viewModel.setAlertReportMessage(text.toString())
                textInput.background = resources.getDrawable(R.drawable.border_for_table_row_product_detail)
            }
            buttonCancel.setOnClickListener {
                alert.cancel()

            }
            buttonReady.setOnClickListener {
                val imm =
                    context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view?.windowToken, 0)
                val output = viewModel.sendAlertReportMessage()
                if (output){
                    cLInput.visibility = View.GONE
                    cLSend.visibility = View.VISIBLE
                    buttonOk.setOnClickListener {
                        alert.cancel()
                    }
                }else{
                    textInput.background = resources.getDrawable(R.drawable.border_for_text_input_red)
                }
            }
            alert.show()
        }


    }
    private fun onReferenceButton() {
        binding.profileReferenceButton.setOnClickListener {
            val i = Intent(Intent.ACTION_VIEW, Uri.parse(resources.getString(R.string.licenceAgreementAddress)))
            startActivity(i)
        }
    }


//    override fun onDetach() {
//        super.onDetach()
//        requireActivity().findViewById<BottomNavigationView>(R.id.nav_view).visibility = View.VISIBLE
//    }

//    override fun onStop() {
//        Handler().postDelayed({
//            requireActivity().findViewById<Toolbar>(R.id.my_toolbar).visibility = View.VISIBLE
//            activity?.findViewById<Toolbar>(R.id.my_toolbar)?.setBackgroundColor(resources.getColor(R.color.white))
//            _window.statusBarColor = ContextCompat.getColor(requireContext(), R.color.purple_light)
//            _window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
//        },1000)
//
//        super.onStop()
//    }



}