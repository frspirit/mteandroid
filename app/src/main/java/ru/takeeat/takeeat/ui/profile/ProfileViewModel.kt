package ru.takeeat.takeeat.ui.profile

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import ru.takeeat.takeeat.db.AppDataBase
import ru.takeeat.takeeat.db.customer.Customer
import ru.takeeat.takeeat.network.mtesapi.customer.CustomerAPI
import ru.takeeat.takeeat.network.mtesapi.customer.data.Android
import ru.takeeat.takeeat.network.mtesapi.order.NotifyAPI
import ru.takeeat.takeeat.network.mtesapi.order.data.AlertError
import java.util.*

class ProfileViewModel (val db: AppDataBase, application: Application ): AndroidViewModel(application) {
    private var viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    private val _serviceCustomer = CustomerAPI()
    private val _serviceNotify = NotifyAPI()
    val errorStatus = MutableLiveData<Int>(0)
    private val customerServer = MutableLiveData<ru.takeeat.takeeat.network.mtesapi.customer.data.Customer>()
    fun onGetOrUpdateCustomer(customerDB: Customer){
        uiScope.launch {

            _serviceCustomer.callGetCustomer(customerDB.uuid!!,customerServer,errorStatus)
            customerServer.observeForever { customerN ->


                if(customerN != null){
                    if (customerN.uuid != customerDB.uuid) {
                        customerDB.phone = null
                    }
                    uiScope.launch {
                        customerDB.uuid = customerN.uuid
                        customerDB.name = customerN.name
                        customerDB.surname = customerN.surname
                        customerDB.email = customerN.email
//                        customerDB.phone = customerN.phone
                        customerDB.balance = customerN.balance
                        customerDB.sex = customerN.sex
                        customerDB.consentStatus = customerN.consentStatus
                        customerDB.birthday = customerN.birthday
                        db.customerDAO().update(customerDB)
                    }
                    return@observeForever
                }
            }

        }
    }
    fun onCreateNewCustomer(){
        _auth.value = false
        _serviceCustomer.callAttachCustomer(
            ru.takeeat.takeeat.network.mtesapi.customer.data.Customer(
                android_device = Android(
                    name = " ",
                    registration_id = _customer.value?.android_device?.registration_id
                        ?: "",
                )
            ),
            customerServer,
            errorStatus
        )
    }

    private var _customer= db.customerDAO().getCheckCustomer()
    val customer: LiveData<ru.takeeat.takeeat.db.customer.Customer>
        get() = _customer

    private val _balance= MutableLiveData<Int>()
    val balance: LiveData<Int>
        get() = _balance
    private val _text_balance= MutableLiveData<String>()
    val text_balance: LiveData<String>
        get() = _text_balance

    fun onTextBalance(text: String){
        _text_balance.value = text
    }
    fun getServerCustomerInfo(){
        uiScope.launch {
            val customerDB = db.customerDAO().getCheckCustomerNoLive()
            if(customerDB != null){
                onGetOrUpdateCustomer(customerDB)
            }

        }
    }

    private val _auth = MutableLiveData<Boolean>()
    val auth: LiveData<Boolean>
        get() = _auth


//    fun visibilityAuthButton(): Int {
//        if (_auth.value == true){
//            return View.INVISIBLE
//        }
//        else{
//            return View.VISIBLE
//        }
//    }
//    fun authorizationCustomerRenew() {
//        _auth.value = true
//    }

    private var _alertReportMessage: String? = null
    fun setAlertReportMessage(message: String){
        _alertReportMessage = message
    }
    private val _outputAlertError = MutableLiveData<AlertError>()
    val outputAlertError: LiveData<AlertError> get() = _outputAlertError
    fun sendAlertReportMessage(): Boolean{
        if (_alertReportMessage != null && _alertReportMessage != "") {
            uiScope.launch {
                _serviceNotify.callSendAlertError(
                    _customer.value?.uuid ?: "",
                    AlertError(text = _alertReportMessage!!),
                    _outputAlertError
                )
            }
            return true
        }else{
            return false
        }
    }



    fun getOverCustomer(){
        _customer = db.customerDAO().getCheckCustomer()
    }
    init {
        _customer.observeForever {
            if (it == null){
                getOverCustomer()
            }else{
                if(it.phone != null){
                    _auth.value = true
                    _balance.value = it.balance
//                    android.os.Handler().postDelayed({
//                        onGetOrUpdateCustomer(it)
//                    },300000)
//                    return@observeForever
                }else{
                    _auth.value = false
                }
            }

        }

    }

}