package ru.takeeat.takeeat.ui.profile

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.takeeat.takeeat.db.AppDataBase


class  ProfileViewModelFactory(private val db: AppDataBase, private val application: Application) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ProfileViewModel::class.java)) {
            return ProfileViewModel(db,application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}