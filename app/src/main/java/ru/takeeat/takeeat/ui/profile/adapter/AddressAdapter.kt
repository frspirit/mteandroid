package ru.takeeat.takeeat.ui.profile.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.db.customer.Address
import ru.takeeat.takeeat.ui.profile.interfaces.AddressClickListener

class AddressAdapter(
    private val addresses: List<Address>, private val onClickListener: AddressClickListener,
) : RecyclerView.Adapter<AddressAdapter.MyViewHolder>() {

    class MyViewHolder constructor(view: View) : RecyclerView.ViewHolder(view) {
        val addressName: TextView = view.findViewById(R.id.customerAddressName)
        val addressText: TextView = view.findViewById(R.id.customerAddressText)
        val addressCardView: CardView = view.findViewById(R.id.customerAddressCardView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_customer_address_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val address = addresses[position]
        if (address.name.isNullOrEmpty()){
            holder.addressName.visibility = View.GONE
        }else{
            holder.addressName.text = address.name
        }
        val appartment = if (address.apartment.isNullOrEmpty()){
            ", кв. ${address.apartment},"
        } else {
            ""
        }
//        holder.addressText.text = "${address.city},${address.street}, д. ${address.home} ${address.apartment?.let{", кв. $it,"} ?: ""}${address.entrance?.let {" под. $it," } ?: ""}${address.floor?.let { " этаж $it" }?: ""}"
        holder.addressText.text = "${address.city},${address.street}, д. ${address.home}$appartment"
        holder.addressCardView.setOnClickListener { view ->
            onClickListener.onEditAddress(address)
        }
    }


    override fun getItemCount(): Int {
        return addresses.size
    }

}