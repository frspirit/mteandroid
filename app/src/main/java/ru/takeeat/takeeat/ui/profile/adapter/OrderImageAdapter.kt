package ru.takeeat.takeeat.ui.profile.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.transform.CircleCropTransformation
import coil.transform.RoundedCornersTransformation
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.SERVER_IMAGE
import ru.takeeat.takeeat.network.mtesapi.core.Order
import java.text.SimpleDateFormat
import java.util.*

class OrderImageAdapter(
    private val imageUrls: List<String>
) : RecyclerView.Adapter<OrderImageAdapter.MyViewHolder>() {

    class MyViewHolder constructor(view: View) : RecyclerView.ViewHolder(view) {
        val orderProductImage: ImageView = view.findViewById(R.id.orderProductImage)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_customer_order_history_product_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val imageUrl = imageUrls[position]
        holder.orderProductImage.load("$SERVER_IMAGE${imageUrl}"){
            crossfade(false)
            placeholder(R.drawable.defimage)
        }

    }


    override fun getItemCount(): Int {
        return imageUrls.size
    }

}