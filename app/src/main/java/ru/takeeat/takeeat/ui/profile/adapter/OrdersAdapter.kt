package ru.takeeat.takeeat.ui.profile.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import okhttp3.internal.userAgent
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.network.mtesapi.order.data.OrderHistory
import java.text.SimpleDateFormat
import java.util.*

class OrdersAdapter(
    private val orders: List<OrderHistory>
) : RecyclerView.Adapter<OrdersAdapter.MyViewHolder>() {

    class MyViewHolder constructor(view: View) : RecyclerView.ViewHolder(view) {
        val orderProductItemRV: RecyclerView = view.findViewById(R.id.orderProductImageRV)
        val orderDate: TextView = view.findViewById(R.id.orderDate)
        val orderTotalAmount: TextView = view.findViewById(R.id.orderTotalAmount)
        val orderIdOrNumber: TextView = view.findViewById(R.id.orderIdOrNumber)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_customer_order_history_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val order = orders[position]
        holder.orderIdOrNumber.text = "Заказ №${order.id}"
        holder.orderTotalAmount.text = "${order.total_amount}"
        val format = SimpleDateFormat()
        format.applyPattern("yyyy-MM-dd HH:mm")
        val formatDate: Date = format.parse(order.date_create)
        holder.orderDate.text = "от ${formatDate.date} ${getMonthName(formatDate.month+1)} ${formatDate.year+1900} в ${changeNumber(formatDate.hours)}:${changeNumber(formatDate.minutes)}"
        val imageUrls:MutableList<String> = mutableListOf()
        order.products?.forEach{ productOrder ->
            if(!productOrder.delivery){
                imageUrls.add(productOrder.image?:"")
            }

        }
        // Log.d("IMAGELISTURL", imageUrls.toString())
        val adapter = OrderImageAdapter(imageUrls)
        holder.orderProductItemRV.adapter = adapter


    }

    private fun getMonthName(intMonth: Int): String{
        when(intMonth){
            1 -> {return "января"}
            2 -> {return "февраля"}
            3 -> {return "марта"}
            4 -> {return "апреля"}
            5 -> {return "мая"}
            6 -> {return "июня"}
            7 -> {return "июля"}
            8 -> {return "августа"}
            9 -> {return "сентября"}
            10 -> {return "октября"}
            11 -> {return "ноября"}
            12 -> {return "декабря"}
            else ->{return ""}


        }
    }
    private fun changeNumber(num: Int): String{
        when(num){
            in 0..9 -> {return "0$num" }
            else -> {return "$num"}
        }
    }
    override fun getItemCount(): Int {
        return orders.size
    }

}