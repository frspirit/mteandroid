package ru.takeeat.takeeat.ui.profile.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ru.takeeat.takeeat.R


class ShopPickupAdapter(
    private val shopEntities: List<ru.takeeat.takeeat.db.core.ShopEntity>,
) : RecyclerView.Adapter<ShopPickupAdapter.MyViewHolder>() {
    private var lastRadioButton: RadioButton? = null
    class MyViewHolder constructor(view: View) : RecyclerView.ViewHolder(view) {
        val textView: TextView = view.findViewById(R.id.profileShippingAndPaymentPickupText)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_profile_shipping_and_payment_pickup_address_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val shop_item = shopEntities[position]
        holder.textView.text = "${position+1}. ${shop_item.address}"
    }


    override fun getItemCount(): Int {
        return shopEntities.size
    }

}