package ru.takeeat.takeeat.ui.profile.auth

import android.content.Context
import android.content.Intent
import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.PorterDuff
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.snackbar.Snackbar
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.databinding.FragmentAuthBinding
import ru.takeeat.takeeat.db.AppDataBase
import ru.takeeat.takeeat.PhoneTextFormatter
import ru.takeeat.takeeat.network.mtesapi.customer.data.OutPinCode
import ru.takeeat.takeeat.ui.profile.interfaces.OutStatusGetCodeListener


class AuthFragment : Fragment() {
    private lateinit  var _binding: FragmentAuthBinding
    private val binding get() = _binding
    private lateinit var viewModel: AuthViewModel
    private  var phone: String = ""
    private lateinit var viewModelFactory: AuthViewModelFactory



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        activity?.let { actv ->
            actv.findViewById<AppBarLayout>(R.id.AppBarLayout)?.visibility  = View.VISIBLE
            actv.findViewById<Toolbar>(R.id.my_toolbar)?.setBackgroundColor(resources.getColor(R.color.AccentColor))
            actv.findViewById<Toolbar>(R.id.my_toolbar)?.setTitleTextColor(resources.getColor(R.color.white))
            actv.findViewById<Toolbar>(R.id.my_toolbar)?.navigationIcon?.apply {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    colorFilter =
                        BlendModeColorFilter(resources.getColor(R.color.white), BlendMode.SRC_IN)
                } else {
                    setColorFilter(resources.getColor(R.color.white), PorterDuff.Mode.SRC_IN)
                }
            }
//            actv.findViewById<AppBarLayout>(R.id.AppBarLayout)?. = resources.getColor(R.color.AccentColor)
            actv.findViewById<BottomNavigationView>(R.id.nav_view)?.visibility = View.GONE
        }

        val application = requireNotNull(this.activity).application
        val db = AppDataBase.getInstance(application)
        viewModelFactory = AuthViewModelFactory(db, application)
        viewModel = ViewModelProvider(this, viewModelFactory).get(AuthViewModel::class.java)
        _binding = FragmentAuthBinding.inflate(inflater, container, false)
        viewModel.subOrgEntity.observe(viewLifecycleOwner) {
            if (it.auth_description_login != null) {
                binding.profileAuthPhoneTitle.text = it.auth_description_login
            }
            if (it.auth_description_passwd != null) {
                binding.profileAuthPinCodeTextInfo.text = it.auth_description_passwd
            }
        }
        onInputPhone()
        onReferenceButton()
        viewModel.currentTimeString.observeForever{ time ->
            if(time != "00:00"){
                binding.profileAuthPinCodeRetrySendingButton.text = "${resources.getString(R.string.auth_pincode_resend_sms_button_title)} через ${time}"
            }else{
                binding.profileAuthPinCodeRetrySendingButton.text = resources.getString(R.string.auth_pincode_resend_sms_button_title)
            }
        }
        return binding.root

    }

    override fun onResume() {
        super.onResume()
        activity?.let { actv ->
            actv.findViewById<AppBarLayout>(R.id.AppBarLayout)?.visibility  = View.VISIBLE
            actv.findViewById<Toolbar>(R.id.my_toolbar)?.setBackgroundColor(resources.getColor(R.color.AccentColor))
            actv.findViewById<Toolbar>(R.id.my_toolbar)?.navigationIcon?.apply {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    colorFilter =
                        BlendModeColorFilter(resources.getColor(R.color.white), BlendMode.SRC_IN)
                } else {
                    setColorFilter(resources.getColor(R.color.white), PorterDuff.Mode.SRC_IN)
                }
            }
            actv.findViewById<BottomNavigationView>(R.id.nav_view)?.visibility = View.GONE
            actv.window.statusBarColor = resources.getColor( R.color.AccentColor,null)
            actv.window.navigationBarColor = resources.getColor(R.color.AccentColor,null)
            actv.window.decorView.systemUiVisibility = 0
        }

    }

    private fun onInputPhone() {
        binding.profileAuthPhoneEditText.addTextChangedListener(
            PhoneTextFormatter(
                binding.profileAuthPhoneEditText,
                "+7 (###) ###-##-##"
            )
        )
        binding.profileAuthPhoneEditText.doOnTextChanged { text, _, _, count ->
            if (text?.count() == 18) {
                binding.profilePhoneGetPinAuthButton.isEnabled = true
                val imm =
                    context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view?.windowToken, 0)

                phone = text.toString()
                onClickListenerAuthGetPinCodeButton(phone)

            } else if (text?.count()?: 0 < 18) {
                    phone = text.toString()
                    binding.profilePhoneGetPinAuthButton.isEnabled = false

            }
        }
    }


    private fun onReferenceButton() {
        binding.authReferenceButton.setOnClickListener {
            val i = Intent(Intent.ACTION_VIEW, Uri.parse(resources.getString(R.string.licenceAgreementAddress)))
            startActivity(i)
        }
    }

    fun onClickListenerAuthGetPinCodeButton(phone:String){
        binding.profilePhoneGetPinAuthButton.setOnClickListener {
            binding.authErrorMessagePhone.visibility = View.GONE
            var outPhone = phone.replace(" ","")
            outPhone = outPhone.replace("(","")
            outPhone = outPhone.replace(")","")
            outPhone = outPhone.replace("-","")
            binding.includeProgressBar.progressBarLL.visibility = View.VISIBLE
            binding.profilePhoneGetPinAuthButton.isEnabled = false
            viewModel.getPinCode(outPhone,object: OutStatusGetCodeListener<Boolean>{
                override fun onSend(success: Boolean, outCode: OutPinCode?) {
                    Log.d("SENDCODEDATA", "${success} - ${outCode}")
                    if (success){
                        if (outCode!!.succeed ) {
                            Snackbar.make(binding.root.rootView,"${outCode.message}", Snackbar.LENGTH_LONG).apply {
                                setBackgroundTint(resources.getColor(R.color.AccentColor))
                                setTextColor(resources.getColor(R.color.white))

                                val params = view.layoutParams as FrameLayout.LayoutParams
                                params.topMargin = 118

                                params.gravity = Gravity.CENTER_HORIZONTAL or Gravity.TOP
                                show()
                            }
                            binding.profilePhoneGetPinAuthButton.isEnabled = true
                            binding.includeProgressBar.progressBarLL.visibility = View.GONE
                            binding.profileAuthLayoutInputPhone.visibility = View.GONE
                            binding.profileAuthLayoutInputPinCode.visibility = View.VISIBLE
                            onInputPinCode()
                        } else {
//                        binding.checkoutAddressShopCardView.startAnimation(
//                            AnimationUtils.loadAnimation(
//                                context,
//                                R.anim.shake
//                            )
//                        )
                            binding.profilePhoneGetPinAuthButton.isEnabled = true
                            binding.authErrorMessagePhone.visibility = View.VISIBLE
                            binding.includeProgressBar.progressBarLL.visibility = View.GONE
                            binding.authErrorMessagePhone.text = outCode.message
//                        Toast.makeText(context, "3 попытки исчерпаны, повторите через 5 мин!",Toast.LENGTH_LONG).show()
                        }
                    }else {
                        Snackbar.make(binding.root.rootView,"Ошибка сервиса!", Snackbar.LENGTH_LONG).apply {
                            setBackgroundTint(resources.getColor(R.color.AccentColor))
                            setTextColor(resources.getColor(R.color.white))

                            val params = view.layoutParams as FrameLayout.LayoutParams
                            params.topMargin = 118
                            params.gravity = Gravity.CENTER_HORIZONTAL or Gravity.TOP
                            show()
                        }
                        binding.includeProgressBar.progressBarLL.visibility = View.GONE
                        binding.profilePhoneGetPinAuthButton.isEnabled = true
//                    Toast.makeText(context, "Повторите попытку!",Toast.LENGTH_LONG).show()
                    }

                }
            })

        }

    }

    private fun onInputPinCode(){



        binding.profileAuthPinCodeEditText.doOnTextChanged { text, start, before, count ->
            if(text!!.length == 4){
//                binding.profileAuthPinCodeButton.isEnabled = true
                val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view?.windowToken, 0)
                onClickListenerAuthCheckPinCodeButton(text.toString())
            }
//            else if (text.length < 4 ) {
//                binding.profileAuthPinCodeButton.isEnabled = false
//
//            }
        }

        binding.profileAuthPinCodeChangePhoneButton.setOnClickListener{
            binding.profileAuthLayoutInputPhone.visibility = View.VISIBLE
            binding.profileAuthPinCodeEditText.text = Editable.Factory.getInstance().newEditable("")
            binding.profileAuthLayoutInputPinCode.visibility = View.GONE
//            binding.profilePhoneGetPinAuthButton.isEnabled = true
        }

        viewModel.accessRepeatCode.observeForever{
            binding.profileAuthPinCodeRetrySendingButton.isEnabled = it
        }
        binding.profileAuthPinCodeRetrySendingButton.setOnClickListener{

            binding.authErrorMessagePhone.visibility = View.GONE
            var outPhone = phone.replace(" ","")
            outPhone = outPhone.replace("(","")
            outPhone = outPhone.replace(")","")
            outPhone = outPhone.replace("-","")
            binding.includeProgressBar.progressBarLL.visibility = View.VISIBLE
            viewModel.getPinCode(outPhone, object: OutStatusGetCodeListener<Boolean>{
                override fun onSend(success: Boolean, outCode: OutPinCode?) {
                    if (success){
                        if (outCode!!.succeed ) {
                            binding.includeProgressBar.progressBarLL.visibility = View.GONE
                            if (outCode.message != ""){
                                Snackbar.make(binding.root.rootView,"${outCode.message}", Snackbar.LENGTH_LONG).apply {
                                    setBackgroundTint(resources.getColor(R.color.AccentColor))
                                    setTextColor(resources.getColor(R.color.white))

                                    val params = view.layoutParams as FrameLayout.LayoutParams
                                    params.topMargin = 118
                                    params.gravity = Gravity.CENTER_HORIZONTAL or Gravity.TOP
                                    show()
                                }
                            }

//                        onInputPinCode()
                        }else{
                            binding.includeProgressBar.progressBarLL.visibility = View.GONE
                            binding.authErrorMessagePhone.visibility = View.VISIBLE
                            binding.authErrorMessagePhone.text = outCode.message
                        }
//                    else if(!outPinCode.again && !outPinCode.send!!) {
//                        binding.includeProgressBar.progressBarLL.visibility = View.GONE
//                        Toast.makeText(context, "3 попытки исчерпаны, повторите через 5 мин!",Toast.LENGTH_LONG).show()
//                    }
                    } else {
                        binding.includeProgressBar.progressBarLL.visibility = View.GONE
//                    binding.profilePhoneGetPinAuthButton.isEnabled = true

                        Snackbar.make(binding.root.rootView,"Ошибка сервиса!", Snackbar.LENGTH_LONG).apply {
                            setBackgroundTint(resources.getColor(R.color.AccentColor))
                            setTextColor(resources.getColor(R.color.white))

                            val params = view.layoutParams as FrameLayout.LayoutParams
                            params.topMargin = 118
                            params.gravity = Gravity.CENTER_HORIZONTAL or Gravity.TOP
                            show()
                        }
//                    Toast.makeText(context, "Ошибка сервиса!",Toast.LENGTH_LONG).show()
                    }
                }
            })


        }
    }
    private fun onClickListenerAuthCheckPinCodeButton(pinCode:String){
        binding.authErrorMessageCode.visibility = View.GONE
        viewModel.checkPinCode(pinCode)
        binding.includeProgressBar.progressBarLL.visibility = View.VISIBLE
        viewModel.outCheckPinCode.observe(viewLifecycleOwner, {
            Log.d("AUTHASF","${it}")
            if (it  != null) {
                if (it.succeed && it.customer != null){
                    if (it.message != ""){
                        Snackbar.make(binding.root.rootView,"${it.message}", Snackbar.LENGTH_LONG).apply {
                            setBackgroundTint(resources.getColor(R.color.AccentColor))
                            setTextColor(resources.getColor(R.color.white))

                            val params = view.layoutParams as FrameLayout.LayoutParams
                            params.topMargin = 118
                            params.gravity = Gravity.CENTER_HORIZONTAL or Gravity.TOP
                            show()
                        }
                    }
                    binding.profileAuthLayoutInputPhone.visibility = View.GONE
                    binding.profileAuthLayoutInputPinCode.visibility = View.VISIBLE
                    val success =  MutableLiveData<Boolean>()
                    viewModel.updateCustomer(it.customer,success)
                    success.observe(viewLifecycleOwner, {
                        if (it) {
                            binding.includeProgressBar.progressBarLL.visibility = View.GONE
                            requireView().findNavController().popBackStack()
                        }else if (!it){
                            binding.includeProgressBar.progressBarLL.visibility = View.GONE
                            Toast.makeText(context, "Повторите подтвеждение!",Toast.LENGTH_LONG).show()
                        }
                    })

                }else if (!it.succeed){
                    binding.authErrorMessageCode.visibility = View.VISIBLE
                    binding.includeProgressBar.progressBarLL.visibility = View.GONE
                    binding.authErrorMessageCode.text = it.message

//                        Toast.makeText(context, "Повторите попытку!",Toast.LENGTH_LONG).show()
                }

            } else {
                binding.includeProgressBar.progressBarLL.visibility = View.GONE
                Snackbar.make(binding.root.rootView,"Ошибка сервиса!", Snackbar.LENGTH_LONG).apply {
                    setBackgroundTint(resources.getColor(R.color.AccentColor))
                    setTextColor(resources.getColor(R.color.white))

                    val params = view.layoutParams as FrameLayout.LayoutParams
                    params.topMargin = 118
                    params.gravity = Gravity.CENTER_HORIZONTAL or Gravity.TOP
                    show()
                }
//                    Toast.makeText(context, "Ошибка сервиса!",Toast.LENGTH_LONG).show()
            }

        })

    }

//    private fun onClickListenerAuthCheckPinCodeButton(pinCode:String){
//        binding.profileAuthPinCodeButton.setOnClickListener{
//            binding.authErrorMessageCode.visibility = View.GONE
//            viewModel.checkPinCode(pinCode)
//            binding.includeProgressBar.progressBarLL.visibility = View.VISIBLE
//            viewModel.outCheckPinCode.observe(viewLifecycleOwner, {
//                Log.d("AUTHASF","${it}")
//                if (it  != null) {
//                    if (it.succeed && it.customer != null){
//                        if (it.message != ""){
//                            Snackbar.make(binding.root.rootView,"${it.message}", Snackbar.LENGTH_LONG).apply {
//                                setBackgroundTint(resources.getColor(R.color.AccentColor))
//                                setTextColor(resources.getColor(R.color.white))
//
//                                val params = view.layoutParams as FrameLayout.LayoutParams
//                                params.topMargin = 118
//                                params.gravity = Gravity.CENTER_HORIZONTAL or Gravity.TOP
//                                show()
//                            }
//                        }
//                        binding.profileAuthLayoutInputPhone.visibility = View.GONE
//                        binding.profileAuthLayoutInputPinCode.visibility = View.VISIBLE
//                        val success =  MutableLiveData<Boolean>()
//                        viewModel.updateCustomer(it.customer,success)
//                        success.observe(viewLifecycleOwner, {
//                            if (it) {
//                                binding.includeProgressBar.progressBarLL.visibility = View.GONE
//                                requireView().findNavController().popBackStack()
//                            }else if (!it){
//                                binding.includeProgressBar.progressBarLL.visibility = View.GONE
//                                Toast.makeText(context, "Повторите подтвеждение!",Toast.LENGTH_LONG).show()
//                            }
//                        })
//
//                    }else if (!it.succeed){
//                        binding.authErrorMessageCode.visibility = View.VISIBLE
//                        binding.includeProgressBar.progressBarLL.visibility = View.GONE
//                        binding.authErrorMessageCode.text = it.message
//
////                        Toast.makeText(context, "Повторите попытку!",Toast.LENGTH_LONG).show()
//                    }
//
//                } else {
//                    binding.includeProgressBar.progressBarLL.visibility = View.GONE
//                    Snackbar.make(binding.root.rootView,"Ошибка сервиса!", Snackbar.LENGTH_LONG).apply {
//                        setBackgroundTint(resources.getColor(R.color.AccentColor))
//                        setTextColor(resources.getColor(R.color.white))
//
//                        val params = view.layoutParams as FrameLayout.LayoutParams
//                        params.topMargin = 118
//                        params.gravity = Gravity.CENTER_HORIZONTAL or Gravity.TOP
//                        show()
//                    }
////                    Toast.makeText(context, "Ошибка сервиса!",Toast.LENGTH_LONG).show()
//                }
//
//            })
//        }
//    }

    override fun onStop() {
        super.onStop()
        activity?.let { actv ->
            actv.findViewById<AppBarLayout>(R.id.AppBarLayout).visibility  = View.GONE
            actv.findViewById<BottomNavigationView>(R.id.nav_view).visibility =
                View.VISIBLE
            actv.findViewById<Toolbar>(R.id.my_toolbar)?.setBackgroundColor(resources.getColor(R.color.white))
            actv.findViewById<Toolbar>(R.id.my_toolbar)?.setTitleTextColor(resources.getColor(R.color.black))
            actv.findViewById<Toolbar>(R.id.my_toolbar)?.navigationIcon?.apply {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    colorFilter = BlendModeColorFilter(resources.getColor(R.color.black), BlendMode.SRC_IN)
                }else{
                    setColorFilter(resources.getColor(R.color.black), PorterDuff.Mode.SRC_IN)
                }
            }
            actv.window.statusBarColor = resources.getColor(R.color.purple_light,null)
            actv.window.navigationBarColor = resources.getColor(R.color.purple_light,null)
            actv.window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }

    }

    override fun onDestroyView() {
        viewModel.stopTimer()
        super.onDestroyView()
    }

}
