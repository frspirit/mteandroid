package ru.takeeat.takeeat.ui.profile.auth

import android.app.Application
import android.os.CountDownTimer
import android.text.format.DateUtils
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import ru.takeeat.takeeat.db.AppDataBase
import ru.takeeat.takeeat.db.core.SubOrgEntity
import ru.takeeat.takeeat.db.customer.Customer
import ru.takeeat.takeeat.network.mtesapi.core.CoreAPI
import ru.takeeat.takeeat.network.mtesapi.core.SubOrg
import ru.takeeat.takeeat.network.mtesapi.customer.CustomerAPI
import ru.takeeat.takeeat.network.mtesapi.customer.data.OutPinCode
import ru.takeeat.takeeat.network.mtesapi.interfaces.Result
import ru.takeeat.takeeat.ui.profile.interfaces.OutStatusGetCodeListener

class AuthViewModel(val db: AppDataBase, application: Application) :
    AndroidViewModel(application) {
    private val _service = CustomerAPI()
    private val _serviceCore = CoreAPI()
    private var viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    private val _customer = MutableLiveData<Customer>()
    val customer: LiveData<Customer>
        get() = _customer

    private var _subOrgEntity = MutableLiveData<SubOrgEntity>()
    val subOrgEntity:  LiveData<SubOrgEntity>
        get() = _subOrgEntity
    fun subOrgEntity() {

//        return outServer
    }
    fun getPinCode(phone: String,outStatus: OutStatusGetCodeListener<Boolean>) {
//        val outServer: MutableLiveData<OutPinCode> = MutableLiveData()
        restartTimer()
        _customer.observeForever {
            if (it != null) {
                _service.callGetPinCode(
                    customerUuid = it.uuid!!,
                    customerPhone = phone,
                    outStatus = outStatus
                )
                return@observeForever
            } else {
                return@observeForever
            }
        }

//        return outServer
    }

    private val _outCheckPinCode = MutableLiveData<OutPinCode>()
    val outCheckPinCode: LiveData<OutPinCode> get() = _outCheckPinCode
    fun checkPinCode(pinCode: String) {
        _customer.observeForever {
            if (it != null) {
                _service.callCheckPinCode(
                    customerUuid = it.uuid!!,
                    pinCode = pinCode,
                    _outCheckPinCode
                )
                return@observeForever
            } else {
                return@observeForever
            }

        }
//        uiScope.launch {
//            _service.callCheckPinCode(
//                customerUuid = _customer.value?.uuid!!,
//                pinCode = pinCode,
//                _outCheckPinCode
//            )
//        }

    }

    fun updateCustomer(customerUuid: String, success: MutableLiveData<Boolean>) {

        uiScope.launch {

            val customerDB = db.customerDAO().getCheckCustomerNoLive()
            val customerServer =
                MutableLiveData<ru.takeeat.takeeat.network.mtesapi.customer.data.Customer>()
            _service.callGetCustomer(customerUuid, customerServer, null)
            customerServer.observeForever { customerApiServer ->
                if (customerApiServer != null && customerApiServer.phone != null) {
                    if (customerDB != null) {
                        customerDB.uuid = customerUuid
                        customerDB.name = customerApiServer.name
                        customerDB.surname = customerApiServer.surname
                        customerDB.email = customerApiServer.email
                        customerDB.phone = customerApiServer.phone
                        customerDB.sex = customerApiServer.sex
                        customerDB.balance = customerApiServer.balance
                        customerDB.consentStatus = customerApiServer.consentStatus
                        customerDB.birthday = customerApiServer.birthday
                        // Log.d("PHONECUSTOMER: ","${customerDB.phone} - ${customerApiServer.phone}")
                        uiScope.launch {

                            db.customerDAO().update(customerDB)
                            if (!customerDB.phone.isNullOrEmpty()) {
                                success.value = true

                            }
                        }
                    }

                } else {
                    success.value = false
                }
            }


        }
//            val customerServer = MutableLiveData<xyz.kebr.takeeat.network.mtesapi.customer.data.Customer>()
//            _service.callGetCustomer(customerUuid, customerServer)
//            customerServer.observeForever {  customerApiServer ->
//                if(customerApiServer != null){
//                    val customerNew = Customer(
//                        uuid = customerUuid,
//                        name = customerApiServer.name,
//                        surname = customerApiServer.surname,
//                        email = customerApiServer.email,
//                        phone = customerApiServer.phone,
//                        sex = customerApiServer.sex,
//                        balance = customerApiServer.balance!!,
//                        consentStatus = customerApiServer.consentStatus,
//                        birthday = customerApiServer.birthday,
//                        android_device = Android(
//                            nameAndroid = customerApiServer.android_device.name,
//                            registration_id = customerApiServer.android_device.registration_id
//                        )
//                    )
//
//                    uiScope.launch{
//                        db.customerDAO().removeCustomer()
//                        db.customerDAO().insert(customerNew)
//                    }
//
//                }
//
//            }
//
//        }

    }


    companion object {
        // These represent different important times in the game, such as game length.

        // This is when the game is over
        private const val DONE = 0L

        // This is the number of milliseconds in a second
        private const val ONE_SECOND = 1000L

        // This is the total time of the game
        private const val COUNTDOWN_TIME = 90000L

    }

    private var timer: CountDownTimer
    private val _currentTime = MutableLiveData<Long>()
    val currentTime: LiveData<Long>
        get() = _currentTime

    private val _accessRepeatCode= MutableLiveData<Boolean>()
    val accessRepeatCode: LiveData<Boolean>
        get() = _accessRepeatCode


    // The String version of the current time
    val currentTimeString = Transformations.map(currentTime) { time ->
        DateUtils.formatElapsedTime(time)
    }

    fun restartTimer(){
        _accessRepeatCode.value = false
        timer.start()
    }
    fun stopTimer(){
        _accessRepeatCode.value = true
        timer.cancel()
    }

    init {
        uiScope.launch {
            _customer.value = db.customerDAO().getCheckCustomerNoLive()
        }
        uiScope.launch {
            _subOrgEntity.value = db.subOrgDAO().getTonight()
        }
        uiScope.launch {
            val serviceCore = CoreAPI()
            serviceCore.callSubOrgInfo(object: Result<SubOrg> {
                override fun result(data: SubOrg) {
                    uiScope.launch {
                        db.subOrgDAO().removeAll()
                        val idSubOrg = db.subOrgDAO().insert(
                            SubOrgEntity(
                                name=data.name,
                                auth_description_login = data.auth_description_login,
                                auth_description_passwd = data.auth_description_passwd,
                                logo = data.logo,
                                web_link = data.web_link,
                                vk_link = data.vk_link,
                            )
                        )
                    }
                }

                override fun statusCode(code: Int) {
                    super.statusCode(code)
                }
            }
            )

        }
        timer = object : CountDownTimer(COUNTDOWN_TIME, ONE_SECOND) {

            override fun onTick(millisUntilFinished: Long) {
                _currentTime.value = (millisUntilFinished / ONE_SECOND)
            }

            override fun onFinish() {
                _accessRepeatCode.value = true
                _currentTime.value = DONE

            }
        }

    }


}