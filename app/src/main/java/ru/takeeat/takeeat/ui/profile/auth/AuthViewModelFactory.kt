package ru.takeeat.takeeat.ui.profile.auth

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.takeeat.takeeat.db.AppDataBase

class AuthViewModelFactory(private val db: AppDataBase, private val application: Application) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(AuthViewModel::class.java)) {
            return AuthViewModel(db,application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}

