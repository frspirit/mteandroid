package ru.takeeat.takeeat.ui.profile.customer

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.view.Gravity
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import androidx.appcompat.widget.Toolbar
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.snackbar.Snackbar
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.databinding.FragmentCustomerEditInfoBinding
import ru.takeeat.takeeat.db.AppDataBase
import ru.takeeat.takeeat.ui.profile.interfaces.SuccessSendClickListener


class CustomerEditInfoFragment : Fragment() {
    private lateinit var _binding: FragmentCustomerEditInfoBinding
    private val binding get() = _binding
    private lateinit var viewModel: CustomerEditInfoViewModel
    private lateinit var viewModelFactory: CustomerEditInfoViewModelFactory
    private lateinit var args: CustomerEditInfoFragmentArgs
    private lateinit var name: String
    private lateinit var phone: String
    private lateinit var email: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.let { actv ->
            actv.findViewById<AppBarLayout>(R.id.AppBarLayout)?.visibility  = View.VISIBLE
            actv.findViewById<Toolbar>(R.id.my_toolbar)?.setBackgroundColor(resources.getColor(R.color.white,null))
            actv.window.statusBarColor = resources.getColor(R.color.white,null)
            actv.window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }

//        activity?.findViewById<MaterialTextView>(R.id.toolbar_)?.text = "Профиль"
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val application = requireNotNull(this.activity).application
        val db = AppDataBase.getInstance(application)
        args = CustomerEditInfoFragmentArgs.fromBundle(requireArguments())
        viewModelFactory = CustomerEditInfoViewModelFactory(db, application)
        viewModel = ViewModelProvider(this,viewModelFactory).get(CustomerEditInfoViewModel::class.java)
        _binding = FragmentCustomerEditInfoBinding.inflate(inflater,container, false)
        onCustomerSetInfo()
        onNameEdit()
        onEmailEdit()
        onPhoneEdit()
        if (args.checkoutEdit){
            binding.customerExitButton.visibility = View.GONE
        }

        return binding.root
    }
    private fun onCustomerSetInfo(){
        viewModel.customer.observe(viewLifecycleOwner,{
            if(it != null){
                binding.customerEditInfoNameInputEditText.text = Editable.Factory.getInstance().newEditable(it.name ?: "" )
                binding.customerEditInfoEmailInputEditText.text = Editable.Factory.getInstance().newEditable(it.email ?: "" )
                var phone:String = it.phone ?: ""
                if (phone.length == 12) {
                    var phone2 = phone.toMutableList()
                    phone2.add(2," ".first())
                    phone2.add(3,"(".first())
                    phone2.add(7,")".first())
                    phone2.add(8," ".first())
                    phone2.add(12,"-".first())
                    phone2.add(15,"-".first())
                    phone = phone2.joinToString("")
                }
                binding.customerEditInfoPhoneInputEditText.text = Editable.Factory.getInstance().newEditable(phone)
                binding.customerEditInfoPhoneInputEditText.isEnabled = false
                return@observe
            }
        })


        binding.customerEditInfoButton.setOnClickListener{
            val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view?.windowToken, 0)
            binding.includeProgressBar.progressBarLL.visibility = View.VISIBLE
            viewModel.updateCustomerInfo(name=name, phone=phone,email=email,object :
                SuccessSendClickListener {
                override fun onEditInfo(success: Boolean) {
//                    super.onEditInfo(success)
                    if (success) {
                        binding.includeProgressBar.progressBarLL.visibility = View.GONE
                        Snackbar.make(requireView(),"Данные сохранены!", Snackbar.LENGTH_LONG).apply {
                            setBackgroundTint(resources.getColor(R.color.AccentColor))
                            setTextColor(resources.getColor(R.color.white))

                            val params = view.layoutParams as FrameLayout.LayoutParams
                            params.topMargin = 118
                            params.gravity = Gravity.CENTER_HORIZONTAL or Gravity.TOP
                            show()
                        }
//                        requireView().findNavController().popBackStack()
                    } else {
                        binding.includeProgressBar.progressBarLL.visibility = View.GONE
                        Snackbar.make(requireView(),"Ошибка сохранения!", Snackbar.LENGTH_LONG).apply {
                            setBackgroundTint(resources.getColor(R.color.AccentColor))
                            setTextColor(resources.getColor(R.color.white))

                            val params = view.layoutParams as FrameLayout.LayoutParams
                            params.topMargin = 118
                            params.gravity = Gravity.CENTER_HORIZONTAL or Gravity.TOP
                            show()
                        }
//                        requireView().findNavController().popBackStack()
                    }
                }
            })


        }
        binding.customerExitButton.setOnClickListener{
            binding.customerExitButton.isEnabled = false
            viewModel.lagoutCustomer(viewObj = requireView() )

        }
    }
    private fun onNameEdit(){
        binding.customerEditInfoNameInputEditText.doOnTextChanged { text, start, before, count ->
            name = text.toString()
        }
    }

    private fun onPhoneEdit(){
        binding.customerEditInfoPhoneInputEditText.doOnTextChanged { text, start, before, count ->
            phone = text.toString()
        }
    }

    private fun onEmailEdit(){
        binding.customerEditInfoEmailInputEditText.doOnTextChanged { text, start, before, count ->
            email = text.toString()
        }
    }


}