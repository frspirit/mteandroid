package ru.takeeat.takeeat.ui.profile.customer

import android.app.Application
import android.provider.Settings
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.findNavController
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import ru.takeeat.takeeat.db.AppDataBase
import ru.takeeat.takeeat.db.customer.Customer
import ru.takeeat.takeeat.network.mtesapi.customer.CustomerAPI
import ru.takeeat.takeeat.network.mtesapi.customer.data.Android
import ru.takeeat.takeeat.ui.profile.interfaces.SuccessSendClickListener

class CustomerEditInfoViewModel(val db: AppDataBase, application: Application) :
    AndroidViewModel(application) {
    private val _service = CustomerAPI()
    private val _customer = db.customerDAO().getCheckCustomer()
    val customer: LiveData<Customer> get() = _customer

    val viewModelJob = Job()
    val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    fun updateCustomerNameDB(customerDB: Customer, name: String) {
        customerDB.name = name
        uiScope.launch {
            db.customerDAO().update(customerDB)
        }

    }

    fun updateCustomerPhoneDB(customerDB: Customer, phone: String) {
        customerDB.phone = phone
        uiScope.launch {
            db.customerDAO().update(customerDB)
        }
    }

    fun updateCustomerEmailDB(customerDB: Customer, email: String) {
        customerDB.email = email
        uiScope.launch {
            db.customerDAO().update(customerDB)
        }
    }
    fun lagoutCustomer(viewObj: View){
        uiScope.launch{
            db.customerDAO().removeAddressAll()
            val customerDB = db.customerDAO().getCheckCustomerNoLive()
            val customerCreate: MutableLiveData<ru.takeeat.takeeat.network.mtesapi.customer.data.Customer> = MutableLiveData()

            if (customerDB != null) {
                _service.callAttachCustomer(
                    ru.takeeat.takeeat.network.mtesapi.customer.data.Customer(
                        android_device = Android(
                            name = Settings.Secure.NAME,
                            registration_id = customerDB.android_device!!.registration_id
                        )
                    ), customerCreate,
                    null
                )

                customerCreate.observeForever {
                    if (it != null) {
                        uiScope.launch {
                            customerDB.uuid = it.uuid
                            customerDB.name = it.name
                            customerDB.surname = it.surname
                            customerDB.email = it.email
                            customerDB.phone = null
                            customerDB.balance = it.balance
                            customerDB.sex = it.sex
                            customerDB.consentStatus = it.consentStatus
                            customerDB.birthday = it.birthday
                            customerDB.android_device = ru.takeeat.takeeat.db.customer.Android(
                                nameAndroid = it.android_device.name,
                                registration_id = it.android_device.registration_id,
                            )
                            db.customerDAO().update(customerDB)
                            viewObj.findNavController().popBackStack()
                        }
                        return@observeForever
                    }else{
                        viewObj.findNavController().popBackStack()
                        return@observeForever
                    }
                }
            }
        }
    }
    fun updateCustomerInfo(name: String, phone: String, email: String, onClickListener: SuccessSendClickListener) {
        val customerApIServer =
            MutableLiveData<ru.takeeat.takeeat.network.mtesapi.customer.data.Customer>()
        uiScope.launch {
            val customerDB = db.customerDAO().getCheckCustomerNoLive()
            val customerUpdInfo = ru.takeeat.takeeat.network.mtesapi.customer.data.Customer(
                android_device = Android(
                    name = customerDB?.android_device?.nameAndroid,
                    registration_id = customerDB?.android_device?.registration_id!!
                )
            )
            if (customerDB.name != name) {
                customerUpdInfo.name = name
                updateCustomerNameDB(customerDB = customerDB, name = name)
            }
            if (customerDB.phone.toString().replace("+7","") != phone) {
                customerUpdInfo.phone = phone
                updateCustomerPhoneDB(customerDB = customerDB, phone = phone)
            }
            if (customerDB.email != email) {
                customerUpdInfo.email = email
                updateCustomerEmailDB(customerDB = customerDB, email = email)
            }

            _service.callUpdateCustomer(
                customerUuid = customerDB.uuid!!,
                customerUpdateInfo = customerUpdInfo,
                liveData = customerApIServer
            )
            customerApIServer.observeForever{ customer ->
                if (customer != null){
                    onClickListener.onEditInfo(success = true)
                }else{
                    onClickListener.onEditInfo(success = false)
                }
                return@observeForever

            }
        }

    }


}