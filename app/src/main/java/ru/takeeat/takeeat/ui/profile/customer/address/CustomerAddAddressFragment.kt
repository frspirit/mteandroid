package ru.takeeat.takeeat.ui.profile.customer.address

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.google.android.material.snackbar.Snackbar
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.databinding.FragmentCustomerAddAddressBinding
import ru.takeeat.takeeat.db.AppDataBase
import ru.takeeat.takeeat.db.customer.Address
import ru.takeeat.takeeat.ui.profile.interfaces.SuccessSendClickListener

class CustomerAddAddressFragment : Fragment() {
    private lateinit var _binding: FragmentCustomerAddAddressBinding
    private val binding get() = _binding
    private lateinit var viewModel: CustomerAddAddressViewModel
    private lateinit var viewModelFactory: CustomerAddAddressViewModelFactory
    private lateinit var args: CustomerAddAddressFragmentArgs
    private var idCustomer: Long? = null
    private var uuidCustomer: String? = null


    companion object {
        const val TAG = "CustomerAddAddressFragment"
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val application = requireNotNull(this.activity).application
        val db = AppDataBase.getInstance(application)
        viewModelFactory = CustomerAddAddressViewModelFactory(db, application)
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(CustomerAddAddressViewModel::class.java)
        args = CustomerAddAddressFragmentArgs.fromBundle(requireArguments())
        _binding = FragmentCustomerAddAddressBinding.inflate(inflater, container, false)

        binding.root.isFocusable = true

        viewModel.customer.observe(viewLifecycleOwner, {
            idCustomer = it.id
            uuidCustomer = it.uuid
        })

        if (args.id == null) {
            binding.customerAddAddressRemoveCardView.visibility = View.GONE
        } else {
            binding.customerAddAddressRemoveCardView.visibility = View.VISIBLE
        }

        onSetAddress()
        onAddAddress()
        onRemoveAddress()
        return binding.root
    }


    private fun onSetAddress() {
        binding.customerAddAddressNameInputEditText.text =
            Editable.Factory.getInstance().newEditable(args.name ?: "")

        binding.customerAddAddressCityInputEditText.text =
            Editable.Factory.getInstance().newEditable(args.city)
        binding.customerAddAddressCityInputEditText.isEnabled = false

        binding.customerAddAddressStreetInputEditText.text =
            Editable.Factory.getInstance().newEditable(args.street)
        binding.customerAddAddressStreetInputEditText.isEnabled = false

        binding.customerAddAddressHomeInputEditText.text =
            Editable.Factory.getInstance().newEditable(args.home)
        binding.customerAddAddressHomeInputEditText.isEnabled = false

        binding.customerAddAddressApartmentInputEditText.text =
            Editable.Factory.getInstance().newEditable(args.apartment ?: "")

        binding.customerAddAddressEntranceInputEditText.text =
            Editable.Factory.getInstance().newEditable(args.entrance ?: "")

        binding.customerAddAddressFloorInputEditText.text =
            Editable.Factory.getInstance().newEditable(args.floor ?: "")

    }


    private fun onAddAddress() {

        binding.customerAddAddressInsertMaterialButton.setOnClickListener {
            val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view?.windowToken, 0)
            binding.includeProgressBar.progressBarLL.visibility = View.VISIBLE
            if (args.id != null) {
                binding.includeProgressBar.progressBarLL.visibility = View.GONE
                viewModel.onUpdateAddress(
                    args.id!!.toLong(),
                    binding.customerAddAddressNameInputEditText.text.toString(),
                    binding.customerAddAddressApartmentInputEditText.text.toString(),
                    binding.customerAddAddressEntranceInputEditText.text.toString(),
                    binding.customerAddAddressFloorInputEditText.text.toString(),
                )
//                Toast.makeText(context, "Адрес обновлён!", Toast.LENGTH_SHORT).show()
                Snackbar.make(requireView(),"Адрес обновлён!", Snackbar.LENGTH_LONG).apply {
                    setBackgroundTint(resources.getColor(R.color.AccentColor))
                    setTextColor(resources.getColor(R.color.white))

                    val params = view.layoutParams as FrameLayout.LayoutParams
                    params.topMargin = 118
                    params.gravity = Gravity.CENTER_HORIZONTAL or Gravity.TOP
                    show()
                }
                requireView().findNavController().navigate(
                    CustomerAddAddressFragmentDirections.actionCustomerAddAddressFragmentToCustomerAddressesFragment()
                )
            } else {
                val address = Address(
                    idCustomer = idCustomer ?: 0,
                    name=binding.customerAddAddressNameInputEditText.text.toString(),
                    city = args.city,
                    street = args.street,
                    home = args.home,
                    apartment = binding.customerAddAddressApartmentInputEditText.text.toString(),
                    entrance = binding.customerAddAddressEntranceInputEditText.text.toString(),
                    floor = binding.customerAddAddressFloorInputEditText.text.toString(),
                    lat = args.latLng?.latitude,
                    lng = args.latLng?.longitude,
                    latLng = args.latLng
                )

                viewModel.onAddAddress(uuidCustomer ?: "", address, object:
                    SuccessSendClickListener {
                    override fun onEditInfo(success: Boolean) {
                        if(success){
                            binding.includeProgressBar.progressBarLL.visibility = View.GONE
                            Snackbar.make(requireView(),"Адресс добавлен!", Snackbar.LENGTH_LONG).apply {
                                setBackgroundTint(resources.getColor(R.color.AccentColor))
                                setTextColor(resources.getColor(R.color.white))

                                val params = view.layoutParams as FrameLayout.LayoutParams
                                params.topMargin = 118
                                params.gravity = Gravity.CENTER_HORIZONTAL or Gravity.TOP
                                show()
                            }

                            requireView().findNavController().navigate(
                                CustomerAddAddressFragmentDirections.actionCustomerAddAddressFragmentToCustomerAddressesFragment()
                            )
                        }else{
                            binding.includeProgressBar.progressBarLL.visibility = View.GONE
                            Snackbar.make(requireView(),"Ошибка сохранения!", Snackbar.LENGTH_LONG).apply {
                                setBackgroundTint(resources.getColor(R.color.AccentColor))
                                setTextColor(resources.getColor(R.color.white))

                                val params = view.layoutParams as FrameLayout.LayoutParams
                                params.topMargin = 118
                                params.gravity = Gravity.CENTER_HORIZONTAL or Gravity.TOP
                                show()
                            }
                        }
                    }
                })

            }

        }
    }

    private fun onRemoveAddress() {
        binding.customerAddAddressRemoveMaterialButton.setOnClickListener {
            if (viewModel.onRemoveAddress(args.id?.toLong())) {
//                requireView().findNavController().popBackStack()
//                requireView().findNavController().navigate(
//                    CustomerAddAddressFragmentDirections.actionCustomerAddAddressFragmentToCustomerAddressesFragment()
//                )
                requireView().findNavController().navigate(
                    CustomerAddAddressFragmentDirections.actionCustomerAddAddressFragmentToCustomerAddressesFragment()
                )
            } else {
                Toast.makeText(context, "Ошибка удаления!", Toast.LENGTH_SHORT).show()
            }


        }
    }
}