package ru.takeeat.takeeat.ui.profile.customer.address

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.takeeat.takeeat.db.AppDataBase


    class CustomerAddAddressViewModelFactory (private val db: AppDataBase, private val application: Application) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(CustomerAddAddressViewModel::class.java)) {
                return CustomerAddAddressViewModel(db,application) as T
            }
            throw IllegalArgumentException("Unknown ViewModel class")
        }
    }
