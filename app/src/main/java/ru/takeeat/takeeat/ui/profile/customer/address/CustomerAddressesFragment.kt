package ru.takeeat.takeeat.ui.profile.customer.address

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.google.android.material.appbar.AppBarLayout
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.databinding.FragmentCustomerAddressesBinding
import ru.takeeat.takeeat.db.AppDataBase
import ru.takeeat.takeeat.db.customer.Address
import ru.takeeat.takeeat.ui.profile.adapter.AddressAdapter
import ru.takeeat.takeeat.ui.profile.interfaces.AddressClickListener

class CustomerAddressesFragment : Fragment() {
    private lateinit var _binding: FragmentCustomerAddressesBinding
    private val binding get() = _binding
    private lateinit var viewModel: CustomerAddressesViewModel
    private lateinit var viewModelFactory: CustomerAddressesViewModelFactory

    companion object{
        const val TAG: String = "CustomerAddAddressFragment"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        activity?.let { actv ->
            actv.findViewById<AppBarLayout>(R.id.AppBarLayout)?.visibility  = View.VISIBLE
            actv.findViewById<Toolbar>(R.id.my_toolbar)?.setBackgroundColor(resources.getColor(R.color.white))
            actv.window.statusBarColor = ContextCompat.getColor(requireContext(), R.color.white)
            actv.window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }

        super.onCreate(savedInstanceState)
    }
    override fun onResume() {
        super.onResume()
        activity?.title = "Адреса"
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        val application = requireNotNull(this.activity).application
        val db = AppDataBase.getInstance(application)
        viewModelFactory = CustomerAddressesViewModelFactory(db, application)
        viewModel = ViewModelProvider(this,viewModelFactory).get(CustomerAddressesViewModel::class.java)
        _binding = FragmentCustomerAddressesBinding.inflate(inflater, container, false)



        // init functions
        onRenderAddress()
        return binding.root
    }


    private fun onRenderAddress(){
        viewModel.addresses.observeForever{ listAddress ->
//            Log.d("listNoNull",listAddresses.toString())
            if (listAddress.isNullOrEmpty()){
                binding.profileAddressCL.visibility = View.GONE
                binding.includeProgressBar.root.visibility = View.GONE
                binding.addressesEmptyLayout.visibility = View.VISIBLE
                binding.addressesEmptyMB.setOnClickListener{
                    requireView().findNavController().navigate(
                        CustomerAddressesFragmentDirections.actionCustomerAddressesFragmentToMapsFragment()
                    )
                }
            }else{
                binding.profileAddressCL.visibility = View.VISIBLE
                binding.addressesEmptyLayout.visibility = View.GONE
                binding.includeProgressBar.root.visibility = View.GONE
                val adapter = AddressAdapter(listAddress, object: AddressClickListener {
                    override fun onEditAddress(address: Address) {
                        // Log.d(TAG, "Navigate to edit address: id=${address.id}")
                        requireView().findNavController().navigate(
                            CustomerAddressesFragmentDirections.actionCustomerAddressesFragmentToCustomerAddAddressFragment(

                                address.city,
                                address.street,
                                address.home,
                                address.apartment,
                                address.entrance,
                                address.floor,
                                address.id.toString(),
                                null,
                                address.name,
                            )
                        )
                    }
                })
                onAddAddress()
                binding.customerAddressRecyclerView.adapter = adapter


            }


        }

    }
    private fun onAddAddress(){
        binding.customerAddressAddMaterialButton.setOnClickListener{
            // Log.d(TAG, "Navigate to adding address")
            requireView().findNavController().navigate(
                CustomerAddressesFragmentDirections.actionCustomerAddressesFragmentToMapsFragment()
            )
        }

    }


}