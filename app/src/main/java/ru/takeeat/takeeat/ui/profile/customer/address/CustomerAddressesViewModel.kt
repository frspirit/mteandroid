package ru.takeeat.takeeat.ui.profile.customer.address

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import ru.takeeat.takeeat.db.AppDataBase
import ru.takeeat.takeeat.db.customer.Address
import ru.takeeat.takeeat.network.mtesapi.customer.CustomerAPI

class CustomerAddressesViewModel(
    private val db: AppDataBase,
    application: Application
) : AndroidViewModel(application) {
    private val _service = CustomerAPI()
    private var viewModelJob = Job()
    private val _status = MutableLiveData<Boolean>(false)
    val status: LiveData<Boolean> get() = _status
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    private val _addresses = db.customerDAO().getAddressAll()
    val addresses: LiveData<List<Address>> get() = _addresses
    // TODO(developer): Write a function to for all customer addresses in DB


    init{
        uiScope.launch {
            val customer = db.customerDAO().getCheckCustomerNoLive()
            onGetServerAddresses(customer?.uuid!!)
        }
    }
//    result: Result<Boolean>
    fun onGetServerAddresses(customerUuid: String){
        uiScope.launch{
            val getAddresses = MutableLiveData<List<Address>>()
            _service.callGetAddresses(customerUuid, getAddresses)

            getAddresses.observeForever{listAddresses ->
                if(!listAddresses.isNullOrEmpty()){

                    uiScope.launch {

                        db.customerDAO().removeAddressAll()
                        listAddresses.forEach{addressS ->
                            db.customerDAO().insertAddress(addressS)

                        }

                    }

                }



            }
        }
    }
}