package ru.takeeat.takeeat.ui.profile.customer.orderhistory

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.navigation.findNavController
import com.google.android.material.appbar.AppBarLayout
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.databinding.FragmentCustomerOrderHistoryBinding
import ru.takeeat.takeeat.db.AppDataBase
import ru.takeeat.takeeat.ui.profile.adapter.OrdersAdapter

class OrderHistoryFragment : Fragment() {
    private lateinit var _binding: FragmentCustomerOrderHistoryBinding
    private val binding get() = _binding

//    companion object {
//        fun newInstance() = OrderHistoryFragment()
//    }
    private lateinit var viewModelFactory: OrderHistoryViewModelFactory
    private lateinit var viewModel: OrderHistoryViewModel
    private lateinit var args: OrderHistoryFragmentArgs


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.let { actv ->
            actv.findViewById<AppBarLayout>(R.id.AppBarLayout).visibility  = View.VISIBLE
            actv.findViewById<Toolbar>(R.id.my_toolbar)?.setBackgroundColor(resources.getColor(R.color.white))
            actv.window.statusBarColor = ContextCompat.getColor(requireContext(), R.color.white)
            actv.window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            actv.title = "Заказы"
        }


    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentCustomerOrderHistoryBinding.inflate(inflater,container,false)

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val application = requireNotNull(this.activity).application
        val db = AppDataBase.getInstance(application)
        args = OrderHistoryFragmentArgs.fromBundle(requireArguments())
        viewModelFactory = OrderHistoryViewModelFactory(db,application)
        viewModel = ViewModelProvider(this,viewModelFactory).get(OrderHistoryViewModel::class.java)
        viewModel.onGetOrderHistory(args.customerUuid)
        onOrderHistoryRender()
    }

    private fun onOrderHistoryRender(){
        viewModel.orderHistory.observe(viewLifecycleOwner,{
            if (it != null){
                binding.includeProgressBar.progressBarLL.visibility = View.GONE
                if (it.isNullOrEmpty()){
                    binding.customerOrderHistoryCL.visibility = View.GONE
                    binding.customerOrderHistoryEmptyLayout.visibility = View.VISIBLE
                    binding.customerOrderHistoryEmptyMB.setOnClickListener {
                        requireView().findNavController().navigate(
                            OrderHistoryFragmentDirections.actionCustomerAddressesFragmentToMain()
                        )
                    }

                }else{
                    binding.customerOrderHistoryCL.visibility = View.VISIBLE
                    binding.customerOrderHistoryEmptyLayout.visibility = View.GONE

                    val adapter = OrdersAdapter(it)
                    binding.customerAddressRecyclerView.adapter = adapter
                }

            }else{
                binding.includeProgressBar.progressBarLL.visibility = View.GONE
                binding.customerOrderHistoryCL.visibility = View.GONE
                binding.customerOrderHistoryEmptyLayout.visibility = View.VISIBLE
                binding.customerOrderHistoryEmptyMB.setOnClickListener {
                    requireView().findNavController().navigate(
                        OrderHistoryFragmentDirections.actionCustomerAddressesFragmentToMain()
                    )
                }
            }
        })
    }


}