package ru.takeeat.takeeat.ui.profile.customer.orderhistory

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ru.takeeat.takeeat.db.AppDataBase
import ru.takeeat.takeeat.network.mtesapi.order.OrderAPI
import ru.takeeat.takeeat.network.mtesapi.order.data.OrderHistory

class OrderHistoryViewModel(val db: AppDataBase, application: Application) :AndroidViewModel(application) {
    private val _serviceOrder = OrderAPI()

    private val _orderHistory = MutableLiveData<List<OrderHistory>>()
    val orderHistory: LiveData<List<OrderHistory>> get() = _orderHistory
    fun onGetOrderHistory(customerUuid: String){
        _serviceOrder.callGetOrderHistory(customerUuid,_orderHistory)
    }

}