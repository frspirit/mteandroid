package ru.takeeat.takeeat.ui.profile.customer.orderhistory

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.takeeat.takeeat.db.AppDataBase

class OrderHistoryViewModelFactory (private val db: AppDataBase, private val application: Application) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(OrderHistoryViewModel::class.java)) {
            return OrderHistoryViewModel(db,application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}