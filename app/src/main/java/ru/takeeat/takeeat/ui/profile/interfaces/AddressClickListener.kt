package ru.takeeat.takeeat.ui.profile.interfaces

import ru.takeeat.takeeat.db.customer.Address

interface AddressClickListener {
    fun onEditAddress(address: Address){}
}