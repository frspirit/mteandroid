package ru.takeeat.takeeat.ui.profile.interfaces

import ru.takeeat.takeeat.network.mtesapi.customer.data.OutPinCode

interface OutPinCodeListener {
    fun onResponse(data: OutPinCode?){}
}