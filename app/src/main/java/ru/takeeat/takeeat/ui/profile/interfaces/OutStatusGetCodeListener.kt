package ru.takeeat.takeeat.ui.profile.interfaces

import ru.takeeat.takeeat.network.mtesapi.core.OrderCreated
import ru.takeeat.takeeat.network.mtesapi.customer.data.OutPinCode

interface OutStatusGetCodeListener<T> {
    fun onSend(success: Boolean, outCode: OutPinCode?){}
}
