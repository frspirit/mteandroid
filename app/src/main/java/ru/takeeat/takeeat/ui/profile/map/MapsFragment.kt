package ru.takeeat.takeeat.ui.profile.map

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Address
import android.location.Location
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap

import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.data.kml.KmlLayer
import ru.takeeat.takeeat.MapsUtils
import ru.takeeat.takeeat.PermissionUtils.PermissionDeniedDialog.Companion.newInstance
import ru.takeeat.takeeat.PermissionUtils.isPermissionGranted
import ru.takeeat.takeeat.PermissionUtils.requestPermission
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.databinding.FragmentMapsBinding
import android.widget.RelativeLayout
import android.location.LocationManager
import android.content.DialogInterface
import android.app.AlertDialog

import android.content.Intent
import android.net.Uri
import android.provider.Settings
import android.view.inputmethod.InputMethodManager
import com.google.android.gms.location.LocationServices
import com.google.android.material.bottomnavigation.BottomNavigationView
import java.io.FileInputStream
import java.io.FileReader
import java.net.URL
import androidx.annotation.NonNull
import com.google.android.gms.maps.MapView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.maps.android.data.geojson.GeoJsonLayer
import kotlinx.coroutines.*
import java.io.File
import java.io.InputStream
import java.util.concurrent.Executors

//import com.mapbox.maps.*



class MapsFragment : Fragment(),
    GoogleMap.OnMyLocationButtonClickListener,
    GoogleMap.OnMyLocationClickListener,
    OnMapReadyCallback,
    ActivityCompat.OnRequestPermissionsResultCallback,
    GoogleMap.OnCameraIdleListener
{
    private lateinit var _binding: FragmentMapsBinding
    private val binding get() = _binding
    private lateinit var mapView: MapView
    private var address: Address? = null
    private var city: String? = null
    private var street: String? = null
    private var home: String? = null
    private var latLngAddress: LatLng? = null
    private lateinit var mapsUtils:MapsUtils
    private lateinit var mGoogleMap: GoogleMap
    private var permissionDenied = false
    private var animateCameraPosition = true
    private var latLng: LatLng = LatLng(59.866364, 29.773897)
    private var layer: KmlLayer? = null
    private val uiScope = CoroutineScope(Dispatchers.IO + Job())

    companion object {
        const val TAG = "MapsFragment"
        private val MAPVIEW_BUNDLE_KEY = "MapViewBundleKey"
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMapsBinding.inflate(inflater, container, false)
//        Places.initialize(requireContext(), "AIzaSyCcSsGillk_DYKWiRzWDYp2rU2tuZOmBeM")

        mapsUtils = MapsUtils(requireContext())
        mapView = binding.mapView
        initGoogleMap(savedInstanceState)
        return binding.root
    }

    private fun initGoogleMap(savedInstanceState: Bundle?) {

        val mapView = binding.mapView
        var mapViewBundle: Bundle? = null
        if (savedInstanceState != null) {
            val MAPVIEW_BUNDLE_KEY = resources.getString(R.string.gapikey)
            mapViewBundle = savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY)
        }

        mapView.onCreate(mapViewBundle)
        mapView.getMapAsync(this)
    }


    override fun onMapReady(map: GoogleMap) {
        map.let {
            mGoogleMap = it
            mGoogleMap.clear()
            mGoogleMap.setOnCameraIdleListener(this)
            it.setMinZoomPreference(5.0f)
            it.setMaxZoomPreference(50.0f)
            it.uiSettings.isCompassEnabled  = false
            it.uiSettings.isZoomControlsEnabled = true
            it.uiSettings.isMyLocationButtonEnabled = true
//            it.setPadding(0,100,0,0)
            it.setOnMyLocationButtonClickListener(this)
            it.setOnMyLocationClickListener(this)
            animateCamera(null)
            enableMyLocation()
            getDeviceLocation()
            binding.addressEditText.text = Editable.Factory.getInstance().newEditable("")
            binding.addressEditText.hint = "Введите адрес"

        }

        val location_button: View = requireView().findViewWithTag("GoogleMapMyLocationButton")
        val zoom_in_button: View = requireView().findViewWithTag("GoogleMapZoomInButton")
        val zoom_layout = zoom_in_button.parent as View

        val location_layout = location_button.layoutParams as RelativeLayout.LayoutParams
        location_layout.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
        location_layout.addRule(RelativeLayout.ABOVE, zoom_layout.id)
//        Log.d("MAPS", geo_remote.toString())


        layer = KmlLayer(map, R.raw.geo , context)
        layer?.addLayerToMap()


        binding.mapsMaterialButton.setOnClickListener {
            if (address != null) {
                requireView().findNavController().navigate(
                    MapsFragmentDirections.actionMapsFragmentToCustomerAddAddressFragment(
                        city ?: "",
                        street ?: "",
                        home ?: "",
                        null,
                        null,
                        null,
                        null,
                        latLngAddress,
                        null,
                    )
                )
            }
        }
        searchLatLngAddress()
    }



    private fun enableMyLocation() {
        if (!::mGoogleMap.isInitialized) return
        // [START maps_check_location_permission]
        if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION)
            == PackageManager.PERMISSION_GRANTED) {
            mGoogleMap.isMyLocationEnabled = true
            animateCameraPosition = true
        } else {
            // Permission to access the location is missing. Show rationale and request permission
            requestPermission(requireActivity(), LOCATION_PERMISSION_REQUEST_CODE,
                Manifest.permission.ACCESS_FINE_LOCATION, true
            )
        }
        // [END maps_check_location_permission]
    }

    override fun onMyLocationButtonClick(): Boolean {
        val locationManager = requireContext().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        val enabledGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        if(enabledGPS){
            return false
        }else{
            Toast.makeText(context, "Внимание!", Toast.LENGTH_SHORT).show()
            val dialog = AlertDialog.Builder(context)
            dialog.setMessage("Для автоопределения местоположения включите GPS!")
            dialog.setPositiveButton("Ок",
                DialogInterface.OnClickListener { paramDialogInterface, paramInt ->
                    // TODO Auto-generated method stub
                    val myIntent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    requireContext().startActivity(myIntent)
                    //get gps
                })
            dialog.setNegativeButton("Отмена",
                DialogInterface.OnClickListener { paramDialogInterface, paramInt ->
                    // TODO Auto-generated method stub
                })
            dialog.show()
            return true
        }
        // Return false so that we don't consume the event and the default behavior still occurs
        // (the camera animates to the user's current position).

    }

    override fun onMyLocationClick(location: Location) {
//        Toast.makeText(context, "Current location:\n$location", Toast.LENGTH_LONG).show()
    }

    // [START maps_check_location_permission_result]
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return
        }
        if (isPermissionGranted(permissions, grantResults, Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Enable the my location layer if the permission has been granted.
            enableMyLocation()
            animateCameraPosition = true
        } else {
            // Permission was denied. Display an error message
            // [START_EXCLUDE]
            // Display the missing permission error dialog when the fragments resume.
            permissionDenied = true
            animateCameraPosition = false
            // [END_EXCLUDE]
        }
    }

    // [END maps_check_location_permission_result]



    /**
     * Displays a dialog with error message explaining that the location permission is missing.
     */
    private fun showMissingPermissionError() {
        newInstance(true).show(childFragmentManager, "dialog")
    }
    private fun getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (!permissionDenied) {
                val locationResult = LocationServices.getFusedLocationProviderClient(requireActivity()).lastLocation
                locationResult.addOnCompleteListener(requireActivity()) { task ->
                    if (task.isSuccessful) {
                        // Set the map's camera position to the current location of the device.
                        val lastKnownLocation = task.result
                        if (lastKnownLocation != null) {
                            animateCamera(latLng = LatLng(lastKnownLocation.latitude,lastKnownLocation.longitude))
                        }
                    } else {
                        // Log.d(TAG, "Current location is null. Using defaults.")
                        Log.e(TAG, "Exception: %s", task.exception)
                    }
                }
            }
        } catch (e: SecurityException) {
            Log.e("Exception: %s", e.message, e)
        }
    }

    override fun onCameraIdle() {
        Log.d("KMLGEOClick", "layer: ${layer}")

        mGoogleMap.let {
            it.cameraPosition.let { position ->
                if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                    mGoogleMap.isMyLocationEnabled = true
                    if(animateCameraPosition){
                        getDeviceLocation()
                        animateCameraPosition = false
                    }
                }

                latLng = position.target

                val polygonInfo = if (layer != null) mapsUtils.checkPolygon(latLng = latLng,layer = layer!!) else null
                Log.d("KMLGEOClick", "polygonInfo: ${latLng} - ${mapsUtils.checkPolygon(latLng = latLng,layer = layer!!)}")

                if (polygonInfo != null){
                    address = mapsUtils.getAddress(latLng)
                    if (address != null) {

                        city = address!!.locality
                        street = address!!.thoroughfare
                        home = address!!.featureName
                        latLngAddress = latLng
                        binding.addressInfoText.text = polygonInfo.text
                        if (address!!.thoroughfare != null || address!!.subThoroughfare != null){
                            if ((city != null && city != "" && city != "null") && (home != null && home != "" && home != "Неизвестная дорога") && (street != null && street != "" && street != "Неизвестная дорога")){
                                if ("корпус" in home!!.lowercase()){
                                    val sArray = home!!.lowercase().replace(" ","").replace(",","").split("корпус")
                                    home = sArray.joinToString("/")
                                } else if ("корп." in home!!){
                                    val sArray = home!!.lowercase().replace(" ","").replace(",","").split("корп.")
                                    home = sArray.joinToString("/")
                                } else if ("корп" in home!!){
                                    val sArray = home!!.lowercase().replace(" ","").replace(",","").split("корп")
                                    home = sArray.joinToString("/")
                                }
                                binding.mapsMaterialButton.isEnabled = true
                                binding.addressEditText.text =
                                    Editable.Factory.getInstance().newEditable("${address!!.thoroughfare}, ${home}, ${address!!.locality}")
                            }else{
                                binding.mapsMaterialButton.isEnabled = false
                                binding.addressEditText.text =
                                    Editable.Factory.getInstance().newEditable("Не определено!")
                            }

                        }else{
                            binding.mapsMaterialButton.isEnabled = false
                            binding.addressEditText.text =
                                Editable.Factory.getInstance().newEditable("Не определено!")

                        }

                    }else {

                        binding.mapsMaterialButton.isEnabled = false
                        binding.addressInfoText.text = "Не удалось определить адресс!"
                    }

                }else{
                    binding.mapsMaterialButton.isEnabled = false
                    address = null

                    binding.addressInfoText.text = ""

                    binding.addressEditText.text =
                        Editable.Factory.getInstance().newEditable("Нет доставки..")
                }
                binding.addressEditText.clearFocus()

            }

        }


    }

    private fun searchLatLngAddress() {
//        val resultPlaces: MutableLiveData<List<AutocompletePrediction>> = MutableLiveData()
//        val placesClient = Places.createClient(requireContext())
//        binding.addressEditText.setSelectAllOnFocus(true)
//        binding.addressEditText.setOnClickListener{
//            if (!binding.addressEditText.isTextSelectable){
//                binding.addressEditText.selectAll()
//            }else{
//                binding.addressEditText.setTextIsSelectable(false)
//            }
//
//        }
        binding.addressEditText.setOnEditorActionListener { textView, actionId, keyEvent ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH
                || actionId == EditorInfo.IME_ACTION_DONE
                || keyEvent.action === KeyEvent.ACTION_DOWN
                || keyEvent.action === KeyEvent.KEYCODE_ENTER
            ){

                val latLng = mapsUtils.getLatLngAddress(textView.text.toString())
                if (latLng != null) {
                    mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17.5F))
                }else{
                    binding.addressInfoText.text = "Уточните данные для поиска"
                }
            }
            false
        }

        binding.addressSearchButton.setOnClickListener{
            val imm =
                context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view?.windowToken, 0)
            val text = binding.addressEditText.text.toString()
            val latLng = mapsUtils.getLatLngAddress(text)
            if (latLng != null) {
                mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17.5F))
            }else{
                binding.addressInfoText.text = "Уточните данные для поиска"
            }
        }


//        binding.addressEditText.doOnTextChanged { text, start, before, count ->
//            mapsUtils.getPlace(placesClient,text.toString(), result = resultPlaces)
//        }
//        resultPlaces.observeForever {
//            if (it != null){
//                if (it.count()>= 5){
//                    val adapter = PlacesAdapter(it.subList(0,5), object: PlacesClickListener{
//                        override fun onPlaceClick(prediction: AutocompletePrediction) {
//                            binding.addressEditText.text =
//                                Editable.Factory.getInstance().newEditable(prediction.getFullText(null))
//                        }
//                    })
//                    binding.addressPredictionsRecyclerView.adapter = adapter
//                }else{
//                    val adapter = PlacesAdapter(it, object: PlacesClickListener{
//                        override fun onPlaceClick(prediction: AutocompletePrediction) {
//                            binding.addressEditText.text =
//                                Editable.Factory.getInstance().newEditable(prediction.getFullText(null))
//                            val latLng = mapsUtils.getLatLngAddress(prediction.getFullText(null).toString())
//                            if (latLng != null) {
//                                mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17.5F))
//                            }else{
//                                binding.addressInfoText.text = "Уточните данные для поиска"
//                            }
//                                    }
//                    })
//                    binding.addressPredictionsRecyclerView.adapter = adapter
//                }
//            }else{
//                binding.addressPredictionsRecyclerView.adapter = null
//            }
//        }
    }

    private fun animateCamera(latLng: LatLng?) {
        if (latLng != null){
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 9.5F))
        }else{
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mapsUtils.takeEatGeo, 9.5F))
            binding.addressEditText.text = Editable.Factory.getInstance().newEditable("")
            binding.addressEditText.hint = "Введите адрес"
        }

    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        var mapViewBundle = outState.getBundle(MAPVIEW_BUNDLE_KEY)
        if (mapViewBundle == null) {
            mapViewBundle = Bundle()
            outState.putBundle(MAPVIEW_BUNDLE_KEY, mapViewBundle)
        }
        mapView.onSaveInstanceState(mapViewBundle)
    }

    override fun onResume() {
        super.onResume()

        if (permissionDenied) {
            // Permission was not granted, display error dialog.
            showMissingPermissionError()
            permissionDenied = false
        }
        if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION)
            == PackageManager.PERMISSION_GRANTED) {
            if(animateCameraPosition){
                getDeviceLocation()
                animateCameraPosition = false
            }
        }
        activity?.title = "Карта"
        mapView.onResume()
    }

    override fun onStart() {
        super.onStart()
        mapView.onStart()
    }




    override fun onPause() {
        mapView.onPause()
        super.onPause()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        activity?.let { actv ->
            actv.findViewById<BottomNavigationView>(R.id.nav_view)?.visibility = View.GONE
        }

        super.onCreate(savedInstanceState)

    }
    override fun onStop() {
        super.onStop()
        mapView.onStop()
        activity?.let { actv ->
            actv.findViewById<BottomNavigationView>(R.id.nav_view)?.visibility = View.VISIBLE
        }
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

}



//class MapsFragment : Fragment() {
//    private lateinit var _binding: FragmentMapsBinding
//    private val binding get() = _binding
//
//    private lateinit var mapView: MapView
//    private lateinit var mapboxMap: MapboxMap
//    private lateinit var onMapReady: (MapboxMap) -> Unit
//    private lateinit var listener: OnCameraChangeListener
//    private lateinit var mapsUtils:MapsUtils
//
//
//
//    companion object {
//        const val TAG = "MapsFragment"
//        private const val GEOJSON_SOURCE_ID = "FeatureCollection"
//        private const val LATITUDE = -122.486052
//        private const val LONGITUDE = 37.830348
//        private const val ZOOM = 11.0
//        private const val geojsonS = ""
//    }
//
//    override fun onCreateView(
//        inflater: LayoutInflater,
//        container: ViewGroup?,
//        savedInstanceState: Bundle?
//    ): View? {
//        _binding = FragmentMapsBinding.inflate(inflater, container, false)
////        val mapView = MapView(
////            inflater.context,
////            MapInitOptions(inflater.context)
////        )
//        mapView = binding.mapView
//
////        mapView.getMapboxMap().loadStyleUri(Style.MAPBOX_STREETS,)
//        mapView.getMapboxMap().setCamera(
//            CameraOptions.Builder().center(
//                Point.fromLngLat(
//                    30.084208,
//                    59.874444
//                )
//            ).zoom(ZOOM).build()
//        )
//        val uri: Uri = Uri.parse("android.resource://ru.takeeat.takeeat/raw/geoas.geojson")
//        mapView.getMapboxMap().loadStyle(
//            (
//                    style(styleUri = Style.MAPBOX_STREETS) {
//                        +geoJsonSource(GEOJSON_SOURCE_ID) {
//                            this.
//                            url("android.resource://ru.takeeat.takeeat/raw/geoas.geojson")
//                        }
//
//                    }
//                    )
//        )
//        mapView.gestures.rotateEnabled = false
//
//        cameraListener()
//        return binding.root
//    }
//
//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//        mapboxMap = mapView.getMapboxMap()
//        if (::onMapReady.isInitialized) {
//            onMapReady.invoke(mapboxMap)
//        }
//    }
//
//
//    fun cameraListener(){
//        mapsUtils = MapsUtils(requireContext())
//        listener = OnCameraChangeListener{ cameraChangedEventData ->
//            val center = mapView.getMapboxMap().cameraState.center
//
//            binding.addressEditText.text = Editable.Factory.getInstance().newEditable("${center.latitude()}\n${center.longitude()}")
////            latLng = position.target
////            val polygonInfo = mapsUtils.testCheckPolygon(latLng = latLng,layer = layer!!)
////            if (polygonInfo != null){
////                address = mapsUtils.getAddress(latLng)
////                if (address != null) {
////
////                    city = address!!.locality
////                    street = address!!.thoroughfare
////                    home = address!!.featureName
////                    latLngAddress = latLng
////                    binding.addressInfoText.text = polygonInfo.text
////                    if (address!!.thoroughfare != null || address!!.subThoroughfare != null){
////                        if ((city != null && city != "" && city != "null") && (home != null && home != "" && home != "Неизвестная дорога") && (street != null && street != "" && street != "Неизвестная дорога")){
////                            if ("корпус" in home!!.lowercase()){
////                                val sArray = home!!.lowercase().replace(" ","").replace(",","").split("корпус")
////                                home = sArray.joinToString("/")
////                            } else if ("корп." in home!!){
////                                val sArray = home!!.lowercase().replace(" ","").replace(",","").split("корп.")
////                                home = sArray.joinToString("/")
////                            } else if ("корп" in home!!){
////                                val sArray = home!!.lowercase().replace(" ","").replace(",","").split("корп")
////                                home = sArray.joinToString("/")
////                            }
////                            binding.mapsMaterialButton.isEnabled = true
////                            binding.addressEditText.text =
////                                Editable.Factory.getInstance().newEditable("${address!!.thoroughfare}, ${home}, ${address!!.locality}")
////                        }else{
////                            binding.mapsMaterialButton.isEnabled = false
////                            binding.addressEditText.text =
////                                Editable.Factory.getInstance().newEditable("Не определено!")
////                        }
////
////                    }else{
////                        binding.mapsMaterialButton.isEnabled = false
////                        binding.addressEditText.text =
////                            Editable.Factory.getInstance().newEditable("Не определено!")
////
////                    }
////
////                }else {
////
////                    binding.mapsMaterialButton.isEnabled = false
////                    binding.addressInfoText.text = "Не удалось определить адресс!"
////                }
////
////            }else{
////                binding.mapsMaterialButton.isEnabled = false
////                address = null
////
////                binding.addressInfoText.text = ""
////
////                binding.addressEditText.text =
////                    Editable.Factory.getInstance().newEditable("Нет доставки..")
////            }
//        }
//        mapView.getMapboxMap().addOnCameraChangeListener(listener)
//    }
//
//    fun getMapAsync(callback: (MapboxMap) -> Unit) = if (::mapboxMap.isInitialized) {
//        callback.invoke(mapboxMap)
//    } else this.onMapReady = callback
//
//    fun getMapView(): MapView? {
//        return mapView
//    }
//    override fun onCreate(savedInstanceState: Bundle?) {
//        activity?.let { actv ->
//            actv.findViewById<BottomNavigationView>(R.id.nav_view)?.visibility = View.GONE
//        }
//
//        super.onCreate(savedInstanceState)
//
//    }
//    override fun onStop() {
//        super.onStop()
//        activity?.let { actv ->
//            actv.findViewById<BottomNavigationView>(R.id.nav_view)?.visibility = View.VISIBLE
//        }
//    }
//
//    override fun onDestroy() {
//        super.onDestroy()
//        mapboxMap.removeOnCameraChangeListener(listener)
//    }
//}

