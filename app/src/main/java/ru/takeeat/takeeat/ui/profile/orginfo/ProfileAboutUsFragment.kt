package ru.takeeat.takeeat.ui.profile.orginfo

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.MaterialToolbar
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.databinding.FragmentProfileAboutUsBinding

class ProfileAboutUsFragment : Fragment() {
    private var _binding: FragmentProfileAboutUsBinding? = null
    private val binding get() = _binding!!
    override fun onCreate(savedInstanceState: Bundle?) {
        activity?.let { actv ->
            actv.findViewById<AppBarLayout>(R.id.AppBarLayout)?.visibility  = View.VISIBLE
            actv.findViewById<Toolbar>(R.id.my_toolbar)?.setBackgroundColor(resources.getColor(R.color.white))
            actv.window.statusBarColor = ContextCompat.getColor(requireContext(), R.color.white)
            actv.window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }
        //activity?.findViewById<TextView>(R.id.toolbar_)?.text = resources.getString(R.string.profileAboutUsTitle)

        super.onCreate(savedInstanceState)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentProfileAboutUsBinding.inflate(inflater,container,false)

        return binding.root
    }

    override fun onDestroyView() {
        //activity?.findViewById<TextView>(R.id.toolbar_)?.text = resources.getString(R.string.app_name)
        super.onDestroyView()
    }


}