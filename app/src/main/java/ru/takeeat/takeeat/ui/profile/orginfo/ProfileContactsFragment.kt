package ru.takeeat.takeeat.ui.profile.orginfo

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebSettings
import android.webkit.WebSettings.PluginState
import android.widget.FrameLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import ru.takeeat.takeeat.R
import ru.takeeat.takeeat.databinding.FragmentProfileContactsBinding


class ProfileContactsFragment : BottomSheetDialogFragment() {
    private lateinit var _binding: FragmentProfileContactsBinding
    private val binding get() = _binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialog)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProfileContactsBinding.inflate(inflater, container, false)

//        dialog?.setOnShowListener { dialog ->
//            val d = dialog as BottomSheetDialog
//            val bottomSheet = d.findViewById<View>(R.id.design_bottom_sheet) as FrameLayout
//            val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
//            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
//            bottomSheetBehavior.isDraggable = false
//        }

        onPhone()
        onEmail()
        onGeoMap()
        return binding.root
    }

    private fun onPhone() {
        val intent =
            Intent(Intent.ACTION_DIAL, Uri.parse("tel:${resources.getString(R.string.phone_org)}"))
        binding.profileContactsPhoneCard.setOnClickListener {
            if (intent.resolveActivity(requireActivity().packageManager) != null) {
                startActivity(intent)
            }
        }
        binding.profileContactsPhoneButton.setOnClickListener {
            if (intent.resolveActivity(requireActivity().packageManager) != null) {
                startActivity(intent)
            }
        }
    }

    private fun onEmail() {
        val intent = Intent(
            Intent.ACTION_SENDTO,

            ).apply {
            data = Uri.parse("mailto:${resources.getString(R.string.email_org)}")
            putExtra(Intent.EXTRA_EMAIL, resources.getString(R.string.email_org))
            putExtra(Intent.EXTRA_SUBJECT, "")
        }
        binding.profileContactsEmailCard.setOnClickListener {
            if (intent.resolveActivity(requireActivity().packageManager) != null) {
                startActivity(intent)
            }
        }
        binding.profileContactsEmailButton.setOnClickListener {
            if (intent.resolveActivity(requireActivity().packageManager) != null) {
                startActivity(intent)
            }
        }
    }

    private fun onGeoMap() {
        val intent = Intent(
            Intent.ACTION_VIEW,
            Uri.parse(resources.getString(R.string.address_geo))
        )
        binding.profileContactsAddressCard.setOnClickListener {
            if (intent.resolveActivity(requireActivity().packageManager) != null) {
                startActivity(intent)
            }
        }
        binding.profileContactsAddressButton.setOnClickListener {
            if (intent.resolveActivity(requireActivity().packageManager) != null) {
                startActivity(intent)
            }
        }

    }

    override fun onDestroyView() {
        //activity?.findViewById<TextView>(R.id.toolbar_)?.text = resources.getString(R.string.app_name)
        super.onDestroyView()
    }

}