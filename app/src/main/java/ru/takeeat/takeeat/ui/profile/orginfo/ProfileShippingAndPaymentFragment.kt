package ru.takeeat.takeeat.ui.profile.orginfo


import android.content.Context

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.maps.android.data.kml.KmlLayer
import ru.takeeat.takeeat.MapsUtils
//import com.yandex.mapkit.Animation
//import com.yandex.mapkit.MapKitFactory
//import com.yandex.mapkit.RawTile
//import com.yandex.mapkit.ZoomRange
//import com.yandex.mapkit.geometry.Point
//import com.yandex.mapkit.geometry.geo.Projection
//import com.yandex.mapkit.geometry.geo.Projections
//import com.yandex.mapkit.layers.LayerOptions
//import com.yandex.mapkit.map.CameraPosition
//import com.yandex.mapkit.mapview.MapView
//import com.yandex.mapkit.resource_url_provider.ResourceUrlProvider
//import com.yandex.mapkit.tiles.TileProvider
//import com.yandex.runtime.image.ImageProvider
//import com.yandex.runtime.ui_view.ViewProvider

import ru.takeeat.takeeat.databinding.FragmentProfileShippingAndPaymentBinding
import android.view.MotionEvent

import com.google.android.gms.maps.GoogleMapOptions

import android.util.AttributeSet
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.Marker


import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter
import ru.takeeat.takeeat.R

import android.app.Activity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.findNavController
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.appbar.AppBarLayout
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import ru.takeeat.okimaki.OnSwipeNavTouchListener
import ru.takeeat.takeeat.db.AppDataBase
import ru.takeeat.takeeat.db.core.ShopEntity
import ru.takeeat.takeeat.network.mtesapi.interfaces.Result
import ru.takeeat.takeeat.network.mtesapi.core.CoreAPI
import ru.takeeat.takeeat.network.mtesapi.core.EMPTY_SHOPS
import ru.takeeat.takeeat.network.mtesapi.core.ERR_GET_SHOPS
import ru.takeeat.takeeat.network.mtesapi.core.Shop
import ru.takeeat.takeeat.ui.profile.adapter.ShopPickupAdapter


class AppMapView : MapView {
    constructor(context: Context?) : super(context!!) {}
    constructor(context: Context?, attrs: AttributeSet?) : super(context!!, attrs) {}
    constructor(context: Context?, attrs: AttributeSet?, defStyle: Int) : super(
        context!!,
        attrs!!,
        defStyle
    ) {
    }

    constructor(context: Context?, options: GoogleMapOptions?) : super(context!!, options) {}

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        /**
         * Request all parents to relinquish the touch events
         */
        parent.requestDisallowInterceptTouchEvent(true)
        return super.dispatchTouchEvent(ev)
    }
}

class TakeEatInfoWindowGoogleMap(private val context: Context) :
    InfoWindowAdapter {
    override fun getInfoWindow(marker: Marker): View? {
        return null
    }

    override fun getInfoContents(marker: Marker): View {
        val view: View = (context as Activity).layoutInflater
            .inflate(ru.takeeat.takeeat.R.layout.info_marker, null)
        val name_tv = view.findViewById<TextView>(R.id.markerName)
        val snippet = view.findViewById<TextView>(R.id.markerSnippet)
        name_tv.text = marker.title
        snippet.text = marker.snippet
        return view
    }
}




class ProfileShippingAndPaymentFragment : Fragment(), OnMapReadyCallback{

    private lateinit var _binding: FragmentProfileShippingAndPaymentBinding
    private val binding get() = _binding
    private var shops: LiveData<List<ShopEntity>> = MutableLiveData<List<ShopEntity>>()
    private lateinit var mGoogleMap: GoogleMap
    private lateinit var mapView: MapView
    private lateinit var mapsUtils: MapsUtils
    private var viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    override fun onCreate(savedInstanceState: Bundle?) {
        activity?.let { actv ->
            actv.findViewById<AppBarLayout>(R.id.AppBarLayout)?.visibility  = View.VISIBLE
            actv.findViewById<Toolbar>(R.id.my_toolbar)?.setBackgroundColor(resources.getColor(R.color.background))
            actv.window.statusBarColor = ContextCompat.getColor(requireContext(), R.color.background)
            actv.window.decorView.systemUiVisibility =  0
        }

        //activity?.findViewById<TextView>(R.id.toolbar_)?.text = resources.getString(R.string.shippingAndPaymentTitle)
        val application = requireNotNull(this.activity).application
        val db = AppDataBase.getInstance(application)
        shops = db.shopDAO().getAll()
        val service = CoreAPI()
        val shopsData = MutableLiveData<List<Shop>>()
        service.callShops(shopsData, object: Result<Int> {
            override fun result(code: Int) {
                if(code == ERR_GET_SHOPS || code == EMPTY_SHOPS){
                    uiScope.launch {
                        db.shopDAO().removeAll()
                    }
                }else{
                    shopsData.observe(viewLifecycleOwner) {
                        if (!it.isNullOrEmpty()) {
                            uiScope.launch {
                                db.shopDAO().removeAll()
                                it.forEach { shop ->
                                    db.shopDAO().insert(
                                        ShopEntity(
                                            uuid = shop.uuid,
                                            pickup = shop.pickup,
                                            name = shop.name,
                                            phone = shop.phone,
                                            date_work = shop.date_work,
                                            time_work = shop.time_work,
                                            address = shop.address,
                                            latitude = shop.latitude,
                                            longitude = shop.longitude
                                        )
                                    )
                                }
                            }
                        }
                    }
                }
            }
        })
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProfileShippingAndPaymentBinding.inflate(inflater,container, false)
        mapView = binding.mapView
        mapsUtils = MapsUtils(requireContext())
        initGoogleMap(savedInstanceState)
        val width = resources.displayMetrics.widthPixels

        val params = binding.mapViewCV.layoutParams
        params.height = (width.toDouble() / 1.15).toInt()
        binding.mapViewCV.layoutParams = params
//        activity?.let {
//            binding.deliveryScroll.setOnTouchListener(object: OnSwipeNavTouchListener(requireContext()){
//                override fun onSwipeRight() {
//                    super.onSwipeRight()
//                    requireView().findNavController()?.popBackStack()
//                }
//            })
//        }

        return binding.root
    }

    override fun onDestroyView() {
        //activity?.findViewById<TextView>(R.id.toolbar_)?.text = resources.getString(R.string.app_name)
        super.onDestroyView()
    }

    override fun onMapReady(map: GoogleMap) {
        map.let {
            mGoogleMap = it
            mGoogleMap.clear()
            mGoogleMap.setMinZoomPreference(5.0f)
            mGoogleMap.setMaxZoomPreference(50.0f)
            mGoogleMap.uiSettings.isZoomControlsEnabled = true
            mGoogleMap.uiSettings.isMyLocationButtonEnabled = false
            mGoogleMap.uiSettings.isZoomGesturesEnabled = false

            animateCamera()

        }

        shops.observe(viewLifecycleOwner, { listShop ->

            if (!listShop.isNullOrEmpty()) {
                val pickupShopEntity: MutableList<ShopEntity> = mutableListOf()
                listShop.forEach { shop ->
                    if (shop.latitude != null && shop.longitude != null) {

                        map.addMarker(
                            MarkerOptions()
                                .position(LatLng(shop.latitude, shop.longitude))
                                .title(shop.name)
                                .snippet("${if (!shop.pickup) "Самовывоз не осуществляется " else "Тел: ${shop.phone} Время раб:${shop.time_work} ${shop.date_work}"}")
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker))
                                .visible(true)
                        )
                        if (shop.pickup) {
                            pickupShopEntity.add(shop)
                        }
                    }

                }
                val adapter = ShopPickupAdapter(pickupShopEntity)
                binding.profileShippingAndPaymentPickupRV.adapter = adapter
                binding.profileShippingAndPaymentPickupTitle.visibility = View.VISIBLE
            }else{
                binding.profileShippingAndPaymentPickupTitle.visibility = View.GONE
            }


        })

        val layer = KmlLayer(map, R.raw.geo, context)
        layer.addLayerToMap()
    }

    private fun animateCamera() {
        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mapsUtils.takeEatGeo, 9.5F))
    }

    private fun initGoogleMap(savedInstanceState: Bundle?) {

        val mapView = binding.mapView
        var mapViewBundle: Bundle? = null
        if (savedInstanceState != null) {
            val MAPVIEW_BUNDLE_KEY = resources.getString(R.string.google_maps_key)
            mapViewBundle = savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY)
        }
        mapView.onCreate(mapViewBundle)
        mapView.getMapAsync(this)
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onStart() {
        super.onStart()
        mapView.onStart()
    }

    override fun onStop() {
        super.onStop()
        mapView.onStop()
    }


    override fun onPause() {
        mapView.onPause()
        super.onPause()
    }

    override fun onDestroy() {
        mapView.onDestroy()
        super.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

}